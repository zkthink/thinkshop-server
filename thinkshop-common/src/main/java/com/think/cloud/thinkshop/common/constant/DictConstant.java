package com.think.cloud.thinkshop.common.constant;

/**
 * @author zkthink
 * @apiNote 字典常量
 **/
public interface DictConstant {
    /**
     * 国家
     */
    String SYS_NATION = "sys_nation";
}
