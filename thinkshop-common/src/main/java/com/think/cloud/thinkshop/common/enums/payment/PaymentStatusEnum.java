package com.think.cloud.thinkshop.common.enums.payment;

/**
 * @author zkthink
 * @apiNote 支付状态
 **/
public enum PaymentStatusEnum {
    PAYMENT_STATUS_UNPAID(0, "未支付"),
    PAYMENT_STATUS_PAID(1, "已支付"),
    PAYMENT_STATUS_PAID_PART(2, "支付中"),
    ;
    private final int code;
    private final String description;

    PaymentStatusEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
