package com.think.cloud.thinkshop.common.constant.order;

/**
 * 订单
 *
 * @author zkthink
 */
public class OrderConstants {
    /**
     * 订单编号前缀
     */
    public static final String ORDER_CODE_PREFIX = "ord";


    /**
     * 订单编号key
     */
    public static final String ORDER_CODE_REDIS_KEY = "order_code_key";

    /**
     * 流水号日期格式
     */
    public static final String DATE_FORMAT = "yyyyMMddHHmmss";


    /**
     * 升序编码格式
     */
    public static final String NUMBER_FORMAT = "%04d";
}
