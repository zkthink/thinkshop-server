package com.think.cloud.thinkshop.common.constant.aftersales;

/**
 * 售后单
 *
 * @author zkthink
 */
public class AfterSalesConstants {
    /**
     * 售后单号前缀
     */
    public static final String AFTER_SALES_CODE_PREFIX = "AS";


    /**
     * 售后单号key
     */
    public static final String ORDER_CODE_REDIS_KEY = "after_sales_code_key";

    /**
     * 流水号日期格式
     */
    public static final String DATE_FORMAT = "yyyyMMddHHmmss";


    /**
     * 升序编码格式
     */
    public static final String NUMBER_FORMAT = "%04d";
}
