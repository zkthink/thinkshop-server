package com.think.cloud.thinkshop.common.constant;

/**
 * 服务名称
 *
 * @author zkthink
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "thinkshop-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "thinkshop-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "thinkshop-assemble";
    /**
     * 商城服务的serviceid
     */
    public static final String MALL_SERVICE = "thinkshop-mall";
}
