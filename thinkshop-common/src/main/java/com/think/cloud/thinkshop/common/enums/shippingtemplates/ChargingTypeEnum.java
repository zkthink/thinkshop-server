package com.think.cloud.thinkshop.common.enums.shippingtemplates;



public enum ChargingTypeEnum {
    BY_ITEM_COUNT(1, "按件数计费"),
    BY_WEIGHT(2, "按重量计费"),
    FIXED_FEE(3, "固定费用");

    private final int code;
    private final String description;

    ChargingTypeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
