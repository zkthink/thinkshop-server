package com.think.cloud.thinkshop.common.enums.user;

public enum SwitchEnum {
    ACTIVE(0, "正常"),
    DISABLED(1, "停用");

    private int code;
    private String description;

    SwitchEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static SwitchEnum fromCode(int code) {
        for (SwitchEnum status : values()) {
            if (status.getCode() == code) {
                return status;
            }
        }
        throw new IllegalArgumentException("Invalid switch code: " + code);
    }
}
