package com.think.cloud.thinkshop.common.enums.user;
public enum UserRegistrationSource {


    EMAIL("email", "邮箱注册"),
    GOOGLE("google", "Google账户"),
    FACEBOOK("facebook", "Facebook账户");

    private final String code;
    private final String description;

    UserRegistrationSource(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static UserRegistrationSource fromCode(String code) {
        for (UserRegistrationSource source : values()) {
            if (source.getCode().equals(code)) {
                return source;
            }
        }
        throw new IllegalArgumentException("Invalid registration source code: " + code);
    }
}
