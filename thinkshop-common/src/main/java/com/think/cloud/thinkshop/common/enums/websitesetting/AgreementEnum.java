package com.think.cloud.thinkshop.common.enums.websitesetting;

/**
 * @author zkthink
 * @apiNote 协议类型枚举
 **/
public enum AgreementEnum {
    AGREEMENT_TYPE_1(1, "用户协议"),
    AGREEMENT_TYPE_2(2, "隐私政策");
    private final int code;
    private final String description;

    AgreementEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
