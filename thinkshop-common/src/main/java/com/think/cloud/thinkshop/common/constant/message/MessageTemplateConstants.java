package com.think.cloud.thinkshop.common.constant.message;

public class MessageTemplateConstants {
    public static final String USER_EMAIL = "{用户邮箱}";
    public static final String V_CODE = "{验证码}";
    public static final String CURRENCY_UNIT = "{货币单位}";
    public static final String PAY_PRICE = "{金额}";
    public static final String ORDER_CODE = "{订单号}";
    public static final String PRODUCT = "{商品}";  //商品+规格
    public static final String TIME_LIMIT = "{时间}";
    public static final String PAY_LINK = "{支付链接}";
    public static final String REJECT_REASON = "{拒绝原因}";


}
