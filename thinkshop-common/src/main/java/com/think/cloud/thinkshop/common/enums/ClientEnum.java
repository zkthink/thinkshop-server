package com.think.cloud.thinkshop.common.enums;

public enum ClientEnum {
    H5("h5"),
    PC("pc"),
    ;

    private final String code;

    ClientEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
