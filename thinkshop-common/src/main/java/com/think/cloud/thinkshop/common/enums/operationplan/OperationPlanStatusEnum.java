package com.think.cloud.thinkshop.common.enums.operationplan;

/**
 * @author zkthink
 * @apiNote 0：未开始 ，1进行中， 2：结束
 **/
public enum OperationPlanStatusEnum {
    UNSTARTED(0, "未开始"),
    IN_PROGRESS(1, "进行中"),
    END(2, "结束");

    private final Integer code;
    private final String description;

    OperationPlanStatusEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static OperationPlanStatusEnum toEnum(Integer code) {
        for (OperationPlanStatusEnum item : OperationPlanStatusEnum.values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }
        return null;
    }
}
