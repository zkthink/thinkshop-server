package com.think.cloud.thinkshop.common.enums.operationplan;

/**
 * @author zkthink
 * @apiNote
 **/
public enum OperationPlanRefTypeEnum {
    REF_GROUP(1, "客户分群"),
    REF_PRODUCT_COUPON(2, "商品优惠券"),
    ;
    private Integer code;
    private String description;

    OperationPlanRefTypeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static OperationPlanRefTypeEnum toEnum(Integer code) {
        for (OperationPlanRefTypeEnum operationPlanRefTypeEnum : OperationPlanRefTypeEnum.values()) {
            if (operationPlanRefTypeEnum.getCode().equals(code)) {
                return operationPlanRefTypeEnum;
            }
        }
        return null;
    }
}
