package com.think.cloud.thinkshop.common.constant.product;

/**
 * 商品
 *
 * @author zkthink
 */
public class ProductConstants {
    /**
     * 商品编号前缀
     */
    public static final String PRODUCT_CODE_PREFIX = "SPU";

    /**
     * 商品编号key
     */
    public static final String PRODUCT_CODE_REDIS_KEY = "product_code_key";

    /**
     * 流水号日期格式
     */
    public static final String DATE_FORMAT = "yyyyMMdd";

    /**
     * 升序编码格式
     */
    public static final String NUMBER_FORMAT = "%06d";

    /**
     * 商品锁key
     */
    public static final String PRODUCT_LOCK_KEY = "product:lock:";
}
