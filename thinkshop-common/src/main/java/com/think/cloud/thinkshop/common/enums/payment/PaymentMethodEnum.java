package com.think.cloud.thinkshop.common.enums.payment;

public enum PaymentMethodEnum {
    PAYPAL("paypal"),
    STRIPE("stripe");

    private final String code;

    // 构造方法
    PaymentMethodEnum(String code) {
        this.code = code;
    }

    // 获取显示名称的方法
    public String getCode() {
        return code;
    }
}
