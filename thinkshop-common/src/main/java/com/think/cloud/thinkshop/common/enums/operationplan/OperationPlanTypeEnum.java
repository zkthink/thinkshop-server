package com.think.cloud.thinkshop.common.enums.operationplan;

/**
 * @author zkthink
 * @apiNote planType  1：自动  ，2：手动
 **/
public enum OperationPlanTypeEnum {
    AUTO(1, "自动"),
    MANUAL(2, "手动");

    private final Integer code;
    private final String description;

    OperationPlanTypeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static OperationPlanTypeEnum toEnum(Integer code) {
        for (OperationPlanTypeEnum value : OperationPlanTypeEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }
}
