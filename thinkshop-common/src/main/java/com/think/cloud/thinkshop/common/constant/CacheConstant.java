package com.think.cloud.thinkshop.common.constant;

/**
 * @author zkthink
 * @apiNote
 **/
public class CacheConstant {
    /**
     * 网站设置
     */
    public static final String CACHE_WEBSITE_SETTING = "websiteSetting";

    /**
     * 仅退款原因
     */
    public static final String REFUND_ONLY_REASON = "refund_only_reason";
    /**
     * 退货退款原因
     */
    public static final String RETURN_GOODS_REFUND = "return_goods_refund";
}
