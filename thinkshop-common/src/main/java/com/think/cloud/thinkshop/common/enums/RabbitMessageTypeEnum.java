package com.think.cloud.thinkshop.common.enums;

import java.util.stream.Stream;
/**
 * @author zkthink
 * @apiNote
 **/
public enum RabbitMessageTypeEnum {
    DELAY_ORDER_CANCEL_HOUR("1", "订单自动取消小时"),
    DELAY_ORDER_AUTOMATIC_RECEIPT_DAYS("2", "自动收货天数"),
    DELAY_ORDER_AFTER_SALE_CLOSE("3", "客户确认收货x天后，不可进行售后,同时赠送积分"),
    DELAY_ORDER_AFTER_SALE_AUTOMATIC_CANCELLATION("4", "同意退货退款后,x天客户未寄回商品，则售后自动取消"),
    DELAY_COUPON_START("5", "优惠券活动开始"),
    DELAY_COUPON_END("6", "优惠券活动结束"),
    DELAY_OPERATION_PLAN_EXECUTE_AUTO_START("7", "运营计划开始"),
    DELAY_OPERATION_PLAN_EXECUTE_AUTO_END("8", "运营计划结束"),
    DELAY_OPERATION_PLAN_EXECUTE_MANUAL_START("9", "运营计划手动开始"),
    DELAY_ORDER_WAIT_PAY_NOTICE("10", "订单待支付通知"),
    DELAY_ORDER_AUTO_RECEIPT_NOTICE("11", "自动收货前24小时的通知"),

    //================= 普通消息 =============

    MEMBER_GROUP_CONFIG_INSERT_OR_UPDATE("101","客户分群:自动触发分群(新增和修改配置人群的配置的时候触发)"),
    MEMBER_GROUP_UPDATE_BY_USER_ACTION("102","客户分群:自动触发分群(用户创建订单，加购物车，支付，更新标签，登录（最后到访）时候触发)"),
    OPERATION_PLAN_EXECUTE("103","运营计划执行"),
    ;
    private final String code;
    private final String description;

    RabbitMessageTypeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static RabbitMessageTypeEnum toEnum(String code) {
        return Stream.of(RabbitMessageTypeEnum.values())
                .filter(p -> p.code.equals(code))
                .findAny()
                .orElse(null);
    }
}
