delete from sys_dict_data where id in (102,103);

INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(0, '(UTC±00:00)格林尼治标准时间、伦敦、都柏林', '0', 'sys_time_zone', NULL, 'default', 'N');
-- 东一时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(1, '(UTC+01:00)欧洲中部时间', '+1', 'sys_time_zone', NULL, 'default', 'N');

-- 东二时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(2, '(UTC+02:00)东欧、中欧、中东', '+2', 'sys_time_zone', NULL, 'default', 'N');

-- 东三时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(3, '(UTC+03:00)莫斯科、东非', '+3', 'sys_time_zone', NULL, 'default', 'N');

-- 东四时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(4, '(UTC+04:00)阿布扎比、第比利斯', '+4', 'sys_time_zone', NULL, 'default', 'N');

-- 东五时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(5, '(UTC+05:00)伊斯兰堡、喀布尔', '+5', 'sys_time_zone', NULL, 'default', 'N');

-- 东六时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(6, '(UTC+06:00)达卡、仰光', '+6', 'sys_time_zone', NULL, 'default', 'N');

-- 东七时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(7, '(UTC+07:00)曼谷、雅加达', '+7', 'sys_time_zone', NULL, 'default', 'N');

-- 东八时区（已提供，示例重复）
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(8, '(UTC+08:00)北京时间', '+8', 'sys_time_zone', NULL, 'default', 'N');

-- 东九时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(9, '(UTC+09:00)东京、首尔', '+9', 'sys_time_zone', NULL, 'default', 'N');

-- 西一时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(17, '(UTC-01:00)亚速尔群岛', '-1', 'sys_time_zone', NULL, 'default', 'N');

-- 西二时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(18, '(UTC-02:00)南大西洋标准时间', '-2', 'sys_time_zone', NULL, 'default', 'N');

-- 西三时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(19, '(UTC-03:00)巴西利亚、布宜诺斯艾利斯', '-3', 'sys_time_zone', NULL, 'default', 'N');

-- 西四时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(20, '(UTC-04:00)加拉加斯、纽约', '-4', 'sys_time_zone', NULL, 'default', 'N');

-- 西五时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(21, '(UTC-05:00)芝加哥、墨西哥城', '-5', 'sys_time_zone', NULL, 'default', 'N');

-- 西六时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(22, '(UTC-06:00)中北美中部时间', '-6', 'sys_time_zone', NULL, 'default', 'N');

-- 西七时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(23, '(UTC-07:00)美国山地时间', '-7', 'sys_time_zone', NULL, 'default', 'N');

-- 西八时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(24, '(UTC-08:00)太平洋时间（美国）', '-8', 'sys_time_zone', NULL, 'default', 'N');

-- 西九时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(25, '(UTC-09:00)阿拉斯加时间', '-9', 'sys_time_zone', NULL, 'default', 'N');

-- 西十时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(26, '(UTC-10:00)夏威夷时间', '-10', 'sys_time_zone', NULL, 'default', 'N');

-- 西十一时区
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(27, '(UTC-11:00)中途岛、萨摩亚', '-11', 'sys_time_zone', NULL, 'default', 'N');

-- 美国
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(1, 'USD(美元)', 'USD', 'sys_currency', NULL, 'default', 'N');

-- 加拿大
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(2, 'CAD(加拿大元)', 'CAD', 'sys_currency', NULL, 'default', 'N');

-- 英国
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(3, 'GBP(英镑)', 'GBP', 'sys_currency', NULL, 'default', 'N');

-- 捷克
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(4, 'CZK(捷克克朗)', 'CZK', 'sys_currency', NULL, 'default', 'N');

-- 丹麦
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(5, 'DKK(丹麦克朗)', 'DKK', 'sys_currency', NULL, 'default', 'N');

-- 欧元区（代表所有使用欧元的国家）
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(6, 'EUR(欧元)', 'EUR', 'sys_currency', NULL, 'default', 'N');

-- 马来西亚
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(7, 'MYR(马来西亚林吉特)', 'MYR', 'sys_currency', NULL, 'default', 'N');

-- 挪威
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(8, 'NOK(挪威克朗)', 'NOK', 'sys_currency', NULL, 'default', 'N');

-- 瑞典
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(9, 'SEK(瑞典克朗)', 'SEK', 'sys_currency', NULL, 'default', 'N');

-- 澳大利亚
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(10, 'AUD(澳大利亚元)', 'AUD', 'sys_currency', NULL, 'default', 'N');

-- 新西兰
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(11, 'NZD(新西兰元)', 'NZD', 'sys_currency', NULL, 'default', 'N');

-- 新加坡
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(12, 'SGD(新加坡元)', 'SGD', 'sys_currency', NULL, 'default', 'N');

-- 瑞士
INSERT INTO sys_dict_data (dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default)
VALUES(13, 'CHF(瑞士法郎)', 'CHF', 'sys_currency', NULL, 'default', 'N');

ALTER TABLE mall_order MODIFY COLUMN region_id varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '地区编码';
ALTER TABLE mall_shipping_templates_region_ref MODIFY COLUMN region_id varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域编码';
ALTER TABLE mall_user_address MODIFY COLUMN country_id varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '国家编码';
ALTER TABLE mall_user_address MODIFY COLUMN province_id varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份编码';
