
-- --------------1.1版本中需要修改的表结构------------------------
ALTER TABLE `mall_after_sales` ADD `user_del` bit(1) DEFAULT b'0' NOT NULL COMMENT '用户是否删除 0：否  1：是';

ALTER TABLE `mall_order` ADD `user_del` bit(1) DEFAULT b'0' NOT NULL COMMENT '用户是否删除 0：否  1：是';
ALTER TABLE `mall_order` ADD `use_integral` int(7) DEFAULT NULL COMMENT '订单使用的积分';
ALTER TABLE `mall_order` ADD `integral_deduct` decimal(10,2) DEFAULT NULL COMMENT '积分抵扣的金额';
ALTER TABLE `mall_order` ADD `complete_time` datetime DEFAULT NULL COMMENT '订单完成时间';

ALTER TABLE `mall_page_config` ADD `page_bottom_qr_code_url` text COMMENT '页脚二维码';

ALTER TABLE `mall_product` ADD `sku_type` int(11) NOT NULL DEFAULT '2' COMMENT '商品规格类型：1：单规格 2：多规格';

ALTER TABLE `mall_website_setting` ADD`pass_day_reject_comment` varchar(100) DEFAULT '15' COMMENT '订单完成多少天不能评价';

ALTER TABLE `sys_notice` MODIFY COLUMN status char(1) CHARACTER
    SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '1' NULL COMMENT '公告状态（0: 正常  1：关闭）';


-- -------------------------新增表------------------
--  mall_integral_bill   mall_integral_detail   mall_integral_rule
--  mall_message_template    mall_message_user_record
--  mall_product_comment
-- ------------------------------

-- thinkshop.mall_integral_bill definition

CREATE TABLE `mall_integral_bill` (
                                      `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                      `user_id` bigint(20) NOT NULL COMMENT '用户id',
                                      `detail` text COMMENT '详情',
                                      `integral` int(7) NOT NULL COMMENT '积分值',
                                      `bill_type` int(4) NOT NULL COMMENT '账单类型 1：增加  2：消费 3：过期',
                                      `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
                                      `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                      `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
                                      `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                      `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
                                      `user_integral` int(8) DEFAULT NULL COMMENT '用户积分剩余',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='积分流水';


-- thinkshop.mall_integral_detail definition

CREATE TABLE `mall_integral_detail` (
                                        `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                        `user_id` bigint(20) NOT NULL COMMENT '用户',
                                        `status` int(2) NOT NULL DEFAULT '1' COMMENT '积分状态 1：正常（含部分使用），2：完全使用：3：完全过期，4部分过期（部分已使用）',
                                        `total_integral` int(7) NOT NULL COMMENT '总积分值',
                                        `remain_integral` int(7) NOT NULL COMMENT '剩余积分值',
                                        `detail` varchar(64) DEFAULT NULL COMMENT '描述',
                                        `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
                                        `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                        `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
                                        `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                        `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='积分详情';


-- thinkshop.mall_integral_rule definition

CREATE TABLE `mall_integral_rule` (
                                      `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                      `is_use` int(4) NOT NULL COMMENT '使用可用于抵扣 0：不能 1：能',
                                      `deduction_ratio` decimal(7,2) NOT NULL COMMENT '抵扣比值，1积分可以抵扣的金额',
                                      `deduction_type` int(4) NOT NULL COMMENT '抵扣类型 1；最多积分 ，2：最高比例',
                                      `deduction_value` decimal(7,2) DEFAULT NULL COMMENT '抵扣类型值',
                                      `order_award` int(7) DEFAULT NULL COMMENT '订单赠送积分',
                                      `clean_type` int(4) NOT NULL COMMENT '积分清空类型:0:不清零 1： 2：:3： ',
                                      `describe_info` text NOT NULL COMMENT '描述',
                                      `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
                                      `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                      `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
                                      `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                      `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='积分规则';


-- thinkshop.mall_message_template definition

CREATE TABLE `mall_message_template` (
                                         `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                         `scene_name` varchar(25) NOT NULL COMMENT '场景名称',
                                         `use_inform` bit(1) NOT NULL DEFAULT b'0' COMMENT '开启站内信,0:不使用 1：使用',
                                         `use_email` bit(1) NOT NULL DEFAULT b'0' COMMENT '开启邮件通知,0:不使用 1：使用',
                                         `inform_title` varchar(50) NOT NULL COMMENT '站内信标题',
                                         `inform_template` text NOT NULL COMMENT '站内信内容模版',
                                         `email_title` varchar(50) NOT NULL COMMENT '邮件标题',
                                         `email_template` text NOT NULL COMMENT '站内信内容',
                                         `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
                                         `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                         `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
                                         `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                         `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
                                         PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统消息模版配置';
-- mall_message_template base data  ,按需修改数据库名
INSERT INTO thinkshop.mall_message_template (scene_name,use_inform,use_email,inform_title,inform_template,email_title,email_template,create_by,create_time,update_by,update_time,deleted) VALUES
('注册验证码通知',0,1,'账户邮箱注册验证（THINKSHOP）','亲爱的用户 {用户邮箱}，我们检测到您正在申请注册,请输入如下验证码完成操作:{验证码}，为保障账号安全，验证码有效期为10分钟，使用一次后将失效!THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','账户邮箱注册验证（THINKSHOP）','亲爱的用户{用户邮箱}，我们检测到您正在申请注册,请输入如下验证码完成操作:{验证码}为保障账号安全，验证码有效期为10分钟，使用一次后将失效!THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-25 10:24:31',0),
('重置密码发送验证码通知',0,1,'账户重置密码验证（THINKSHOP）','亲爱的用户{用户邮箱}，我们检测到您正在申请重置密码,请输入如下验证码完成操作:{验证码}为保障账号安全，验证码有效期为10分钟，使用一次后将失效!如非本人操作，请忽略本封邮件。THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','账户重置密码验证（THINKSHOP）','亲爱的用户{用户邮箱}，我们检测到您正在申请重置密码,请输入如下验证码完成操作:{验证码}为保障账号安全，验证码有效期为10分钟，使用一次后将失效!如非本人操作，请忽略本封邮件。THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-23 10:57:45',0),
('支付成功通知',1,1,'订单支付成功','您购买的{商品}已支付成功，支付金额{货币单位}{金额}，订单号 {订单号}','订单支付成功','您购买的{商品}已支付成功，支付金额{货币单位}{金额}，订单号{订单号},感谢您的光临！THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-31 15:17:05',0),
('已发货通知消息',1,1,'订单发货通知','您购买的{商品}已发货，订单号 {订单号},请注意查收！','订单发货通知','您购买的{商品}已发货，订单号{订单号},请注意查收！THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-25 10:30:28',0),
('自动确认收货通知消息（24小时前）',1,1,'请确认您的订单是否签收','您的订单{订单号}即将在24小时后自动确认收货，如未收到货请及时联系平台处理。','请确认您的订单是否签收','您的订单{订单号}即将在24小时后自动确认收货，如未收到货请及时联系平台处理。THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-25 10:29:29',0),
('订单自动取消通知',1,1,'订单取消','由于您超时未付款，您的订单{订单号}已被系统自动取消。','订单取消','由于您超时未付款，您的订单{订单号}已被系统自动取消。THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-25 10:29:03',0),
('待付款通知消息（用户下单后1小时未付款后发送）',1,1,'订单待付款','您的订单{订单号}已生成，总金额为{货币单位}{金额}。请在{时间}内完成支付，以确保订单顺利处理，祝您购物愉快！','订单待付款','您的订单{订单号}已生成，总金额为{货币单位}{金额}。请在{时间}内完成支付，以确保订单顺利处理。祝您购物愉快！THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-31 16:55:16',0),
('退款成功通知',1,1,'退款成功','您的订单{订单号}退款已成功处理，金额{货币单位}{金额}将在24小时内退回至您的原支付账户。感谢您的耐心等待与支持。如有任何疑问，请联系客服。','退款成功','您的订单{订单号}退款已成功处理，金额{货币单位}{金额}将在24小时内退回至您的原支付账户。感谢您的耐心等待与支持。如有任何疑问，请联系客服。THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-08-01 17:35:36',0),
('售后失败通知',1,1,'售后被拒绝','尊敬的{用户邮箱}，关于您的订单{订单号}的售后申请，我们深感抱歉地通知您，由于{拒绝原因}，本次申请未能通过。如需进一步咨询或帮助，请联系平台客服。','售后被拒绝','尊敬的{用户邮箱}，关于您的订单{订单号}的售后申请，我们深感抱歉地通知您，由于{拒绝原因}，本次申请未能通过。如需进一步咨询或帮助，请联系平台客服。THINKSHOP团队访问地址:https://thinkshop.zkthink.com/系统邮件，请勿回复','','2024-07-19 15:16:14','','2024-07-30 15:13:05',0);



-- thinkshop.mall_message_user_record definition

CREATE TABLE `mall_message_user_record` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                            `user_id` bigint(20) NOT NULL COMMENT '用户id',
                                            `title` varchar(50) NOT NULL COMMENT '标题',
                                            `content` text NOT NULL COMMENT '内容',
                                            `user_read` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否已读,0:未读 1：已读',
                                            `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
                                            `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                            `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
                                            `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                            `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
                                            `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
                                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户接收的站内信';


-- thinkshop.mall_product_comment definition

CREATE TABLE `mall_product_comment` (
                                        `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                        `product_id` bigint(20) NOT NULL COMMENT '商品id',
                                        `sku_id` bigint(20) NOT NULL COMMENT '商品规格id',
                                        `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
                                        `comment` varchar(500) NOT NULL COMMENT '评论内容',
                                        `score` decimal(3,1) NOT NULL COMMENT '评分 ',
                                        `comment_user` varchar(20) NOT NULL COMMENT '评论人',
                                        `image_url` text NOT NULL COMMENT '评论图链接',
                                        `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态:0-不展示 ，1-展示',
                                        `comment_time` datetime NOT NULL COMMENT '评论时间',
                                        `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '创建者',
                                        `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                        `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '更新者',
                                        `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                                        `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商品评价表';

--  -------------解决1.0版本中部分表中字段无法保存富文本表情的问题-------------------
-- 推荐： 创建数据库时先将默认字符集设置为 utf8mb4 ，方法如下：
-- ALTER DATABASE [your_database_name] CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
-- 以下是按需的针对性修改（按需修改数据库名）：
ALTER TABLE thinkshop.mall_product MODIFY COLUMN detail varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '详情';
ALTER TABLE thinkshop.mall_message_template MODIFY COLUMN inform_template text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '站内信内容模版';
ALTER TABLE thinkshop.mall_product_comment MODIFY COLUMN comment varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论内容';
ALTER TABLE thinkshop.mall_integral_rule MODIFY COLUMN describe_info text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述';
ALTER TABLE thinkshop.sys_notice MODIFY COLUMN notice_content longblob NULL COMMENT '公告内容';