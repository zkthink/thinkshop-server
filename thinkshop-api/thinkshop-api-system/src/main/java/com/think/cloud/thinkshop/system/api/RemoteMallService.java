package com.think.cloud.thinkshop.system.api;

import com.think.cloud.thinkshop.common.constant.ServiceNameConstants;
import com.think.cloud.thinkshop.system.api.dto.AuthUserDTO;
import com.think.cloud.thinkshop.system.api.dto.MemberUserDTO;
import com.think.cloud.thinkshop.system.api.factory.RemoteUserFallbackFactory;
import com.think.common.core.domain.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zkthink
 * @apiNote
 **/
@FeignClient(contextId = "remoteMallService", value = ServiceNameConstants.MALL_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteMallService {

    @GetMapping("/admin/user/getByUsername")
    R<MemberUserDTO> getByUsername(@RequestParam(value = "username") String username);

    @PostMapping("/admin/user/oauthCallback")
    R<MemberUserDTO> oauthCallback(@RequestBody AuthUserDTO authUser);

    @GetMapping("/admin/operation-log/runMemberOperationLogTask")
    R<Boolean>  runMemberOperationLogTask();

    @GetMapping("/admin/memberGroup/memberGroupUserUpdate")
    R<Boolean> runMemberGroupUserUpdate();

    @GetMapping("/admin/memberGroup/memberGroupUserUpdateByUserId")
    R<Boolean> runMemberGroupUserUpdateByUserId(@RequestParam(value = "userId") Long userId);

    @GetMapping("/admin/integralRule/cleanTask")
    R<Boolean> cleanTask();
}
