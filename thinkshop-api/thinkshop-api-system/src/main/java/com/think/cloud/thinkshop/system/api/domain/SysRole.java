package com.think.cloud.thinkshop.system.api.domain;

import com.think.common.core.domain.BaseRole;

/**
 * 角色表 sys_role
 * 
 * @author zkthink
 */
public class SysRole extends BaseRole
{
    private static final long serialVersionUID = 1L;

    public SysRole() {
    }

    public SysRole(Long roleId) {
        this.roleId = roleId;
    }

}
