package com.think.cloud.thinkshop.system.api.factory;

import com.think.cloud.thinkshop.system.api.RemoteMallService;
import com.think.cloud.thinkshop.system.api.dto.AuthUserDTO;
import com.think.cloud.thinkshop.system.api.dto.MemberUserDTO;
import com.think.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author zkthink
 * @apiNote
 **/
@Component
public class RemoteMallFallbackFactory implements FallbackFactory<RemoteMallService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteMallFallbackFactory.class);

    @Override
    public RemoteMallService create(Throwable cause) {
        log.error("商城服务调用失败:{}", cause.getMessage());
        return new RemoteMallService() {
            @Override
            public R<MemberUserDTO> getByUsername(String username) {
                return R.fail("查询用户信息失败:" + cause.getMessage());
            }

            @Override
            public R<MemberUserDTO> oauthCallback(AuthUserDTO authUser) {
                return R.fail("三方授权回调失败:" + cause.getMessage());
            }

            @Override
            public R<Boolean> runMemberOperationLogTask() {
                return R.fail("运营数据任务执行失败:" + cause.getMessage());
            }

            @Override
            public R<Boolean> runMemberGroupUserUpdate() {
                return R.fail("客户分群用户更新失败:" + cause.getMessage());
            }

            @Override
            public R<Boolean> runMemberGroupUserUpdateByUserId(Long userId) {
                {
                    return R.fail("用户分群更新失败:" + cause.getMessage());
                }
            }

            @Override
            public R<Boolean> cleanTask() {
                return R.fail("积分清空失败:" + cause.getMessage());
            }
        };
    }
}
