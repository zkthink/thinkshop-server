package com.think.cloud.thinkshop.system.api.domain;

import com.think.common.core.domain.BaseOperLog;

/**
 * 操作日志记录表 oper_log
 * 
 * @author zkthink
 */
public class SysOperLog extends BaseOperLog
{
    private static final long serialVersionUID = 1L;
}
