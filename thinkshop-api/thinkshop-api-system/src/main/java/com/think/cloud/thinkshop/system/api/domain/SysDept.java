package com.think.cloud.thinkshop.system.api.domain;

import com.think.common.core.domain.BaseDept;

/**
 * 部门表 sys_dept
 * 
 * @author zkthink
 */
public class SysDept extends BaseDept<SysDept>
{
    private static final long serialVersionUID = 1L;

}
