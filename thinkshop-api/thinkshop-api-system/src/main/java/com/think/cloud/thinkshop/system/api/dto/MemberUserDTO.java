package com.think.cloud.thinkshop.system.api.dto;

import com.think.common.core.annotation.Excel;

/**
 * @author zkthink
 * @apiNote
 **/
public class MemberUserDTO {
    private Long id;

    /** 账号 */
    private String username;

    /** 邮箱 */
    private String email;

    /** 密码 */
    private String password;

    /** first name */
    private String firstName;

    /** last name */
    private String lastName;

    /** 头像 */
    private String avatar;

    /** 注册来源 */
    private String source;

    /** 三方用户id */
    private String thirdUid;

    /** 帐号状态（0正常 1停用） */
    private Integer status;

    /** 手机号码 */
    private String phonenumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getThirdUid() {
        return thirdUid;
    }

    public void setThirdUid(String thirdUid) {
        this.thirdUid = thirdUid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
}
