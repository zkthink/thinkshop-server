package com.think.cloud.thinkshop.system.api.domain;

import com.think.common.core.domain.BaseUser;

/**
 * 用户对象 sys_user
 * 
 * @author zkthink
 */
public class SysUser extends BaseUser<SysRole,SysDept>
{
    private static final long serialVersionUID = 1L;


    public SysUser() {
    }

    public SysUser(Long userId) {
        this.userId = userId;
    }

}
