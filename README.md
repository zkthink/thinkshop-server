# THINK-SHOP 跨境电商微服务平台

一个基于Spring Cloud Alibaba构建的高性能、高可用的跨境电商微服务解决方案，专注于提供高效的商品管理、订单处理、支付集成以及全球化物流跟踪等功能，致力于优化跨境贸易体验。

## 一、项目亮点

- 微服务架构：采用Spring Cloud Alibaba实现服务治理，极大提升系统的可扩展性和容错能力。
- 分布式事务管理：集成Seata，确保跨服务间的事务一致性。
- 服务发现与配置：Nacos不仅用于服务注册与发现，还负责配置中心管理，简化运维复杂度。
- 点击查看功能详细介绍API网关：Spring Cloud Gateway作为统一入口，实现智能路由与API请求过滤。
- 全球化特性：支持多语言界面和货币自动转换，无缝对接全球用户。
- 安全机制：集成OAuth2与JWT，确保API接口的安全访问控制。

## 二、技术栈概览

- 后端框架：Spring Boot, Spring Cloud Alibaba
- 服务治理与配置：Nacos
- API 网关：Spring Cloud Gateway
- 服务间通信：Spring Cloud OpenFeign
- 消息队列：RabbitMQ
- 数据存储：MySQL, Redis
- 部署环境：Docker, Kubernetes

## 三、文档与API

- 服务管理与配置：访问Nacos控制台查看服务列表与配置详情。
- API文档：通过Swagger UI浏览和测试API接口。

## 四、功能清单
# 1.0版本

| 端口       | 模块           | 功能简介                                                                 |
|:----------:|:--------------:| ------------------------------------------------------------------------:|
| 管理端     | 数据概览       | 快捷入口                                                                 |
|            |                | 客户数据统计                                                            |
|            |                | 订单数据统计                                                            |
|            |                | 财务数据统计                                                            |
|            |                | 转化率                                                                   |
|            | 商品管理       | 商品类目                                                                 |
|            |                | 商品分组                                                                 |
|            |                | 商品标签管理                                                            |
|            |                | 商品新增、商品上下架、商品编辑、商品是否收税                             |
|            |                | 商品SKU管理                                                             |
|            | 订单管理       | 订单-未支付、未发货、已收货                                             |
|            |                | 售后订单-退款、退货退款                                                 |
|            | 客户管理       | 客户列表                                                                 |
|            |                | 客户标签                                                                 |
|            |                | 客户分群                                                                 |
|            |                | 客户运营                                                                 |
|            | 网站设置       | 基本信息管理                                                            |
|            |                | 发/退货地址                                                             |
|            |                | 运费模版方案                                                            |
|            |                | 语言配置                                                                 |
|            |                | 时区配置                                                                 |
|            |                | 货币配置                                                                 |
|            |                | 税费配置                                                                 |
|            |                | 收款配置（支付）                                                         |
|            | 营销           | 优惠券/优惠码                                                            |
|            | 规则配置       | 协议配置                                                                 |
|            |                | 物流配置                                                                 |
|            |                | 订单规则配置                                                            |
|            |                | 退款原因配置                                                            |
|            |                | 客服配置                                                                 |
|            | 系统           | 系统用户                                                                 |
|            |                | 部门管理                                                                 |
|            |                | 角色管理                                                                 |
|            |                | 菜单管理                                                                 |
|            | 首页配置       | banner、图文、页眉、页尾配置                                            |

| 端口         | 客户端（pc、h5） | 功能简介       | 备注     |
|:------------:|:-----------------:|:--------------:|:--------:|
| 首页         | 首页             | 视装修而定     |          |
| 注册登录     | 注册登录         | 手机号/邮箱注册登录，三方注册登录 | 忘记密码 |
| 搜索         | 搜索             | 商品搜索       |          |
| 分类         | 分类             | 商品三级分类   | 分类详情页 |
| 商品         | 商品             | 商品详情       |          |
| 购物车       | 购物车           | 购物车添加、删除、清空 |          |
| 结算支付     | 结算支付         | paypal、Stripe |          |
| 用户基本信息 | 用户基本信息     | 账户资料       | 密码管理 |
| 地址管理     | 地址管理         | 增删改查       | 默认地址 |
| 订单管理     | 订单管理         | 待支付         | 待发货   |
|              |                  | 待发货         | 已取消   |
|              |                  |                | 已完成   |
|              |                  |                | 订单售后-退款、退货退款 |

# 1.1版本

| 端口       | 模块     | 功能简介                         | 备注                 |
|:----------:|:--------:|:--------------------------------:|:--------------------:|
| 管理端     | 商品     | 增加商品导入/导出               | EXCEL导入导出       |
|            |          | 增加商品单规格                   | 创建商品时可创建单规格 |
|            |          | 商品评价                         | 商品评价列表，增删改查 |
| 客户管理   | 客户管理 | 客户积分                         | 客户积分明细列表   |
|            |          | 客户积分规则                     | 设置积分获取消耗比例、积分规则等 |
| 消息通知   | 消息通知 | 公告                             | 公告列表、发布、新增、删除、取消发布 |
|            |          | 系统消息                         | 系统消息模版配置、站内信开关、邮件开关 |
| 订单管理   | 订单管理 | 订单列表优化                     |                       |
|            |          | 订单详情优化                     | 增加税费信息，修改商品总额、实付款、优惠券抵扣、积分抵扣等 |
|            |          | 售后订单列表优化                 |                       |
|            |          | 售后订单详情优化                 |                       |
| 页脚配置   | 页脚配置 | 增加二维码配置                   |                       |
| 订单规则   | 订单规则 | 可评价时间配置                   | 配置用户可进行评价的时间 |
| 客户端（pc、h5） | 注册 | 注册验证码                   | 增加邮箱注册验证码 |
| 商品下单   | 商品下单 | 积分抵扣                         | 增加积分抵扣金额   |
| 商品详情   | 商品详情 | 商品详情增加商品评价             | 增加商品评价展示   |
| 订单       | 订单     | 商品评价                         | 提交商品评价（以商品规格为维度） |
|            |          | 优化商品图                       | 订单列表的商品图片为选中规格的商品图 |
| 购物车     | 购物车   | 优化商品图                       | 添加购物车的商品图片为选中规格的商品图 |
| 消息通知   | 消息通知 | 公告通知列表、详情               |                       |
|            |          | 系统消息列表                     |                       |
| 我的积分    | 我的积分  | 剩余积分                         |                       |
|            |          | 积分消耗列表展示                 |                       |
|            |          | 积分规则                         |                       |

## 五、THINK-SHOP使用的主要技术栈

| 名称       | 版本     | 说明   |
|------------|----------|--------|
| jdk        | 1.8      |        |
| spring boot | 2.7      |        |
| spring cloud alibaba | 2021.0.5 |        |
| nacos      | 2.0.3    |        |
| rabbitmq   | 3.8      |        |
| mysql      | 8.0      |        |
| redis      | 6.0      |        |

## 六、在线演示

演示地址：
- [THINK-SHOP-WEB端](https://thinkshop.zkthink.com/)
- [THINK-SHOP-移动端](https://thinkshop.zkthink.com/h5)
- [THINK-SHOP-管理端](https://thinkshop.zkthink.com/admin) 演示账号 账号:ZKTHINKSHOP 密码:123456

## 七、代码仓库

- 服务端代码
- 服务端公共依赖代码
- 移动端代码
- WEB端代码
- 管理端代码

## 八、环境部署要求

### 1、准备工作

确保已安装以下软件及版本：
- JDK: >= 1.8 （推荐使用1.8版本）
- MySQL: >= 5.7.0 （推荐5.7版本）
- Redis: >= 3.0
- Maven: >= 3.0
- Node.js: >= 12
- Nacos: >= 2.0.4
- Sentinel: >= 1.6.0

### 2、运行步骤

- 获取代码：从版本控制系统中拉取项目代码（服务端代码和服务端公共依赖代码）。
- 打包公共依赖: 打开终端，进入think-cloud-build目录，执行`mvn clean install`命令。
- 数据库准备：创建数据库thinkshop，并导入初始化脚本thinkshopxxxx.sql。
- 配置Nacos：修改conf/application.properties文件，添加MySQL数据源配置以支持持久化。
- 启动服务：ThinkshopGatewayApplication：网关服务 ThinkshopAuthApplication：认证服务 ThinkshopMallApplication：商场服务 ThinkshopJobApplication：定时任务服务 ThinkshopSystemApplication：系统服务 ThinkshopAssembleApplication：聚合服务

### 3、后端部署指南

- 打包应用：在项目bin目录下运行package.bat脚本，生成.war或.jar包。打包后，target目录下将包含生成的文件。
- 部署执行：使用命令行部署：执行`java -jar thinkshop-xxxx.jar`。或者，通过脚本部署：运行bin/run-xxxx.bat。

## THINK-SHOP功能模块预览

- 后台管理
![示例图片1](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image.png)
![示例图片2](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(1).png)
![示例图片3](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(2).png)
![示例图片4](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(3).png)
![示例图片5](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(4).png)
- H5端
![示例图片6](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(5).png)
![示例图片7](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(6).png)
![示例图片8](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(7).png)
- PC端
![示例图片9](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(8).png)
![示例图片10](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(9).png)
![示例图片11](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image%20(11).png)

## 联系我们

关注公众号
![公众号](http://think-shop.oss-cn-hangzhou.aliyuncs.com/example/image.jpeg)
QQ群: 720680973
官网地址：[https://zkthink.com/](https://zkthink.com/)

