package com.think.cloud.thinkshop.system.controller.app;

import com.think.cloud.thinkshop.system.domain.SysNotice;
import com.think.cloud.thinkshop.system.service.ISysNoticeService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/app/notice")
public class AppSysNoticeController extends BaseController {
    @Autowired
    private ISysNoticeService noticeService;

    /**
     * 查询公告列表
     * @return
     */

    @GetMapping("/list")
    public TableDataInfo list() {
        startPage();
        List<SysNotice> list = noticeService.getAppNoticeList();
        return getDataTable(list);
    }

    /**
     * 获取公告详情
     * @return
     */
    @GetMapping("/get/{noticeId}")
    public AjaxResult get(@PathVariable Long noticeId) {
        return success(noticeService.selectNoticeById(noticeId));
    }
}
