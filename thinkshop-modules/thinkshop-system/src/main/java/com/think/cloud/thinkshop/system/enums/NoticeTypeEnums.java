package com.think.cloud.thinkshop.system.enums;

public enum NoticeTypeEnums {
    //实际上通知并没有使用，只是为了保留原有结构
    TYPE_1("1","通知"),
    TYPE_2("2","公告"),

    ;

    private String type;
    private String desc;

     NoticeTypeEnums(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
