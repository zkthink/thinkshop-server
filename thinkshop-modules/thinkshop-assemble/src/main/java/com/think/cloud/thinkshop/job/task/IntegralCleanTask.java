package com.think.cloud.thinkshop.job.task;

import com.think.cloud.thinkshop.system.api.RemoteMallService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("integralCleanTask")
public class IntegralCleanTask {
    private final Logger log = LoggerFactory.getLogger(IntegralCleanTask.class);
    @Autowired
    private RemoteMallService remoteMallService;
    public void run(){
        log.info("---------------到期积分清理-------------------------");
        remoteMallService.cleanTask();
        log.info("---------------到期积分清理  完成-------------------------");
    }
}
