package com.think.cloud.thinkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.think.common.security.annotation.EnableCustomConfig;
import com.think.common.security.annotation.EnableRyFeignClients;
import com.think.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 文件服务
 *
 * @author zkthink
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class ThinkShopAssembleApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(ThinkShopAssembleApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  聚合服务模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
