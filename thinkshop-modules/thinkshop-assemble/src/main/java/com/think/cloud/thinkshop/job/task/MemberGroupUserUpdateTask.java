package com.think.cloud.thinkshop.job.task;

import com.think.cloud.thinkshop.system.api.RemoteMallService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zkthink
 * @apiNote 更新用户分群task
 **/
@Component("memberGroupUserUpdateTask")
public class MemberGroupUserUpdateTask {
    private final Logger log = LoggerFactory.getLogger(MemberGroupUserUpdateTask.class);
    @Autowired
    private RemoteMallService remoteMallService;

    //memberGroupUserUpdateTask.run()

    public void run(){
        log.info("memberGroupUserUpdateTask任务开始执行");
        remoteMallService.runMemberGroupUserUpdate();
        log.info("memberGroupUserUpdateTask任务执行完成");
    }
}
