package com.think.cloud.thinkshop.job.task;

import com.think.cloud.thinkshop.system.api.RemoteMallService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zkthink
 * @apiNote
 **/
@Component("memberOperationLogTask")
public class MemberOperationLogTask {
    private final Logger log = LoggerFactory.getLogger(MemberOperationLogTask.class);
    @Autowired
    private RemoteMallService remoteMallService;

    //memberOperationLogTask.run()

    public void run(){
        log.info("memberOperationLogTask任务开始执行");
        remoteMallService.runMemberOperationLogTask();
        log.info("memberOperationLogTask任务执行完成");
    }
}
