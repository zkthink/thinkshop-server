import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author zkthink
 * @apiNote
 **/
public class PasswordTest {
    public static void main(String[] args) {
        String password = "zkthink2024";
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        System.out.println(encoder.encode(password));
    }
}
