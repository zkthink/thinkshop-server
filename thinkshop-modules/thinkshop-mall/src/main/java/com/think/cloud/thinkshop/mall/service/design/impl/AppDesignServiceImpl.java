package com.think.cloud.thinkshop.mall.service.design.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerPageRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexProductGroupDesignRespVO;
import com.think.cloud.thinkshop.mall.controller.app.design.vo.AppIndexDesignVO;
import com.think.cloud.thinkshop.mall.controller.app.design.vo.AppIndexProductGroupInfoVO;
import com.think.cloud.thinkshop.mall.convert.design.BannerConvert;
import com.think.cloud.thinkshop.mall.convert.design.IndexDesignConvert;
import com.think.cloud.thinkshop.mall.convert.design.IndexProductGroupConvert;
import com.think.cloud.thinkshop.mall.domain.design.Banner;
import com.think.cloud.thinkshop.mall.domain.design.IndexDesign;
import com.think.cloud.thinkshop.mall.domain.design.PageConfig;
import com.think.cloud.thinkshop.mall.mapper.design.BannerMapper;
import com.think.cloud.thinkshop.mall.mapper.design.IndexDesignMapper;
import com.think.cloud.thinkshop.mall.mapper.design.IndexProductGroupDesignMapper;
import com.think.cloud.thinkshop.mall.mapper.design.PageConfigMapper;
import com.think.cloud.thinkshop.mall.service.design.AppDesignService;
import com.think.cloud.thinkshop.mall.service.uv.AppUVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * AppDesignServiceImpl
 */
@Service
public class AppDesignServiceImpl implements AppDesignService {
    public static final Integer SEARCH_STATUS = 1;

    @Autowired
    private BannerMapper bannerMapper;
    @Autowired
    private IndexDesignMapper indexDesignMapper;
    @Autowired
    private PageConfigMapper pageConfigMapper;
    @Autowired
    private IndexProductGroupDesignMapper indexProductGroupDesignMapper;
    @Autowired
    private AppUVService appUVService;

    @Override
    public AppIndexDesignVO getIndexDesign(HttpServletRequest request) {
        appUVService.record(request);
        List<IndexDesignRespVO> indexDesignRespVOS = getImageTextVideo();
        return AppIndexDesignVO.builder()
                .banners(selectBannerList(SEARCH_STATUS))
                .imageAndTexts(indexDesignRespVOS.stream().filter(x -> x.getType() != 0).collect(Collectors.toList()))
                .videos(indexDesignRespVOS.stream().filter(x -> x.getType() == 0).collect(Collectors.toList()))
                .pageConfig(selectPageConfig())
                .productGroupInfo(getIndexProductGroup())
                .build();
    }

    @Override
    public List<BannerPageRespVO> selectBannerList(Integer status) {
        return BannerConvert.INSTANCE.convertList(
                bannerMapper.selectList(Wrappers.<Banner>lambdaQuery().eq(Banner::getStatus, status))
        );
    }

    @Override
    public PageConfig selectPageConfig() {
        return pageConfigMapper.selectById(1);
    }

    @Override
    public List<IndexDesignRespVO> getImageTextVideo() {
        List<IndexDesign> indexDesigns = indexDesignMapper.selectList(
                Wrappers.<IndexDesign>lambdaQuery().eq(IndexDesign::getStatus, SEARCH_STATUS)
        );
        return IndexDesignConvert.INSTANCE.convertList(indexDesigns);
    }

    @Override
    public List<AppIndexProductGroupInfoVO> getIndexProductGroup() {
        List<IndexProductGroupDesignRespVO> productGroupList = indexProductGroupDesignMapper.getProductGroupDesignList(1);
        List<AppIndexProductGroupInfoVO> indexProductGroupInfo = IndexProductGroupConvert.INSTANCE.convertList(productGroupList);
        return indexProductGroupInfo.stream()
                .peek(appIndexProductGroupInfoVO -> {
                    Long groupId = appIndexProductGroupInfoVO.getGroupId();
                    appIndexProductGroupInfoVO.setProductDetail(indexProductGroupDesignMapper.selectProductDetailByGroupId(groupId));
                })
                .limit(4)
                .collect(Collectors.toList());
    }
}
