package com.think.cloud.thinkshop.mall.controller.app.payment;

import com.think.cloud.thinkshop.common.enums.payment.PaymentMethodEnum;
import com.think.cloud.thinkshop.mall.controller.app.payment.dto.ToPayDTO;
import com.think.cloud.thinkshop.mall.controller.app.payment.vo.PayResultVO;
import com.think.cloud.thinkshop.mall.controller.app.payment.vo.PaymentConfigVO;
import com.think.cloud.thinkshop.mall.convert.payment.PaymentConfigConvert;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentConfig;
import com.think.cloud.thinkshop.mall.service.payment.PaymentService;
import com.think.cloud.thinkshop.mall.service.payment.IPaymentConfigService;
import com.think.common.core.context.ClientHolder;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "APP:发起支付")
@Slf4j
@RequestMapping("/app/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private IPaymentConfigService paymentConfigService;

    @GetMapping("config")
    @ApiOperation(value = "支付配置")
    public AjaxResult config() {
        PaymentConfig paymentConfig = new PaymentConfig();
        paymentConfig.setStatus(Boolean.TRUE);
        List<PaymentConfig> list = paymentConfigService.selectPaymentConfigList(paymentConfig);
        List<PaymentConfigVO> resList = PaymentConfigConvert.INSTANCE.convertList(list);
        return AjaxResult.success(resList);
    }

    @PostMapping
    @ApiOperation(value = "支付下单")
    public AjaxResult toPay(@RequestBody ToPayDTO dto) {
        PayResultVO vo = null;
        String client = ClientHolder.getClientType();
        Long userId = SecurityUtils.getUserId();
        if (dto.getPayMethod().equals(PaymentMethodEnum.PAYPAL.getCode())) {
            vo = paymentService.paypalPay(dto, userId, client);
        } else {
            vo = paymentService.stripePay(dto, userId);
        }
        return AjaxResult.success(vo);
    }

    @GetMapping("paymentStatus")
    @ApiOperation(value = "支付状态查询")
    public AjaxResult paymentStatus(@RequestParam("orderId") Long orderId) {
        return AjaxResult.success(paymentService.paymentStatus(orderId));
    }
}
