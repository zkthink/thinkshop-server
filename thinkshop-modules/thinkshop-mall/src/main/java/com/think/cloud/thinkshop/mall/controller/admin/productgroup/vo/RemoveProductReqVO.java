package com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * 商品移出分组请求VO
 *
 * @author zkthink
 * @date 2024-05-13
 */
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class RemoveProductReqVO {

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private Long groupId;

    /**
     * 商品id集合
     */
    @ApiModelProperty("商品id集合")
    private List<Long> productIds;
}
