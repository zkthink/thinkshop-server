package com.think.cloud.thinkshop.mall.enums.integral;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IntegralDetailStatusEnum {

    NORMAL(1, "正常(含部分使用)"),
    FULL_USE(2, "完全使用"),
    OUT_TIME(3, "过期"),



    ;
    private int code;
    private String desc;
}
