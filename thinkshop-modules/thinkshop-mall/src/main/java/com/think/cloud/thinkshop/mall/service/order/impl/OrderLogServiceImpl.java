package com.think.cloud.thinkshop.mall.service.order.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderLogRespVO;
import com.think.cloud.thinkshop.mall.convert.order.OrderConvert;
import com.think.cloud.thinkshop.mall.enums.order.OrderOperateTypeEnum;
import com.think.cloud.thinkshop.mall.enums.order.OrderOperateUserTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.think.cloud.thinkshop.mall.mapper.order.OrderLogMapper;
import com.think.cloud.thinkshop.mall.domain.order.OrderLog;
import com.think.cloud.thinkshop.mall.service.order.IOrderLogService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 订单日志Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-23
 */
@Service
public class OrderLogServiceImpl implements IOrderLogService {

    @Autowired
    private OrderLogMapper orderLogMapper;

    /**
     * 查询订单日志
     *
     * @param id 订单日志主键
     * @return 订单日志
     */
    @Override
    public OrderLog selectOrderLogById(Long id) {
        return orderLogMapper.selectById(id);
    }

    /**
     * 查询订单日志列表
     *
     * @param orderLog 订单日志
     * @return 订单日志
     */
    @Override
    public List<OrderLog> selectOrderLogList(OrderLog orderLog) {
        return orderLogMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增订单日志
     *
     * @param orderLog 订单日志
     * @return 结果
     */
    @Override
    public int insertOrderLog(OrderLog orderLog) {
        return orderLogMapper.insert(orderLog);
    }

    /**
     * 修改订单日志
     *
     * @param orderLog 订单日志
     * @return 结果
     */
    @Override
    public int updateOrderLog(OrderLog orderLog) {
        return orderLogMapper.updateById(orderLog);
    }

    /**
     * 批量删除订单日志信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return orderLogMapper.deleteBatchIds(ids);
    }

    @Override
    public List<OrderLogRespVO> selectOrderLogListByOrderId(Long orderId) {
        List<OrderLogRespVO> logs =
                OrderConvert.INSTANCE.convertLogList(orderLogMapper.selectList(new LambdaQueryWrapper<OrderLog>()
                        .eq(OrderLog::getOrderId, orderId)
                        .orderByAsc(OrderLog::getId)
                ));
        if (CollectionUtils.isNotEmpty(logs)) {
            for (OrderLogRespVO vo : logs) {
                vo.setUserTypeName(OrderOperateUserTypeEnum.toEnum(vo.getUserType()).getDesc());
                vo.setOperateTypeName(OrderOperateTypeEnum.toEnum(vo.getOperateType()).getDesc());
            }
        }
        return logs;
    }
}
