package com.think.cloud.thinkshop.mall.mapper.shippingtemplates;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplatesProductRef;

/**
 * 物流方案商品关联Mapper接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Mapper
public interface ShippingTemplatesProductRefMapper extends BaseMapper<ShippingTemplatesProductRef> {

}
