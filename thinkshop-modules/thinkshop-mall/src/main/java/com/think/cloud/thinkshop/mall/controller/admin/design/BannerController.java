package com.think.cloud.thinkshop.mall.controller.admin.design;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerEditReqVO;
import com.think.cloud.thinkshop.mall.service.design.IBannerService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;

@RestController
@RequestMapping("/admin/banner")
@Api(tags = "ADMIN:Banner")
public class BannerController extends BaseController {
    @Autowired
    private IBannerService bannerService;

    /**
     * 查询Banner列表
     */
    @RequiresPermissions("mall:banner:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询Banner列表")
    public TableDataInfo list() {
        startPage();
        return getDataTable(bannerService.selectBannerList());
    }
    /**
     * 新增Banner
     */
    @RequiresPermissions("mall:banner:add")
    @Log(title = "Banner", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增Banner")
    public AjaxResult add(@RequestBody BannerAddReqVO vo) {
        return toAjax(bannerService.insertBanner(vo));
    }

    /**
     * 删除Banner
     */
    @RequiresPermissions("mall:banner:remove")
    @Log(title = "Banner", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除Banner")
    public AjaxResult remove(@Valid @PathVariable("id") Long id) {
        return toAjax(bannerService.batchDelete(Collections.singletonList(id)));
    }

    /**
     * 修改（编辑 & 修改状态）Banner
     */
    @RequiresPermissions("mall:banner:edit")
    @Log(title = "Banner", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改Banner")
    public AjaxResult update(@RequestBody BannerEditReqVO vo) {
        return toAjax(bannerService.updateBanner(vo));
    }

    /**
     * 获取Banner详细信息
     */
    @RequiresPermissions("mall:banner:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取Banner详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(bannerService.selectBannerById(id));
    }



//    /**
//     * 导出Banner列表
//     */
//    @RequiresPermissions("mall:banner:export")
//    @Log(title = "Banner", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ApiOperation(value = "导出Banner列表")
//    public void export(HttpServletResponse response, Banner banner)
//    {
//        List<Banner> list = bannerService.selectBannerList(banner);
//        ExcelUtil<Banner> util = new ExcelUtil<Banner>(Banner.class);
//        util.exportExcel(response, list, "Banner数据");
//    }


}
