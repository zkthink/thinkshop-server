package com.think.cloud.thinkshop.mall.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.ProductAttr;

import java.util.List;

/**
 * 商品属性Mapper接口
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Mapper
public interface ProductAttrMapper extends BaseMapper<ProductAttr> {

    void batchInsertProductAttr(List<ProductAttr> list);

    default List<ProductAttr> selectList(ProductAttr productAttr) {
        return selectList(new LambdaQueryWrapper<ProductAttr>()
                .eq(ProductAttr::getProductId, productAttr.getProductId())
                .orderByAsc(ProductAttr::getAttrId));
    }
}
