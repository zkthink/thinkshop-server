package com.think.cloud.thinkshop.mall.enums.aftersales;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 售后类型枚举
 */
@Getter
@AllArgsConstructor
public enum AfterSalesTypeEnum {

    REFUND_ONLY(0, "仅退款"),
    RETURNS_AND_REFUND(1, "退货退款");

    private Integer value;
    private String desc;


    public static AfterSalesTypeEnum toEnum(Integer value) {
        return Stream.of(AfterSalesTypeEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
