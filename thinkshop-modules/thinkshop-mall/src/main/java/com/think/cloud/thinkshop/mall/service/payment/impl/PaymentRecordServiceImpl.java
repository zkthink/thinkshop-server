package com.think.cloud.thinkshop.mall.service.payment.impl;

import java.util.List;

import com.think.cloud.thinkshop.mall.domain.payment.PaymentRecord;
import com.think.cloud.thinkshop.mall.mapper.payment.PaymentRecordMapper;
import com.think.cloud.thinkshop.mall.service.payment.IPaymentRecordService;
import com.think.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 支付记录Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Service
public class PaymentRecordServiceImpl implements IPaymentRecordService {
    @Autowired
    private PaymentRecordMapper paymentRecordMapper;

    /**
     * 查询支付记录
     *
     * @param id 支付记录主键
     * @return 支付记录
     */
    @Override
    public PaymentRecord selectPaymentRecordById(Long id) {
        return paymentRecordMapper.selectById(id);
    }

    /**
     * 查询支付记录列表
     *
     * @param paymentRecord 支付记录
     * @return 支付记录
     */
    @Override
    public List<PaymentRecord> selectPaymentRecordList(PaymentRecord paymentRecord) {
        return paymentRecordMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增支付记录
     *
     * @param paymentRecord 支付记录
     * @return 结果
     */
    @Override
    public int insertPaymentRecord(PaymentRecord paymentRecord) {

        return paymentRecordMapper.insert(paymentRecord);
    }

    /**
     * 修改支付记录
     *
     * @param paymentRecord 支付记录
     * @return 结果
     */
    @Override
    public int updatePaymentRecord(PaymentRecord paymentRecord) {
        return paymentRecordMapper.updateById(paymentRecord);
    }

    /**
     * 批量删除支付记录信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return paymentRecordMapper.deleteBatchIds(ids);
    }

    @Override
    public PaymentRecord selectByPaymentId(String paymentId) {
        return paymentRecordMapper.selectOne(new LambdaQueryWrapper<PaymentRecord>().eq(PaymentRecord::getPaymentId, paymentId));
    }
}
