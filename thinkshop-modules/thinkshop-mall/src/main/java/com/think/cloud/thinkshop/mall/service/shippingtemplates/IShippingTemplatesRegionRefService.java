package com.think.cloud.thinkshop.mall.service.shippingtemplates;

import java.util.List;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplatesRegionRef;

/**
 * 物流方案地区关联Service接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
public interface IShippingTemplatesRegionRefService
{
    /**
     * 查询物流方案地区关联
     *
     * @param id 物流方案地区关联主键
     * @return 物流方案地区关联
     */
    public ShippingTemplatesRegionRef selectShippingTemplatesRegionRefById(Long id);

    /**
     * 查询物流方案地区关联列表
     *
     * @param shippingTemplatesRegionRef 物流方案地区关联
     * @return 物流方案地区关联集合
     */
    public List<ShippingTemplatesRegionRef> selectShippingTemplatesRegionRefList(ShippingTemplatesRegionRef shippingTemplatesRegionRef);

    /**
     * 新增物流方案地区关联
     *
     * @param shippingTemplatesRegionRef 物流方案地区关联
     * @return 结果
     */
    public int insertShippingTemplatesRegionRef(ShippingTemplatesRegionRef shippingTemplatesRegionRef);

    /**
     * 修改物流方案地区关联
     *
     * @param shippingTemplatesRegionRef 物流方案地区关联
     * @return 结果
     */
    public int updateShippingTemplatesRegionRef(ShippingTemplatesRegionRef shippingTemplatesRegionRef);

    /**
     * 批量删除物流方案地区关联信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    List<String> selectRegionIdsByTemplateId(Long id);

    /**
     * 批量保存
     * @param id 方案id
     * @param regionIds 地区id集合
     * @param isAdd 是否新增
     */
    void saveBatch(Long id, List<String> regionIds, boolean isAdd);

    /**
     * 根据方案id删除
     * @param templateId 方案id
     */
    void deleteByTemplateId(Long templateId);

    ShippingTemplatesRegionRef selectByRegionId(String regionId);

    List<ShippingTemplatesRegionRef> selectByRegionIds(List<String> regionIds);
}
