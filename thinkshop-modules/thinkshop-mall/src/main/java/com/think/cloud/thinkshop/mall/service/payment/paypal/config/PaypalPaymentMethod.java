package com.think.cloud.thinkshop.mall.service.payment.paypal.config;

public enum PaypalPaymentMethod {
    credit_card, paypal
}
