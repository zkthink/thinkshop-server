package com.think.cloud.thinkshop.mall.mapper.integral;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.integral.vo.IntegralBillRecordVO;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralBill;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 积分规则Mapper接口
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@Mapper
public interface IntegralBillMapper extends BaseMapper<IntegralBill> {

    @Select("<script>" +
            "select t.*,mmu.first_name,mmu.last_name,mmu.email from mall_integral_bill t \n" +
            "left join mall_member_user mmu \n"  +
            "on t.user_id=mmu.id " +
            "<where>" +
            "   <if test=\"type != null and type != ''\">\n" +
            "     AND t.bill_type=#{type}\n" +
            "   </if>"+
            "   <if test=\"email != null and email != ''\">\n" +
            "     AND mmu.email=#{email}\n" +
            "   </if>"+
            "   <if test=\"startTime != null and endTime != null\">\n" +
            "     AND t.create_time &gt;= #{startTime} and t.create_time &lt;= #{endTime}\n" +
            "   </if>"+
            "</where> " +
            " order by t.create_time desc"+
            "</script>")
    List<IntegralBillRecordVO> page(@Param("type") Integer type,
                                    @Param("email")  String email,
                                    @Param("startTime")  String startTime,
                                    @Param("endTime")  String endTime
    );

}
