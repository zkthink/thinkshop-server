package com.think.cloud.thinkshop.mall.service.memberuser.impl;

import java.util.List;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.think.cloud.thinkshop.common.enums.payment.PaymentStatusEnum;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationLog;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationPlan;
import com.think.cloud.thinkshop.mall.mapper.memberuser.MemberOperationLogMapper;
import com.think.cloud.thinkshop.mall.service.coupon.AppProductCouponRelationService;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberOperationLogService;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberOperationPlanService;
import com.think.cloud.thinkshop.mall.service.order.AppOrderService;
import com.think.common.core.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 客户运营数据Service业务层处理
 *
 * @author zkthink
 * @date 2024-06-14
 */
@Service
@Slf4j
public class MemberOperationLogServiceImpl implements IMemberOperationLogService {
    @Autowired
    private MemberOperationLogMapper memberOperationLogMapper;
    @Autowired
    private IMemberOperationPlanService memberOperationPlanService;
    @Autowired
    private AppProductCouponRelationService appProductCouponRelationService;
    @Autowired
    private AppOrderService appOrderService;


    /**
     * 查询客户运营数据
     *
     * @param id 客户运营数据主键
     * @return 客户运营数据
     */
    @Override
    public MemberOperationLog selectMemberOperationLogById(Long id) {
        return memberOperationLogMapper.selectById(id);
    }

    /**
     * 查询客户运营数据列表
     *
     * @param memberOperationLog 客户运营数据
     * @return 客户运营数据
     */
    @Override
    public List<MemberOperationLog> selectMemberOperationLogList(MemberOperationLog memberOperationLog) {
        return memberOperationLogMapper.selectList(new LambdaQueryWrapper<MemberOperationLog>().eq(MemberOperationLog::getPlanId, memberOperationLog.getPlanId()).orderByDesc(MemberOperationLog::getCreateTime));
    }

    /**
     * 新增客户运营数据
     *
     * @param memberOperationLog 客户运营数据
     * @return 结果
     */
    @Override
    public int insertMemberOperationLog(MemberOperationLog memberOperationLog) {

        return memberOperationLogMapper.insert(memberOperationLog);
    }

    /**
     * 修改客户运营数据
     *
     * @param memberOperationLog 客户运营数据
     * @return 结果
     */
    @Override
    public int updateMemberOperationLog(MemberOperationLog memberOperationLog) {
        return memberOperationLogMapper.updateById(memberOperationLog);
    }

    /**
     * 批量删除客户运营数据信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return memberOperationLogMapper.deleteBatchIds(ids);
    }

    @Override
    public void run() {
        DateTime yesterday = DateUtil.yesterday();

        //查询进行中,或者今日结束的运营计划
        List<MemberOperationPlan> list = memberOperationPlanService.selectPendingList(yesterday);
        log.info("查询到{}条进行中,或者今日结束的运营计划", list.size());

        String dateStr = DateUtil.formatDate(yesterday);
        DateTime start = DateUtil.beginOfDay(yesterday);
        DateTime end = DateUtil.endOfDay(yesterday);
        List<MemberOperationLog> logList = ListUtil.list(false);
        list.parallelStream().forEach(bean -> {
            Long planId = bean.getId();
            MemberOperationLog log = new MemberOperationLog();
            log.setPlanId(planId);
            log.setDateStr(dateStr);
            log.setCouponUserCount(appProductCouponRelationService.getCouponUserCount(planId,start,end));

            log.setOrderUserCount(appOrderService.getOrderUserCount(planId,start,end,null));
            log.setOrderCount(appOrderService.getOrderCount(planId,start,end,null));
            log.setOrderAmount(appOrderService.getOrderAmount(planId,start,end,null));
            log.setPayUserCount(appOrderService.getOrderUserCount(planId,start,end, PaymentStatusEnum.PAYMENT_STATUS_PAID.getCode()));
            log.setPayOrderCount(appOrderService.getOrderCount(planId,start,end, PaymentStatusEnum.PAYMENT_STATUS_PAID.getCode()));
            logList.add(log);
        });
        Db.saveBatch(logList);
    }
}
