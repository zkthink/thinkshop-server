package com.think.cloud.thinkshop.mall.controller.app.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * App优惠券 Base VO，提供给添加、修改、详细的子 VO 使用
 * 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
 */
@Data
public class AppCouponBaseVO {

    @ApiModelProperty("优惠券id")
    private Long id;

    @ApiModelProperty("优惠券名称")
    private String couponName;

    @ApiModelProperty("优惠券面值")
    private BigDecimal couponValue;

    @ApiModelProperty("优惠券类型：1、满减券，2、折扣券")
    private Integer couponType;

    @ApiModelProperty("满减门槛")
    private BigDecimal threshold;

    @ApiModelProperty("折扣")
    private BigDecimal discount;

    @ApiModelProperty("优惠券范围：1、所有商品，2、选中商品")
    private Integer couponScope;

    @ApiModelProperty("范围值")
    private String scopeValues;

    @ApiModelProperty("优惠券数量")
    private Long number;

    @ApiModelProperty("生效时间")
    private Timestamp takingEffectTime;

    @ApiModelProperty("过期时间")
    private Timestamp expirationTime;

    @ApiModelProperty("领取限制类型 1 无限制 2限制次数")
    private Integer receiveType;

    @ApiModelProperty("限制每人领取数量")
    private Long limitNumber;

    @ApiModelProperty("过期类型：1、按时间，2、按天数")
    private Integer expirationType;

    @ApiModelProperty("过期天数")
    private Long expirationDay;

    @ApiModelProperty("备注")
    private String remark;


}
