package com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.vo;

import com.think.cloud.thinkshop.mall.domain.Product;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class ShippingTemplatesVO {
    /** ID */
    private Long id;

    /** 方案名称 */
    @ApiModelProperty("方案名称")
    private String name;

    /** 方案介绍 */
    @ApiModelProperty("方案介绍")
    private String desc;

    /** 计费方式 */
    @ApiModelProperty("计费方式")
    private Integer chargingType;

    /** 首件/首重 */
    @ApiModelProperty("首件/首重")
    private BigDecimal firstPiece;

    /** 运费 */
    @ApiModelProperty("运费")
    private BigDecimal firstPrice;

    /** 续件 */
    @ApiModelProperty("续件")
    private BigDecimal otherPiece;

    /** 续费 */
    @ApiModelProperty("续费")
    private BigDecimal otherPrice;

    /** 固定运费 */
    @ApiModelProperty("固定运费")
    private BigDecimal fixedPrice;

    /** 开启免运费 */
    @ApiModelProperty("开启免运费")
    private Boolean enableFree;

    /** 订单金额大于xx */
    @ApiModelProperty("订单金额大于xx")
    private BigDecimal orderAmount;

    /** 订单数量大于xx */
    @ApiModelProperty("订单数量大于xx")
    private Long orderItemCount;

    /** 是否默认方案 */
    @ApiModelProperty("是否默认方案")
    private Boolean isDefault;

    /** 是否删除 */
    @ApiModelProperty("是否删除")
    private Integer deleted;

    @ApiModelProperty("商品ID")
    private List<Long> productIds;

    @ApiModelProperty("地区ID")
    private List<String> regionIds;

    @ApiModelProperty("商品列表")
    private List<Product> productList;
}
