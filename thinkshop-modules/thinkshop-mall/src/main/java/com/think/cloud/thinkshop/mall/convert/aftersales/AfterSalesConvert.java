package com.think.cloud.thinkshop.mall.convert.aftersales;

import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesInfoRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesLogRespVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesLogRespVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesSendReqVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppApplyAfterSalesReqVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesInfoRespVO;
import com.think.cloud.thinkshop.mall.domain.aftersales.AfterSales;
import com.think.cloud.thinkshop.mall.domain.aftersales.AfterSalesLog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 售后 Convert
 *
 * @author zkthink
 */
@Mapper
public interface AfterSalesConvert {

    AfterSalesConvert INSTANCE = Mappers.getMapper(AfterSalesConvert.class);

    AfterSales convert(AppApplyAfterSalesReqVO vo);

    AppAfterSalesInfoRespVO convert(AfterSales bean);

    @Mappings({
            @Mapping(source = "number", target = "refundNumber"),
    })
    AfterSalesInfoRespVO convert1(AfterSales bean);

    List<AppAfterSalesLogRespVO> convertList(List<AfterSalesLog> list);

    AfterSales convert(AppAfterSalesSendReqVO vo);


    @Mappings({
            @Mapping(source = "createBy", target = "userName"),
    })
    AfterSalesLogRespVO convert(AfterSalesLog log);

    List<AfterSalesLogRespVO> convertLogList(List<AfterSalesLog> list);
}
