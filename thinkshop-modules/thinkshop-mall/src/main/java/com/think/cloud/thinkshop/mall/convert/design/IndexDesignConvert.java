package com.think.cloud.thinkshop.mall.convert.design;


import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignRespVO;
import com.think.cloud.thinkshop.mall.domain.design.IndexDesign;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * pageConfig Convert
 *
 * @author zkthink
 */
@Mapper
public interface IndexDesignConvert {

    IndexDesignConvert INSTANCE = Mappers.getMapper(IndexDesignConvert.class);

    IndexDesign convert(IndexDesignAddReqVO dto);
    IndexDesignRespVO convert(IndexDesign dto);
    IndexDesign convert(IndexDesignEditReqVO dto);

    List<IndexDesignRespVO> convertList(List<IndexDesign> dto);

}
