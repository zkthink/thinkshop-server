package com.think.cloud.thinkshop.mall.convert.message;


import com.think.cloud.thinkshop.mall.controller.admin.message.dto.MessageTemplateDTO;
import com.think.cloud.thinkshop.mall.domain.message.MessageTemplate;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
@Mapper
public interface MessageTemplateConvert {

    MessageTemplateConvert INSTANCE = Mappers.getMapper(MessageTemplateConvert.class);

    MessageTemplateDTO convert(MessageTemplate dto);

}
