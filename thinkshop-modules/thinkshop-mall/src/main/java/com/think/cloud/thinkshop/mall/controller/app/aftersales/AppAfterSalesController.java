package com.think.cloud.thinkshop.mall.controller.app.aftersales;


import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesPageReqVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesSendReqVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppApplyAfterSalesReqVO;
import com.think.cloud.thinkshop.mall.service.aftersales.AppAfterSalesService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * App售后记录Controller
 *
 * @author zkthink
 * @date 2024-06-12
 */
@RestController
@RequestMapping("/app/sales")
@Api(tags = "APP:售后记录")
public class AppAfterSalesController extends BaseController {


    @Autowired
    private AppAfterSalesService appAfterSalesService;

    /**
     * 售后单列表
     */
    @GetMapping("/list")
    public TableDataInfo list(AppAfterSalesPageReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        startPage();
        return appAfterSalesService.list(vo);
    }

    /**
     * 售后申请
     */
    @PostMapping("/apply")
    public AjaxResult add(@RequestBody AppApplyAfterSalesReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        return success(appAfterSalesService.apply(vo));
    }

    /**
     * 售后详情
     */
    @GetMapping("/get/{id}")
    public AjaxResult get(@PathVariable("id") Long id) {
        return success(appAfterSalesService.get(id));
    }

    /**
     * 发货
     */
    @PostMapping("/send")
    public AjaxResult send(@RequestBody AppAfterSalesSendReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        appAfterSalesService.send(vo);
        return success();
    }

    /**
     * 取消售后
     */
    @GetMapping("/cancel/{id}")
    public AjaxResult cancel(@PathVariable("id") Long id) {
        appAfterSalesService.cancel(id, SecurityUtils.getUserId());
        return success();
    }

    /**
     * 删除售后(用户侧)
     */
    @DeleteMapping("/delete/{id}")
    public AjaxResult remove(@PathVariable("id") Long id) {
        appAfterSalesService.remove(id, SecurityUtils.getUserId());
        return success();
    }

}
