package com.think.cloud.thinkshop.mall.controller.admin.category.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;


@Data
@ToString(callSuper = true)
public class ProductRelationCategoryReqVO {

    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;

    /**
     * 商品id集合
     */
    @ApiModelProperty("商品id集合")
    private List<Long> productIds;

}
