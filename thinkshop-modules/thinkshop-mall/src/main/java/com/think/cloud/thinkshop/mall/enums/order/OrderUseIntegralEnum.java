package com.think.cloud.thinkshop.mall.enums.order;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderUseIntegralEnum {
    NO_USE(0, "不使用"),
    USE(1, "使用"),
    ;
    private int code;
    private String desc;
}
