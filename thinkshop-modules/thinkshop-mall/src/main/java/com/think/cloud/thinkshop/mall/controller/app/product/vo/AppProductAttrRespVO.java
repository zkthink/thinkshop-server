package com.think.cloud.thinkshop.mall.controller.app.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * App商品属性响应VO
 *
 * @author zkthink
 * @date 2024-05-16
 */

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppProductAttrRespVO {


    /** id */
    @ApiModelProperty("id")
    private Long attrId;

    /** 商品ID */
    @ApiModelProperty("商品ID")
    private Long productId;

    /** 属性名 */
    @ApiModelProperty("属性名")
    private String attrName;

    /** 属性值 */
    @ApiModelProperty("属性值")
    private String attrValues;

    /** 属性值集合 */
    @ApiModelProperty("属性值集合")
    private List<String> attrValueArr;

}
