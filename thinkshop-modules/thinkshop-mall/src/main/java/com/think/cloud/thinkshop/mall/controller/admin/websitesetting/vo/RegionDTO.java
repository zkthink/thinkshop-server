package com.think.cloud.thinkshop.mall.controller.admin.websitesetting.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class RegionDTO {

    private String label;

    private String value;

    private List<RegionDTO> children;
}
