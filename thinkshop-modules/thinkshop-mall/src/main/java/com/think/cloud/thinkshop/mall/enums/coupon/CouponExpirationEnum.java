package com.think.cloud.thinkshop.mall.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 优惠券过期类型枚举
 */
@Getter
@AllArgsConstructor
public enum CouponExpirationEnum {
    BY_TIME(1, "按时间"),
    BY_DAY(2, "按天数");

    private Integer value;
    private String desc;
}
