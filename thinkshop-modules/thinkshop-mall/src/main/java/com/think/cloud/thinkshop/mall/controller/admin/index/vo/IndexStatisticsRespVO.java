package com.think.cloud.thinkshop.mall.controller.admin.index.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Data
@Schema(description = "首页-统计结果")
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IndexStatisticsRespVO {
    @ApiModelProperty(value = "订单笔数")
    private BigDecimal ordersCount =BigDecimal.ZERO;
    @ApiModelProperty(value = "订单笔数趋势")
    private List<BigDecimal> ordersCountTrend;

    @ApiModelProperty(value = "营收总额")
    private BigDecimal turnoverTotal = BigDecimal.ZERO;
    @ApiModelProperty(value = "营收总额趋势")
    private List<BigDecimal> turnoverTotalTrend;

    @ApiModelProperty(value = "售后订单数")
    private BigDecimal saledOrdersCount = BigDecimal.ZERO;
    @ApiModelProperty(value = "售后订单数趋势")
    private List<BigDecimal> saledOrdersCountTrend;

    @ApiModelProperty(value = "售后订单总额")
    private BigDecimal saledOrdersTotal = BigDecimal.ZERO;
    @ApiModelProperty(value = "售后订单总额趋势")
    private List<BigDecimal> saledOrdersTotalTrend;

    @ApiModelProperty(value = "客户总数")
    private Long memberCount = 0L;

    @ApiModelProperty(value = "新增客户数")
    private BigDecimal newMemberCount = BigDecimal.ZERO;
    @ApiModelProperty(value = "新增客户数趋势")
    private List<BigDecimal> newMemberCountTrend;

    @ApiModelProperty(value = "订单转化率")
    private String orderConvertRate = "0%";

    @ApiModelProperty(value = "付款转化率")
    private String payConvertRate = "0%";

    @ApiModelProperty(value = "趋势日期范围")
    private List<String> trendDateRange;




}
