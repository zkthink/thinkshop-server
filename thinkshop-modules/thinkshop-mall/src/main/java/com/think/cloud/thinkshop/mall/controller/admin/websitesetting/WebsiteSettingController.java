package com.think.cloud.thinkshop.mall.controller.admin.websitesetting;

import com.think.cloud.thinkshop.common.constant.DictConstant;
import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto.WebsiteSettingDTO;
import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.vo.RegionDTO;
import com.think.cloud.thinkshop.mall.service.websitesetting.IWebsiteSettingService;
import com.think.cloud.thinkshop.system.api.domain.SysDictData;
import com.think.cloud.thinkshop.system.api.utils.DictUtils;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 网站设置Controller
 *
 * @author zkthink
 * @date 2024-05-14
 */
@RestController
@RequestMapping("/admin")
@Api(tags = "ADMIN:网站设置")
public class WebsiteSettingController extends BaseController {
    @Autowired
    private IWebsiteSettingService websiteSettingService;

    /**
     * 获取网站设置详细信息
     */
    @RequiresPermissions("mall:websitesetting:query")
    @GetMapping("/websitesetting")
    @ApiOperation("获取网站设置详细信息")
    public AjaxResult getInfo() {
        return success(websiteSettingService.getInfo());
    }

    /**
     * 保存网站设置
     */
    @RequiresPermissions("mall:websitesetting:edit")
    @Log(title = "网站设置", businessType = BusinessType.INSERT)
    @PostMapping("/websitesetting")
    @ApiOperation("保存网站设置")
    public AjaxResult save(@RequestBody WebsiteSettingDTO websiteSetting) {
        return toAjax(websiteSettingService.save(websiteSetting));
    }


    @GetMapping("regionList")
    @ApiOperation("查询地区列表")
    public AjaxResult regionList() {
        List<SysDictData> nationList = DictUtils.getDictCache(DictConstant.SYS_NATION);

        List<RegionDTO> collect = nationList.stream().map(nation -> {
            RegionDTO regionDTO = getRegionDTO(nation);
            List<RegionDTO> children = DictUtils.getDictCache(nation.getDictValue()).stream().map(this::getRegionDTO).collect(Collectors.toList());
            regionDTO.setChildren(children);
            return regionDTO;
        }).collect(Collectors.toList());
        return success(collect);
    }

    private RegionDTO getRegionDTO(SysDictData nation) {
        RegionDTO regionDTO = new RegionDTO();
        regionDTO.setLabel(nation.getDictLabel());
        regionDTO.setValue(String.valueOf(nation.getDictCode()));
        return regionDTO;
    }


    @GetMapping("/zoneTime")
    public AjaxResult zoneTime() {
        return success(websiteSettingService.getZoneTimeInfo());
    }

}
