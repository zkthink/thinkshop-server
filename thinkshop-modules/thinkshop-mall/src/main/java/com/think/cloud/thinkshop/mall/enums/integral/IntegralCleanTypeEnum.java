package com.think.cloud.thinkshop.mall.enums.integral;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IntegralCleanTypeEnum {
    NO(0, "不清空"),
    YEAR(1, "年"),
    QUARTER(2, "季度"),
    MONTH(3, "月"),


    ;
    private int code;
    private String desc;
}
