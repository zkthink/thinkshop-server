package com.think.cloud.thinkshop.mall.enums.order;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 订单操作类型枚举
 */
@Getter
@AllArgsConstructor
public enum OrderOperateTypeEnum {

    SUBMIT_ORDER(1, "提交订单"),
    CANCEL_ORDER(2, "取消订单"),
    AUTO_CANCEL_ORDER_BY_TIMEOUT(3, "超时自动取消订单"),
    COMPLETE_PAYMENT(4, "完成支付"),
    SHIP_BY_MERCHANT(5, "商家发货"),
    CONFIRM_RECEIPT(6, "确认收货"),
    AUTO_CONFIRM_RECEIPT_BY_TIMEOUT(7, "超时自动确认收货");

    private Integer value;
    private String desc;


    public static OrderOperateTypeEnum toEnum(Integer value) {
        return Stream.of(OrderOperateTypeEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
