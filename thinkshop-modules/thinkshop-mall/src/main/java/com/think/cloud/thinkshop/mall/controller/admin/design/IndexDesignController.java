package com.think.cloud.thinkshop.mall.controller.admin.design;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignEditReqVO;
import com.think.cloud.thinkshop.mall.service.design.IIndexDesignService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/admin/indexDesign")
@Api(tags = "ADMIN:首页图文&视频配置")
public class IndexDesignController extends BaseController {
    @Autowired
    private IIndexDesignService indexDesignService;

    /**
     * 查询图文 or 视频配置
     */
    @RequiresPermissions("mall:design:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询图文 or 视频配置")
    public TableDataInfo list(@ApiParam(value = "查询类型 ，0:视频，1：图文",required = true) @RequestParam Integer type) {
        startPage();
        return getDataTable(indexDesignService.getIndexDesign(type));
    }
    /**
     * 新增 图文、视频
     */
    @RequiresPermissions("mall:design:add")
    @Log(title = "indexDesign", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增图文、视频")
    public AjaxResult add(@RequestBody IndexDesignAddReqVO vo) {
        return toAjax(indexDesignService.insetIndexDesign(vo));
    }

    /**
     * 图文删除
     */
    @RequiresPermissions("mall:design:remove")
    @Log(title = "indexDesign", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除图文")
    public AjaxResult remove(@Valid @PathVariable("id") Long id) {
        return toAjax(indexDesignService.delete(id));
    }

    /**
     * 修改（编辑 & 修改状态）
     */
    @RequiresPermissions("mall:design:edit")
    @Log(title = "indexDesign", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改文图、视频")
    public AjaxResult update(@RequestBody IndexDesignEditReqVO vo) {
        return toAjax(indexDesignService.updateIndexDesign(vo));
    }

    /**
     * 获取文图、视频 详细信息
     */
    @RequiresPermissions("mall:design:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取图文、视频的详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(indexDesignService.selectById(id));
    }
}
