package com.think.cloud.thinkshop.mall.service.product.dto;

import lombok.*;

import java.math.BigDecimal;

/**
 * 商品计算结果
 *
 * @author zkthink
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResultDTO {

    /**
     * 最小价格
     */
    private BigDecimal minPrice;

    /**
     * 最大价格
     */
    private BigDecimal maxPrice;

    /**
     * 库存
     */
    private Integer stock;
}
