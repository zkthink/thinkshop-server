package com.think.cloud.thinkshop.mall.domain;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import java.sql.Timestamp;

/**
 * 商品类目对象 mall_product_category
 *
 * @author zkthink
 * @date 2024-05-08
 */
@TableName("mall_product_category")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductCategory
{
    private static final long serialVersionUID = 1L;

    /** 类目id */
    @TableId(type = IdType.AUTO)
    private Long categoryId;

    /** 类目级别 */
    @Excel(name = "类目级别")
    private Long level;

    /** 父类目id */
    @Excel(name = "父类目id")
    private Long parentCategoryId;

    /** 类目名称 */
    @Excel(name = "类目名称")
    private String name;

    /** 类目图片 */
    @Excel(name = "类目图片")
    private String pictureUrl;

    /** 权重 */
    @Excel(name = "权重")
    private Long weight;

    /** 是否删除 */
    @Excel(name = "是否删除")
    @TableLogic
    private Integer deleted;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Timestamp createTime;

    /** 修改人 */
    @Excel(name = "修改人")
    private String updateBy;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private Timestamp updateTime;

}
