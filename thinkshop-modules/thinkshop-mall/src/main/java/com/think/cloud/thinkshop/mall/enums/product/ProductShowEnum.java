package com.think.cloud.thinkshop.mall.enums.product;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 上架状态枚举
 */
@Getter
@AllArgsConstructor
public enum ProductShowEnum {

    NO(0, "未上架"),
    YES(1, "上架");

    private Integer value;
    private String desc;

}
