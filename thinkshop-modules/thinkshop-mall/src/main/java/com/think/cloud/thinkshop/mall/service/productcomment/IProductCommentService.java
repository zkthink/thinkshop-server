package com.think.cloud.thinkshop.mall.service.productcomment;


import com.think.cloud.thinkshop.mall.controller.admin.productcomment.param.ProductCommentAddParam;
import com.think.cloud.thinkshop.mall.controller.admin.productcomment.vo.ProductCommentVO;
import com.think.cloud.thinkshop.mall.domain.ProductComment;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;

/**
 * 商品评价Service接口
 *
 * @author moxiangrong
 * @date 2024-07-12
 */
public interface IProductCommentService {
    /**
     * 查询商品评价
     *
     * @param id 商品评价主键
     * @return 商品评价
     */
    public ProductComment selectProductCommentById(Long id);

    /**
     * 查询商品评价列表
     *
     * @return 商品评价集合
     */
    public TableDataInfo selectProductCommentList();

    /**
     * 构建商品的评论list
     * @param list  评论
     * @param userHidden  是否用户隐藏
     * @param showProduct 是否展示商品
     * @param showOrder
     * @return
     */
    public List<ProductCommentVO>  viewProductCommentList(List<ProductComment> list,boolean userHidden,
                                                          boolean showProduct,boolean showOrder);

    /**
     * 新增商品评价
     *
     * @param param 商品评价
     * @return 结果
     */
    public int insertProductComment(ProductCommentAddParam param);

    /**
     * 修改商品评价
     *
     * @param productComment 商品评价
     * @return 结果
     */
    public int updateProductComment(ProductComment productComment);

    /**
     * 修改评论状态
     * @param id 主键
     * @param status  状态:0-不展示 ，1-展示
     * @return
     */
    public int updateProductCommentStatus(Long id,Integer status);
    /**
     * 批量删除商品评价信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);
}
