package com.think.cloud.thinkshop.mall.mapper.design;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.design.PageConfig;
import org.apache.ibatis.annotations.Mapper;


/**
 * BannerMapper接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Mapper
public interface PageConfigMapper extends BaseMapper<PageConfig> {

}

