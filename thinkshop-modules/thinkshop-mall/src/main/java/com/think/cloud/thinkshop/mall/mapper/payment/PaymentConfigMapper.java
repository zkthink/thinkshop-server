package com.think.cloud.thinkshop.mall.mapper.payment;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentConfig;

/**
 * 支付配置Mapper接口
 *
 * @author zkthink
 * @date 2024-05-14
 */
@Mapper
public interface PaymentConfigMapper extends BaseMapper<PaymentConfig> {

}
