package com.think.cloud.thinkshop.mall.enums.group;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 分组智能筛选枚举
 */
@Getter
@AllArgsConstructor
public enum IntelligentGroupTypeEnum {
    TYPE_STOCK(0,"mp.stock"),
    TYPE_PRICE(1,"mpav.price"),
    TYPE_WEIGHT(2,"mpav.weight"),
    TYPE_SALES(3,"mp.sales");

    private Integer type;
    private String value;


    public static IntelligentGroupTypeEnum toType(int type) {
        return Stream.of(IntelligentGroupTypeEnum.values())
                .filter(p -> p.type == type)
                .findAny()
                .orElse(null);
    }


}
