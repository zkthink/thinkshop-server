package com.think.cloud.thinkshop.mall.service.coupon;

import cn.hutool.core.date.DateTime;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.AppUserCouponRespVO;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.CartCouponRespVO;
import com.think.cloud.thinkshop.mall.controller.app.coupon.dto.CartCouponDTO;
import com.think.cloud.thinkshop.mall.domain.coupon.ProductCouponRelation;

import java.util.List;

/**
 * 商品优惠券关联AppService接口
 *
 * @author zkthink
 * @date 2024-05-22
 */
public interface AppProductCouponRelationService {

    /**
     * 领取优惠券
     *
     * @param id     优惠券id
     * @param userId 用户id
     * @param num 领取优惠券数量
     * @param planId 运营计划id
     */
    void receiveCoupon(Long id, Long userId,Integer num ,Long planId);

    /**
     * 兑换优惠券
     *
     * @param couponCode     优惠券编号
     * @param userId
     * @return
     */
    void redeem(String couponCode, Long userId);

    /**
     * 核销优惠券
     *
     * @param id 优惠券明细id
     * @return
     */
    void verificationCoupon(Long id);

    /**
     * 回退优惠券
     *
     * @param id 优惠券明细id
     * @return
     */
    void returnCoupon(Long id);

    /**
     * 查询用户已有优惠券
     *
     * @param type 查询类型
     * @param id   优惠券id
     * @param userId   用户id
     * @return
     */
    List<AppUserCouponRespVO> searchUserCoupon(Integer type, Long id, Long userId);

    /**
     * 查询购物车可用优惠券
     *
     * @param cartCouponDtoList 购物车信息
     * @return
     */
    List<CartCouponRespVO> searchCartCoupon(List<CartCouponDTO> cartCouponDtoList, Long id, Long userId);

    /**
     * 获取优惠券可用商品id合集
     *
     * @param id 优惠券明细id
     * @return
     */
    List<Long> getProductIdList(Long id);

    /**
     * 获取优惠券数量
     *
     * @param userId 用户id
     */
    Integer getCouponNumber(Long userId);

    /**
     * 获取已领取优惠券数量
     *
     * @param id  优惠券id
     * @param uid 用户id
     * @return /
     */
    long getReceivedCount(Long id, Long uid);

    /**
     * 获取已领取优惠券数量
     *
     * @param planId 运营计划id
     * @param start  开始时间
     * @param end    结束时间
     * @return /
     */
    Long getCouponUserCount(Long planId, DateTime start, DateTime end);

    /**
     * 获取优惠券关系数据
     * @param id
     * @return
     */
    ProductCouponRelation getById(Long id);
}
