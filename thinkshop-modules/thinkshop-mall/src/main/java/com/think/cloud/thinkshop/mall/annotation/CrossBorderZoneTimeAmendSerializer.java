package com.think.cloud.thinkshop.mall.annotation;

import cn.hutool.core.date.DateUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.think.cloud.thinkshop.mall.service.websitesetting.IWebsiteSettingService;
import com.think.common.core.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;
@Slf4j
@JsonComponent
public class CrossBorderZoneTimeAmendSerializer extends JsonSerializer<Object>  implements ContextualSerializer {
    private DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    @Override
    public void serialize(Object object, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
       //系统时区-配置时区 的差值
        IWebsiteSettingService bean = SpringUtil.getBean(IWebsiteSettingService.class);
        int i = bean.zoneTimeGap();
        Date date = (Date) object;
        String out = DateUtil.format(DateUtils.addHours(date, -i), format);
//        log.info("------------原本时间：{} ,时间差：{} ,out:{}----------",date,i,out);
        jsonGenerator.writeString(out);
    }

    /**
     * createContextual可以获得字段的类型以及注解。
     * createContextual方法只会在第一次序列化字段时调用（因为字段的上下文信息在运行期不会改变），所以不用担心影响性能。
     *
     * @param serializerProvider
     * @param beanProperty
     * @return
     * @throws JsonMappingException
     */
    @Override
    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) throws JsonMappingException {
        //当前字段不为空
        if (beanProperty != null) {
            //如果当前字段类型为BigDecimal类型则处理,否则跳过
            if (Objects.equals(beanProperty.getType().getRawClass(), Date.class) ||
                    Objects.equals(beanProperty.getType().getRawClass(), Timestamp.class)
            ) {
                //获取当前字段的自定义注解
                CrossBorderZoneTimeAmend annotation = beanProperty.getAnnotation(CrossBorderZoneTimeAmend.class);
                //没有打自定义注解,获取上下文中的自定义注解
                if (annotation == null) {
                    annotation = beanProperty.getContextAnnotation(CrossBorderZoneTimeAmend.class);
                }
                CrossBorderZoneTimeAmendSerializer crossBorderZoneBuild = new CrossBorderZoneTimeAmendSerializer();
                if (annotation != null) {
                    if (!annotation.pattern().isEmpty()) {
                        crossBorderZoneBuild.format = DateTimeFormatter.ofPattern(annotation.pattern());;
                    }
                }
                return crossBorderZoneBuild;
            }
            return serializerProvider.findContentValueSerializer(beanProperty.getType(), beanProperty);
        }
        return serializerProvider.findNullValueSerializer(beanProperty);
    }
}
