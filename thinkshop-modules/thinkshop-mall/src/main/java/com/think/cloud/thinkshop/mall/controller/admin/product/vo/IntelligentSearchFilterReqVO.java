package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 智能查找过滤条件请求VO
 *
 * @author zkthink
 * @date 2024-05-11
 */
@Data
public class IntelligentSearchFilterReqVO {

    /**
     * 过滤类型
     */
    @ApiModelProperty("过滤类型")
    private Integer type;

    /**
     * 条件
     */
    @ApiModelProperty("条件")
    private Integer condition;

    /**
     * 过滤值
     */
    @ApiModelProperty("过滤值")
    private Object value;
}
