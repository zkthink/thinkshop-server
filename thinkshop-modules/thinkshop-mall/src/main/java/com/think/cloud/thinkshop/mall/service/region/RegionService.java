package com.think.cloud.thinkshop.mall.service.region;

import com.think.cloud.thinkshop.mall.controller.app.region.vo.AppRegionRespVO;

import java.util.List;

public interface RegionService {

    List<AppRegionRespVO> getRegion(String keyword);
}
