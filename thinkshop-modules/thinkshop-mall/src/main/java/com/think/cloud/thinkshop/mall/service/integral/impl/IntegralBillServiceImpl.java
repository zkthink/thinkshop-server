package com.think.cloud.thinkshop.mall.service.integral.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.controller.admin.integral.param.IntegralBillSearchParam;
import com.think.cloud.thinkshop.mall.controller.admin.integral.vo.IntegralBillRecordVO;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralBill;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralDetail;
import com.think.cloud.thinkshop.mall.enums.integral.IntegralDetailStatusEnum;
import com.think.cloud.thinkshop.mall.mapper.integral.IntegralBillMapper;
import com.think.cloud.thinkshop.mall.mapper.integral.IntegralDetailMapper;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralBillService;
import com.think.common.core.utils.PageUtils;
import com.think.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 积分流水Service业务层处理
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@Service
public class IntegralBillServiceImpl implements IIntegralBillService {
    @Autowired
    private IntegralBillMapper integralBillMapper;

    @Autowired
    private IntegralDetailMapper integralDetailMapper;

    /**
     * 查询积分流水
     *
     * @param id 积分流水主键
     * @return 积分流水
     */
    @Override
    public IntegralBill selectIntegralBillById(Long id) {
        return integralBillMapper.selectById(id);
    }

    /**
     * 查询积分流水列表
     *
     * @param integralBill 积分流水
     * @return 积分流水
     */
    @Override
    public List<IntegralBill> selectIntegralBillList(IntegralBill integralBill) {
        return integralBillMapper.selectList(new LambdaQueryWrapper<>());
    }

    @Override
    public List<IntegralBill> userIntegralBillList(Long userId) {
        return integralBillMapper.selectList(Wrappers.<IntegralBill>lambdaQuery()
                .eq(IntegralBill::getUserId, userId)
                .orderByDesc(IntegralBill::getCreateTime)
        );
    }

    @Override
    public TableDataInfo page(IntegralBillSearchParam param) {
        List<IntegralBillRecordVO> page = integralBillMapper.page(
                param.getType(), param.getEmail(),
                param.getStartTime(), param.getEndTime());
//        page.stream()
//                .peek(item -> item.setUserIntegral(this.getUserCurrentIntegral(item.getUserId())))
//                .collect(Collectors.toList());
        // 有个分页bug,如果 page=page.stream..重新赋值，PageUtils.getTotal(page) 计算的total会出错。。
        return new TableDataInfo(page, PageUtils.getTotal(page));
    }

    private int getUserCurrentIntegral(Long userId) {
        List<IntegralDetail> integralDetails = integralDetailMapper.selectList(
                Wrappers.<IntegralDetail>lambdaQuery()
                        .eq(userId != null, IntegralDetail::getUserId, userId)
                        .eq(IntegralDetail::getStatus, IntegralDetailStatusEnum.NORMAL.getCode())
        );
        if (integralDetails == null || integralDetails.isEmpty()) {
            return 0;
        }
        return integralDetails.stream().mapToInt(IntegralDetail::getRemainIntegral).sum();
    }

    /**
     * 新增积分流水
     *
     * @param integralBill 积分流水
     * @return 结果
     */
    @Override
    public int insertIntegralBill(IntegralBill integralBill) {
        return integralBillMapper.insert(integralBill);
    }

    /**
     * 修改积分流水
     *
     * @param integralBill 积分流水
     * @return 结果
     */
    @Override
    public int updateIntegralBill(IntegralBill integralBill) {
//        integralBill.setUpdateTime(DateUtils.getNowDate());
        return integralBillMapper.updateById(integralBill);
    }

    /**
     * 批量删除积分流水信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return integralBillMapper.deleteBatchIds(ids);
    }
}
