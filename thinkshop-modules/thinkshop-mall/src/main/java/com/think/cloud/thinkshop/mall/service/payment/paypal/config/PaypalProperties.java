package com.think.cloud.thinkshop.mall.service.payment.paypal.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zkthink
 * @apiNote
 **/
@ConfigurationProperties("pay.paypal")
@Data
public class PaypalProperties {
    /**
     * paypal连接环境：live表示生产，sandbox表示沙盒
     */
    private String mode;

    /**
     * paypal_success_url 支付成功 回调url
     */
    private String paypalSuccessUrl;
    /**
     * paypal_cancel_url 支付取消 回调url
     */
    private String paypalCancelUrl;
}
