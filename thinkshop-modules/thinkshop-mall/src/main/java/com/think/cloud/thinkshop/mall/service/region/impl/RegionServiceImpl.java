package com.think.cloud.thinkshop.mall.service.region.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.think.cloud.thinkshop.common.constant.DictConstant;
import com.think.cloud.thinkshop.mall.controller.app.region.vo.AppRegionRespVO;
import com.think.cloud.thinkshop.mall.service.region.RegionService;
import com.think.cloud.thinkshop.system.api.domain.SysDictData;
import com.think.cloud.thinkshop.system.api.utils.DictUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RegionServiceImpl implements RegionService {
    @Override
    public List<AppRegionRespVO> getRegion(String keyword) {
        List<SysDictData> nationList = DictUtils.getDictCache(DictConstant.SYS_NATION);
        List<AppRegionRespVO> collect = nationList.stream().map(nation -> {
            AppRegionRespVO region = getRegion(nation);
            List<AppRegionRespVO> children =
                    DictUtils.getDictCache(nation.getDictValue()).stream()
                            .filter(child -> StrUtil.isNotBlank(keyword) && (child.getDictLabel().contains(keyword) || child.getDictValue().contains(keyword)))
                            .map(this::getRegion)
                            .collect(Collectors.toList());
            region.setChildren(children);
            return region;
        })
                .filter(nation -> CollectionUtil.isNotEmpty(nation.getChildren()))
                .collect(Collectors.toList());
        return collect;
    }

    private AppRegionRespVO getRegion(SysDictData nation) {
        AppRegionRespVO region = new AppRegionRespVO();
        region.setRegionId(nation.getDictCode());
        region.setName(nation.getDictLabel());
        return region;
    }
}
