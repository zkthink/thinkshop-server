package com.think.cloud.thinkshop.mall.domain.memberuser;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 用户标签关联对象 mall_member_tag_ref
 *
 * @author zkthink
 * @date 2024-05-15
 */
@TableName("mall_member_tag_ref")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberTagRef
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 标签ID */
    @Excel(name = "标签ID")
    private Long tagId;

    public MemberTagRef(Long userId, Long tagId) {
        this.userId = userId;
        this.tagId = tagId;
    }
}
