package com.think.cloud.thinkshop.mall.service.order;

import cn.hutool.json.JSONArray;
import com.think.cloud.thinkshop.mall.controller.admin.index.dto.StatisticalTrendViewDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberOrderConsumeViewDTO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderDeliveryReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderInfoRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderPageRespVO;
import com.think.common.core.model.LoginUser;

import java.util.List;

/**
 * 订单Service接口
 *
 * @author zkthink
 * @date 2024-05-23
 */
public interface IOrderService {
    /**
     * 查询订单
     *
     * @param id 订单主键
     * @return 订单
     */
    public OrderInfoRespVO selectOrderById(Long id);

    /**
     * 查询订单列表
     *
     * @param vo
     * @return 订单集合
     */
    public List<OrderPageRespVO> selectOrderList(OrderPageReqVO vo);

    /**
     * 发货
     *
     * @param vo
     * @param loginUser
     * @return
     */
    public void delivery(OrderDeliveryReqVO vo, LoginUser loginUser);

    /**
     * 计算订单数量
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     * @return
     */
    Long orderCount(String startTime, String endTime);

    /**
     * 计算订单数量趋势
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     * @return
     */
    List<StatisticalTrendViewDTO> orderCountTrend(String startTime, String endTime);

    /**
     * 时间范围内支付买家的数量
     * @param startTime
     * @param endTime
     * @return
     */
    double payUserCount(String startTime, String endTime);
    /**
     * 营收总额
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     * @return
     */
    Long turnoverTotal(String startTime, String endTime);

    /**
     * 营收总额趋势
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     * @return
     */
    List<StatisticalTrendViewDTO> turnoverTotalTrend(String startTime, String endTime);

    /**
     * 范围内每天订单金额记录
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     * @return
     */
    List<String> orderAmountRecord(String startTime, String endTime);

    /**
     * 获取物流信息
     *
     * @param deliveryId@return /
     */
    JSONArray getLogistics(String deliveryId);

    /**
     * 订单转化率 （订单转化率=订单总数/网站访问UV*100%）
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     */
    Float orderConvertRate(String startTime, String endTime);

    /**
     * 付款转化率 （付款转化率=支付买家数/访客数UV*100%）
     *
     * @param startTime 起始时间
     * @param endTime   结束时间
     */
    Float payConvertRate(String startTime, String endTime);

    /**
     * 用户订单消费情况
     *
     * @return
     */
    MemberOrderConsumeViewDTO userOrderConsumeView(Long userId);

    /**
     * 删除订单
     * @param id
     * @return
     */
    int deleteOrder(Long id);

}
