package com.think.cloud.thinkshop.mall.mapper;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.category.vo.ProductCategoryLevelVO;
import com.think.cloud.thinkshop.mall.controller.admin.product.vo.ProductPageReqVO;
import com.think.cloud.thinkshop.mall.domain.Product;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.ProductCategory;

import java.util.List;

/**
 * 商品类目Mapper接口
 *
 * @author zkthink
 * @date 2024-05-08
 */
@Mapper
public interface ProductCategoryMapper extends BaseMapper<ProductCategory> {

    List<ProductCategoryLevelVO> getRelationProductCount();

    default List<ProductCategory> selectList(ProductCategory bean) {
        return selectList(new LambdaQueryWrapper<ProductCategory>()
                .like(ObjectUtil.isNotNull(bean.getName()), ProductCategory::getName, bean.getName())
                .eq(ObjectUtil.isNotNull(bean.getLevel()), ProductCategory::getLevel, bean.getLevel())
                .eq(ObjectUtil.isNotNull(bean.getParentCategoryId()), ProductCategory::getParentCategoryId, bean.getParentCategoryId())
                .orderByDesc(ProductCategory::getCategoryId));
    }

}
