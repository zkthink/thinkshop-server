package com.think.cloud.thinkshop.mall.domain.memberuser;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

/**
 * 客户运营计划关联对象 mall_member_operation_plan_ref
 *
 * @author zkthink
 * @date 2024-06-07
 */
@TableName("mall_member_operation_plan_ref")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberOperationPlanRef {
    /**
     * ID
     */
    private Long id;
    /**
     * 计划id
     */
    private Long planId;
    /**
     * 关联id
     */
    private Long refId;
    /**
     * 关联类型,1:组id ,2： 券id
     */
    private Integer refType;

    /**
     * 发放数量(每人)
     */
    private Integer couponIssueNum;
}
