package com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class UpdateTagDTO {
    @ApiModelProperty(value = "用户ID")
    private Long id;

    @ApiModelProperty(value = "标签ID")
    private List<Long> tagIds;
}

