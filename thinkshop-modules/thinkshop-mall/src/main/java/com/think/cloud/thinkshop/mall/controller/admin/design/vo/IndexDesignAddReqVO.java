package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * IndexDesignAddReqVO
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Data
@ToString(callSuper = true)
@Schema(description = "管理后台 - 图文、视频新增参数")
public class IndexDesignAddReqVO {
    @Schema(description = "资源地址", required = true, example = "http://xxxxxx")
    @NotNull(message = "资源地址不能为空")
    private String resourceUrl;

    @Schema(description = "标题", required = true, example = "title")
    @NotNull(message = "标题不能为空")
    private String title;

    @Schema(description = "简介", required = true, example = "xxxx")
    @NotNull(message = "简介不能为空")
    private String introduction;

    @Schema(description = "链接", required = true, example = "http://xxxxx")
    @NotNull(message = "链接不能为空")
    private String redirectUrl;

    @Schema(description = "类型，1：左图右文，2：左文右图，视频：0", required = true, example = "1")
    @NotNull(message = "类型不能为空")
    private Integer type;
}
