package com.think.cloud.thinkshop.mall.enums.logisticslog;

public enum LogisticsOrderTypeEnum {
    ORDINARY_ORDER(1, "订单"),
    AFTER_SALE_ORDER(2, "售后订单");

    private int code;
    private String description;

    LogisticsOrderTypeEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    // 根据code值获取枚举类型
    public static LogisticsOrderTypeEnum fromCode(int code) {
        for (LogisticsOrderTypeEnum type : LogisticsOrderTypeEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        throw new IllegalArgumentException("未知的订单类型代码: " + code);
    }
}
