package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

@Data
@Schema(description = "管理后台 - banner查询列表")
@ToString(callSuper = true)
public class BannerPageRespVO {

    @Schema(description = "序号", required = true, example = "1")
    private Long id;

    @Schema(description = "图片地址", required = true, example = "http://xxxx")
    private String imageUrl;

    @Schema(description = "重定向地址", required = true, example = "http://xxxx")
    private String redirectUrl;
    @Schema(description = "状态", required = true, example = "1")
    private Integer status;
}
