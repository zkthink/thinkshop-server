package com.think.cloud.thinkshop.mall.domain.memberuser;

import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import java.math.BigDecimal;

/**
 * 客户运营数据对象 mall_member_operation_log
 *
 * @author zkthink
 * @date 2024-06-14
 */
@TableName("mall_member_operation_log")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberOperationLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 日期(yyyyMMdd) */
    @Excel(name = "日期(yyyyMMdd)")
    private String dateStr;

    /** 发券人数 */
    @Excel(name = "发券人数")
    private Long couponUserCount;

    /** 下单人数 */
    @Excel(name = "下单人数")
    private Long orderUserCount;

    /** 下单笔数 */
    @Excel(name = "下单笔数")
    private Long orderCount;

    /** 下单金额 */
    @Excel(name = "下单金额")
    private BigDecimal orderAmount;

    /** 支付人数 */
    @Excel(name = "支付人数")
    private Long payUserCount;

    /** 支付订单数 */
    @Excel(name = "支付订单数")
    private Long payOrderCount;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 运营计划ID */
    @Excel(name = "运营计划ID")
    private Long planId;

}
