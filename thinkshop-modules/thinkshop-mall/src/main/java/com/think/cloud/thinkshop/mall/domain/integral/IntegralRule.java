package com.think.cloud.thinkshop.mall.domain.integral;


import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 积分规则对象 mall_integral_rule
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@TableName("mall_integral_rule")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntegralRule {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 使用可用于抵扣 0：不能 1：能
     */
    @Excel(name = "使用可用于抵扣 0：不能 1：能")
    @NotNull(message = "使用状态不能为空")
    private Integer isUse;

    /**
     * 抵扣比值，1积分可以抵扣的金额
     */
    @Excel(name = "抵扣比值，1积分可以抵扣的金额")
    @NotNull(message = "抵扣比值不能为空")
    private BigDecimal deductionRatio;

    /**
     * 抵扣类型 1；最多积分 ，2：最高比例
     */
    @Excel(name = "抵扣类型 1；最多积分 ，2：最高比例")
    @NotNull(message = "抵扣类型不能为空")
    private Integer deductionType;

    /**
     * 抵扣类型值
     */
    @Excel(name = "抵扣类型值")
    @NotNull(message = "配置值不能为空")
    private BigDecimal deductionValue;

    /**
     * 订单赠送积分
     */
    @Excel(name = "订单赠送积分")
    @NotNull(message = "订单赠送积分值不能为空")
    private Integer orderAward;

    /**
     * 积分清空类型:0:不清零 1： 2：:3：
     */
    @Excel(name = "积分清空类型:0:不清零 1： 2：:3： ")
    @NotNull(message = "积分清空类型不能为空")
    private Integer cleanType;

    /**
     * 描述
     */
    @Excel(name = "描述")
    private String describeInfo;

//    /**
//     * 是否删除
//     */
//    @Excel(name = "是否删除")
//    private Integer deleted;

}
