package com.think.cloud.thinkshop.mall.domain.websitesetting;

import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 协议对象 mall_agreement
 *
 * @author zkthink
 * @date 2024-05-27
 */
@TableName("mall_agreement")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Agreement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 协议类型 */
    @Excel(name = "协议类型")
    private Integer type;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

}
