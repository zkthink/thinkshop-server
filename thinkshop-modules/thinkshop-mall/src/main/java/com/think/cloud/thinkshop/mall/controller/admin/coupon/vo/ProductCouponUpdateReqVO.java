package com.think.cloud.thinkshop.mall.controller.admin.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@ToString(callSuper = true)
public class ProductCouponUpdateReqVO {

    @ApiModelProperty("优惠券id")
    @NotNull(message = "优惠券id不能为空")
    private Long id;

    @ApiModelProperty("优惠券名称")
    private String couponName;

    @ApiModelProperty("优惠券面值")
    private BigDecimal couponValue;

    @ApiModelProperty("优惠券类型：1、满减券，2、折扣券")
    private Integer couponType;

    @ApiModelProperty("满减门槛")
    private BigDecimal threshold;

    @ApiModelProperty("折扣")
    private BigDecimal discount;

    @ApiModelProperty("优惠券范围类型：1、所有商品，2、选中商品，3、选中商品不可用")
    private Integer couponScope;

    @ApiModelProperty("范围值")
    private String[] ids;

    @ApiModelProperty("优惠券数量")
    private Long number;

    @ApiModelProperty("领取限制类型 1 无限制 2限制次数")
    private Integer receiveType;

    @ApiModelProperty("限制每人领取数量")
    private Long limitNumber;

    @ApiModelProperty("过期类型：1、按时间，2、按天数")
    private Integer expirationType;

    @ApiModelProperty("生效时间")
    private Timestamp takingEffectTime;

    @ApiModelProperty("过期时间")
    private Timestamp expirationTime;

    @ApiModelProperty("过期天数")
    private Long expirationDay;

    @ApiModelProperty("备注")
    private String remark;
}
