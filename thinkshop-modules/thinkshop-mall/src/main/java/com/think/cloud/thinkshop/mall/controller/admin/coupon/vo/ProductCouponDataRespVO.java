package com.think.cloud.thinkshop.mall.controller.admin.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;


@Data
@ToString(callSuper = true)
public class ProductCouponDataRespVO{

    @ApiModelProperty("优惠券状态名")
    private String couponName;

    @ApiModelProperty("领取人数")
    private Long receivePerson;

    @ApiModelProperty("领取数量")
    private Long receiveNumber;

    @ApiModelProperty("剩余数量")
    private Long residueNumber;

    @ApiModelProperty("交易总额")
    private BigDecimal totalAmount;

    @ApiModelProperty("优惠总额")
    private BigDecimal totalCouponAmount;

    @ApiModelProperty("商品总数")
    private Long totalNumber;

    @ApiModelProperty("平均优惠金额")
    private BigDecimal averageCouponAmount;

    @ApiModelProperty("明细")
    private List<ProductCouponDataDetailRespVO> details;
}
