package com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.dto;

import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class DeliveryCompanyImportDTO {
}
