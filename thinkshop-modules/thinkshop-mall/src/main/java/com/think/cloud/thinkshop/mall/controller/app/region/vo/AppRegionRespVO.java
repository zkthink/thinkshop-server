package com.think.cloud.thinkshop.mall.controller.app.region.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class AppRegionRespVO {

    /**
     * 地区id
     */
    @ApiModelProperty("地区id")
    private Long regionId;
    /**
     * 地区名称
     */
    @ApiModelProperty("地区名称")
    private String Name;


    @ApiModelProperty("子地区")
    private List<AppRegionRespVO> children;
}
