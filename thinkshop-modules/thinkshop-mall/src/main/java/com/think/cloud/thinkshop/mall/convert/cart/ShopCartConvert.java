package com.think.cloud.thinkshop.mall.convert.cart;

import com.think.cloud.thinkshop.mall.controller.app.cart.vo.AddShopCartReqVO;
import com.think.cloud.thinkshop.mall.controller.app.cart.vo.MyCartRespVO;
import com.think.cloud.thinkshop.mall.domain.ShopCart;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 购物车 Convert
 *
 * @author zkthink
 */
@Mapper
public interface ShopCartConvert {

    ShopCartConvert INSTANCE = Mappers.getMapper(ShopCartConvert.class);

    ShopCart convert(AddShopCartReqVO vo);

    List<MyCartRespVO> convertList(List<ShopCart> list);
}
