package com.think.cloud.thinkshop.mall.controller.app.verificationcode;

import com.think.cloud.thinkshop.mall.service.verificationcode.VerificationCodeService;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zkthink
 * @apiNote
 **/
@RestController
@RequestMapping("/verification-code")
@Api(tags = "APP:验证码")
public class VerificationCodeController {
    @Autowired
    private VerificationCodeService service;

    @GetMapping("/email")
    @ApiOperation("邮箱验证码-注册")
    public AjaxResult sendEmailCode(@RequestParam String email) {
        return AjaxResult.success(service.sendEmailCode(email));
    }

    @GetMapping("/email-forget")
    @ApiOperation("邮箱验证码-忘记密码")
    public AjaxResult sendEmailCodeForget(@RequestParam String email) {
        service.sendEmailCodeForget(email);
        return AjaxResult.success();
    }

    @GetMapping("/email-update")
    @ApiOperation("邮箱验证码-更新用户信息")
    public AjaxResult sendEmailCodeUpdate() {
        service.sendEmailCodeUpdate(SecurityUtils.getUserId());
        return AjaxResult.success();
    }
}

