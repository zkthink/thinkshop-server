package com.think.cloud.thinkshop.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.ProductComment;

/**
 * 商品评价Mapper接口
 *
 * @author moxiangrong
 * @date 2024-07-12
 */
@Mapper
public interface ProductCommentMapper extends BaseMapper<ProductComment> {
}
