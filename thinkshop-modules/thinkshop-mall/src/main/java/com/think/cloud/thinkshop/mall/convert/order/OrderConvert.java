package com.think.cloud.thinkshop.mall.convert.order;


import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderDeliveryReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderInfoRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderLogRespVO;
import com.think.cloud.thinkshop.mall.controller.app.order.dto.AppOrderInfoDTO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.*;
import com.think.cloud.thinkshop.mall.domain.order.Order;
import com.think.cloud.thinkshop.mall.domain.order.OrderDetail;
import com.think.cloud.thinkshop.mall.domain.order.OrderLog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 用户订单 Convert
 *
 * @author zkthink
 */
@Mapper
public interface OrderConvert {

    OrderConvert INSTANCE = Mappers.getMapper(OrderConvert.class);

    AppConfirmOrderRespVO convert(AppOrderInfoDTO dto);

    List<AppOrderPageRespVO> convertList(List<Order> list);

    @Mappings({
            @Mapping(source = "price", target = "originalPrice"),
            @Mapping(source = "realPrice", target = "price"),
    })
    OrderDetail convert(AppOrderCartRespVO vo);

    AppOrderInfoRespVO convert(Order bean);

    Order convert(OrderDeliveryReqVO vo);

    OrderInfoRespVO convert1(Order bean);

    @Mappings({
            @Mapping(source = "createBy", target = "userName")
    })
    OrderLogRespVO convert(OrderLog bean);

    List<OrderLogRespVO> convertLogList(List<OrderLog> bean);

    List<AppOrderLogRespVO> convertLogList1(List<OrderLog> bean);
}
