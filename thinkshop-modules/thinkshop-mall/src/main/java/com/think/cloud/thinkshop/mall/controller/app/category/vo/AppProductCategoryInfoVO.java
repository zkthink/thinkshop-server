package com.think.cloud.thinkshop.mall.controller.app.category.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class AppProductCategoryInfoVO {

    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;

    /**
     * 类目名称
     */
    @ApiModelProperty("类目名称")
    private String name;

    /**
     * 类路径
     */
    @ApiModelProperty("类路径")
    private String path;

}
