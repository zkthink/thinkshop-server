package com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 售后单分页响应VO
 *
 * @author zkthink
 * @date 2024-06-14
 */
@Data
public class AfterSalesOrderDetailRespVO {

    @ApiModelProperty("售后id")
    private Long afterSalesId;

    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("规格")
    private String sku;

    @ApiModelProperty("规格图片")
    private String image;

    @ApiModelProperty("商品数量")
    private Integer num;

    @ApiModelProperty("原价")
    private BigDecimal originalPrice;

    @ApiModelProperty("优惠价格")
    private BigDecimal couponPrice;

    @ApiModelProperty("实际价格")
    private BigDecimal price;

    @ApiModelProperty("合计")
    private BigDecimal totalPrice;
}
