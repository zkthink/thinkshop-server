package com.think.cloud.thinkshop.mall.service.payment.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentConfig;
import com.think.cloud.thinkshop.mall.mapper.payment.PaymentConfigMapper;
import com.think.cloud.thinkshop.mall.service.payment.IPaymentConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 支付配置Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-14
 */
@Service
public class PaymentConfigServiceImpl implements IPaymentConfigService {
    @Autowired
    private PaymentConfigMapper paymentConfigMapper;

    /**
     * 查询支付配置
     *
     * @param id 支付配置主键
     * @return 支付配置
     */
    @Override
    public PaymentConfig selectPaymentConfigById(Long id) {
        return paymentConfigMapper.selectById(id);
    }

    /**
     * 查询支付配置列表
     *
     * @param paymentConfig 支付配置
     * @return 支付配置
     */
    @Override
    public List<PaymentConfig> selectPaymentConfigList(PaymentConfig paymentConfig) {
        return paymentConfigMapper.selectList(Wrappers.<PaymentConfig>lambdaQuery()
                .eq(paymentConfig.getStatus()!=null,PaymentConfig::getStatus,paymentConfig.getStatus())
        );
    }

    /**
     * 新增支付配置
     *
     * @param paymentConfig 支付配置
     * @return 结果
     */
    @Override
    public int insertPaymentConfig(PaymentConfig paymentConfig) {

        return paymentConfigMapper.insert(paymentConfig);
    }

    /**
     * 修改支付配置
     *
     * @param paymentConfig 支付配置
     * @return 结果
     */
    @Override
    public int updatePaymentConfig(PaymentConfig paymentConfig) {
        return paymentConfigMapper.updateById(paymentConfig);
    }

    /**
     * 批量删除支付配置信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return paymentConfigMapper.deleteBatchIds(ids);
    }
}
