package com.think.cloud.thinkshop.mall.mapper.aftersales;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.aftersales.AfterSalesLog;

/**
 * 售后日志Mapper接口
 *
 * @author zkthink
 * @date 2024-06-13
 */
@Mapper
public interface AfterSalesLogMapper extends BaseMapper<AfterSalesLog> {

}
