package com.think.cloud.thinkshop.mall.service.product.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.think.cloud.thinkshop.mall.domain.ProductAttrValue;
import com.think.cloud.thinkshop.mall.mapper.ProductAttrValueMapper;
import com.think.cloud.thinkshop.mall.service.product.IProductAttrValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品属性值Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Service
public class ProductAttrValueServiceImpl implements IProductAttrValueService {
    @Autowired
    private ProductAttrValueMapper productAttrValueMapper;

    /**
     * 查询商品属性值
     *
     * @param skuId 商品属性值主键
     * @return 商品属性值
     */
    @Override
    public ProductAttrValue selectProductAttrValueBySkuId(Long skuId) {
        return productAttrValueMapper.selectById(skuId);
    }

    @Override
    public ProductAttrValue findProductAttrValueBySkuId(Long skuId) {
        return productAttrValueMapper.findProductAttrValueBySkuId(skuId);
    }

    /**
     * 查询商品属性值列表
     *
     * @param productAttrValue 商品属性值
     * @return 商品属性值
     */
    @Override
    public List<ProductAttrValue> selectProductAttrValueList(ProductAttrValue productAttrValue) {
        return productAttrValueMapper.selectList(productAttrValue);
    }

    /**
     * 批量新增商品属性值
     *
     * @param productAttrValues 商品属性值集合
     * @return 结果
     */
    @Override
    public void batchInsertProductAttrValue(List<ProductAttrValue> productAttrValues) {
        productAttrValueMapper.batchInsertProductAttrValue(productAttrValues);
    }

    /**
     * 修改商品属性值
     *
     * @param productAttrValue 商品属性值
     * @return 结果
     */
    @Override
    public int updateProductAttrValue(ProductAttrValue productAttrValue) {
        return productAttrValueMapper.updateById(productAttrValue);
    }

    /**
     * 批量删除商品属性值信息
     *
     * @param skuIds 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> skuIds) {
        return productAttrValueMapper.deleteBatchIds(skuIds);
    }

    @Override
    public void deleteByProductId(Long productId) {
        productAttrValueMapper.delete(new LambdaQueryWrapper<ProductAttrValue>()
                .eq(ProductAttrValue::getProductId, productId));
    }
}
