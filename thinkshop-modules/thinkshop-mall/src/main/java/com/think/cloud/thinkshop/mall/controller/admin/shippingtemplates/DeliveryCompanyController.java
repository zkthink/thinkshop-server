package com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates;


import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.think.cloud.thinkshop.mall.domain.shippingtemplates.DeliveryCompany;
import com.think.cloud.thinkshop.mall.service.shippingtemplates.IDeliveryCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 快递公司Controller
 *
 * @author zkthink
 * @date 2024-06-05
 */
@RestController
@RequestMapping("/admin/delivery-company")
@Api(tags ="ADMIN:快递公司")
public class DeliveryCompanyController extends BaseController
{
    @Autowired
    private IDeliveryCompanyService deliveryCompanyService;

    /**
     * 查询快递公司列表
     */
    @RequiresPermissions("mall:company:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询快递公司列表")
    public TableDataInfo list(DeliveryCompany deliveryCompany)
    {
        startPage();
        List<DeliveryCompany> list = deliveryCompanyService.selectDeliveryCompanyList(deliveryCompany);
        return getDataTable(list);
    }

    /**
     * 导出快递公司列表
     */
    @RequiresPermissions("mall:company:export")
    @Log(title = "快递公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出快递公司列表")
    public void export(HttpServletResponse response, DeliveryCompany deliveryCompany)
    {
        List<DeliveryCompany> list = deliveryCompanyService.selectDeliveryCompanyList(deliveryCompany);
        ExcelUtil<DeliveryCompany> util = new ExcelUtil<DeliveryCompany>(DeliveryCompany.class);
        util.exportExcel(response, list, "快递公司数据");
    }

    /**
     * 获取快递公司详细信息
     */
    @RequiresPermissions("mall:company:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取快递公司详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(deliveryCompanyService.selectDeliveryCompanyById(id));
    }

    /**
     * 新增快递公司
     */
    @RequiresPermissions("mall:company:add")
    @Log(title = "快递公司", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增快递公司")
    public AjaxResult add(@RequestBody DeliveryCompany deliveryCompany)
    {
        return toAjax(deliveryCompanyService.insertDeliveryCompany(deliveryCompany));
    }

    /**
     * 修改快递公司
     */
    @RequiresPermissions("mall:company:edit")
    @Log(title = "快递公司", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改快递公司")
    public AjaxResult edit(@RequestBody DeliveryCompany deliveryCompany)
    {
        return toAjax(deliveryCompanyService.updateDeliveryCompany(deliveryCompany));
    }

    /**
     * 删除快递公司
     */
    @RequiresPermissions("mall:company:remove")
    @Log(title = "快递公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete")
    @ApiOperation(value = "删除快递公司")
    public AjaxResult remove(@Valid Long[] ids)
    {
        return toAjax(deliveryCompanyService.batchDelete(Arrays.asList(ids)));
    }

    @GetMapping("simple-list")
    @ApiOperation(value = "获取快递公司列表")
    public AjaxResult simpleList()
    {
        return success(deliveryCompanyService.simpleList());
    }

    /**
     * 导入快递公司 17track获取地址 https://api.17track.net/zh-cn/doc?version=v2.2&anchor=%E8%BF%90%E8%BE%93%E5%95%86%E4%BB%A3%E7%A0%81
     * @param file json格式文件
     * @return /
     */

    @PostMapping("import")
    @ApiOperation(value = "导入快递公司")
    public AjaxResult importData(MultipartFile file) {
        deliveryCompanyService.importData(file);
        return success();
    }
}
