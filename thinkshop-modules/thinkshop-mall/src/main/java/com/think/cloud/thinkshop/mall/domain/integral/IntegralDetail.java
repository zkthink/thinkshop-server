package com.think.cloud.thinkshop.mall.domain.integral;


import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

/**
 * 积分详情对象 mall_integral_detail
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@TableName("mall_integral_detail")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntegralDetail
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户 */
    @Excel(name = "用户")
    private Long userId;

    /** 积分状态 1：正常（含部分使用），2：完全使用：3：完全过期，4部分过期（部分已使用） */
    @Excel(name = "积分状态 1：正常 2：完全使用：3：完全过期，4部分过期（部分已使用）")
    private Integer status;

    /** 总积分值 */
    @Excel(name = "总积分值")
    private Integer totalIntegral;

    /** 剩余积分值 */
    @Excel(name = "剩余积分值")
    private Integer remainIntegral;

    /** 描述 */
    @Excel(name = "描述")
    private String detail;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    private Date createTime;
}
