package com.think.cloud.thinkshop.mall.controller.admin.payment;


import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.think.cloud.thinkshop.mall.domain.payment.PaymentRecord;
import com.think.cloud.thinkshop.mall.service.payment.IPaymentRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 支付记录Controller
 *
 * @author zkthink
 * @date 2024-05-27
 */
@RestController
@RequestMapping("/admin/payment-record")
@Api(tags = "ADMIN:支付记录")
public class PaymentRecordController extends BaseController
{
    @Autowired
    private IPaymentRecordService paymentRecordService;

    /**
     * 查询支付记录列表
     */
    @RequiresPermissions("mall:payment-record:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询支付记录列表")
    public TableDataInfo list(PaymentRecord paymentRecord)
    {
        startPage();
        List<PaymentRecord> list = paymentRecordService.selectPaymentRecordList(paymentRecord);
        return getDataTable(list);
    }

    /**
     * 导出支付记录列表
     */
    @RequiresPermissions("mall:payment-record:export")
    @Log(title = "支付记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出支付记录列表")
    public void export(HttpServletResponse response, PaymentRecord paymentRecord)
    {
        List<PaymentRecord> list = paymentRecordService.selectPaymentRecordList(paymentRecord);
        ExcelUtil<PaymentRecord> util = new ExcelUtil<PaymentRecord>(PaymentRecord.class);
        util.exportExcel(response, list, "支付记录数据");
    }

    /**
     * 获取支付记录详细信息
     */
    @RequiresPermissions("mall:payment-record:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取支付记录详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(paymentRecordService.selectPaymentRecordById(id));
    }

    /**
     * 新增支付记录
     */
    @RequiresPermissions("mall:payment-record:add")
    @Log(title = "支付记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增支付记录")
    public AjaxResult add(@RequestBody PaymentRecord paymentRecord)
    {
        return toAjax(paymentRecordService.insertPaymentRecord(paymentRecord));
    }

    /**
     * 修改支付记录
     */
    @RequiresPermissions("mall:payment-record:edit")
    @Log(title = "支付记录", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改支付记录")
    public AjaxResult edit(@RequestBody PaymentRecord paymentRecord)
    {
        return toAjax(paymentRecordService.updatePaymentRecord(paymentRecord));
    }

    /**
     * 删除支付记录
     */
    @RequiresPermissions("mall:payment-record:remove")
    @Log(title = "支付记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete")
    @ApiOperation(value = "删除支付记录")
    public AjaxResult remove(@Valid Long[] ids)
    {
        return toAjax(paymentRecordService.batchDelete(Arrays.asList(ids)));
    }
}
