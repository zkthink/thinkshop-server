package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import com.think.cloud.thinkshop.mall.annotation.CrossBorderZoneTimeAmend;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;


/**
 * 售后操作响应VO
 *
 * @author zkthink
 * @date 2024-06-13
 */
@Data
public class AppAfterSalesLogRespVO {

    @ApiModelProperty("操作类型")
    private Integer operateType;


    @ApiModelProperty("创建时间")
    @CrossBorderZoneTimeAmend
    private Timestamp createTime;
}
