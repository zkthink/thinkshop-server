package com.think.cloud.thinkshop.mall.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 优惠券领取类型枚举
 */
@Getter
@AllArgsConstructor
public enum CouponReceiveTypeEnum {
    UNLIMITED(1, "无限制"),
    LIMITATION(2, "限制数量");

    private Integer value;
    private String desc;
}
