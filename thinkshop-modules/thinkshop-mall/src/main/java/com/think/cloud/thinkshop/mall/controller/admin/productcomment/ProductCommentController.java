package com.think.cloud.thinkshop.mall.controller.admin.productcomment;


import com.think.cloud.thinkshop.mall.controller.admin.productcomment.param.ProductCommentAddParam;
import com.think.cloud.thinkshop.mall.service.productcomment.IProductCommentService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;

/**
 * 商品评价Controller
 *
 * @author moxiangrong
 * @date 2024-07-12
 */
@RestController
@RequestMapping("/admin/comment")
@Api(tags = "ADMIN:商品评价")
@Validated
public class ProductCommentController extends BaseController {
    @Autowired
    private IProductCommentService productCommentService;

    @RequiresPermissions("mall:comment:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询商品评价列表")
    public TableDataInfo list() {
        startPage();
        return productCommentService.selectProductCommentList();
    }

    @RequiresPermissions("mall:comment:add")
    @Log(title = "手动新增商品评价", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "手动新增商品评价")
    public AjaxResult add(@RequestBody ProductCommentAddParam param) {
        return toAjax(productCommentService.insertProductComment(param));
    }

    @RequiresPermissions("mall:comment:updateStatus")
    @Log(title = "修改评论状态", businessType = BusinessType.UPDATE)
    @PutMapping("/status/{id}/{status}")
    @ApiOperation(value = "修改评论状态")
    public AjaxResult updateStatus(@ApiParam("主键") @PathVariable Long id,
                                   @ApiParam("状态：0：不可见 ，1：可见") @PathVariable Integer status) {
        return toAjax(productCommentService.updateProductCommentStatus(id, status));
    }

    @RequiresPermissions("mall:comment:remove")
    @Log(title = "删除商品评价", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除商品评价")
    public AjaxResult remove(@ApiParam("主键集合") @Valid Long[] ids) {
        return toAjax(productCommentService.batchDelete(Arrays.asList(ids)));
    }

    //    @RequiresPermissions("mall:comment:edit")
//    @Log(title = "商品评价", businessType = BusinessType.UPDATE)
//    @PutMapping("/update")
//    @ApiOperation(value = "修改商品评价")
//    public AjaxResult edit(@RequestBody ProductComment productComment) {
//        return toAjax(productCommentService.updateProductComment(productComment));
//    }

//    @RequiresPermissions("mall:comment:export")
//    @Log(title = "商品评价", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ApiOperation(value = "导出商品评价列表")
//    public void export(HttpServletResponse response, ProductComment productComment) {
//        List<ProductComment> list = productCommentService.selectProductCommentList(productComment);
//        ExcelUtil<ProductComment> util = new ExcelUtil<ProductComment>(ProductComment.class);
//        util.exportExcel(response, list, "商品评价数据");
//    }

//    @RequiresPermissions("mall:comment:query")
//    @GetMapping(value = "/get/{id}")
//    @ApiOperation(value = "获取商品评价详细信息")
//    public AjaxResult getInfo(@PathVariable("id") Long id) {
//        return success(productCommentService.selectProductCommentById(id));
//    }
}
