package com.think.cloud.thinkshop.mall.controller.app.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 购物车优惠券明细VO
 */
@Data
@Builder
public class CartCouponDetailRespVO {

    @ApiModelProperty("商品id")
    private Long productId;

    @ApiModelProperty("规格id")
    private Long skuId;

    @ApiModelProperty("商品优惠")
    private BigDecimal productCoupon;


}
