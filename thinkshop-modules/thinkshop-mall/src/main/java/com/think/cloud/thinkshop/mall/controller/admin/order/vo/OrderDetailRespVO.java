package com.think.cloud.thinkshop.mall.controller.admin.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 订单明细分页响应VO
 *
 * @author zkthink
 * @date 2024-05-29
 */
@Data
public class OrderDetailRespVO {

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("规格")
    private String sku;

    @ApiModelProperty("规格图片")
    private String image;

    @ApiModelProperty("商品数量")
    private Integer num;

    @ApiModelProperty("原价")
    private BigDecimal originalPrice;

    @ApiModelProperty("优惠价格")
    private BigDecimal couponPrice;

    @ApiModelProperty("实际价格")
    private BigDecimal price;

    @ApiModelProperty("合计")
    private BigDecimal totalPrice;


}
