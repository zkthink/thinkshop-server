package com.think.cloud.thinkshop.mall.controller.app.region;

import com.think.cloud.thinkshop.mall.service.region.RegionService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 地区AppController
 *
 * @author zkthink
 * @date 2024-05-21
 */
@RestController
@RequestMapping("/app/region")
@Api(tags = "APP:网站设置")
public class AppRegionController extends BaseController {

    @Autowired
    private RegionService regionService;
    @GetMapping("/list")
    @ApiOperation("查询地区列表")
    public AjaxResult regionList(@RequestParam(required = false) String keyword) {
        return success(regionService.getRegion(keyword));
    }

}
