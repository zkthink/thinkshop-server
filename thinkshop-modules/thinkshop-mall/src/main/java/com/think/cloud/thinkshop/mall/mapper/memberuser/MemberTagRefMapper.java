package com.think.cloud.thinkshop.mall.mapper.memberuser;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagRefDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTagRef;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户标签关联Mapper接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Mapper
public interface MemberTagRefMapper extends BaseMapper<MemberTagRef> {

    List<MemberTagRefDTO> selectByUserIds(@Param("userIds") List<Long> userIds);
}
