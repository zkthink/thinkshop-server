package com.think.cloud.thinkshop.mall.mapper.aftersales;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesPageRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.index.dto.StatisticalTrendViewDTO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesPageReqVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesPageRespVO;
import com.think.cloud.thinkshop.mall.domain.order.Order;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.aftersales.AfterSales;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 售后记录Mapper接口
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Mapper
public interface AfterSalesMapper extends BaseMapper<AfterSales> {

    List<AppAfterSalesPageRespVO> selectAfterSaleList(AppAfterSalesPageReqVO vo);

    List<AfterSalesPageRespVO> selectAppAfterSaleList(AfterSalesPageReqVO vo);

    @Select("select DATE_FORMAT(create_time , '%Y-%m-%d') as dateStr,count(*) as value " +
            "from mall_after_sales " +
            "${ew.customSqlSegment} group by dateStr")
    List<StatisticalTrendViewDTO> afterSalesCountTrend(@Param(Constants.WRAPPER) LambdaQueryWrapper<Order> wrapper);

    @Select("select DATE_FORMAT(create_time , '%Y-%m-%d') as dateStr,sum(refund_amount) as value " +
            "from mall_after_sales " +
            "${ew.customSqlSegment} group by dateStr")
    List<StatisticalTrendViewDTO> afterSalesAmountTrend(@Param(Constants.WRAPPER) LambdaQueryWrapper<Order> wrapper);

}
