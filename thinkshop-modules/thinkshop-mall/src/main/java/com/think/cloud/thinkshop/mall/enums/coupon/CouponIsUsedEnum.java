package com.think.cloud.thinkshop.mall.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 优惠券是否使用类型枚举
 */
@Getter
@AllArgsConstructor
public enum CouponIsUsedEnum {
    USED(1, "已使用"),
    NOT_USED(0, "未使用");

    private Integer value;
    private String desc;
}
