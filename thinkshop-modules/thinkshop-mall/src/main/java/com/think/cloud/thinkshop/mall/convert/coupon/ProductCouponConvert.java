package com.think.cloud.thinkshop.mall.convert.coupon;


import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponCreateReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponUpdateReqVO;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.AppCouponDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.AppUserCouponRespVO;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.CartCouponRespVO;
import com.think.cloud.thinkshop.mall.domain.coupon.ProductCoupon;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 优惠券 Convert
 *
 * @author zkthink
 */
@Mapper
public interface ProductCouponConvert {

    ProductCouponConvert INSTANCE = Mappers.getMapper(ProductCouponConvert.class);


    ProductCoupon convert(ProductCouponCreateReqVO vo);

    ProductCoupon convert(ProductCouponUpdateReqVO vo);

    ProductCouponDetailRespVO convert(ProductCoupon bean);

    List<ProductCouponDetailRespVO> convertList(List<ProductCoupon> list);

    List<CartCouponRespVO> convertList1(List<AppUserCouponRespVO> list);

    List<AppCouponDetailRespVO> convertList2(List<ProductCoupon> list);
}
