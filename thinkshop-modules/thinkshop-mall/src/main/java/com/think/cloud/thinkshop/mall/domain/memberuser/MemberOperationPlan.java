package com.think.cloud.thinkshop.mall.domain.memberuser;


import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 客户运营计划对象 mall_member_operation_plan
 *
 * @author zkthink
 * @date 2024-06-07
 */
@TableName("mall_member_operation_plan")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberOperationPlan extends BaseEntity {
    /**
     * ID
     */
    private Long id;
    /**
     * 计划名称
     */
    private String name;
    /**
     * 计划类型  ： 1：自动  ，2：手动
     */
    private Integer planType;

    /**
     * 手动执行类型 1 立即执行 2 定时执行
     */
    private Integer manualExecutionType;
    /**
     * 任务开时间，手动类型下的立刻执行不填
     */
    private Timestamp planStart;
    /**
     * 任务结束时间，手动类型下不填
     */
    private Timestamp planEnd;
    /**
     * 是否删除
     */
    private Integer deleted;
    /**
     * 状态  0：未开始 ，1进行中， 2：结束
     */
    private Integer status;
}
