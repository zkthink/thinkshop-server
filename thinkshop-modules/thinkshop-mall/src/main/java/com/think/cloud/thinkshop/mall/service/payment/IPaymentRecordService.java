package com.think.cloud.thinkshop.mall.service.payment;

import com.think.cloud.thinkshop.mall.domain.payment.PaymentRecord;

import java.util.List;

/**
 * 支付记录Service接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
public interface IPaymentRecordService
{
    /**
     * 查询支付记录
     *
     * @param id 支付记录主键
     * @return 支付记录
     */
    public PaymentRecord selectPaymentRecordById(Long id);

    /**
     * 查询支付记录列表
     *
     * @param paymentRecord 支付记录
     * @return 支付记录集合
     */
    public List<PaymentRecord> selectPaymentRecordList(PaymentRecord paymentRecord);

    /**
     * 新增支付记录
     *
     * @param paymentRecord 支付记录
     * @return 结果
     */
    public int insertPaymentRecord(PaymentRecord paymentRecord);

    /**
     * 修改支付记录
     *
     * @param paymentRecord 支付记录
     * @return 结果
     */
    public int updatePaymentRecord(PaymentRecord paymentRecord);

    /**
     * 批量删除支付记录信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    PaymentRecord selectByPaymentId(String paymentId);
}
