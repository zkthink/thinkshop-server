package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

@Data
@ApiModel("用户分组 新增、编辑 ReqVO")
@ToString(callSuper = true)
public class MemberGroupReqVO {

    /** 人群名称 */
    @ApiModelProperty("人群名称")
    private String name;

    /** 人群数量 */
    @ApiModelProperty("人群数量 min")
    private Long userCountMin;
    /** 人群数量 */
    @ApiModelProperty("人群数量 max")
    private Long userCountMax;
}
