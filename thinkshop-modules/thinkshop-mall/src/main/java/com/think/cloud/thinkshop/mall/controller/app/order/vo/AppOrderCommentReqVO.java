package com.think.cloud.thinkshop.mall.controller.app.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class AppOrderCommentReqVO {
    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 评论集
     */
    private List<Comment> commentSet;

    @Data
    public static class Comment {
        /**
         * 产品ID
         */
        private Long productId;
        /**
         * 规格
         */
        private Long skuId;
        /**
         * 评论内容
         */
        private String comment;
        /**
         * 分数
         */
        private BigDecimal score;
        /**
         * 图片
         */
        private List<String> imageUrls;
    }
}
