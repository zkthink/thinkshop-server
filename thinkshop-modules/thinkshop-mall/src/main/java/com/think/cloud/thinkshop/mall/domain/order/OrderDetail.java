package com.think.cloud.thinkshop.mall.domain.order;

import java.math.BigDecimal;
import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 订单明细对象 mall_order_detail
 *
 * @author zkthink
 * @date 2024-05-23
 */
@TableName("mall_order_detail")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetail
{
    private static final long serialVersionUID = 1L;

    /** 订单明细id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 购物车id */
    @Excel(name = "购物车id")
    private Long cartId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

    /** 规格id */
    @Excel(name = "规格id")
    private Long skuId;

    /** 原价 */
    @Excel(name = "原价")
    private BigDecimal originalPrice;

    /** 实际价格 */
    @Excel(name = "实际价格")
    private BigDecimal price;

    /** 税费 */
    @Excel(name = "税费")
    private BigDecimal taxation;

    /** 状态：1、正常，2、售后中，3、售后完成 */
    @Excel(name = "状态：1、正常，2、售后中，3、售后完成")
    private Integer state;

    /** 售后id */
    @Excel(name = "售后id")
    private Long afterSalesId;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

}
