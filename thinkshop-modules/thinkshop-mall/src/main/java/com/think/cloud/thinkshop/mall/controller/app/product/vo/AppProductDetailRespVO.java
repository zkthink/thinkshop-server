package com.think.cloud.thinkshop.mall.controller.app.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * App商品详情响应VO
 *
 * @author zkthink
 * @date 2024-05-16
 */
@Data
public class AppProductDetailRespVO {

    /** 商品id */
    @ApiModelProperty("商品id")
    private Long productId;

    /** 商品编号 */
    @ApiModelProperty("商品编号")
    private String productCode;

    /** 商品名称 */
    @ApiModelProperty("商品名称")
    private String productName;

    /** 简介 */
    @ApiModelProperty("简介")
    private String introduce;

    /** 商品图片 */
    @ApiModelProperty("商品图片")
    private String image;

    /** 视频 */
    @ApiModelProperty("视频")
    private String video;

    /** 商品详情 */
    @ApiModelProperty("商品详情")
    private String detail;

    /** 分类id */
    @ApiModelProperty("分类id")
    private Long categoryId;

    /** 类目路径 */
    @ApiModelProperty("类目路径")
    private String categoryPath;

    /** 是否收税 0否 1是 */
    @ApiModelProperty("是否收税 0否 1是")
    private Integer tax;

    /** 规格最低价 */
    @ApiModelProperty("规格最低价")
    private BigDecimal minPrice;

    /** 规格最高价 */
    @ApiModelProperty("规格最高价")
    private BigDecimal maxPrice;

    /** 售价区间 */
    @ApiModelProperty("售价区间")
    private String priceRange;

    /** 销量 */
    @ApiModelProperty("销量")
    private Integer sales;

    /** 库存 */
    @ApiModelProperty("库存")
    private Integer stock;

    /** 属性 */
    @ApiModelProperty("属性")
    private List<AppProductAttrRespVO> productAttr;

    /** 规格信息 */
    @ApiModelProperty("规格信息")
    private Map<String,AppProductSkuRespVO> productValue;

}
