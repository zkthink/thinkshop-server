package com.think.cloud.thinkshop.mall.mapper.memberuser;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberGroupUserTraitDetailDTO;
import com.think.cloud.thinkshop.mall.controller.admin.index.dto.StatisticalTrendViewDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberUser;
import com.think.cloud.thinkshop.mall.domain.order.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商城用户Mapper接口
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Mapper
public interface MemberUserMapper extends BaseMapper<MemberUser> {
    /**
     * 每天用户注册数量
     *
     * @param start
     * @param end
     * @return
     */
    @Select("select DATE_FORMAT(create_time , '%Y-%m-%d') as dateStr,count(*) as value\n" +
            "from mall_member_user \n" +
            "where create_time >='#{start}' and create_time <='#{end}'\n" +
            "group by dateStr")
    List<StatisticalTrendViewDTO> registeredUserCount(@Param("start") String start, @Param("end") String end);

    /**
     * 查询用户的特征
     *
     * @param userId ：userId=null -》所有用户
     *               否则为指定用户
     * @return
     */
    /**
     "<if test=\"userId != null\">\n" +
     "  where id=#{userId}\n" +
     "</if>" +
     * @param userId
     * @return
     */
    @Select("<script>" +
            "select t.*,t0.last_order_time,t1.last_car_time,t2.last_visit_time from \n" +
            "(select id as user_id,order_count as buy_count,total_order_amount as buy_amount,last_consume_time as last_buy_time \n" +
            "    from  mall_member_user mmu \n" +
            "<if test=\"userId != null\">\n" +
            "  where id=#{userId}\n" +
            "</if>" +
            ") t\n" +
            "left join \n" +
            "(select user_id,max(create_time) as last_order_time from mall_order mo group by user_id) t0 \n" +
            "on t.user_id=t0.user_id\n" +
            "left join \n" +
            "(select user_id,max(create_time) as last_car_time from mall_shop_cart msc group by user_id) t1\n" +
            "on t.user_id=t1.user_id\n" +
            "left join \n" +
            "(select user_id,max(access_time) as last_visit_time  from sys_logininfor sl where sl.user_id is not null group by user_id ) t2\n" +
            "on t.user_id=t2.user_id\n"+
            "</script>")
    List<MemberGroupUserTraitDetailDTO> memberUserTrait(@Param("userId") Long userId);

    /**
     * 范围内每日注册用户数量
     *
     * @param wrapper  查询条件
     * @return
     */
    @Select("select DATE_FORMAT(create_time , '%Y-%m-%d') as dateStr,count(*) as value " +
            "from mall_member_user " +
            "${ew.customSqlSegment} group by dateStr")
    List<StatisticalTrendViewDTO> newMemberCountTrend(@Param(Constants.WRAPPER) LambdaQueryWrapper<MemberUser> wrapper);


    void updateConsume(@Param("userId") Long id, @Param("payPrice") BigDecimal payPrice);

    void updateUserRefundData(@Param("userId")Long userId, @Param("refundAmount")BigDecimal refundAmount);

}
