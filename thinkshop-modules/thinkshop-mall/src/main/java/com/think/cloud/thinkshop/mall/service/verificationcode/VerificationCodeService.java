package com.think.cloud.thinkshop.mall.service.verificationcode;

/**
 * @author zkthink
 * @apiNote
 **/
public interface VerificationCodeService {
    /**
     * 注册发送验证码
     * @param email /
     */
    boolean sendEmailCode(String email);

    /**
     * 忘记密码发送验证码
     * @param email /
     */

    void sendEmailCodeForget(String email);

    /**
     * 邮箱验证码-更新用户信息
     * @param userId /
     */
    void sendEmailCodeUpdate(Long userId);

    /**
     * 验证码是否正确
     * @param verificationCodeRegisterKey 前缀
     * @param email 邮箱
     * @param code 验证码
     */

    void checkVerificationCode(String verificationCodeRegisterKey, String email, String code);

    /**
     * 删除验证码
     * @param verificationCodeRegisterKey 前缀
     * @param email 邮箱
     */
    void deleteVerificationCode(String verificationCodeRegisterKey, String email);
}
