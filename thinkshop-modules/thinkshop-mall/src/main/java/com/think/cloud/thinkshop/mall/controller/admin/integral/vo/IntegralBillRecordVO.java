package com.think.cloud.thinkshop.mall.controller.admin.integral.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.think.common.core.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 积分流水
 *
 * @author moxiangrong
 * @date 2024-07-17
 */

@Data

public class IntegralBillRecordVO {
    /**
     * id
     */
    private Long id;
    private Long userId;
    /**
     * 积分值
     */
    @Excel(name = "积分值")
    private String integral;

    /**
     * 账单类型 1：增加  2：消费 3：过期
     */
    @Excel(name = "账单类型 1：增加  2：消费 3：过期")
    private Integer billType;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    private String firstName;
    private String lastName;
    private String email;
    private Integer userIntegral;
}
