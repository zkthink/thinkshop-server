package com.think.cloud.thinkshop.mall.service.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagRefDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTagRef;

import java.util.List;

/**
 * 用户标签关联Service接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
public interface IMemberTagRefService
{
    /**
     * 查询用户标签关联
     *
     * @param id 用户标签关联主键
     * @return 用户标签关联
     */
    public MemberTagRef selectMemberTagRefById(Long id);

    /**
     * 查询用户标签关联列表
     *
     * @param memberTagRef 用户标签关联
     * @return 用户标签关联集合
     */
    public List<MemberTagRef> selectMemberTagRefList(MemberTagRef memberTagRef);

    /**
     * 新增用户标签关联
     *
     * @param memberTagRef 用户标签关联
     * @return 结果
     */
    public int insertMemberTagRef(MemberTagRef memberTagRef);

    /**
     * 修改用户标签关联
     *
     * @param memberTagRef 用户标签关联
     * @return 结果
     */
    public int updateMemberTagRef(MemberTagRef memberTagRef);

    /**
     * 批量删除用户标签关联信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    /**
     * 根据标签id集合查询用户标签关联信息
     *
     * @param idList 标签id集合
     * @return 结果
     */
    List<MemberTagRef> selectByTagIds(List<Long> idList);

    /**
     * 根据标签id集合删除用户标签关联信息
     *
     * @param tagIdList 标签id集合
     * @return 结果
     */
    void deleteByTagIds(List<Long> tagIdList);

    /**
     * 根据用户id集合查询用户标签关联信息
     *
     * @param userIds 用户id集合
     * @return 结果
     */
    List<MemberTagRefDTO> selectByUserIds(List<Long> userIds);

    void deleteByUserId(Long userId);
}
