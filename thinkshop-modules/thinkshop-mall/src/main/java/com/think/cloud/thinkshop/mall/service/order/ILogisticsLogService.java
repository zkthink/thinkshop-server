package com.think.cloud.thinkshop.mall.service.order;

import java.util.List;
import com.think.cloud.thinkshop.mall.domain.LogisticsLog;
import com.think.cloud.thinkshop.mall.domain.order.Order;

/**
 * 物流日志Service接口
 *
 * @author zkthink
 * @date 2024-05-31
 */
public interface ILogisticsLogService
{
    /**
     * 查询物流日志
     *
     * @param id 物流日志主键
     * @return 物流日志
     */
    public LogisticsLog selectLogisticsLogById(Long id);

    /**
     * 查询物流日志列表
     *
     * @param logisticsLog 物流日志
     * @return 物流日志集合
     */
    public List<LogisticsLog> selectLogisticsLogList(LogisticsLog logisticsLog);

    /**
     * 新增物流日志
     *
     * @param logisticsLog 物流日志
     * @return 结果
     */
    public int insertLogisticsLog(LogisticsLog logisticsLog);

    /**
     * 修改物流日志
     *
     * @param logisticsLog 物流日志
     * @return 结果
     */
    public int updateLogisticsLog(LogisticsLog logisticsLog);

    /**
     * 批量删除物流日志信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    /**
     * 注册物流信息
     * @param deliveryId 快递单号
     * @param deliveryName 公司名称
     * @param deliverySn 公司编码
     */
    void register(Long orderId,Integer type,String deliveryId,String deliveryName,String deliverySn);

    /**
     * 根据订单id查询物流信息
     * @param orderId 订单id
     * @return  /
     */
    LogisticsLog selectByOrderId(Long orderId);

    /**
     * 17track webhook
     * @param body /
     */
    void webHook17Track(String body);

    LogisticsLog selectByDeliveryId(String deliveryId);
}
