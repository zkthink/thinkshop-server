package com.think.cloud.thinkshop.mall.convert.websitesetting;

import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto.WebsiteSettingDTO;
import com.think.cloud.thinkshop.mall.controller.app.websitesetting.vo.WebsiteSettingVO;
import com.think.cloud.thinkshop.mall.domain.websitesetting.WebsiteSetting;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author zkthink
 * @apiNote
 **/
@Mapper
public interface WebsiteSettingConvert {
    WebsiteSettingConvert INSTANCE = Mappers.getMapper(WebsiteSettingConvert.class);
    WebsiteSettingDTO convert(WebsiteSetting bean);
    WebsiteSetting convert(WebsiteSettingDTO bean);

    WebsiteSettingVO convert01(WebsiteSetting bean);

}
