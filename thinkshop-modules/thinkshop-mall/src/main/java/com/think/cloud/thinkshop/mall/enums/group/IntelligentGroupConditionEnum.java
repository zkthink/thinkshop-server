package com.think.cloud.thinkshop.mall.enums.group;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 分组智能筛选枚举
 */
@Getter
@AllArgsConstructor
public enum IntelligentGroupConditionEnum {

    CONDITION_GT(0, ">"),
    CONDITION_LT(1, "<"),
    CONDITION_EQ(2, "=");

    private Integer type;
    private String value;


    public static IntelligentGroupConditionEnum toType(int type) {
        return Stream.of(IntelligentGroupConditionEnum.values())
                .filter(p -> p.type == type)
                .findAny()
                .orElse(null);
    }


}
