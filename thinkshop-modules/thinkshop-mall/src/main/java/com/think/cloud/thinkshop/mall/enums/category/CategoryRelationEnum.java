package com.think.cloud.thinkshop.mall.enums.category;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 关联状态枚举
 */
@Getter
@AllArgsConstructor
public enum CategoryRelationEnum {

    NO(0, "未关联"),
    YES(1, "已关联");

    private Integer value;
    private String desc;

}
