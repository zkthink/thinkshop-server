package com.think.cloud.thinkshop.mall.service.productgroup;

import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.*;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;

/**
 * 商品分组Service接口
 * 
 * @author zkthink
 * @date 2024-05-10
 */
public interface IProductGroupService 
{
    /**
     * 查询商品分组
     * 
     * @param groupId 商品分组主键
     * @return 商品分组
     */
    public ProductGroupDetailRespVO selectProductGroupByGroupId(Long groupId);

    /**
     * 查询商品分组列表
     * 
     * @param vo
     * @return 商品分组集合
     */
    public TableDataInfo selectProductGroupList(ProductGroupPageReqVO vo);

    /**
     * 新增商品分组
     * 
     * @param vo
     * @return 结果
     */
    public void insertProductGroup(SaveProductGroupReqVO vo);

    /**
     * 修改商品分组
     * 
     * @param vo
     * @return 结果
     */
    public void updateProductGroup(UpdateProductGroupReqVO vo);

    /**
     * 批量删除商品分组信息
     * 
     * @param groupIds 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> groupIds);

    /**
     * 商品移除分组
     *
     * @param vo
     * @return 结果
     */
    public void removeProduct(RemoveProductReqVO vo);

    /**
     * 通过组名查找组id
     * @param names
     * @return
     */
    List<Long> findGroupIdByName(List<String> names);
}
