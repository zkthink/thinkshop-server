package com.think.cloud.thinkshop.mall.controller.app.memberuser.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class UpdateUserInfoDTO {

    /** first name */
    @ApiModelProperty(value = "first name")
    private String firstName;

    /** last name */
    @ApiModelProperty(value = "last name")
    private String lastName;
}
