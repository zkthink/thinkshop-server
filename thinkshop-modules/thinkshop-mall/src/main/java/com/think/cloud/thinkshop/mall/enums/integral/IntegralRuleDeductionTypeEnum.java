package com.think.cloud.thinkshop.mall.enums.integral;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IntegralRuleDeductionTypeEnum {
    MAX_INTEGRAL(1, "最多抵扣积分"),
    MAX_SCALE(2, "最大抵扣比例"),
    ;
    private final int code;
    private final String desc;
}
