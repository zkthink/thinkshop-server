package com.think.cloud.thinkshop.mall.controller.app.integral.vo;


import com.think.common.core.annotation.Excel;
import lombok.*;

import java.math.BigDecimal;

/**
 * 积分规则对象 mall_integral_rule
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntegralRuleVO {

    /**
     * id
     */
    private Long id;

    /**
     * 使用可用于抵扣 0：不能 1：能
     */
    private Integer isUse;

    /**
     * 抵扣比值，1积分可以抵扣的金额
     */
    private BigDecimal deductionRatio;

    /**
     * 抵扣类型 1；最多积分 ，2：最高比例
     */
    private Integer deductionType;

    /**
     * 抵扣类型值
     */
    @Excel(name = "抵扣类型值")
    private BigDecimal deductionValue;


}
