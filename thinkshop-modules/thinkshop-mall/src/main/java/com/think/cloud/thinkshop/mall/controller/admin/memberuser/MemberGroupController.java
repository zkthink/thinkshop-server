package com.think.cloud.thinkshop.mall.controller.admin.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberGroupAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberGroupReqVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberGroup;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberGroupService;
import com.think.common.core.domain.R;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * 客户分群Controller
 *
 * @author guangxian
 * @date 2024-06-03
 */
@RestController
@RequestMapping("/admin/memberGroup")
@Api(tags = "ADMIN:客户分群")
public class MemberGroupController extends BaseController {
    @Autowired
    private IMemberGroupService memberGroupService;

    /**
     * 查询客户分群列表
     */
    @RequiresPermissions("mall:mgroup:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询客户分群列表")
    public TableDataInfo list(MemberGroupReqVO vo) {
        startPage();
        List<MemberGroup> list = memberGroupService.selectMemberGroupList(vo);
        return getDataTable(list);
    }

    /**
     * 获取客户分群详细信息
     */
    @RequiresPermissions("mall:mgroup:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取客户分群详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(memberGroupService.selectMemberGroupById(id));
    }

//    /**
//     * 客户群的条件特征选项集合
//     */
//    @RequiresPermissions("mall:group:traitOption")
//    @GetMapping("/traitOption")
//    @ApiOperation(value = "客户群的条件选项")
//    public AjaxResult traitOption() {
//        return success(memberGroupService.getTraitOption());
//    }

    /**
     * 新增客户分群
     */
    @RequiresPermissions("mall:mgroup:add")
    @Log(title = "客户分群", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增客户分群")
    public AjaxResult add(@RequestBody MemberGroupAddReqVO vo) {
        return toAjax(memberGroupService.insertMemberGroup(vo));
    }

    /**
     * 修改客户分群
     */
    @RequiresPermissions("mall:mgroup:edit")
    @Log(title = "客户分群", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改客户分群")
    public AjaxResult edit(@RequestBody MemberGroupAddReqVO vo) {
        return toAjax(memberGroupService.updateMemberGroup(vo));
    }


    /**
     * 查看组中客户详情
     */
    @RequiresPermissions("mall:mgroup:detail")
    @GetMapping("/detail/{groupId}")
    @ApiOperation(value = "查询客户分群列表")
    public TableDataInfo detail(@Valid @PathVariable Long groupId) {
        startPage();
        return memberGroupService.memberGroupDetail(groupId);
    }

    /**
     * 删除客户分群
     */
    @RequiresPermissions("mall:mgroup:remove")
    @Log(title = "客户分群", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除客户分群")
    public AjaxResult remove(@Valid Long[] ids) {
        return toAjax(memberGroupService.batchDelete(Arrays.asList(ids)));
    }




    @GetMapping("/memberGroupUserUpdate")
    public R<Boolean> memberGroupUserUpdate(){
        memberGroupService.memberGroupUserUpdate();
        return R.ok();
    }

    // 用户登录时调用，异步触发客户分群
    @GetMapping("/memberGroupUserUpdateByUserId")
    public R<Boolean> memberGroupByUser(@RequestParam("userId") Long userId){
        memberGroupService.memberGroupUserUpdateByUserIdSync(userId);
        return R.ok();
    }

}
