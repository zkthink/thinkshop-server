package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

/**
 * 售后分页请求VO
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Data
@ToString(callSuper = true)
@Builder
public class AppAfterSalesPageReqVO {

    @ApiModelProperty("订单号或商品名")
    private String key;

    @ApiModelProperty("状态")
    private Integer state;

    private List<Integer> states;

    private Long userId;

}
