package com.think.cloud.thinkshop.mall.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroupTraitConditionEnum {
    GT("1", ">"),
    LT("2", ">"),
    EQ("3", "=");
    private String type;
    private String value;

    public static MemberGroupTraitConditionEnum get(String param) {
        for (MemberGroupTraitConditionEnum memEnum : MemberGroupTraitConditionEnum.values()) {
            if (memEnum.type.equals(param)) {
                return memEnum;
            }
        }
        return null;
    }
}
