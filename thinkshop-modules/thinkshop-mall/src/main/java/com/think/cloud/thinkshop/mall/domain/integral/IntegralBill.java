package com.think.cloud.thinkshop.mall.domain.integral;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.think.cloud.thinkshop.mall.annotation.CrossBorderZoneTimeAmend;
import com.think.common.core.annotation.Excel;
import lombok.*;

import java.sql.Timestamp;

/**
 * 积分流水对象 mall_integral_bill
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@TableName("mall_integral_bill")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntegralBill
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    private Long userId;

    /** 详情 */
    @JsonIgnore
    @Excel(name = "详情")
    private String detail;

    /** 积分值 */
    @Excel(name = "积分值")
    private String integral;

    /** 账单类型 1：增加  2：消费 3：过期 */
    @Excel(name = "账单类型 1：增加  2：消费 3：过期")
    private Integer billType;

    //用户剩余的积分
    private Integer userIntegral;


    /** 是否删除 */
    @Excel(name = "是否删除")
    @JsonIgnore
    private Integer deleted;

    @CrossBorderZoneTimeAmend
    private Timestamp createTime;


}
