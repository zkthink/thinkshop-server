package com.think.cloud.thinkshop.mall.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 优惠券是否使用类型枚举
 */
@Getter
@AllArgsConstructor
public enum CouponSearchTypeEnum {
    CAN_USE(1, "可使用"),
    USED(2, "已使用"),
    EXPIRED(3, "已失效");

    private Integer value;
    private String desc;

    public static CouponSearchTypeEnum toType(int value) {
        return Stream.of(CouponSearchTypeEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
