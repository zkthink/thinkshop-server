package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
@Schema(description = "产品组添加Resp VO")
@Data
public class ProductGroupDesignAddReqVO {
    @Schema(description = "组id", required = true, example = "1")
    private Long groupId;
}
