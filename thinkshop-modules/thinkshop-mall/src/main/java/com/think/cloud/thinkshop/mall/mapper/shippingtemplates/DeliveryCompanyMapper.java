package com.think.cloud.thinkshop.mall.mapper.shippingtemplates;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.DeliveryCompany;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

/**
 * 快递公司Mapper接口
 *
 * @author zkthink
 * @date 2024-06-05
 */
@Mapper
public interface DeliveryCompanyMapper extends BaseMapper<DeliveryCompany> {

    /**
     * 删除所有
     */
    @Delete("delete from mall_delivery_company")
    void deleteAll();
}
