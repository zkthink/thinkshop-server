package com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 商品分组分页请求VO
 *
 * @author zkthink
 * @date 2024-05-10
 */
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductGroupPageReqVO {

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    private String name;

    /**
     * 分组类型
     */
    @ApiModelProperty("分组类型")
    private Integer type;
}
