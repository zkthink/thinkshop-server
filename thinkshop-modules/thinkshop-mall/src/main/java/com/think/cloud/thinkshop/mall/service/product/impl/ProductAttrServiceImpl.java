package com.think.cloud.thinkshop.mall.service.product.impl;

import java.util.List;

import com.think.cloud.thinkshop.mall.service.product.IProductAttrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.think.cloud.thinkshop.mall.mapper.ProductAttrMapper;
import com.think.cloud.thinkshop.mall.domain.ProductAttr;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 商品属性Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Service
public class ProductAttrServiceImpl implements IProductAttrService {
    @Autowired
    private ProductAttrMapper productAttrMapper;

    /**
     * 查询商品属性
     *
     * @param attrId 商品属性主键
     * @return 商品属性
     */
    @Override
    public ProductAttr selectProductAttrByAttrId(Long attrId) {
        return productAttrMapper.selectById(attrId);
    }

    /**
     * 查询商品属性列表
     *
     * @param productAttr 商品属性
     * @return 商品属性
     */
    @Override
    public List<ProductAttr> selectProductAttrList(ProductAttr productAttr) {
        return productAttrMapper.selectList(productAttr);
    }

    /**
     * 批量新增商品属性
     *
     * @param productAttrs 商品属性
     * @return 结果
     */
    @Override
    public void batchInsertProductAttr(List<ProductAttr> productAttrs) {
        productAttrMapper.batchInsertProductAttr(productAttrs);
    }

    /**
     * 修改商品属性
     *
     * @param productAttr 商品属性
     * @return 结果
     */
    @Override
    public int updateProductAttr(ProductAttr productAttr) {
        return productAttrMapper.updateById(productAttr);
    }

    /**
     * 批量删除商品属性信息
     *
     * @param attrIds 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> attrIds) {
        return productAttrMapper.deleteBatchIds(attrIds);
    }

    @Override
    public void deleteByProductId(Long productId) {
        productAttrMapper.delete(new LambdaQueryWrapper<ProductAttr>().eq(ProductAttr::getProductId, productId));
    }
}
