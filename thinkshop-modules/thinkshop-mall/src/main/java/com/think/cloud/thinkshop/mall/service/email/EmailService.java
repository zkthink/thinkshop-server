package com.think.cloud.thinkshop.mall.service.email;

public interface EmailService {
    /**
     * 获取默认标题
     */
    String getDefaultTitle();

    /**
     * 获取默认模版
     */
    String getDefaultTemplate();

    /**
     * 发送验证码到邮箱
     *
     * @param email
     * @param code
     */
    void sendEmail(String email, String code);

    /**
     * 发送普通文本到邮箱
     *
     * @param to      目标邮箱
     * @param subject 标题
     * @param content 内容
     */
    void sendTextEmail(String to, String subject, String content);

    /**
     * 发送富文本内容
     *
     * @param to      目标邮箱
     * @param subject 标题
     * @param content 内容
     */
    void sendHtmlMail(String to, String subject, String content);

}
