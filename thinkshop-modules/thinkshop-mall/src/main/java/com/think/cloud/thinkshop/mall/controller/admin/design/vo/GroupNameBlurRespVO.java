package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

@ToString(callSuper = true)
@Schema(description = "商品分组模糊查询")
@Data
public class GroupNameBlurRespVO {
    @Schema(description = "组名称", required = true, example = "1")
    private String name;

    @Schema(description = "组id", required = true, example = "1")
    private Long groupId;
}
