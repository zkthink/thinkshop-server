package com.think.cloud.thinkshop.mall.convert.address;

import com.think.cloud.thinkshop.mall.controller.app.address.vo.AddUserAddressReqVO;
import com.think.cloud.thinkshop.mall.controller.app.address.vo.UpdateUserAddressReqVO;
import com.think.cloud.thinkshop.mall.controller.app.address.vo.UserAddressDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.app.address.vo.UserAddressPageRespVO;
import com.think.cloud.thinkshop.mall.domain.UserAddress;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 用户地址 Convert
 *
 * @author zkthink
 */
@Mapper
public interface UserAddressConvert {

    UserAddressConvert INSTANCE = Mappers.getMapper(UserAddressConvert.class);

    UserAddress convert(AddUserAddressReqVO vo);

    UserAddress convert(UpdateUserAddressReqVO vo);

    UserAddressDetailRespVO convert(UserAddress bean);

    List<UserAddressPageRespVO> convertList(List<UserAddress> list);
}
