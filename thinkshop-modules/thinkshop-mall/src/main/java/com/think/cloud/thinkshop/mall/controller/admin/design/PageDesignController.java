package com.think.cloud.thinkshop.mall.controller.admin.design;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.PageConfigAddOrEditReqVO;
import com.think.cloud.thinkshop.mall.service.design.IPageConfigService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/pageDesign")
@Api(tags = "ADMIN:页眉页脚设计")
public class PageDesignController extends BaseController {
    @Autowired
    private IPageConfigService pageConfigService;

    /**
     * 页眉详细信息
     */
    @RequiresPermissions("mall:page:query")
    @GetMapping(value = "/get/pageTop")
    @ApiOperation(value = "获取页眉详细信息")
    public AjaxResult pageTop() {
        return success(pageConfigService.selectPageConfigTop());
    }
    /**
     *页脚详细信息
     */
    @RequiresPermissions("mall:page:query")
    @GetMapping(value = "/get/pageBottom")
    @ApiOperation(value = "获取页脚详细信息")
    public AjaxResult pageBottom() {
        return success(pageConfigService.selectPageConfigBottom());
    }

    /**
     * 新增
     */
    @RequiresPermissions("mall:page:add")
    @Log(title = "PageConfig", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增 pageConfig")
    public AjaxResult add(@RequestBody PageConfigAddOrEditReqVO vo) {
        return toAjax(pageConfigService.insertOrUpdatePageConfig(vo));
    }
    /**
     * 更新
     */
    @RequiresPermissions("mall:page:edit")
    @Log(title = "PageConfig", businessType = BusinessType.UPDATE)
    @PutMapping("/edit")
    @ApiOperation(value = "编辑 pageConfig")
    public AjaxResult edit(@RequestBody PageConfigAddOrEditReqVO vo) {
        return toAjax(pageConfigService.insertOrUpdatePageConfig(vo));
    }

}
