package com.think.cloud.thinkshop.mall.controller.app.coupon;


import com.think.cloud.thinkshop.mall.service.coupon.AppProductCouponRelationService;
import com.think.cloud.thinkshop.mall.service.coupon.AppProductCouponService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品优惠券AppController
 *
 * @author zkthink
 * @date 2024-05-21
 */
@RestController
@RequestMapping("/app/coupon")
@Api(tags = "APP:商品优惠券")
public class AppProductCouponController extends BaseController {

    @Autowired
    AppProductCouponService appProductCouponService;

    @Autowired
    AppProductCouponRelationService appProductCouponRelationService;

    @GetMapping("/receive-list/{productId}")
    @Operation(summary = "查询商品可领优惠券")
    public TableDataInfo list(@PathVariable Long productId) {
        return getDataTable(appProductCouponService.receiveList(productId, SecurityUtils.getUserId()));
    }

    @GetMapping("/receive/{id}")
    @Operation(summary = "领取优惠券")
    public AjaxResult receiveCoupon(@PathVariable Long id) {
        appProductCouponRelationService.receiveCoupon(id, SecurityUtils.getUserId(),1,null);
        return success(Boolean.TRUE);
    }

    @GetMapping("/searchUserCoupon/{type}")
    @Operation(summary = "查询优惠券")
    @Parameter(name = "type", description = "查询类型：1、可使用，2、已使用，3、已失效", required = true, example = "1")
    public TableDataInfo searchUserCoupon(@PathVariable Integer type) {
        return getDataTable(appProductCouponRelationService.searchUserCoupon(type, null, SecurityUtils.getUserId()));
    }

    @GetMapping("/redeem/{couponCode}")
    @Operation(summary = "兑换优惠券")
    public AjaxResult redeem(@PathVariable String couponCode) {
        appProductCouponRelationService.redeem(couponCode, SecurityUtils.getUserId());
        return success(Boolean.TRUE);
    }
}
