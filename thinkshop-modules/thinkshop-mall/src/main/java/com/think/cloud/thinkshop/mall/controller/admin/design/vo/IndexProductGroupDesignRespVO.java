package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@Schema(description = "管理后台 - 首页的商品组合列表")
public class IndexProductGroupDesignRespVO {
    private Long id;
    @Schema(description = "组id", required = true, example = "1")
    private Long groupId;
    @Schema(description = "组名称", required = true, example = "xxx")
    private String name;
    @Schema(description = "组描述", required = true, example = "xxx")
    private String remark;
    @Schema(description = "状态", required = true, example = "1")
    private Integer status;
}
