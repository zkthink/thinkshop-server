package com.think.cloud.thinkshop.mall.controller.app.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;


/**
 * 订单再次购买响应VO
 *
 * @author zkthink
 * @date 2024-05-31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppOrderPurchaseAgainRespVO {

    @ApiModelProperty("购物车ID集合")
    private List<Long> cartId;

}
