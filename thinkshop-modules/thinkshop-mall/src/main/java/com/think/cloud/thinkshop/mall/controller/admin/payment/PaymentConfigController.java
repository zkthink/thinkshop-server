package com.think.cloud.thinkshop.mall.controller.admin.payment;


import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.think.cloud.thinkshop.mall.service.payment.IPaymentConfigService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentConfig;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 支付配置Controller
 *
 * @author zkthink
 * @date 2024-05-14
 */
@RestController
@RequestMapping("/admin/payment-config")
@Api(tags = "ADMIN:支付配置")
public class PaymentConfigController extends BaseController
{
    @Autowired
    private IPaymentConfigService paymentConfigService;

    /**
     * 查询支付配置列表
     */
    @RequiresPermissions("mall:payment-config:list")
    @GetMapping("/list")
    public TableDataInfo list(PaymentConfig paymentConfig)
    {
        startPage();
        List<PaymentConfig> list = paymentConfigService.selectPaymentConfigList(paymentConfig);
        return getDataTable(list);
    }

    /**
     * 导出支付配置列表
     */
    @RequiresPermissions("mall:payment-config:export")
    @Log(title = "支付配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PaymentConfig paymentConfig)
    {
        List<PaymentConfig> list = paymentConfigService.selectPaymentConfigList(paymentConfig);
        ExcelUtil<PaymentConfig> util = new ExcelUtil<PaymentConfig>(PaymentConfig.class);
        util.exportExcel(response, list, "支付配置数据");
    }

    /**
     * 获取支付配置详细信息
     */
    @RequiresPermissions("mall:payment-config:query")
    @GetMapping(value = "/get/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(paymentConfigService.selectPaymentConfigById(id));
    }

    /**
     * 新增支付配置
     */
    @RequiresPermissions("mall:payment-config:add")
    @Log(title = "支付配置", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody PaymentConfig paymentConfig)
    {
        return toAjax(paymentConfigService.insertPaymentConfig(paymentConfig));
    }

    /**
     * 修改支付配置
     */
    @RequiresPermissions("mall:payment-config:edit")
    @Log(title = "支付配置", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody PaymentConfig paymentConfig)
    {
        return toAjax(paymentConfigService.updatePaymentConfig(paymentConfig));
    }

    /**
     * 删除支付配置
     */
    @RequiresPermissions("mall:payment-config:remove")
    @Log(title = "支付配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete")
    public AjaxResult remove(@Valid Long[] ids)
    {
        return toAjax(paymentConfigService.batchDelete(Arrays.asList(ids)));
    }
}
