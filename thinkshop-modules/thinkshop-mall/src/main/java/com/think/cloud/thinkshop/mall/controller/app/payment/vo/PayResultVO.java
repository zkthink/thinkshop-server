package com.think.cloud.thinkshop.mall.controller.app.payment.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class PayResultVO {
    @ApiModelProperty("支付跳转地址 stripe时返回结果为clientSecret")
    private String payUrl;

    public PayResultVO(String payUrl) {
        this.payUrl = payUrl;
    }

    public PayResultVO() {
    }
}
