package com.think.cloud.thinkshop.mall.domain.shippingtemplates;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 快递公司对象 mall_delivery_company
 *
 * @author zkthink
 * @date 2024-06-05
 */
@TableName("mall_delivery_company")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryCompany
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    @TableField("`key`")
    private String key;

    /** 国家编号 */
    @Excel(name = "国家编号")
    private String country;

    /** 国家 */
    @Excel(name = "国家")
    private String countryIso;

    /** 邮件 */
    @Excel(name = "邮件")
    private String email;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String tel;

    /** 网站地址 */
    @Excel(name = "网站地址")
    private String url;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String name;

    /** 公司名称(中文) */
    @Excel(name = "公司名称(中文)")
    private String nameZhCn;

    /** 公司名称(香港) */
    @Excel(name = "公司名称(香港)")
    private String nameZhHk;

    /** 分组 */
    @Excel(name = "分组")
    @TableField("`group`")
    private String group;

}
