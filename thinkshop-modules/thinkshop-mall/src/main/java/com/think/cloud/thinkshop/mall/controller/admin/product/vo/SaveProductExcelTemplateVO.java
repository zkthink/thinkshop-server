package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import com.think.cloud.thinkshop.mall.handler.ExcelImage2UrlHandler;
import com.think.common.core.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 商品导入导出模版
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SaveProductExcelTemplateVO {
    private Long productId;

    @Excel(name = "商品名称", height = 40,prompt="填写商品名称，如果是商品规格明细则末尾加上 |规格明细")
    private String productName;
    @Excel(name = "商品简介", height = 40, prompt="请填写商品简介")
    private String introduce;
    @Excel(name = "商品详情描述", height = 60,prompt="请填写商品描述详情")
    private String detail;
    @Excel(name = "商品/规格图片", height = 40, prompt = "请使用wps嵌入图片到单元格", cellType = Excel.ColumnType.IMAGE, handler = ExcelImage2UrlHandler.class)
    private String image;
    @Excel(name = "规格类型", height = 40, width = 10, prompt = "请选择商品规格类型",combo = "单规格,多规格", readConverterExp = "1=单规格,2=多规格")
    private Integer skuType;
    @Excel(name = "规格属性", height = 40, width = 40, prompt = "请参照模版填写,符号均为英文符号,规格信息中不能含有此处没有的属性配置,单规格时可不填")
    private String attr;
    @Excel(name = "规格信息", height = 40, width = 40, prompt = "请参照模版填写,多规格配置请换行,单规格时只有第一条生效")
    private String value;
    @Excel(name = "商品类目", height = 40, prompt = "请填写已存在的商品类目,仅支持填写一个类目")
    private String category;

    @Excel(name = "商品分组", height = 40, prompt = "请填写已存在的商品分组，多个请用 | 符号隔开")
    private String groups;

    @Excel(name = "是否收税", height = 40, prompt = "商品中填写,规格详情中不填",combo = "否,是", readConverterExp = "0=否,1=是,-1=×")
    private Integer tax;

    @Excel(name = "上架状态", height = 40, prompt = "商品中填写,规格详情中不填",combo = "未上架,已上架", readConverterExp = "0=未上架,1=已上架,-1=×")
    private Integer isShow;

    private Long categoryId;
//    @Excel(name = "属性集合", height = 40)
//    private List<ProductAttrReqVO> attrs;
//
//    @Excel(name = "sku集合", height = 40)
//    private List<ProductSkuReqVO> skus;

}
