package com.think.cloud.thinkshop.mall.domain.aftersales;

import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;

/**
 * 售后明细对象 mall_after_sales_detail
 *
 * @author zkthink
 * @date 2024-06-26
 */
@TableName("mall_after_sales_detail")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AfterSalesDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 售后明细id */
    private Long id;

    /** 售后id */
    @Excel(name = "售后id")
    private Long afterSalesId;

    /** 订单明细 */
    @Excel(name = "订单明细")
    private Long orderDetailId;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

}
