package com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates;


import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.ObjectUtil;
import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.dto.ShippingTemplatesDTO;
import com.think.cloud.thinkshop.mall.service.shippingtemplates.IShippingTemplatesService;
import com.think.common.core.exception.ServiceException;
import com.think.common.core.exception.enums.ErrorCode;
import com.think.common.core.exception.util.ServiceExceptionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplates;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 物流方案Controller
 *
 * @author zkthink
 * @date 2024-05-15
 */
@RestController
@RequestMapping("/admin/shipping-templates")
@Api(tags = "ADMIN:物流方案")
public class ShippingTemplatesController extends BaseController
{
    @Autowired
    private IShippingTemplatesService shippingTemplatesService;

    /**
     * 查询物流方案列表
     */
    @RequiresPermissions("mall:shipping-templates:list")
    @GetMapping("/list")
    @ApiOperation("查询物流方案列表")
    public TableDataInfo list(ShippingTemplates shippingTemplates)
    {
        startPage();
        List<ShippingTemplates> list = shippingTemplatesService.selectShippingTemplatesList(shippingTemplates);
        return getDataTable(list);
    }

    /**
     * 导出物流方案列表
     */
    @RequiresPermissions("mall:shipping-templates:export")
    @Log(title = "物流方案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("导出物流方案列表")
    public void export(HttpServletResponse response, ShippingTemplates shippingTemplates)
    {
        List<ShippingTemplates> list = shippingTemplatesService.selectShippingTemplatesList(shippingTemplates);
        ExcelUtil<ShippingTemplates> util = new ExcelUtil<ShippingTemplates>(ShippingTemplates.class);
        util.exportExcel(response, list, "物流方案数据");
    }

    /**
     * 获取物流方案详细信息
     */
    @RequiresPermissions("mall:shipping-templates:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation("获取物流方案详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(shippingTemplatesService.selectShippingTemplatesById(id));
    }

    /**
     * 新增物流方案
     */
    @RequiresPermissions("mall:shipping-templates:add")
    @Log(title = "物流方案", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("新增物流方案")
    public AjaxResult add(@RequestBody ShippingTemplatesDTO shippingTemplates)
    {
        checkParam(shippingTemplates);
        return toAjax(shippingTemplatesService.insertShippingTemplates(shippingTemplates));
    }

    /**
     * 修改物流方案
     */
    @RequiresPermissions("mall:shipping-templates:edit")
    @Log(title = "物流方案", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation("修改物流方案")
    public AjaxResult edit(@RequestBody ShippingTemplatesDTO shippingTemplates)
    {
        checkParam(shippingTemplates);
        return toAjax(shippingTemplatesService.updateShippingTemplates(shippingTemplates));
    }

    /**
     * 删除物流方案
     */
    @RequiresPermissions("mall:shipping-templates:remove")
    @Log(title = "物流方案", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete/{id}")
    @ApiOperation("删除物流方案")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(shippingTemplatesService.deleteById(id));
    }

    private void checkParam(ShippingTemplatesDTO shippingTemplates){
        if(ObjectUtil.isEmpty(shippingTemplates.getRegionIds())){
            throw ServiceExceptionUtil.exception(ErrorCode.PARAMETER_ERROR);
        }
    }
}
