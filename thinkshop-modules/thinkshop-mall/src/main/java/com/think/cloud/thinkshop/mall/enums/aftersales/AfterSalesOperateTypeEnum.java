package com.think.cloud.thinkshop.mall.enums.aftersales;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 售后操作类型枚举
 */
@Getter
@AllArgsConstructor
public enum AfterSalesOperateTypeEnum {

    SUBMIT_APPLICATION(1, "提交申请"),
    APPROVAL_SUCCESSFUL(2, "审核成功"),
    GOODS_RETURNED(3, "货品已寄回"),
    MERCHANT_AGREED_TO_REFUND(4, "商家同意退款"),
    REFUND_SUCCESSFUL(5, "退款成功"),
    APPROVAL_FAILED(6, "审核失败"),
    USER_CANCELED(7, "用户取消"),
    AUTOMATICALLY_CANCELED_FOR_TIMEOUT(8, "超时自动取消");

    private Integer value;
    private String desc;


    public static AfterSalesOperateTypeEnum toEnum(Integer value) {
        return Stream.of(AfterSalesOperateTypeEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
