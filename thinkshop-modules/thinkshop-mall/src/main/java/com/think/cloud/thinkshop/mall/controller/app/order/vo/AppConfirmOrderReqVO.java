package com.think.cloud.thinkshop.mall.controller.app.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 确认订单请求VO
 *
 * @author zkthink
 * @date 2024-05-24
 */
@Data
public class AppConfirmOrderReqVO extends AppOrderCommonReqVO {
    /**
     * 0-计算所用费用；1-只计算商品总价
     */
    @ApiModelProperty("类型  0-计算所用费用；1-只计算商品总价")
    private Integer type;
}
