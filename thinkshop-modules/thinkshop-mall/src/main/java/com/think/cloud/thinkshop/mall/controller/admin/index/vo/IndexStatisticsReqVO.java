package com.think.cloud.thinkshop.mall.controller.admin.index.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

@Data
@Schema(description = "首页-统计查询的参数")
@ToString(callSuper = true)
public class IndexStatisticsReqVO {
    @ApiModelProperty(value = "查询起始时间，例子：2020-03-05 00:00:00")
    private String startTime;
    @ApiModelProperty(value = "查询结束时间,例子：2020-03-08 00:00:00")
    private String endTime;
}
