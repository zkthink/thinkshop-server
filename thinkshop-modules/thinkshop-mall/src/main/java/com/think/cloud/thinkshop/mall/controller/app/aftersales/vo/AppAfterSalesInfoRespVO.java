package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 售后信息响应VO
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Data
@ToString(callSuper = true)
@Builder
public class AppAfterSalesInfoRespVO {

    @ApiModelProperty("售后单id")
    private Long id;

    @ApiModelProperty("售后单号")
    private String afterSalesCode;

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("订单编号")
    private String orderCode;

    @ApiModelProperty("退货数量")
    private Integer number;

    @ApiModelProperty("商品总价")
    private BigDecimal totalPrice;

    @ApiModelProperty("邮费")
    private BigDecimal totalPostage;

    @ApiModelProperty("优惠券金额")
    private BigDecimal couponPrice;

    @ApiModelProperty("税费")
    private BigDecimal taxation;

    @ApiModelProperty("退款金额")
    private BigDecimal refundAmount;

    @ApiModelProperty("服务类型：0、仅退款，1、退货退款")
    private Integer serviceType;

    @ApiModelProperty("申请原因")
    private String reasons;

    @ApiModelProperty("说明")
    private String explains;

    @ApiModelProperty("说明图片")
    private String explainImg;

    @ApiModelProperty("物流公司编码")
    private String shipperCode;

    @ApiModelProperty("物流单号")
    private String deliverySn;

    @ApiModelProperty("物流名称")
    private String deliveryName;

    @ApiModelProperty("状态：-2、商家拒绝，-1、用户取消，0、已提交等待平台审核，1、平台已审核 等待用户发货，2、用户已发货，3、退款成功")
    private Integer state;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("商家收货人")
    private String consignee;

    @ApiModelProperty("商家手机号")
    private String phoneNumber;

    @ApiModelProperty("商家地址")
    private String address;

    @ApiModelProperty("商家国家")
    private String country;

    @ApiModelProperty("商家邮编")
    private String postCode;

    @ApiModelProperty("退货说明")
    private String returnPolicy;

    @ApiModelProperty("退货凭证")
    private String returnVoucher;

    @ApiModelProperty("收货状态：0、未收到，1、已收到")
    private Integer receivingStatus;

    @ApiModelProperty("售后明细")
    private List<AppAfterSalesDetailRespVO> details;

    @ApiModelProperty("售后操作记录")
    private List<AppAfterSalesLogRespVO> logs;

    @ApiModelProperty("订单扣除积分值")
    private Integer useIntegral;

    @ApiModelProperty("积分抵扣的金额")
    private BigDecimal integralDeduct;

}
