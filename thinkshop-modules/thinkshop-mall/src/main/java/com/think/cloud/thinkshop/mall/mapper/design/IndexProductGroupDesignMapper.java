package com.think.cloud.thinkshop.mall.mapper.design;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.GroupNameBlurRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexProductGroupDesignRespVO;
import com.think.cloud.thinkshop.mall.controller.app.design.vo.AppIndexProductGroupDetailVO;
import com.think.cloud.thinkshop.mall.domain.design.IndexProductGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * IndexDesignMapper
 *
 * @author dengguanxian
 * @date 2024-05-30
 */
@Mapper
public interface IndexProductGroupDesignMapper extends BaseMapper<IndexProductGroup> {
    /**
     * 获取首页的产品组信息
     * @param status
     * @return
     */
    @Select("<script>" +
            "select mipg.*,mpg.name,mpg.remark \n" +
            "from \n" +
            "(select id,group_id,status \n" +
            "from mall_index_product_group\n" +
            "where deleted = 0 \n" +
            "<if test=\"status!=null and status !=''\">" +
            "and status=#{status}" +
            "</if>" +
            ") mipg \n" +
            "left join mall_product_group mpg on mipg.group_id=mpg.group_id</script>")
    List<IndexProductGroupDesignRespVO> getProductGroupDesignList(@Param("status") Integer status);

    /**
     * 模糊查询商品组名
     * @param name
     * @return
     */
    @Select("<script>" +
            "select name,group_id from mall_product_group\n" +
            "where deleted=0 " +
            "<if test=\"name!=null and name !=''\">" +
            "and name like '%#{name}%'" +
            "</if>" +
            "</script>")
    List<GroupNameBlurRespVO> getGroupNameBlur(@Param("name") String name);

    /**
     * 查看商品组中的商品详情
     * @param groupId
     * @return
     */
    @Select("select t1.*,t2.* from\n" +
            "(select group_id,product_id from mall_product_group_relation where group_id=#{groupId}) t1\n" +
            "inner join \n" +
            "(select product_id,product_name,introduce,image,min_price,max_price from mall_product \n" +
            "where deleted = 0 and is_show = 1) t2 \n" +
            "on t1.product_id=t2.product_id")
    List<AppIndexProductGroupDetailVO> selectProductDetailByGroupId(@Param("groupId") Long groupId);
}

