package com.think.cloud.thinkshop.mall.controller.admin.memberuser;


import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberUserQueryDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.UpdateTagDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberUserVO;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberUserService;
import com.think.cloud.thinkshop.system.api.dto.AuthUserDTO;
import com.think.cloud.thinkshop.system.api.dto.MemberUserDTO;
import com.think.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberUser;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 商城用户Controller
 *
 * @author zkthink
 * @date 2024-05-09
 */
@RestController
@RequestMapping("/admin/user")
@Api(tags = "ADMIN:商城用户")
public class MemberUserController extends BaseController
{
    @Autowired
    private IMemberUserService memberUserService;

    /**
     * 查询商城用户列表
     */
    @RequiresPermissions("mall:user:list")
    @GetMapping("/list")
    @ApiOperation(value = "商城用户列表")
    public TableDataInfo list(MemberUserQueryDTO memberUser)
    {
        return memberUserService.selectMemberUserPage(memberUser);
    }

    /**
     * 导出商城用户列表
     */
    @RequiresPermissions("mall:user:export")
    @Log(title = "商城用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "商城用户导出")
    public void export(HttpServletResponse response, MemberUserQueryDTO memberUser)
    {
        List<MemberUserVO> list = memberUserService.selectMemberUserList(memberUser);
        ExcelUtil<MemberUserVO> util = new ExcelUtil<>(MemberUserVO.class);
        util.exportExcel(response, list, "商城用户数据");
    }

    /**
     * 获取商城用户详细信息
     */
    @RequiresPermissions("mall:user:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "商城用户详情")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(memberUserService.selectMemberUserVOById(id));
    }

    /**
     * 新增商城用户
     */
    @RequiresPermissions("mall:user:add")
    @Log(title = "商城用户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "商城用户新增")
    public AjaxResult add(@RequestBody MemberUser memberUser)
    {
        return toAjax(memberUserService.insertMemberUser(memberUser));
    }

    /**
     * 修改商城用户
     */
    @RequiresPermissions("mall:user:edit")
    @Log(title = "商城用户", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "商城用户修改")
    public AjaxResult edit(@RequestBody MemberUser memberUser)
    {
        return toAjax(memberUserService.updateMemberUser(memberUser));
    }

    /**
     * 删除商城用户
     */
    @RequiresPermissions("mall:user:remove")
    @Log(title = "商城用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete")
    @ApiOperation(value = "商城用户删除")
    public AjaxResult remove(@Valid Long[] ids)
    {
        return toAjax(memberUserService.batchDelete(Arrays.asList(ids)));
    }

    @GetMapping("/getByUsername")
    @ApiOperation(value = "商城用户根据用户名查询")
    public R<MemberUserDTO> getByUsername(@RequestParam(value = "username",required = true)String username)
    {
        return R.ok(memberUserService.getByUsername(username));
    }

    @PostMapping("/oauthCallback")
    public R<MemberUserDTO> oauthCallback(@RequestBody AuthUserDTO authUserDTO){
        return R.ok(memberUserService.oauthCallback(authUserDTO));
    }

    @PostMapping("updateTag")
    @ApiOperation(value = "商城用户修改标签")
    public AjaxResult updateTag(@RequestBody UpdateTagDTO updateTagDTO){
        memberUserService.updateTag(updateTagDTO);
        return AjaxResult.success();
    }
}
