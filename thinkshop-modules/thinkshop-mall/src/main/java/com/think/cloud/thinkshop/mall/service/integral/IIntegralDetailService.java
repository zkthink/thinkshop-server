package com.think.cloud.thinkshop.mall.service.integral;


import com.think.cloud.thinkshop.mall.domain.integral.IntegralDetail;
import com.think.cloud.thinkshop.mall.enums.integral.IntegralBillTypeEnum;

import java.util.List;

/**
 * 积分详情Service接口
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
public interface IIntegralDetailService
{

    /**
     * 查询积分详情
     *
     * @param id 积分详情主键
     * @return 积分详情
     */
    public IntegralDetail selectIntegralDetailById(Long id);

    /**
     * 查询积分详情列表
     *
     * @param integralDetail 积分详情
     * @return 积分详情集合
     */
    public List<IntegralDetail> selectIntegralDetailList(IntegralDetail integralDetail);



    /**
     * 修改积分详情
     *
     * @param integralDetail 积分详情
     * @return 结果
     */
    public int updateIntegralDetailById(IntegralDetail integralDetail);

    /**
     * 批量删除积分详情信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);
    /**
     * 新增积分详情
     *
     * @return 结果
     */
    public int insertIntegralDetail(Long userId, Integer integral, IntegralBillTypeEnum billTypeEnum);

    /**
     * 计算用户当前的可用总积分
     * @param userId
     * @return
     */
    int getUserCurrentIntegral(Long userId);

    /**
     * 扣除积分
     * @param userId
     * @return
     */
    Boolean reduceUserIntegral(Long userId,int num);

    /**
     * 清空到期积分
     */
   void cleanIntegral(Integer cleanType);



}
