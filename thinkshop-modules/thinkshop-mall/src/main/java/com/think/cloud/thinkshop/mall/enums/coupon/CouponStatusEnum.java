package com.think.cloud.thinkshop.mall.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 优惠券状态类型枚举
 */
@Getter
@AllArgsConstructor
public enum CouponStatusEnum {
    NOT_STARTED(0, "未开始"),
    IN_PROGRESS(1, "进行中"),
    ENDED(2, "已结束");

    private Integer value;
    private String desc;

    public static CouponStatusEnum toEnum(Integer value) {
        return Stream.of(CouponStatusEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }
}
