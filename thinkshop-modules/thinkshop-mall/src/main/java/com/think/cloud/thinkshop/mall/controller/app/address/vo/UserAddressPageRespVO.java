package com.think.cloud.thinkshop.mall.controller.app.address.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * 用户地址分页响应VO
 *
 * @author zkthink
 * @date 2024-05-20
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAddressPageRespVO {

    /**
     * 用户地址id
     */
    @ApiModelProperty("用户地址id")
    private Long addressId;

    /**
     * first name
     */
    @ApiModelProperty("first name")
    private String firstName;

    /**
     * last name
     */
    @ApiModelProperty("last name")
    private String lastName;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String detail;

    /**
     * 国家编码
     */
    @ApiModelProperty("国家编码")
    private String countryId;


    /**
     * 收货人所在国家
     */
    @ApiModelProperty("收货人所在国家")
    private String country;

    /**
     * 省份编码
     */
    @ApiModelProperty("省份编码")
    private String provinceId;

    /**
     * 收货人所在省
     */
    @ApiModelProperty("收货人所在省")
    private String province;

    /**
     * 收货人所在市
     */
    @ApiModelProperty("收货人所在市")
    private String city;

    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    private String postCode;

    /**
     * 是否默认
     */
    @ApiModelProperty("是否默认")
    private Integer isDefault;

}
