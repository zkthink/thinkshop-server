
package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 商品属性格式化请求VO
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductFormatAttrReqVO {

    private List<String> detail;

    private String Value;


}
