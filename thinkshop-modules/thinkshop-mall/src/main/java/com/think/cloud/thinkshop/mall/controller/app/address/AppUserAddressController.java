package com.think.cloud.thinkshop.mall.controller.app.address;


import java.util.Arrays;

import com.think.cloud.thinkshop.mall.controller.app.address.vo.AddUserAddressReqVO;
import com.think.cloud.thinkshop.mall.controller.app.address.vo.UpdateUserAddressReqVO;
import com.think.cloud.thinkshop.mall.service.address.AppUserAddressService;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;

/**
 * App用户地址Controller
 *
 * @author zkthink
 * @date 2024-05-20
 */
@RestController
@RequestMapping("/app/address")
@Api(tags = "APP:用户地址")
public class AppUserAddressController extends BaseController {
    @Autowired
    private AppUserAddressService appUserAddressService;

    /**
     * 查询用户地址列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "查询用户地址列表")
    public TableDataInfo list() {
        startPage();
        return appUserAddressService.selectUserAddressList(SecurityUtils.getUserId());
    }

    /**
     * 获取用户地址详细信息
     */
    @GetMapping(value = "/get/{addressId}")
    @ApiOperation(value = "获取用户地址详细信息")
    public AjaxResult getInfo(@PathVariable("addressId") Long addressId) {
        return success(appUserAddressService.selectUserAddressByAddressId(addressId));
    }

    /**
     * 新增用户地址
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增用户地址")
    public AjaxResult add(@Validated @RequestBody AddUserAddressReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        return toAjax(appUserAddressService.insertUserAddress(vo));
    }

    /**
     * 修改用户地址
     */
    @PutMapping("/update")
    @ApiOperation(value = "修改用户地址")
    public AjaxResult edit(@Validated @RequestBody UpdateUserAddressReqVO vo) {
        return toAjax(appUserAddressService.updateUserAddress(vo));
    }

    /**
     * 删除用户地址
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除用户地址")
    public AjaxResult remove(@Valid Long[] addressIds) {
        return toAjax(appUserAddressService.batchDelete(Arrays.asList(addressIds)));
    }

    /**
     * 设置默认地址
     */
    @GetMapping("/set-default/{addressId}")
    @ApiOperation(value = "设置默认地址")
    public AjaxResult setDefaultAddress(@PathVariable("addressId") Long addressId) {
        appUserAddressService.setDefaultAddress(addressId);
        return toAjax(Boolean.TRUE);
    }
}
