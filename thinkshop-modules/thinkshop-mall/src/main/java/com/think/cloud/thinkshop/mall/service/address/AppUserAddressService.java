package com.think.cloud.thinkshop.mall.service.address;

import java.util.List;

import com.think.cloud.thinkshop.mall.controller.app.address.vo.*;
import com.think.cloud.thinkshop.mall.domain.UserAddress;
import com.think.common.core.web.page.TableDataInfo;

/**
 * App用户地址Service接口
 *
 * @author zkthink
 * @date 2024-05-20
 */
public interface AppUserAddressService {
    /**
     * 查询用户地址
     *
     * @param addressId 用户地址主键
     * @return 用户地址
     */
    public UserAddressDetailRespVO selectUserAddressByAddressId(Long addressId);

    /**
     * 查询用户地址列表
     *
     * @param userId
     * @return 用户地址集合
     */
    public TableDataInfo selectUserAddressList(Long userId);

    /**
     * 新增用户地址
     *
     * @param vo
     * @return 结果
     */
    public int insertUserAddress(AddUserAddressReqVO vo);

    /**
     * 修改用户地址
     *
     * @param vo
     * @return 结果
     */
    public int updateUserAddress(UpdateUserAddressReqVO vo);

    /**
     * 批量删除用户地址信息
     *
     * @param addressIds 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> addressIds);

    /**
     * 设置默认地址
     * @param addressId 地址id
     */
    public void setDefaultAddress(Long addressId);

    /**
     * 查询地址，为空获取默认地址
     * @param addressId
     * @param userId
     * @return
     */
    public UserAddress selectDefaultAddress(Long addressId, Long userId);
}
