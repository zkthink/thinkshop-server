package com.think.cloud.thinkshop.mall.service.order;


import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderLogRespVO;
import com.think.cloud.thinkshop.mall.domain.order.OrderLog;

import java.util.List;

/**
 * 订单日志AppService接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
public interface AppOrderLogService {

    /**
     * 新增订单操作日志
     *
     * @param orderLog 订单日志
     */
    public void insertOrderLog(OrderLog orderLog);

    /**
     * 获取订单操作日志
     *
     * @param orderId 订单id
     */
    public List<AppOrderLogRespVO> getOrderLogList(Long orderId);
}
