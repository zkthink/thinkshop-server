package com.think.cloud.thinkshop.mall.convert.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberGroupAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberGroupDetailRespVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberGroup;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Mapper
public interface MemberGroupConvert {
    MemberGroupConvert INSTANCE = Mappers.getMapper(MemberGroupConvert.class);

    MemberGroup convert(MemberGroupAddReqVO bean);

    MemberGroupDetailRespVO convert(MemberGroup bean);

    List<MemberGroupDetailRespVO> convertList(List<MemberGroup> bean);
}
