package com.think.cloud.thinkshop.mall.enums.product;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 上架状态枚举
 */
@Getter
@AllArgsConstructor
public enum ProductCommentStatusEnum {

    NO(0, "用户端不显示"),
    YES(1, "用户端可显示");

    private Integer value;
    private String desc;

}
