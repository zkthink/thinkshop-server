package com.think.cloud.thinkshop.mall.controller.admin.memberuser;


import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationLog;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberOperationLogService;
import com.think.common.core.domain.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 客户运营数据Controller
 *
 * @author zkthink
 * @date 2024-06-14
 */
@RestController
@RequestMapping("/admin/operation-log")
@Api(tags ="客户运营数据")
public class MemberOperationLogController extends BaseController
{
    @Autowired
    private IMemberOperationLogService memberOperationLogService;

    /**
     * 查询客户运营数据列表
     */
    @RequiresPermissions("mall:log:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询客户运营数据列表")
    public TableDataInfo list(MemberOperationLog memberOperationLog)
    {
        startPage();
        List<MemberOperationLog> list = memberOperationLogService.selectMemberOperationLogList(memberOperationLog);
        return getDataTable(list);
    }

    /**
     * 导出客户运营数据列表
     */
    @RequiresPermissions("mall:log:export")
    @Log(title = "客户运营数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出客户运营数据列表")
    public void export(HttpServletResponse response, MemberOperationLog memberOperationLog)
    {
        List<MemberOperationLog> list = memberOperationLogService.selectMemberOperationLogList(memberOperationLog);
        ExcelUtil<MemberOperationLog> util = new ExcelUtil<MemberOperationLog>(MemberOperationLog.class);
        util.exportExcel(response, list, "客户运营数据数据");
    }

    /**
     * 获取客户运营数据详细信息
     */
    @RequiresPermissions("mall:log:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取客户运营数据详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(memberOperationLogService.selectMemberOperationLogById(id));
    }

    /**
     * 新增客户运营数据
     */
    @RequiresPermissions("mall:log:add")
    @Log(title = "客户运营数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增客户运营数据")
    public AjaxResult add(@RequestBody MemberOperationLog memberOperationLog)
    {
        return toAjax(memberOperationLogService.insertMemberOperationLog(memberOperationLog));
    }

    /**
     * 修改客户运营数据
     */
    @RequiresPermissions("mall:log:edit")
    @Log(title = "客户运营数据", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改客户运营数据")
    public AjaxResult edit(@RequestBody MemberOperationLog memberOperationLog)
    {
        return toAjax(memberOperationLogService.updateMemberOperationLog(memberOperationLog));
    }

    /**
     * 删除客户运营数据
     */
    @RequiresPermissions("mall:log:remove")
    @Log(title = "客户运营数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete")
    @ApiOperation(value = "删除客户运营数据")
    public AjaxResult remove(@Valid Long[] ids)
    {
        return toAjax(memberOperationLogService.batchDelete(Arrays.asList(ids)));
    }

    @GetMapping("runMemberOperationLogTask")
    public R<Boolean> runMemberOperationLogTask()
    {
        memberOperationLogService.run();
        return R.ok();
    }
}
