package com.think.cloud.thinkshop.mall.controller.app.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 订单查询请求VO
 *
 * @author zkthink
 * @date 2024-05-24
 */
@Data
public class AppOrderPageReqVO {

    /**
     * 订单类型
     */
    @ApiModelProperty("订单类型")
    private Integer type;

    /**
     * 订单号或商品名
     */
    @ApiModelProperty("订单号或商品名")
    private String key;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private Long userId;

}
