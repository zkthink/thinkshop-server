package com.think.cloud.thinkshop.mall.domain.payment;

import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 支付记录对象 mall_payment_record
 *
 * @author zkthink
 * @date 2024-05-27
 */
@TableName("mall_payment_record")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableField
    private Long id;

    /** 支付编码 */
    @Excel(name = "支付编码")
    private String code;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Long orderId;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderCode;

    /** 请求参数 */
    @Excel(name = "请求参数")
    private String requestInfo;

    /** 请求响应 */
    @Excel(name = "请求响应")
    private String responseInfo;

    /** paypal预支付ID */
    @Excel(name = "paypal预支付ID")
    private String paymentId;

    /** 支付跳转地址 stripe时返回结果为clientSecret */
    @Excel(name = "支付跳转地址 stripe时返回结果为clientSecret")
    private String payUrl;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    @Excel(name = "货币代码")
    private String currencyCode;

}
