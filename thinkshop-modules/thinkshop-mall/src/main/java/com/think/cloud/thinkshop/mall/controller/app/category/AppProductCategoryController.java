package com.think.cloud.thinkshop.mall.controller.app.category;


import com.think.cloud.thinkshop.mall.service.category.AppProductCategoryService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品类目AppController
 *
 * @author zkthink
 * @date 2024-05-14
 */
@RestController
@RequestMapping("/app/category")
@Api(tags = "APP:商品类目")
public class AppProductCategoryController extends BaseController {
    @Autowired
    private AppProductCategoryService appProductCategoryService;

    /**
     * 查询商品类目列表
     */
    @GetMapping("/list")
    public TableDataInfo list() {
        return getDataTable(appProductCategoryService.getProductTree());
    }

    /**
     * 查询商品类目信息
     */
    @GetMapping("/get/{categoryId}")
    public AjaxResult get(@PathVariable("categoryId") Long categoryId) {
        return success(appProductCategoryService.getCategoryInfo(categoryId));
    }
}
