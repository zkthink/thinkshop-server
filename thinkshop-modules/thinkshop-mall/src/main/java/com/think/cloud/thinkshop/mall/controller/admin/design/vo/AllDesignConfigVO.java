package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AllDesignConfigVO {
    @Schema(description = "banner", required = true, example = "")
    private List<BannerPageRespVO> banners;

    @Schema(description = "图文", required = true, example = "")
    private  List<IndexDesignRespVO> imageText;

    @Schema(description = "视频", required = true, example = "")
    private  List<IndexDesignRespVO> video;

    @Schema(description = "页眉", required = true, example = "")
    private  PageConfigPageTopRespVO pageConfigPageTop;

    @Schema(description = "页脚", required = true, example = "")
    private  PageConfigPageBottomRespVO pageConfigPageBottom;

    @Schema(description = "商品组合", required = true, example = "")
    private List<IndexProductGroupDesignRespVO> indexProductGroupDesign;
}
