package com.think.cloud.thinkshop.mall.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroupTraitEnum {
    HAVE_BUY("有购买","1"),
    NO_BUY("无购买","2"),
    HAVE_ORDER("有订单","3"),
    NO_ORDER("无订单","4"),
    HAVE_ADD_CAR("有加购","5"),
    NO_ADD_CAR("无加购","6"),
    HAVE_INTERVIEW("有访问","7"),
    NO_INTERVIEW("无访问","8"),
    BUY_COUNT("购买次数","9"),
    BUY_AMOUNT("购买金额","10"),
    TAG("标签","11"),
    NO_EXIST("不存在的条件类型","0")
    ;
    private String label;
    private String value;

    public static MemberGroupTraitEnum get(String value) {
        for (MemberGroupTraitEnum memEnum : MemberGroupTraitEnum.values()) {
            if (memEnum.value.equals(value)) {
                return memEnum ;
            }
        }
        return NO_EXIST;
    }
}
