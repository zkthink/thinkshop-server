package com.think.cloud.thinkshop.mall.mapper.aftersales;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.aftersales.AfterSalesDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 售后明细Mapper接口
 *
 * @author zkthink
 * @date 2024-06-26
 */
@Mapper
public interface AfterSalesDetailMapper extends BaseMapper<AfterSalesDetail> {

    void batchInsert(@Param("list") List<AfterSalesDetail> list);

    void deleteByAfterSalesId(@Param("afterSalesId") Long afterSalesId);

}
