package com.think.cloud.thinkshop.mall.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 时间字段 跨区时区时间修正  ，只支持 Date,Timestamp
 */
@Retention(RetentionPolicy.RUNTIME)//运行时注解
@Target(ElementType.FIELD)//作用于字段
@JacksonAnnotationsInside
@JsonSerialize(using = CrossBorderZoneTimeAmendSerializer.class)//指定序列化器
public @interface CrossBorderZoneTimeAmend {
    /**
     *  格式 ，默认为 yyyy-MM-dd HH:mm:ss
     */
    String pattern() default "";

}
