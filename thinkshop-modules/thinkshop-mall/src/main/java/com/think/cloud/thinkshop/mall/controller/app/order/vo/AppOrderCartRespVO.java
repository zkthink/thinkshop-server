package com.think.cloud.thinkshop.mall.controller.app.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;


/**
 * 确认订单购物车响应VO
 *
 * @author zkthink
 * @date 2024-05-24
 */
@Data
public class AppOrderCartRespVO {

    @ApiModelProperty("购物车ID")
    private Long cartId;

    @ApiModelProperty("商品ID")
    private Long productId;

    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("规格id")
    private Long skuId;

    @ApiModelProperty("规格")
    private String sku;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @ApiModelProperty("实际价格")
    private BigDecimal realPrice;

    @ApiModelProperty("规格图片")
    private String image;

    @ApiModelProperty("产品图片")
    private String productImage;

    @ApiModelProperty("商品数量")
    private Integer num;

    @ApiModelProperty("税费")
    private BigDecimal taxation;

    @ApiModelProperty("重量")
    private BigDecimal weight;

    @ApiModelProperty("是否收税")
    private Integer tax;

}
