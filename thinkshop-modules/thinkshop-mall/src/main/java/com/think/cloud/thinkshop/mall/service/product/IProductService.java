package com.think.cloud.thinkshop.mall.service.product;

import com.think.cloud.thinkshop.mall.controller.admin.product.vo.*;
import com.think.cloud.thinkshop.mall.domain.Product;
import com.think.common.core.web.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 商品Service接口
 *
 * @author zkthink
 * @date 2024-05-09
 */
public interface IProductService
{
    /**
     * 查询商品
     *
     * @param productId 商品主键
     * @return 商品
     */
    public ProductDetailRespVO selectProductByProductId(Long productId);

    /**
     * 通过id查询商品
     * @param productId
     * @return
     */
    public Product findProductById(Long productId);

    /**
     * 通过id查询商品 （含被删除的商品，避免查询结果为空）
     * @param productId
     * @return
     */
    public Product findHistoryProductById(Long productId);

    /**
     * 查询商品
     *
     * @param productIds 商品主键
     * @return 商品
     */
    public List<Product> selectProductByProductIds(List<Long> productIds);

    /**
     * 查询商品列表
     *
     * @param vo
     * @return 商品集合
     */
    public TableDataInfo selectProductList(ProductPageReqVO vo);

    /**
     * 新增商品
     *
     * @param vo
     * @return 结果
     */
    public void insertProduct(SaveProductReqVO vo);

    /**
     * 修改商品
     *
     * @param vo
     * @return 结果
     */
    public void updateProduct(UpdateProductReqVO vo);

    /**
     * 批量删除商品信息
     *
     * @param productIds 主键集合
     * @return 结果
     */
    public void batchDelete(List<Long> productIds);

    /**
     * 上架商品
     *
     * @param vo
     * @return 结果
     */
    public void grounding(GroundingProductReqVO vo);

    /**
     * 智能查找商品
     *
     * @param vo
     * @return 商品id集合
     */
    public List<Long> intelligentSearch(IntelligentSearchProductReqVO vo);

    /**
     * 获取生成的属性
     *
     * @param id      商品id
     * @param jsonStr jsonStr
     * @return map
     */
    Map<String, Object> getFormatAttr(Long id, String jsonStr);

    /**
     * 获取商品导入的模版
     * @param response
     */
    void getImportTemplate(HttpServletResponse response);

    /**
     * 导入商品
     * @param file
     * @return
     * @throws IOException
     */
    ImportProductResultVO importProductByExcel(MultipartFile file) throws IOException;

    /**
     * 导出商品
     * @param response
     */
    void exportProduct(HttpServletResponse response);
}
