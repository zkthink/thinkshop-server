package com.think.cloud.thinkshop.mall.service.product;

import java.util.List;
import com.think.cloud.thinkshop.mall.domain.ProductAttr;

/**
 * 商品属性Service接口
 * 
 * @author zkthink
 * @date 2024-05-09
 */
public interface IProductAttrService 
{
    /**
     * 查询商品属性
     * 
     * @param attrId 商品属性主键
     * @return 商品属性
     */
    public ProductAttr selectProductAttrByAttrId(Long attrId);

    /**
     * 查询商品属性列表
     * 
     * @param productAttr 商品属性
     * @return 商品属性集合
     */
    public List<ProductAttr> selectProductAttrList(ProductAttr productAttr);

    /**
     * 批量新增商品属性
     * 
     * @param productAttrs 商品属性集合
     * @return 结果
     */
    public void batchInsertProductAttr(List<ProductAttr> productAttrs);

    /**
     * 修改商品属性
     * 
     * @param productAttr 商品属性
     * @return 结果
     */
    public int updateProductAttr(ProductAttr productAttr);

    /**
     * 批量删除商品属性信息
     * 
     * @param attrIds 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> attrIds);

    /**
     * 根据商品id删除商品属性
     *
     * @param productId 商品id
     * @return 结果
     */
    public void deleteByProductId(Long productId);
}
