package com.think.cloud.thinkshop.mall.service.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.index.dto.StatisticalTrendViewDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberGroupUserTraitDetailDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberUserQueryDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.UpdateTagDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberUserVO;
import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.ForgetPasswordDTO;
import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.RegisterDTO;
import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.UpdatePasswordDTO;
import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.UpdateUserInfoDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberUser;
import com.think.cloud.thinkshop.system.api.dto.AuthUserDTO;
import com.think.cloud.thinkshop.system.api.dto.MemberUserDTO;
import com.think.common.core.web.page.TableDataInfo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商城用户Service接口
 *
 * @author zkthink
 * @date 2024-05-09
 */
public interface IMemberUserService {
    /**
     * 查询商城用户
     *
     * @param id 商城用户主键
     * @return 商城用户
     */
    public MemberUser selectMemberUserById(Long id);

    public MemberUserVO selectMemberUserVOById(Long id);

    /**
     * 查询商城用户列表
     *
     * @param memberUser 商城用户
     * @return 商城用户集合
     */
    public List<MemberUserVO> selectMemberUserList(MemberUserQueryDTO memberUser);

    public TableDataInfo selectMemberUserPage(MemberUserQueryDTO memberUser);

    /**
     * 新增商城用户
     *
     * @param memberUser 商城用户
     * @return 结果
     */
    public int insertMemberUser(MemberUser memberUser);

    /**
     * 修改商城用户
     *
     * @param memberUser 商城用户
     * @return 结果
     */
    public int updateMemberUser(MemberUser memberUser);

    /**
     * 批量删除商城用户信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    /**
     * 注册
     *
     * @param registerDTO
     */
    void register(RegisterDTO registerDTO);

    MemberUser getByEmail(String email);

    MemberUserDTO getByUsername(String username);

    /**
     * 忘记密码
     *
     * @param forgetPassword /
     */
    void forgetPassword(ForgetPasswordDTO forgetPassword);

    /**
     * 修改密码
     *
     * @param userId         /
     * @param updatePassword /
     */
    void updatePassword(Long userId, UpdatePasswordDTO updatePassword);

    /**
     * 第三方登录
     *
     * @param authUserDTO /
     * @return /
     */
    MemberUserDTO oauthCallback(AuthUserDTO authUserDTO);

    /**
     * 修改标签
     *
     * @param updateTagDTO /
     */
    void updateTag(UpdateTagDTO updateTagDTO);

    /**
     * 修改用户信息
     *
     * @param userId  /
     * @param infoDTO /
     */
    void updateUserInfo(Long userId, UpdateUserInfoDTO infoDTO);

    /**
     * 用户注册趋势
     *
     * @param startTime
     * @param endTime
     * @return
     */
    List<StatisticalTrendViewDTO> memberRegisterTrend(String startTime, String endTime);

    /**
     * 用户总数
     *
     * @return
     */
    Long memberUserCount();

    /**
     * 日期范围内的用户注册数量
     *
     * @return
     */
    Long memberUserCount(String startTime, String endTime);
    /**
     * 日期范围内的用户注册数量 趋势
     *
     * @return
     */
    List<StatisticalTrendViewDTO> newMemberCountTrend(String startTime, String endTime);

    /**
     * 所有用户的特征
     *
     * @return
     */
    List<MemberGroupUserTraitDetailDTO> memberAllUserTrait();

    /**
     * 查询用户的特征
     * @param userId
     * @return
     */
    List<MemberGroupUserTraitDetailDTO> memberGroupUserTrait(Long userId);

    /**
     * 更新用户的消费信息
     * @param id /
     * @param payPrice /
     */
    void updateConsume(Long id, BigDecimal payPrice);

    /**
     * 更新用户的退款信息
     * @param userId /
     * @param refundAmount /
     */
    void updateUserRefundData(Long userId, BigDecimal refundAmount);

    void recordUserOpt(String uri, Object handler);

}
