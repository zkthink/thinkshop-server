package com.think.cloud.thinkshop.mall;

import com.think.common.security.annotation.EnableCustomConfig;
import com.think.common.security.annotation.EnableRyFeignClients;
import com.think.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 商城模块
 *
 * @author zkthink
 */
@EnableAsync
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class ThinkShopMallApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(ThinkShopMallApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  商城模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
