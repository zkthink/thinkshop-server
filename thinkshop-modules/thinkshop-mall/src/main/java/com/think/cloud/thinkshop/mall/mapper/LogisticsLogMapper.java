package com.think.cloud.thinkshop.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.LogisticsLog;

/**
 * 物流日志Mapper接口
 *
 * @author zkthink
 * @date 2024-05-31
 */
@Mapper
public interface LogisticsLogMapper extends BaseMapper<LogisticsLog> {

}
