package com.think.cloud.thinkshop.mall.mapper;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.ProductGroupPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.ProductGroupRespVO;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.ProductGroup;

import java.util.List;

/**
 * 商品分组Mapper接口
 *
 * @author zkthink
 * @date 2024-05-10
 */
@Mapper
public interface ProductGroupMapper extends BaseMapper<ProductGroup> {
    default List<ProductGroup> selectList(ProductGroupPageReqVO vo) {
        return selectList(new LambdaQueryWrapper<ProductGroup>()
                .like(ObjectUtil.isNotNull(vo.getName()), ProductGroup::getName, vo.getName())
                .eq(ObjectUtil.isNotNull(vo.getType()), ProductGroup::getType, vo.getType())
                .orderByDesc(ProductGroup::getGroupId));
    }

    List<ProductGroupRespVO> selectListByProductId(Long productId);
}
