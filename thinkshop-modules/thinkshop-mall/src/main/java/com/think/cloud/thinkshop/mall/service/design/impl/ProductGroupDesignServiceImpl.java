package com.think.cloud.thinkshop.mall.service.design.impl;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.GroupNameBlurRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexProductGroupDesignRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.ProductGroupDesignStatusReqVO;
import com.think.cloud.thinkshop.mall.domain.design.IndexProductGroup;
import com.think.cloud.thinkshop.mall.mapper.design.IndexProductGroupDesignMapper;
import com.think.cloud.thinkshop.mall.service.design.IProductGroupDesignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductGroupDesignServiceImpl implements IProductGroupDesignService {
    @Autowired
    private IndexProductGroupDesignMapper indexProductGroupDesignMapper;

    @Override
    public List<IndexProductGroupDesignRespVO> getProductGroupDesignList() {
        return indexProductGroupDesignMapper.getProductGroupDesignList(null);
    }

    @Override
    public int insert(Long groupId) {
        return indexProductGroupDesignMapper.insert(IndexProductGroup.builder().groupId(groupId).build());
    }

    @Override
    public int delete(Long id) {
        return indexProductGroupDesignMapper.deleteById(id);
    }

    @Override
    public int updateStatusById(ProductGroupDesignStatusReqVO vo) {
        return indexProductGroupDesignMapper.updateById(IndexProductGroup.builder()
                .id(vo.getId()).status(vo.getStatus()).build())
        ;
    }

    @Override
    public  List<GroupNameBlurRespVO> getGroupNameBlur(String name) {
        return indexProductGroupDesignMapper.getGroupNameBlur(name);
    }
}
