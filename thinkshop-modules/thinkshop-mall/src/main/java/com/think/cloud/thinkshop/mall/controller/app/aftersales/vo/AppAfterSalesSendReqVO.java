package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import com.think.common.core.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * 售后发货请求VO
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppAfterSalesSendReqVO {

    @ApiModelProperty("售后id")
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("物流公司编码")
    private String shipperCode;

    @ApiModelProperty("物流单号")
    private String deliverySn;

    @ApiModelProperty("物流名称")
    private String deliveryName;

}
