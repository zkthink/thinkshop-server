package com.think.cloud.thinkshop.mall.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.think.common.core.annotation.Excel;
import lombok.*;

import java.util.Date;

/**
 * 商品分组对象 mall_product_group
 * 
 * @author zkthink
 * @date 2024-05-10
 */
@TableName("mall_product_group")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductGroup
{
    private static final long serialVersionUID = 1L;

    /** 分组id */
    @TableId(type = IdType.AUTO)
    private Long groupId;

    /** 分组名称 */
    @Excel(name = "分组名称")
    private String name;

    /** 分组描述 */
    @Excel(name = "分组描述")
    private String remark;

    /** 类型: 0、手动选择 1、智能选择 */
    @Excel(name = "类型")
    private Integer type;

    /** 智能选择条件json */
    @Excel(name = "智能选择条件json")
    private String conditionJson;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Date createTime;

    /** 修改人 */
    @Excel(name = "修改人")
    private String updateBy;

    /** 修改时间 */
    @Excel(name = "修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

}
