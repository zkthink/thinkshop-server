package com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class MemberUserQueryDTO {
    @ApiModelProperty(value = "最后消费时间")
    @DateTimeFormat
    private Timestamp startLastConsumeTime;

    @ApiModelProperty(value = "最后消费时间")
    @DateTimeFormat
    private Timestamp endLastConsumeTime;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat
    private Timestamp startCreateTime;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat
    private Timestamp endCreateTime;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "标签id")
    private List<Long> tagIds;

    @ApiModelProperty(value = "用户id")
    private List<Long> userIds;
}
