package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@Schema(description = "管理后台 - banner编辑 Request VO")
@ToString(callSuper = true)
public class BannerEditReqVO {
    @Schema(description = "序号", required = true, example = "1")
    @NotNull(message = "序号不能为空")
    private Long id;

    @Schema(description = "图片地址 (编辑)", required = false, example = "http://xxxx")
    private String imageUrl;

    @Schema(description = "重定向地址 (编辑)", required = false, example = "http://xxxx")
    private String redirectUrl;

    @Schema(description = "状态 (修改状态)", required = false, example = "1")
    private Integer status;
}
