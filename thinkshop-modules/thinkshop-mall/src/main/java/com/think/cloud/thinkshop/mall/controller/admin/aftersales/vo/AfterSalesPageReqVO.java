package com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

/**
 * 售后单分页请求VO
 *
 * @author zkthink
 * @date 2024-06-14
 */
@Data
public class AfterSalesPageReqVO {

    @ApiModelProperty("状态")
    private Integer state;

    @ApiModelProperty("售后类型")
    private Integer serviceType;

    @ApiModelProperty("订单编号")
    private String orderCode;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("开始时间")
    @DateTimeFormat
    private Timestamp startTime;

    @ApiModelProperty("结束时间")
    @DateTimeFormat
    private Timestamp endTime;
}
