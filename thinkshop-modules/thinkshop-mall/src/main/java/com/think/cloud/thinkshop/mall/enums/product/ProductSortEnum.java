package com.think.cloud.thinkshop.mall.enums.product;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 商品排序枚举
 */
@Getter
@AllArgsConstructor
public enum ProductSortEnum {

    RECOMMEND(0, "推荐"),
    NEW(1, "新品"),
    SALES_ASC(2, "销量升序"),
    SALES_DESC(3, "销量降序"),
    PRICE_ASC(4, "价格升序"),
    PRICE_DESC(5, "价格降序");

    private Integer value;
    private String desc;


    public static ProductSortEnum toEnum(Integer value) {
        return Stream.of(ProductSortEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
