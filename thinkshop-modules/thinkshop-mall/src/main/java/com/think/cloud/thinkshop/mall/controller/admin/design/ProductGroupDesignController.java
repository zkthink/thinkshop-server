package com.think.cloud.thinkshop.mall.controller.admin.design;


import com.think.cloud.thinkshop.mall.controller.admin.design.vo.ProductGroupDesignAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.ProductGroupDesignStatusReqVO;
import com.think.cloud.thinkshop.mall.service.design.IProductGroupDesignService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/admin/productGroupDesign")
@Api(tags = "ADMIN:商品组合设计")
public class ProductGroupDesignController extends BaseController {
    @Autowired
    private IProductGroupDesignService productGroupDesignService;

    /**
     * 查询 商品组
     */
    @RequiresPermissions("mall:productGroupDesign:query")
    @GetMapping(value = "/list")
    @ApiOperation(value = "查询商品组列表")
    public TableDataInfo list() {
        startPage();
        return getDataTable(productGroupDesignService.getProductGroupDesignList());
    }

    /**
     * 新增
     */
    @RequiresPermissions("mall:productGroupDesign:add")
    @Log(title = "productGroupDesign", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增 productGroupDesign")
    public AjaxResult add(@RequestBody ProductGroupDesignAddReqVO vo) {
        return toAjax(productGroupDesignService.insert(vo.getGroupId()));
    }

    /**
     * 删除
     */
    @RequiresPermissions("mall:productGroupDesign:remove")
    @Log(title = "productGroupDesign", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除首页的产品组")
    public AjaxResult remove(@Valid @PathVariable("id") Long id) {
        return toAjax(productGroupDesignService.delete(id));
    }

    /**
     * 更新首页产品组状态
     * @param vo
     * @return
     */
    @RequiresPermissions("mall:productGroupDesign:edit")
    @Log(title = "productGroupDesign", businessType = BusinessType.UPDATE)
    @PutMapping("/updateStatus")
    @ApiOperation(value = " 更新首页产品组状态")
    public AjaxResult updateStatus(@RequestBody ProductGroupDesignStatusReqVO vo) {
        return toAjax(productGroupDesignService.updateStatusById(vo));
    }
    /**
     * 模糊查询分组名
     */
    @RequiresPermissions("mall:productGroupDesign:query")
    @GetMapping(value = "/getGroup")
    @ApiOperation(value = "模糊查询商品组")
    public TableDataInfo getGroup(String name) {
        startPage();
        return getDataTable(productGroupDesignService.getGroupNameBlur(name));
    }
}
