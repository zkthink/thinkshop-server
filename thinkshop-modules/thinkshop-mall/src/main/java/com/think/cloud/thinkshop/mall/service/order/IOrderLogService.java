package com.think.cloud.thinkshop.mall.service.order;

import java.util.List;

import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderLogRespVO;
import com.think.cloud.thinkshop.mall.domain.order.OrderLog;

/**
 * 订单日志Service接口
 *
 * @author zkthink
 * @date 2024-05-23
 */
public interface IOrderLogService {
    /**
     * 查询订单日志
     *
     * @param id 订单日志主键
     * @return 订单日志
     */
    public OrderLog selectOrderLogById(Long id);

    /**
     * 查询订单日志列表
     *
     * @param orderLog 订单日志
     * @return 订单日志集合
     */
    public List<OrderLog> selectOrderLogList(OrderLog orderLog);

    /**
     * 新增订单日志
     *
     * @param orderLog 订单日志
     * @return 结果
     */
    public int insertOrderLog(OrderLog orderLog);

    /**
     * 修改订单日志
     *
     * @param orderLog 订单日志
     * @return 结果
     */
    public int updateOrderLog(OrderLog orderLog);

    /**
     * 批量删除订单日志信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    /**
     * 查询订单日志列表
     *
     * @param orderId
     * @return
     */
    public List<OrderLogRespVO> selectOrderLogListByOrderId(Long orderId);
}
