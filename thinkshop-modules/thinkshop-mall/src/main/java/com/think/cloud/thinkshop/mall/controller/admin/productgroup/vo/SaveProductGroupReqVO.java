package com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo;


import com.think.cloud.thinkshop.mall.controller.admin.product.vo.IntelligentSearchProductReqVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * 商品分组保存请求VO
 *
 * @author zkthink
 * @date 2024-05-10
 */
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class SaveProductGroupReqVO {

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    private String name;

    /**
     * 分组描述
     */
    @ApiModelProperty("分组描述")
    private String remark;

    /**
     * 类型: 0、手动选择 1、智能选择
     */
    @ApiModelProperty("类型: 0、手动选择 1、智能选择")
    private Integer type;

    /**
     * 关联商品id集合
     */
    @ApiModelProperty("关联商品id集合")
    private List<Long> productIds;

    /**
     * 智能选择条件
     */
    @ApiModelProperty("智能选择条件")
    private IntelligentSearchProductReqVO intelligent;



    @ApiModelProperty("创建人")
    private String createBy;

}
