package com.think.cloud.thinkshop.mall.controller.admin.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanSearchReqVO;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberOperationPlanService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;

/**
 * 客户运营计划Controller
 *
 * @author zkthink
 * @date 2024-06-07
 */
@RestController
@RequestMapping("/admin/memberPlan")
@Api(tags = "客户运营计划")
public class MemberOperateController extends BaseController {
    @Autowired
    private IMemberOperationPlanService memberOperationPlanService;

    /**
     * 查询客户运营计划列表
     */
    @RequiresPermissions("mall:plan:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询客户运营计划列表")
    public TableDataInfo list(MemberOperationPlanSearchReqVO vo) {
        return memberOperationPlanService.selectMemberOperationPlanList(vo);
    }

    /**
     * 新增客户运营计划
     */
    @RequiresPermissions("mall:plan:add")
    @Log(title = "客户运营计划", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增客户运营计划")
    public AjaxResult add(@RequestBody MemberOperationPlanAddReqVO vo) {
        return toAjax(memberOperationPlanService.insertMemberOperationPlan(vo));
    }

    /**
     * 删除客户运营计划
     */
    @RequiresPermissions("mall:plan:remove")
    @Log(title = "客户运营计划", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除客户运营计划")
    public AjaxResult remove(@Valid Long[] ids) {
        memberOperationPlanService.batchDelete(Arrays.asList(ids));
        return success();
    }

    /**
     * 编辑客户运营计划
     */
    @RequiresPermissions("mall:plan:edit")
    @Log(title = "客户运营计划", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "编辑客户运营计划")
    public AjaxResult edit(@RequestBody MemberOperationPlanEditReqVO vo) {
        return toAjax(memberOperationPlanService.updateMemberOperationPlan(vo));
    }


    /**
     * 获取客户运营计划详细信息
     */
    @RequiresPermissions("mall:plan:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取客户运营计划详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(memberOperationPlanService.selectVOById(id));
    }

}
