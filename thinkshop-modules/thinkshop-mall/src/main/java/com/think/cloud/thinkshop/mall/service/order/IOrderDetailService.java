package com.think.cloud.thinkshop.mall.service.order;

import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesOrderDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderDetailRespVO;

import java.util.List;

/**
 * 订单明细Service接口
 *
 * @author zkthink
 * @date 2024-05-23
 */
public interface IOrderDetailService {

    public List<OrderDetailRespVO> selectOrderDetailListByOrderIds(List<Long> orderIds);

    public List<AfterSalesOrderDetailRespVO> selectOrderDetailListByAfterSalesIds(List<Long> afterSalesIds);

    public void updateOrderDetailByAfterSalesId(Long afterSalesId, Integer state);

    public Boolean determineAfterSalesCompleted(Long orderId);

}
