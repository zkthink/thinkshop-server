package com.think.cloud.thinkshop.mall.domain.aftersales;

import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 售后日志对象 mall_after_sales_log
 *
 * @author zkthink
 * @date 2024-06-13
 */
@TableName("mall_after_sales_log")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AfterSalesLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 售后日志id */
    private Long id;

    /** 售后id */
    @Excel(name = "售后id")
    private Long afterSalesId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户类型：1、用户；2、商户；3、系统 */
    @Excel(name = "用户类型：1、用户；2、商户；3、系统")
    private Integer userType;

    /** 操作类型：1、提交申请；2、审核成功；3、审核失败；4、用户取消；5、超时自动取消；6、货品已寄回；7、商家同意退款；8、退款成功 */
    @Excel(name = "操作类型：1、提交申请；2、审核成功；3、审核失败；4、用户取消；5、超时自动取消；6、货品已寄回；7、商家同意退款；8、退款成功")
    private Integer operateType;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

}
