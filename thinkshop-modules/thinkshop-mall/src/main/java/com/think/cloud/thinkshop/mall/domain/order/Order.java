package com.think.cloud.thinkshop.mall.domain.order;

import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 订单对象 mall_order
 *
 * @author zkthink
 * @date 2024-05-23
 */
@TableName("mall_order")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单id */
    private Long id;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderCode;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 优惠券id */
    @Excel(name = "优惠券id")
    private Long couponId;

    /** first name */
    @Excel(name = "first name")
    private String firstName;

    /** last name */
    @Excel(name = "last name")
    private String lastName;

    /** 电话 */
    @Excel(name = "电话")
    private String userPhone;

    /** 国家 */
    @Excel(name = "国家")
    private String country;

    /** 地区id */
    @Excel(name = "地区id")
    private String regionId;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String addressDetail;

    /** 邮编 */
    @Excel(name = "邮编")
    private String postCode;

    /** 订单商品总数 */
    @Excel(name = "订单商品总数")
    private Integer totalNum;

    /** 订单总价 */
    @Excel(name = "订单总价")
    private BigDecimal totalPrice;

    /** 邮费 */
    @Excel(name = "邮费")
    private BigDecimal totalPostage;

    /** 优惠券金额 */
    @Excel(name = "优惠券金额")
    private BigDecimal couponPrice;

    /** 税费 */
    @Excel(name = "税费")
    private BigDecimal taxation;

    /** 运费税费 */
    @Excel(name = "运费税费")
    private BigDecimal postageTaxation;

    /** 实际支付金额 */
    @Excel(name = "实际支付金额")
    private BigDecimal payPrice;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private Integer paid;

    /** 支付单号 */
    @Excel(name = "支付单号")
    private String payCode;

    /** 支付时间 */
    private Timestamp payTime;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payType;

    /** 订单状态（ -2:已退款，-1：已取消；0：待付款；1：待发货；2：待收货；3：已完成 ） */
    @Excel(name = "订单状态", readConverterExp = "-2:已退款，-1：已取消；0：待付款；1：待发货；2：待收货；3：已完成")
    private Integer status;

    /** 快递公司编号 */
    @Excel(name = "快递公司编号")
    private String deliverySn;

    /** 快递名称 */
    @Excel(name = "快递名称")
    private String deliveryName;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String deliveryId;

    /** 备注 */
    @Excel(name = "备注")
    private String mark;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** stripe支付信息(用于退款) */
    @Excel(name = "stripe支付信息(用于退款)")
    private String latestCharge;

    /** 支付返回id */
    @Excel(name = "支付返回id")
    private String paymentIntentId;

    /** 运营计划id */
    @Excel(name = "运营计划id")
    private Long planId;
    /**
     * 用户侧是否删除 0：否  1：已经删除
     */
    private Integer userDel;

    /**
     * 订单扣除积分值
     */
    private Integer useIntegral;
    /**
     * 积分抵扣的金额
     */
    private BigDecimal integralDeduct;
    /**
     * 订单完成时间
     */
    private Date completeTime;

}
