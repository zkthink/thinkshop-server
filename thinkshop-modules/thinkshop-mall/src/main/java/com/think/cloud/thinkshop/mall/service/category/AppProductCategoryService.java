package com.think.cloud.thinkshop.mall.service.category;


import com.think.cloud.thinkshop.mall.controller.app.category.vo.AppProductCategoryInfoVO;
import com.think.cloud.thinkshop.mall.controller.app.category.vo.AppProductCategoryLevelVO;

import java.util.List;

/**
 * 商品类目AppService接口
 *
 * @author zkthink
 * @date 2024-05-14
 */
public interface AppProductCategoryService {

    /**
     * 查询商品类目树
     *
     * @return 商品类目集合
     */
    public List<AppProductCategoryLevelVO> getProductTree();

    /**
     * 查询商品类目下所有类目id
     *
     * @return 商品类目id集合
     */
    public List<Object> getAllKidCategoryIds(Long categoryId);

    /**
     * 查询商品子类类目id
     *
     * @return 商品类目id集合
     */
    public List<Object> getKidCategoryIds(List<Object> categoryIds);

    /**
     * 获取类目路径
     *
     * @return categoryId
     */
    public String getCategoryPath(Long categoryId);

    /**
     * 获取类目信息
     *
     * @return categoryId
     */
    public AppProductCategoryInfoVO getCategoryInfo(Long categoryId);
}
