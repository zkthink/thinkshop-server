package com.think.cloud.thinkshop.mall.controller.app.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 创建订单请求VO
 *
 * @author zkthink
 * @date 2024-05-24
 */
@Data
public class AppCreateOrderReqVO extends AppOrderCommonReqVO{

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String mark;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String userName;
}
