package com.think.cloud.thinkshop.mall.service.websitesetting;

import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto.AgreementDTO;
import com.think.cloud.thinkshop.mall.domain.websitesetting.Agreement;

import java.util.List;

/**
 * 协议Service接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
public interface IAgreementService
{
    /**
     * 查询协议
     *
     * @param id 协议主键
     * @return 协议
     */
    public Agreement selectAgreementById(Long id);

    /**
     * 查询协议列表
     *
     * @param agreement 协议
     * @return 协议集合
     */
    public List<Agreement> selectAgreementList(Agreement agreement);

    /**
     * 新增协议
     *
     * @param agreement 协议
     * @return 结果
     */
    public int insertAgreement(Agreement agreement);

    /**
     * 修改协议
     *
     * @param agreement 协议
     * @return 结果
     */
    public int updateAgreement(Agreement agreement);

    /**
     * 批量删除协议信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    AgreementDTO getAgreement();

    void saveAgreement(AgreementDTO dto);

    String getAgreementByType(Integer type);
}
