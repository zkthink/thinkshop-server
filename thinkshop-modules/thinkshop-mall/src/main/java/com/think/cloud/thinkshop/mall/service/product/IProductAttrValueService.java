package com.think.cloud.thinkshop.mall.service.product;

import com.think.cloud.thinkshop.mall.domain.ProductAttrValue;

import java.util.List;

/**
 * 商品属性值Service接口
 * 
 * @author zkthink
 * @date 2024-05-09
 */
public interface IProductAttrValueService 
{
    /**
     * 查询商品属性值
     * 
     * @param skuId 商品属性值主键
     * @return 商品属性值
     */
    public ProductAttrValue selectProductAttrValueBySkuId(Long skuId);

    ProductAttrValue findProductAttrValueBySkuId(Long skuId);

    /**
     * 查询商品属性值列表
     * 
     * @param productAttrValue 商品属性值
     * @return 商品属性值集合
     */
    public List<ProductAttrValue> selectProductAttrValueList(ProductAttrValue productAttrValue);

    /**
     * 批量新增商品属性值
     * 
     * @param productAttrValues 商品属性值集合
     * @return 结果
     */
    public void batchInsertProductAttrValue(List<ProductAttrValue> productAttrValues);

    /**
     * 修改商品属性值
     * 
     * @param productAttrValue 商品属性值
     * @return 结果
     */
    public int updateProductAttrValue(ProductAttrValue productAttrValue);

    /**
     * 批量删除商品属性值信息
     * 
     * @param skuIds 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> skuIds);

    /**
     * 根据商品id删除商品属性
     *
     * @param productId 商品id
     * @return 结果
     */
    public void deleteByProductId(Long productId);
}
