package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponDetailRespVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class MemberOperationPlanRefCouponVO extends ProductCouponDetailRespVO {

    @ApiModelProperty("发放数量（每人）,运营计划使用")
    private String couponIssueNum;
}
