package com.think.cloud.thinkshop.mall.service.address.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.think.cloud.thinkshop.mall.controller.app.address.vo.AddUserAddressReqVO;
import com.think.cloud.thinkshop.mall.controller.app.address.vo.UpdateUserAddressReqVO;
import com.think.cloud.thinkshop.mall.controller.app.address.vo.UserAddressDetailRespVO;
import com.think.cloud.thinkshop.mall.convert.address.UserAddressConvert;
import com.think.cloud.thinkshop.mall.domain.UserAddress;
import com.think.cloud.thinkshop.mall.enums.common.CommonWhetherEnum;
import com.think.cloud.thinkshop.mall.mapper.UserAddressMapper;
import com.think.cloud.thinkshop.mall.service.address.AppUserAddressService;
import com.think.common.core.utils.PageUtils;
import com.think.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * App用户地址Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-20
 */
@Service
public class AppUserAddressServiceImpl implements AppUserAddressService {
    @Autowired
    private UserAddressMapper userAddressMapper;

    private UserAddress findUserAddressByAddressId(Long addressId) {
        return userAddressMapper.selectById(addressId);
    }

    private int updateUserAddress(UserAddress param) {
        return userAddressMapper.updateById(param);
    }

    /**
     * 查询用户地址
     *
     * @param addressId 用户地址主键
     * @return 用户地址
     */
    @Override
    public UserAddressDetailRespVO selectUserAddressByAddressId(Long addressId) {
        return UserAddressConvert.INSTANCE.convert(this.findUserAddressByAddressId(addressId));
    }

    /**
     * 查询用户地址列表
     *
     * @param userId
     * @return 用户地址
     */
    @Override
    public TableDataInfo selectUserAddressList(Long userId) {
        List<UserAddress> userAddresses = userAddressMapper.selectListByUserId(userId);
        return new TableDataInfo(UserAddressConvert.INSTANCE.convertList(userAddresses), PageUtils.getTotal(userAddresses));
    }

    /**
     * 新增用户地址
     *
     * @param vo
     * @return 结果
     */
    @Override
    public int insertUserAddress(AddUserAddressReqVO vo) {
        if (CommonWhetherEnum.YES.getValue().equals(vo.getIsDefault())) {
            setOtherAddress(vo.getUserId());
        }else {
            //判断是否为用户第一个地址,是则为默认地址
            if (userAddressMapper.selectListByUserId(vo.getUserId()).isEmpty()) {
                vo.setIsDefault(CommonWhetherEnum.YES.getValue());
            }
        }
        return userAddressMapper.insert(UserAddressConvert.INSTANCE.convert(vo));
    }

    /**
     * 修改用户地址
     *
     * @param vo
     * @return 结果
     */
    @Override
    public int updateUserAddress(UpdateUserAddressReqVO vo) {
        if (CommonWhetherEnum.YES.getValue().equals(vo.getIsDefault())) {
            UserAddress userAddress = this.findUserAddressByAddressId(vo.getAddressId());
            setOtherAddress(userAddress.getUserId());
        }
        return this.updateUserAddress(UserAddressConvert.INSTANCE.convert(vo));
    }

    /**
     * 批量删除用户地址信息
     *
     * @param addressIds 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> addressIds) {
        return userAddressMapper.deleteBatchIds(addressIds);
    }

    /**
     * 设置默认地址
     *
     * @param addressId 地址id
     */
    @Override
    public void setDefaultAddress(Long addressId) {
        UserAddress userAddress = this.findUserAddressByAddressId(addressId);
        setOtherAddress(userAddress.getUserId());
        // 设置默认地址
        this.updateUserAddress(UserAddress.builder().addressId(addressId).isDefault(CommonWhetherEnum.YES.getValue()).build());
    }

    @Override
    public UserAddress selectDefaultAddress(Long addressId, Long userId) {
        return userAddressMapper.selectDefaultAddress(addressId, userId);
    }

    private void setOtherAddress(Long userId) {
        // 将其他地址设为非默认地址
        userAddressMapper.update(null, new LambdaUpdateWrapper<UserAddress>()
                .eq(UserAddress::getUserId, userId)
                .set(UserAddress::getIsDefault, CommonWhetherEnum.NO.getValue()));
    }
}
