package com.think.cloud.thinkshop.mall.convert.shippingtemplates;

import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.dto.ShippingTemplatesDTO;
import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.vo.ShippingTemplatesVO;
import com.think.cloud.thinkshop.mall.convert.productgroup.ProductGroupConvert;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplates;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author zkthink
 * @apiNote
 **/
@Mapper
public interface ShippingTemplatesConvert {
    ShippingTemplatesConvert INSTANCE = Mappers.getMapper(ShippingTemplatesConvert.class);

    ShippingTemplatesDTO convert(ShippingTemplates shippingTemplates);
    ShippingTemplates convert(ShippingTemplatesDTO shippingTemplates);

    ShippingTemplatesVO convert2VO(ShippingTemplates shippingTemplates);
}
