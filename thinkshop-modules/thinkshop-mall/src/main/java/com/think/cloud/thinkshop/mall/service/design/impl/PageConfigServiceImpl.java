package com.think.cloud.thinkshop.mall.service.design.impl;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.*;
import com.think.cloud.thinkshop.mall.convert.design.PageConfigConvert;
import com.think.cloud.thinkshop.mall.domain.design.PageConfig;
import com.think.cloud.thinkshop.mall.mapper.design.PageConfigMapper;
import com.think.cloud.thinkshop.mall.service.design.IPageConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * PageConfigService业务层处理
 *
 * @author dengguangxian
 * @date 2024-05-29
 */
@Service
public class PageConfigServiceImpl implements IPageConfigService {
    public static final Long PAGE_CONFIG_ID = 1L;

    @Autowired
    private PageConfigMapper pageConfigMapper;

    @Override
    public PageConfigPageTopRespVO selectPageConfigTop() {
        return PageConfigConvert.INSTANCE.convert(selectById());
    }

    @Override
    public PageConfigPageBottomRespVO selectPageConfigBottom() {
        PageConfig pageConfig = selectById();
        PageConfigPageBottomRespVO result = PageConfigConvert.INSTANCE.convert1(pageConfig);
        String pageBottomQrCodeUrl = pageConfig.getPageBottomQrCodeUrl();
        if (pageBottomQrCodeUrl != null && !pageBottomQrCodeUrl.isEmpty()) {
            result.setPageBottomQrCodeUrls(Arrays.asList(pageBottomQrCodeUrl.split(",")));
        }
        return result;
    }

    /**
     * 查询页配置
     */
    private PageConfig selectById() {
        return pageConfigMapper.selectById(PAGE_CONFIG_ID);
    }

    @Override
    public int insertPageConfig(PageConfigAddOrEditReqVO vo) {
        PageConfig pageConfig = PageConfigConvert.INSTANCE.convert2(vo);
        pageConfig.setId(PAGE_CONFIG_ID);
        return pageConfigMapper.insert(pageConfig);
    }

    @Override
    public int updatePageConfig(PageConfigAddOrEditReqVO vo) {
        PageConfig pageConfig = PageConfigConvert.INSTANCE.convert2(vo);
        pageConfig.setId(PAGE_CONFIG_ID);
        return pageConfigMapper.updateById(pageConfig);
    }

    @Override
    public int insertOrUpdatePageConfig(PageConfigAddOrEditReqVO vo) {
        PageConfig pageConfig = PageConfigConvert.INSTANCE.convert2(vo);
        pageConfig.setId(PAGE_CONFIG_ID);
        List<String> pageBottomQrCodeUrls = vo.getPageBottomQrCodeUrls();
        pageConfig.setPageBottomQrCodeUrl(pageBottomQrCodeUrls != null ?
                String.join(",", pageBottomQrCodeUrls) : ""
        );
        return selectById() == null ? pageConfigMapper.insert(pageConfig) : pageConfigMapper.updateById(pageConfig);
    }
}
