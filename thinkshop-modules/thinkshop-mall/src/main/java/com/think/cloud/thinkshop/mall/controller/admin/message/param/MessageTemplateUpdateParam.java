package com.think.cloud.thinkshop.mall.controller.admin.message.param;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统消息模版配置对象参数
 *
 * @author moxiangrong
 * @date 2024-07-19
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageTemplateUpdateParam {
    /**
     * 主键
     */
    private Long id;
    /**
     * 开启站内信,0:不使用 1：使用
     */
    private Integer useInform;

    /**
     * 开启邮件通知,0:不使用 1：使用
     */
    private Integer useEmail;
    /**
     * 站内信标题
     */
    private String informTitle;
    /**
     * 站内信内容
     */
    private String informTemplate;
    /**
     * 站内信标题
     */
    private String emailTitle;
    /**
     * 站内信内容
     */
    private String emailTemplate;
}
