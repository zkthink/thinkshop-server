package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.ProductGroupRespVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * 商品明细响应VO
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Data
public class ProductDetailRespVO {

    /** 商品id */
    @ApiModelProperty("商品id")
    private Long productId;

    /** 商品编号 */
    @ApiModelProperty("商品编号")
    private String productCode;

    /** 商品名称 */
    @ApiModelProperty("商品名称")
    private String productName;

    /** 简介 */
    @ApiModelProperty("简介")
    private String introduce;

    /** 商品图片 */
    @ApiModelProperty("商品图片")
    private String image;

    /** 视频 */
    @ApiModelProperty("视频")
    private String video;

    /** 商品详情 */
    @ApiModelProperty("商品详情")
    private String detail;

    /** 分类id */
    @ApiModelProperty("分类id")
    private Long categoryId;

    /** 分类名称 */
    @ApiModelProperty("分类名称")
    private String categoryName;

    /** 是否收税 0否 1是 */
    @ApiModelProperty("是否收税 0否 1是")
    private Integer tax;

    /** 规格最低价 */
    @ApiModelProperty("规格最低价")
    private BigDecimal minPrice;

    /** 规格最高价 */
    @ApiModelProperty("规格最高价")
    private BigDecimal maxPrice;

    /** 销量 */
    @ApiModelProperty("销量")
    private Integer sales;

    /** 库存 */
    @ApiModelProperty("库存")
    private Integer stock;

    /** 状态（0：未上架，1：上架） */
    @ApiModelProperty("状态（0：未上架，1：上架）")
    private Integer isShow;

    @ApiModelProperty("规格类型：1 单规格，2：多规格")
    private Integer skuType;

    /** 创建人 */
    @ApiModelProperty("创建人")
    private String createBy;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    /**
     *  分组
     */
    @ApiModelProperty("分组")
    private List<ProductGroupRespVO> groups;

    /**
     * 属性集合
     */
    @ApiModelProperty("属性集合")
    private List<ProductAttrRespVO> attrs;

    /**
     * sku集合
     */
    @ApiModelProperty("sku集合")
    private List<ProductSkuRespVO> skus;

}
