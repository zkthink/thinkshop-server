package com.think.cloud.thinkshop.mall.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.app.cart.vo.ShopCartRespVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderCartRespVO;
import com.think.cloud.thinkshop.mall.enums.cart.PayStatusEnum;
import com.think.cloud.thinkshop.mall.enums.common.CommonWhetherEnum;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.ShopCart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 购物车Mapper接口
 *
 * @author zkthink
 * @date 2024-05-17
 */
@Mapper
public interface ShopCartMapper extends BaseMapper<ShopCart> {

    default List<ShopCart> selectListByUserId(Long userId) {
        return selectList(new LambdaQueryWrapper<ShopCart>()
                .eq(ShopCart::getUserId, userId)
                .eq(ShopCart::getIsPay, PayStatusEnum.NO.getValue())
                .eq(ShopCart::getIsNew, CommonWhetherEnum.NO.getValue())
                .orderByDesc(ShopCart::getCartId));
    }

    List<AppOrderCartRespVO> getOrderCartInfo(@Param("cartIds") List<Long> cartIds);

    List<ShopCartRespVO> getCartInfo(@Param("cartIds") List<String> cartIds);

    void batchInsert(@Param("list") List<ShopCart> list);
}
