package com.think.cloud.thinkshop.mall.controller.app.category.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;


@Data
@ToString(callSuper = true)
public class AppProductCategoryLevelVO {

    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;

    /**
     * 父类目id
     */
    @ApiModelProperty("父类目id")
    private Long parentCategoryId;

    /**
     * 类目级别
     */
    @ApiModelProperty("类目级别")
    private Long level;

    /**
     * 类目名称
     */
    @ApiModelProperty("类目名称")
    private String name;

    /**
     * 类路径
     */
    @ApiModelProperty("类路径")
    private String path;

    /**
     * 类目图片
     */
    @ApiModelProperty("类目图片")
    private String pictureUrl;

    /**
     * 权重
     */
    @ApiModelProperty("权重")
    private Long weight;

    /**
     * 子类目
     */
    @ApiModelProperty("子类目")
    private List<AppProductCategoryLevelVO> child;

}
