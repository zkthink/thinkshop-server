package com.think.cloud.thinkshop.mall.domain;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 商品分组关联对象 mall_product_group_relation
 *
 * @author zkthink
 * @date 2024-05-10
 */
@TableName("mall_product_group_relation")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductGroupRelation
{
    private static final long serialVersionUID = 1L;

    /** 分组关联id */
    @TableId(type = IdType.AUTO)
    private Long groupRelationId;

    /** 分组id */
    @Excel(name = "分组id")
    private Long groupId;

    /** 商品id */
    @Excel(name = "商品id")
    private Long productId;

}
