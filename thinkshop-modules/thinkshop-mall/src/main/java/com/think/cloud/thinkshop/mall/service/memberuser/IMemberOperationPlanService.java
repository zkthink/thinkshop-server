package com.think.cloud.thinkshop.mall.service.memberuser;


import cn.hutool.core.date.DateTime;
import com.think.cloud.thinkshop.common.enums.operationplan.OperationPlanStatusEnum;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanSearchReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanSearchRespVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationPlan;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;


/**
 * 客户运营计划Service接口
 *
 * @author zkthink
 * @date 2024-06-07
 */
public interface IMemberOperationPlanService {
    /**
     * 查询客户运营计划
     *
     * @param id 客户运营计划主键
     * @return 客户运营计划
     */
    public MemberOperationPlan selectMemberOperationPlanById(Long id);


    /**
     * 查询客户运营计划
     * @param id /
     * @return /
     */
    public MemberOperationPlanSearchRespVO selectVOById(Long id);

    /**
     * 查询客户运营计划列表
     *
     * @param vo 客户运营计划
     * @return 客户运营计划集合
     */
    public TableDataInfo selectMemberOperationPlanList(MemberOperationPlanSearchReqVO vo);

    /**
     * 新增客户运营计划
     *
     * @param vo 客户运营计划
     * @return 结果
     */
    public int insertMemberOperationPlan(MemberOperationPlanAddReqVO vo);

    /**
     * 修改客户运营计划
     *
     * @param vo 客户运营计划
     * @return 结果
     */
    public int updateMemberOperationPlan(MemberOperationPlanEditReqVO vo);

    /**
     * 批量删除客户运营计划信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public void batchDelete(List<Long> ids);

    /**
     * 执行计划
     * @param bean /
     */
    void execute(MemberOperationPlan bean);

    /**
     * 更新状态
     * @param bean /
     * @param operationPlanStatusEnum /
     */
    void updateStatus(MemberOperationPlan bean, OperationPlanStatusEnum operationPlanStatusEnum);

    /**
     * 执行计划
     * @param bean /
     */
    void doExecute(MemberOperationPlan bean);

    /**
     * 客户加入分群后执行运营计划
     * @param groupId 分群id
     * @param userId 用户id
     */
    void doExecuteAfterAddGroup(Long groupId, Long userId);

    /**
     * 获取待执行计划
     * @return /
     */
    List<MemberOperationPlan> selectPendingList(DateTime dateTime);

}
