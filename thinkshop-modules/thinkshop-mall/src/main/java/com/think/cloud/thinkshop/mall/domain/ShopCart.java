package com.think.cloud.thinkshop.mall.domain;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import java.sql.Timestamp;

/**
 * 购物车对象 mall_shop_cart
 * 
 * @author zkthink
 * @date 2024-05-17
 */
@TableName("mall_shop_cart")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopCart
{
    private static final long serialVersionUID = 1L;

    /** 购物车ID */
    @TableId(type = IdType.AUTO)
    private Long cartId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

    /** 商品属性id */
    @Excel(name = "商品属性id")
    private Long skuId;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Integer num;

    /** 0 = 未购买 1 = 已购买 */
    @Excel(name = "0 = 未购买 1 = 已购买")
    private Integer isPay;

    /** 是否为立即购买 */
    @Excel(name = "是否为立即购买")
    private Integer isNew;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Timestamp createTime;

    /** 修改人 */
    @Excel(name = "修改人")
    private String updateBy;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private Timestamp updateTime;

}
