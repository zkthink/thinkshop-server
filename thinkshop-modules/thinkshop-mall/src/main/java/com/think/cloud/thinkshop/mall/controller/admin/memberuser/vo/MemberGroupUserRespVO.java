package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.think.common.core.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(description = "用户分组 组中的用户详情 RespVO")
@ToString(callSuper = true)
public class MemberGroupUserRespVO {
    /**
     * 用户id
     */
    private Long id;
    private String firstName;
    private String lastName;
    @ApiModelProperty(value = "邮箱", required = true, example = "")
    private String email;
    @ApiModelProperty(value = "注册时间", required = true, example = "")
    private Timestamp createTime;
    /** 消费次数 */
    private Integer orderCount;

    /** 累计消费金额 */
    private BigDecimal totalOrderAmount;

    /** 上次消费时间 */
    private Timestamp lastConsumeTime;
    @ApiModelProperty("标签")
    private List<String> tags;
}
