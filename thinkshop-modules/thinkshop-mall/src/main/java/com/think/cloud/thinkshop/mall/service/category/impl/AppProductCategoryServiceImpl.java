package com.think.cloud.thinkshop.mall.service.category.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.think.cloud.thinkshop.mall.controller.app.category.vo.AppProductCategoryInfoVO;
import com.think.cloud.thinkshop.mall.controller.app.category.vo.AppProductCategoryLevelVO;
import com.think.cloud.thinkshop.mall.convert.category.ProductCategoryConvert;
import com.think.cloud.thinkshop.mall.domain.ProductCategory;
import com.think.cloud.thinkshop.mall.mapper.ProductCategoryMapper;
import com.think.cloud.thinkshop.mall.service.category.AppProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 商品类目AppService接口
 *
 * @author zkthink
 * @date 2024-05-14
 */
@Service
public class AppProductCategoryServiceImpl implements AppProductCategoryService {

    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Override
    public List<AppProductCategoryLevelVO> getProductTree() {
        List<AppProductCategoryLevelVO> levelVOList = ProductCategoryConvert.INSTANCE.convertList1(productCategoryMapper.selectList(new LambdaQueryWrapper<>()));
        if (CollectionUtils.isEmpty(levelVOList)) return levelVOList;
        Map<Long, List<AppProductCategoryLevelVO>> levelVOListMap = levelVOList.stream().collect(Collectors.groupingBy(AppProductCategoryLevelVO::getParentCategoryId));
        List<AppProductCategoryLevelVO> oneLevelList = levelVOListMap.get(0l);
        for (AppProductCategoryLevelVO oneLevelVO : oneLevelList) {
            oneLevelVO.setPath(oneLevelVO.getName());
            List<AppProductCategoryLevelVO> twoLevelList = levelVOListMap.get(oneLevelVO.getCategoryId());
            if (CollectionUtils.isEmpty(twoLevelList)) continue;
            for (AppProductCategoryLevelVO twoLevelVO : twoLevelList) {
                twoLevelVO.setPath(oneLevelVO.getPath() + "/" + twoLevelVO.getName());
                List<AppProductCategoryLevelVO> threeLevelList = levelVOListMap.get(twoLevelVO.getCategoryId());
                if (CollectionUtils.isEmpty(threeLevelList)) continue;
                threeLevelList.forEach(res -> res.setPath(twoLevelVO.getPath() + "/" + res.getName()));
                twoLevelVO.setChild(sortByWeight(threeLevelList));
            }
            oneLevelVO.setChild(sortByWeight(twoLevelList));
        }
        return sortByWeight(oneLevelList);
    }

    @Override
    public List<Object> getAllKidCategoryIds(Long categoryId) {
        ProductCategory productCategory = productCategoryMapper.selectById(categoryId);
        List<Object> categoryIds = new ArrayList<>();
        // 判断级别
        if (productCategory.getLevel() == 2) {
            categoryIds.addAll(getKidCategoryIds(Arrays.asList(categoryId)));
        } else if (productCategory.getLevel() == 1) {
            categoryIds.addAll(getKidCategoryIds(Arrays.asList(categoryId)));
            if (CollectionUtils.isNotEmpty(categoryIds)) {
                categoryIds.addAll(getKidCategoryIds(categoryIds));
            }
        }
        categoryIds.add(categoryId);
        return categoryIds;
    }

    @Override
    public List<Object> getKidCategoryIds(List<Object> categoryIds) {
        return productCategoryMapper.selectObjs(new LambdaQueryWrapper<ProductCategory>()
                .in(ProductCategory::getParentCategoryId, categoryIds)
                .select(ProductCategory::getCategoryId));
    }

    @Override
    public String getCategoryPath(Long categoryId) {
        return path(new StringBuilder(), categoryId);
    }

    @Override
    public AppProductCategoryInfoVO getCategoryInfo(Long categoryId) {
        ProductCategory category = productCategoryMapper.selectById(categoryId);
        return new AppProductCategoryInfoVO(categoryId, category.getName(), getCategoryPath(categoryId));
    }

    private String path(StringBuilder path, Long categoryId) {
        ProductCategory category = productCategoryMapper.selectById(categoryId);
        path.insert(0, category.getName());
        if (!Objects.isNull(category.getParentCategoryId()) && category.getParentCategoryId() != 0) {
            path.insert(0, "/");
            path(path, category.getParentCategoryId());
        }
        return path.toString();
    }

    private List<AppProductCategoryLevelVO> sortByWeight(List<AppProductCategoryLevelVO> list) {
        return list.stream().sorted(Comparator.comparing(AppProductCategoryLevelVO::getWeight).reversed()).collect(Collectors.toList());
    }
}
