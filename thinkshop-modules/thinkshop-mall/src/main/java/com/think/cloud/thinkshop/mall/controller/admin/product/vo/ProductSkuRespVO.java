
package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 商品sku响应VO
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductSkuRespVO {

    /** id */
    private Long skuId;

    /** 商品ID */
    private Long productId;

    /** 商品属性索引值 (attr_value|attr_value[|....]) */
    private String sku;

    private Map<String, String> skuDetail;

    /** 图片 */
    private String image;

    /** 原价 */
    private BigDecimal originalPrice;

    /** 价格 */
    private BigDecimal price;

    /** 属性对应的库存 */
    private Integer stock;

    /** 销量 */
    private Integer sales;

    /** 重量 */
    private BigDecimal weight;

    /** 体积 */
    private BigDecimal volume;

}
