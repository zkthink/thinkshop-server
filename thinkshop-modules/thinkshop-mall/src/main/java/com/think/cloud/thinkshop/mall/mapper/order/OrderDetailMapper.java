package com.think.cloud.thinkshop.mall.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesOrderDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderDetailPageRespVO;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.order.OrderDetail;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 订单明细Mapper接口
 *
 * @author zkthink
 * @date 2024-05-23
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

    void batchInsert(List<OrderDetail> list);

    List<AppOrderDetailPageRespVO> getOrderDetail(@Param("orderIds") List<Long> orderIds);

    @Select("<script> </script>")
    Long saledOrderCount(@Param("startTime") String startTime, @Param("endTime") String endTime);

    List<OrderDetailRespVO> selectOrderDetailListByOrderIds(@Param("orderIds") List<Long> orderIds);

    List<AfterSalesOrderDetailRespVO> selectOrderDetailListByAfterSalesIds(@Param("afterSalesIds") List<Long> afterSalesIds);

    List<AppAfterSalesDetailRespVO> selectAfterSalesOrderDetailList(@Param("afterSalesIds") List<Long> afterSalesIds);

    default void batchUpdate(List<OrderDetail> list){
        Db.updateBatchById(list, list.size());
    }

}
