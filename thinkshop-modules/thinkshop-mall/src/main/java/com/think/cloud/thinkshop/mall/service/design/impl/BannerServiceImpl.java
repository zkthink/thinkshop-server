package com.think.cloud.thinkshop.mall.service.design.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerPageRespVO;
import com.think.cloud.thinkshop.mall.convert.design.BannerConvert;
import com.think.cloud.thinkshop.mall.domain.design.Banner;
import com.think.cloud.thinkshop.mall.mapper.design.BannerMapper;
import com.think.cloud.thinkshop.mall.service.design.IBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * BannerService业务层处理
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Service
public class BannerServiceImpl implements IBannerService {
    @Autowired
    private BannerMapper bannerMapper;

    /**
     * 查询Banner
     *
     * @param id Banner主键
     * @return Banner
     */
    @Override
    public Banner selectBannerById(Long id) {
        return bannerMapper.selectById(id);
    }

    /**
     * 查询Banner列表
     *
     * @return Banner
     */
    @Override
    public List<BannerPageRespVO> selectBannerList() {
        return BannerConvert.INSTANCE.convertList(bannerMapper.selectList(new LambdaQueryWrapper<Banner>()));
    }

    /**
     * 新增Banner
     *
     * @param vo
     * @return 结果
     */
    @Override
    public int insertBanner(BannerAddReqVO vo) {
        Banner convert = BannerConvert.INSTANCE.convert(vo);
        return bannerMapper.insert(convert);
    }

    /**
     * 修改Banner
     *
     * @param vo
     * @return 结果
     */
    @Override
    public int updateBanner(BannerEditReqVO vo) {
//        banner.setUpdateTime(DateUtils.getNowDate());
        Banner banner = BannerConvert.INSTANCE.convert(vo);
        return bannerMapper.updateById(banner);
    }

    /**
     * 批量删除Banner信息(逻辑删除)
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return bannerMapper.deleteBatchIds(ids);
    }
}
