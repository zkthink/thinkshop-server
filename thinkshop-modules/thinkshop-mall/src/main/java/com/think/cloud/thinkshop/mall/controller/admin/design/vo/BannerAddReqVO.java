package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@Schema(description = "管理后台 - banner新建 Request VO")
@ToString(callSuper = true)
public class BannerAddReqVO {
    @Schema(description = "图片地址", required = true, example = "http://xxxx")
    @NotNull(message = "图片地址不能为空")
    private String imageUrl;

    @Schema(description = "重定向地址", required = true, example = "http://xxxx")
    @NotNull(message = "重定向地址不能为空")
    private String redirectUrl;
}
