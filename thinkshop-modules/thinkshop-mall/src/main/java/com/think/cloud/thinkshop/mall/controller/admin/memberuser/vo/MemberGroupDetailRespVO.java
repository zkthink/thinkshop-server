package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

@Data
@ApiModel(description = "用户分组 组详情")
@ToString(callSuper = true)
public class MemberGroupDetailRespVO {
    /** ID */
    private Long id;

    @ApiModelProperty(value = "人群名称", required = true, example = "")
    private String name;

    @ApiModelProperty(value = "人群定义", required = true, example = "")
    private String ruleInfo;

    @ApiModelProperty(value = "人群数量", required = true, example = "")
    private Long userCount;

    private Long planId;
}
