package com.think.cloud.thinkshop.mall.service.statistics;

import com.think.cloud.thinkshop.mall.controller.admin.index.vo.IndexStatisticsReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.index.vo.IndexStatisticsRespVO;

public interface IStatisticsService {
    /**
     * 管理后台统计
     * @return 统计内容
     */
    IndexStatisticsRespVO indexStatistics(IndexStatisticsReqVO vo);
}
