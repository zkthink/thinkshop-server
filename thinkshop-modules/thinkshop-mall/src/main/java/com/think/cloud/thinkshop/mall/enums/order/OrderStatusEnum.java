package com.think.cloud.thinkshop.mall.enums.order;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 订单状态枚举
 */
@Getter
@AllArgsConstructor
public enum OrderStatusEnum {

    REFUNDED(-2, "已退款"),
    CANCELED(-1, "已取消"),
    AWAITING_PAYMENT(0, "待付款"),
    AWAITING_SHIPMENT(1, "待发货"),
    AWAITING_RECEIPT(2, "待收货"),
    COMPLETED(3, "已完成");

    private Integer value;
    private String desc;


    public static OrderStatusEnum toEnum(Integer value) {
        return Stream.of(OrderStatusEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
