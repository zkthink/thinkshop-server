package com.think.cloud.thinkshop.mall.domain.coupon;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 商品优惠券对象 mall_product_coupon
 *
 * @author zkthink
 * @date 2024-05-21
 */
@TableName("mall_product_coupon")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductCoupon
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 优惠券编号 */
    @Excel(name = "优惠券编号")
    private String couponCode;

    /** 优惠券名称 */
    @Excel(name = "优惠券名称")
    private String couponName;

    /** 优惠券面值 */
    @Excel(name = "优惠券面值")
    private BigDecimal couponValue;

    /** 优惠券类型：1、满减券，2、折扣券 */
    @Excel(name = "优惠券类型：1、满减券，2、折扣券")
    private Integer couponType;

    /** 满减门槛 */
    @Excel(name = "满减门槛")
    private BigDecimal threshold;

    /** 折扣 */
    @Excel(name = "折扣")
    private BigDecimal discount;

    /** 优惠券范围：1、所有商品，2、选中商品 */
    @Excel(name = "优惠券范围：1、所有商品，2、选中商品")
    private Integer couponScope;

    /** 范围值 */
    @Excel(name = "范围值")
    private String scopeValues;

    /** 剩余优惠券数量 */
    @Excel(name = "剩余优惠券数量")
    private Long number;

    /** 已领取优惠券数量 */
    @Excel(name = "已领取优惠券数量")
    private Long receivedNumber;

    /** 限制每人领取数量 */
    @Excel(name = "限制每人领取数量")
    private Long limitNumber;

    /** 过期类型：1、按时间，2、按天数 */
    @Excel(name = "过期类型：1、按时间，2、按天数")
    private Integer expirationType;

    /** 生效时间 */
    @Excel(name = "生效时间")
    private Timestamp takingEffectTime;

    /** 过期时间 */
    @Excel(name = "过期时间")
    private Timestamp expirationTime;

    /** 过期天数 */
    @Excel(name = "过期天数")
    private Long expirationDay;

    /** 领取限制类型 1 无限制 2限制次数 */
    @Excel(name = "领取限制类型 1 无限制 2限制次数")
    private Integer receiveType;

    /** 备注 */
    private String remark;

    /** 优惠券状态：0、未开始，1、进行中，2、已结束 */
    private Integer status;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;


    /** 创建人 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Timestamp createTime;

    /** 修改人 */
    @Excel(name = "修改人")
    private String updateBy;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private Timestamp updateTime;

}
