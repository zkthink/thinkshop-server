package com.think.cloud.thinkshop.mall.service.product;


import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderCartRespVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductPageReqVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductPageRespVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppRecommendProductPageReqVO;
import com.think.cloud.thinkshop.mall.domain.Product;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;

/**
 * 商品AppService接口
 *
 * @author zkthink
 * @date 2024-05-14
 */
public interface AppProductService {

    /**
     * 查询商品列表
     *
     * @param vo
     * @return 商品集合
     */
    public TableDataInfo selectProductList(AppProductPageReqVO vo);

    /**
     * 获取商品详情
     *
     * @param productId
     * @return 商品集合
     */
    public AppProductDetailRespVO getProductDetail(Long productId);

    /**
     * 推荐好物
     *
     * @param vo
     * @return 商品集合
     */
    public List<AppProductPageRespVO> recommendList(AppRecommendProductPageReqVO vo);

    /**
     * 校验商品状态
     *
     * @param productId
     */
    public void checkProduct(Long productId);

    /**
     * 校验库存
     *
     * @param skuId
     * @param num
     */
    public void checkStock(Long skuId, Integer num);

    /**
     * 处理库存
     *
     * @param carts
     * @param type 类型  0:回退；1:扣减
     */
    public void handleStock(List<AppOrderCartRespVO> carts, Integer type);

    /**
     * 查询商品
     * @param productIds
     * @return
     */
    List<Product> selectListWithDeleted(List<Long> productIds);
}
