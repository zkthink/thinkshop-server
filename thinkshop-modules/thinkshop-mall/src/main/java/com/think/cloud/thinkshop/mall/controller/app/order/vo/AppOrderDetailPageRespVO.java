package com.think.cloud.thinkshop.mall.controller.app.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;


/**
 * 订单查询明细响应VO
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Data
public class AppOrderDetailPageRespVO {

    @ApiModelProperty("订单明细id")
    private Long id;

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("商品ID")
    private Long productId;

    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("规格id")
    private Long skuId;

    @ApiModelProperty("规格")
    private String sku;

    @ApiModelProperty("规格图片")
    private String image;

    @ApiModelProperty("产品图片，规格图片不存在时使用")
    private String productImage;

    @ApiModelProperty("商品数量")
    private Integer num;

    @ApiModelProperty("原价")
    private BigDecimal originalPrice;

    @ApiModelProperty("实际价格")
    private BigDecimal price;

    @ApiModelProperty("状态：1、正常，2、售后中，3、售后完成")
    private Integer state;

}
