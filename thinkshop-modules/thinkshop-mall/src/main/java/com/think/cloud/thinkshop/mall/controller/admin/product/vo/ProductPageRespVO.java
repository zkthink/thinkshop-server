package com.think.cloud.thinkshop.mall.controller.admin.product.vo;


import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * 商品分页请求VO
 *
 * @author zkthink
 * @date 2024-05-13
 */
@Data
public class ProductPageRespVO {

    /** 商品id */
    private Long productId;

    /** 商品编号 */
    private String productCode;

    /** 商品名称 */
    private String productName;

    /** 简介 */
    private String introduce;

    /** 商品图片 */
    private String image;

    /** 视频 */
    private String video;

    /** 商品详情 */
    private String detail;

    /** 分类id */
    private Long categoryId;

    /** 是否收税 0否 1是 */
    private Integer tax;

    /** 规格最低价 */
    private BigDecimal minPrice;

    /** 规格最高价 */
    private BigDecimal maxPrice;

    /** 售价区间 */
    private String priceRange;

    /** 销量 */
    private Integer sales;

    /** 库存 */
    private Integer stock;

    /** 状态（0：未上架，1：上架） */
    private Integer isShow;

    /** 创建人 */
    private String createBy;

    /** 创建时间 */
    private Timestamp createTime;

    /** 修改人 */
    private String updateBy;

    /** 修改时间 */
    private Timestamp updateTime;
    /**
     * 商品规格类型  1：单规格 2：多规格
     */
    private Integer skuType;
}
