package com.think.cloud.thinkshop.mall.domain.design;

import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import lombok.*;

/**
 * Banner对象 mall_banner
 *
 * @author zkthink
 * @date 2024-05-27
 */
@TableName("mall_index_config")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IndexDesign {
    private Long id;
    @Excel(name = "资源地址")
    private String resourceUrl;
    @Excel(name = "标题")
    private String title;
    @Excel(name = "简介")
    private String introduction;
    @Excel(name = "链接")
    private String redirectUrl;
    @Excel(name = "类型，1：左图右文，2：左文右图，视频：0")
    private Integer type;
    @Excel(name = "状态，1：可用，2：不可用")
    private Integer status;
    @Excel(name = "是否删除")
    private Integer deleted;
}
