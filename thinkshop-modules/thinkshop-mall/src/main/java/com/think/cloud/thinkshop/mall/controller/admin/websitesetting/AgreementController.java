package com.think.cloud.thinkshop.mall.controller.admin.websitesetting;


import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto.AgreementDTO;
import com.think.cloud.thinkshop.mall.domain.websitesetting.Agreement;
import com.think.cloud.thinkshop.mall.service.websitesetting.IAgreementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 协议Controller
 *
 * @author zkthink
 * @date 2024-05-27
 */
@RestController
@RequestMapping("/admin/agreement")
@Api(tags="ADMIN:协议")
public class AgreementController extends BaseController
{
    @Autowired
    private IAgreementService agreementService;

//    /**
//     * 查询协议列表
//     */
//    @RequiresPermissions("mall:agreement:list")
//    @GetMapping("/list")
//    @ApiOperation(value = "查询协议列表")
//    public TableDataInfo list(Agreement agreement)
//    {
//        startPage();
//        List<Agreement> list = agreementService.selectAgreementList(agreement);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出协议列表
//     */
//    @RequiresPermissions("mall:agreement:export")
//    @Log(title = "协议", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ApiOperation(value = "导出协议列表")
//    public void export(HttpServletResponse response, Agreement agreement)
//    {
//        List<Agreement> list = agreementService.selectAgreementList(agreement);
//        ExcelUtil<Agreement> util = new ExcelUtil<Agreement>(Agreement.class);
//        util.exportExcel(response, list, "协议数据");
//    }
//
//    /**
//     * 获取协议详细信息
//     */
//    @RequiresPermissions("mall:agreement:query")
//    @GetMapping(value = "/get/{id}")
//    @ApiOperation(value = "获取协议详细信息")
//    public AjaxResult getInfo(@PathVariable("id") Long id)
//    {
//        return success(agreementService.selectAgreementById(id));
//    }
//
//    /**
//     * 新增协议
//     */
//    @RequiresPermissions("mall:agreement:add")
//    @Log(title = "协议", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ApiOperation(value = "新增协议")
//    public AjaxResult add(@RequestBody Agreement agreement)
//    {
//        return toAjax(agreementService.insertAgreement(agreement));
//    }
//
//    /**
//     * 修改协议
//     */
//    @RequiresPermissions("mall:agreement:edit")
//    @Log(title = "协议", businessType = BusinessType.UPDATE)
//    @PutMapping("/update")
//    @ApiOperation(value = "修改协议")
//    public AjaxResult edit(@RequestBody Agreement agreement)
//    {
//        return toAjax(agreementService.updateAgreement(agreement));
//    }
//
//    /**
//     * 删除协议
//     */
//    @RequiresPermissions("mall:agreement:remove")
//    @Log(title = "协议", businessType = BusinessType.DELETE)
//	@DeleteMapping("/delete")
//    @ApiOperation(value = "删除协议")
//    public AjaxResult remove(@Valid Long[] ids)
//    {
//        return toAjax(agreementService.batchDelete(Arrays.asList(ids)));
//    }

    @RequiresPermissions("mall:agreement:list")
    @GetMapping("/get")
    @ApiOperation(value = "获取协议")
    public AjaxResult getAgreement()
    {
        AgreementDTO dto = agreementService.getAgreement();
        return success(dto);
    }

    @RequiresPermissions("mall:agreement:edit")
    @PostMapping("/save")
    @ApiOperation(value = "保存协议")
    public AjaxResult saveAgreement(@RequestBody AgreementDTO dto)
    {
        agreementService.saveAgreement(dto);
        return success();
    }
}
