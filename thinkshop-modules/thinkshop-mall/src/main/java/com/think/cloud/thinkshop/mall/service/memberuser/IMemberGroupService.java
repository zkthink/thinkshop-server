package com.think.cloud.thinkshop.mall.service.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.*;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberGroup;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;

/**
 * 客户分群Service接口
 *
 * @author guangxian
 * @date 2024-06-03
 */
public interface IMemberGroupService {
    /**
     * 查询客户分群
     *
     * @param id 客户分群主键
     * @return 客户分群
     */
    public MemberGroupDetailRespVO selectMemberGroupById(Long id);
    /**
     * 查询客户分群
     *
     * @param id 客户分群主键
     * @return 客户分群
     */
    public List<MemberGroupDetailRespVO> selectMemberGroupById(List<Long> id);
    /**
     * 查询客户分群列表
     *
     * @param vo 客户分群
     * @return 客户分群集合
     */
    public List<MemberGroup> selectMemberGroupList(MemberGroupReqVO vo);

    /**
     * 用于新增、编辑用户分区的特征列表
     *
     * @return
     */
    List<MemberGroupEditTraitRespVO> getTraitOption();

    /**
     * 新增客户分群
     *
     * @param vo 客户分群参数
     * @return 结果
     */
    public int insertMemberGroup(MemberGroupAddReqVO vo);

    /**
     * 修改客户分群
     *
     * @param vo 客户分群
     * @return 结果
     */
    public int updateMemberGroup(MemberGroupAddReqVO vo);

    /**
     * 批量删除客户分群信息
     *
     * @param id 主键
     * @return 结果
     */
    int deleteById(Long id);

    /**
     * 查看组中客户详情
     *
     * @param groupId
     */
    TableDataInfo memberGroupDetail(Long groupId);

    /**
     * 用户组中满足条件的用户更新
     */
    void memberGroupUserUpdate();

    /**
     * 用户组中满足条件的用户更新
     */
    void memberGroupUserUpdate(Long groupId);

    /**
     * 用户组中满足条件的用户更新 (用户组配置 新增 、修改的时候触发)
     */
    void memberGroupUserUpdate(String groupName);
    /**
     * 通过用户id更新用户分群 （用户支付 ，创建订单，加购物车，更新标签，登录（最后到访） 时候触发）
     */
    void memberGroupUserUpdateByUserId(Long userId);

    void memberGroupUserUpdateByUserIdSync(Long userId);

    /**
     * 批量删除客户分群信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);


    /**
     * 查询客户分群中所有客户
     * @param groupIdList /
     * @return /
     */
    List<Long> selectUseridListByGroupIds(List<Long> groupIdList);
}
