package com.think.cloud.thinkshop.mall.controller.app.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 订单记录DTO
 *
 * @author zkthink
 * @date 2024-05-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AppOrderLogDTO {

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("操作类型")
    private Integer operateType;

}
