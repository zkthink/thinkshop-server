package com.think.cloud.thinkshop.mall.service.productcomment.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.controller.admin.productcomment.param.ProductCommentAddParam;
import com.think.cloud.thinkshop.mall.controller.admin.productcomment.vo.ProductCommentVO;
import com.think.cloud.thinkshop.mall.domain.ProductComment;
import com.think.cloud.thinkshop.mall.enums.product.ProductCommentStatusEnum;
import com.think.cloud.thinkshop.mall.mapper.ProductCommentMapper;
import com.think.cloud.thinkshop.mall.service.productcomment.AppProductCommentService;
import com.think.cloud.thinkshop.mall.service.productcomment.IProductCommentService;
import com.think.common.core.utils.PageUtils;
import com.think.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppProductCommentServiceImpl implements AppProductCommentService {
    @Autowired
    private ProductCommentMapper productCommentMapper;

    @Autowired
    private IProductCommentService productCommentService;

    @Override
    public TableDataInfo getProductComments(Long id) {
        LambdaQueryWrapper<ProductComment> query = Wrappers.<ProductComment>lambdaQuery()
                .eq(ProductComment::getProductId, id)
                .eq(ProductComment::getStatus, ProductCommentStatusEnum.YES.getValue())
                .orderByDesc(ProductComment::getCommentTime);
        List<ProductComment> list = this.selectList(query);
        List<ProductCommentVO> page = productCommentService.viewProductCommentList(list, true, false, false);
        return new TableDataInfo(page, PageUtils.getTotal(list));
    }

    private List<ProductComment> selectList(LambdaQueryWrapper<ProductComment> query) {
        return productCommentMapper.selectList(query);
    }

    @Override
    public int submitOrderComment(ProductCommentAddParam param) {
        return productCommentService.insertProductComment(param);
    }

    @Override
    public ProductComment findCommentByOrderId(Long orderId) {
        LambdaQueryWrapper<ProductComment> query = Wrappers.<ProductComment>lambdaQuery()
                .eq(ProductComment::getOrderId, orderId);
        List<ProductComment> list = this.selectList(query);
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}
