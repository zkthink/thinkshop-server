package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * 客户分群 新增 request vo
 *
 * @author guangxian
 * @date 2024-06-03
 */
@Data
@ApiModel(description = "用户分组 新增、编辑 ReqVO")
@ToString(callSuper = true)
public class MemberGroupAddReqVO {
    @ApiModelProperty("id,新增时不填")
    private Long id;
    @ApiModelProperty("人群名称")
    @NotNull(message = "组群名称不能为空")
    private String name;
    @ApiModelProperty("人群定义，格式为json")
    @NotNull(message = "组群定义不能为空")
    private String ruleInfo;
}
