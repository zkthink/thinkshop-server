package com.think.cloud.thinkshop.mall.enums.message;

import com.think.cloud.thinkshop.mall.domain.message.MessageTemplate;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageTemplateEnum {
    // 支持： 邮箱 ，验证码
    TEMPLATE_1(1L, "注册验证码通知", 0, 1),
    // 支持： 邮箱 ，验证码
    TEMPLATE_2(2L, "重置密码发送验证码通知", 0, 1),
    // 支持： 货币单位，支付金额  ，订单号
    TEMPLATE_3(3L, "支付成功通知", 1, 1),
    // 支持： 商品 ，订单号
    TEMPLATE_4(4L, "已发货通知消息", 1, 1),
    // 支持： 订单号
    TEMPLATE_5(5L, "自动确认收货通知消息（24小时前）", 1, 1),
    // 支持： 订单号
    TEMPLATE_6(6L, "订单自动取消通知", 1, 1),
    // 支持： 货币单位，支付金额  ，订单号 ，时间
    TEMPLATE_7(7L, "待付款通知消息（用户下单后1小时未付款后发送）", 1, 1),
    // 支持： 货币单位，支付金额  ，订单号
    TEMPLATE_8(8L, "退款成功通知", 1, 1),
    //  支持 ：邮箱 ，订单号，拒绝原因
    TEMPLATE_9(9L, "售后失败通知", 1, 1),
    ;
    private final Long id;
    /**
     * 场景名称
     */
    private final String sceneName;
    /**
     * 开启站内信,0:不使用 1：使用
     */
    private final Integer useInform;
    /**
     * 开启邮件通知,0:不使用 1：使用
     */
    private final Integer useEmail;

    private final static String informTitle = "";
    private final static String informTemplate = "";
    private final static String emailTitle = "";
    private final static String emailTemplate = "";

    public static MessageTemplate buildTemplate(MessageTemplateEnum template) {
        return MessageTemplate.builder()
                .id(template.getId())
                .sceneName(template.getSceneName())
                .useInform(template.getUseInform())
                .useEmail(template.getUseEmail())
                .informTitle(informTitle)
                .informTemplate(informTemplate)
                .emailTitle(emailTitle)
                .emailTemplate(emailTemplate)
                .build();
    }

}
