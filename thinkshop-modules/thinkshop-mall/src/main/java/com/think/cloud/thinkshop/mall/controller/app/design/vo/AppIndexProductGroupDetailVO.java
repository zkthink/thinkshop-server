package com.think.cloud.thinkshop.mall.controller.app.design.vo;

import lombok.Data;
import lombok.ToString;

@ToString(callSuper = true)
@Data
public class AppIndexProductGroupDetailVO {
    private String productId;
    private String productName;
    private String introduce;
//    private String detail;
    private String image;
    private String minPrice;
    private String maxPrice;
}
