package com.think.cloud.thinkshop.mall.service.uv;

import javax.servlet.http.HttpServletRequest;

public interface AppUVService {
    /**
     * 记录uv
     * @param request
     */
    void record(HttpServletRequest request);

    /**
     * 范围内的站点访问数
     * @param startTime
     * @param endTime
     * @return
     */
    double getRecordTotal(String startTime, String endTime);

}
