package com.think.cloud.thinkshop.mall.controller.admin.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;


@Data
@ToString(callSuper = true)
public class ProductCouponOrderDetailRespVO {

    @ApiModelProperty("商品名")
    private String productName;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("优惠金额")
    private BigDecimal couponPrice;

    @ApiModelProperty("金额")
    private BigDecimal price;

}
