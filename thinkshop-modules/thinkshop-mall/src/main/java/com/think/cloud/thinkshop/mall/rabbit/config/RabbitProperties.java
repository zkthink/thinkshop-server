package com.think.cloud.thinkshop.mall.rabbit.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zkthink
 * @apiNote
 **/
@Component
@ConfigurationProperties(prefix = "spring.rabbitmq")
@Data
public class RabbitProperties {

    private String host;
    private int port;
    private String username;
    private String password;
    private String virtualHost;
    private boolean publisherConfirms;
    private boolean publisherReturns;
}
