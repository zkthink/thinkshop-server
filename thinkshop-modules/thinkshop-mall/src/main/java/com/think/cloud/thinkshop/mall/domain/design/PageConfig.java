package com.think.cloud.thinkshop.mall.domain.design;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

/**
 * 表 mall_page_config 对象
 *
 * @author dengguangxian
 * @date 2024-05-29
 */
@TableName("mall_page_config")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageConfig {
    /**
     * ID
     */
    private Long id;

    /**
     * 页眉图片地址
     */
    private String pageTopUrl;

    /**
     * 客服链接
     */
    private String customerServiceUrl;
    /**
     * 页脚图片地址
     */
    private String pageBottomUrl;
    /**
     * 页脚二维码url
     */
    private String pageBottomQrCodeUrl;
    /**
     * 版权备案号
     */
    private String copyright;
    /**
     * 电话
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 联系地址
     */
    private String address;
    /**
     * 其他展示：
     * 1：法律条款
     * 2：隐私政策，
     * 3：法律声明，
     * 4：付款，
     * 5：配送，
     * 6：换货及退货，
     * 7：关于订单，
     * 8：产品维护，
     * 9：产品维修，
     */
    private Integer other;
    /**
     * other 的内容
     */
    private String otherComment;

}
