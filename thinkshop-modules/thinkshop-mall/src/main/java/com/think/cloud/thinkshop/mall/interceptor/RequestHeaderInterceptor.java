package com.think.cloud.thinkshop.mall.interceptor;

import com.think.cloud.thinkshop.mall.service.memberuser.IMemberUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器：拦截用户请求，记录用户的接口访问情况
 */
//@EnableAsync
//@Component
public class RequestHeaderInterceptor implements HandlerInterceptor {
    @Autowired
    private IMemberUserService memberUserService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        memberUserService.recordUserOpt(request.getRequestURI(),handler);
        return true;
    }
}
