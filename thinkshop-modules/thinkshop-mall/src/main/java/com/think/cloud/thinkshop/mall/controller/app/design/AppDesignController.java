package com.think.cloud.thinkshop.mall.controller.app.design;

import com.think.cloud.thinkshop.mall.service.design.AppDesignService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * AppDesignController
 *
 * @author dengguangxian
 * @date 2024-05-30
 */
@RestController
@RequestMapping("/app/design")
@Api(tags = "APP: 首页的配置")
public class AppDesignController extends BaseController {
    @Autowired
    private AppDesignService designService;

    /**
     * 查询首页的配置
     */
    @GetMapping("/index")
    @ApiOperation(value = "查询首页的配置")
    public AjaxResult index(HttpServletRequest request) {
        return success(designService.getIndexDesign(request));
    }

    /**
     * 查询页眉页脚配置
     */
    @GetMapping("/page")
    @ApiOperation(value = "查询页眉页脚配置")
    public AjaxResult page() {
        return success(designService.selectPageConfig());
    }
}
