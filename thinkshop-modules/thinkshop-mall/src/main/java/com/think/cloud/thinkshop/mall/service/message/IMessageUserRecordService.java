package com.think.cloud.thinkshop.mall.service.message;


import com.think.cloud.thinkshop.mall.domain.message.MessageUserRecord;

import java.util.List;

/**
 * 用户接收的站内信Service接口
 *
 * @author moxiangrong
 * @date 2024-07-22
 */
public interface IMessageUserRecordService
{
    /**
     * 查询用户接收的站内信
     *
     * @param id 用户接收的站内信主键
     * @return 用户接收的站内信
     */
    public MessageUserRecord selectMessageUserRecordById(Long id,Long userId);

    /**
     * 查询用户接收的站内信列表
     *
     * @param messageUserRecord 用户接收的站内信
     * @return 用户接收的站内信集合
     */
    public List<MessageUserRecord> selectMessageUserRecordList(MessageUserRecord messageUserRecord);

    /**
     * 查询用户站内信记录
     * @param userId
     * @return
     */
    public List<MessageUserRecord> listMessageUserRecord(Long userId);

    /**
     * 新增用户接收的站内信
     *
     * @param messageUserRecord 用户接收的站内信
     * @return 结果
     */
    public int insertMessageUserRecord(MessageUserRecord messageUserRecord);

    /**
     * 修改用户接收的站内信
     *
     * @param messageUserRecord 用户接收的站内信
     * @return 结果
     */
    public int updateMessageUserRecord(MessageUserRecord messageUserRecord);

    /**
     * 批量删除用户接收的站内信信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);
}
