package com.think.cloud.thinkshop.mall.mapper.message;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.message.MessageTemplate;
import org.apache.ibatis.annotations.Mapper;


/**
 * 积分规则Mapper接口
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@Mapper
public interface MessageTemplateMapper extends BaseMapper<MessageTemplate> {

}
