package com.think.cloud.thinkshop.mall.service.memberuser.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagRefDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTagRef;
import com.think.cloud.thinkshop.mall.mapper.memberuser.MemberTagRefMapper;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberTagRefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 用户标签关联Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Service
public class MemberTagRefServiceImpl implements IMemberTagRefService {
    @Autowired
    private MemberTagRefMapper memberTagRefMapper;

    /**
     * 查询用户标签关联
     *
     * @param id 用户标签关联主键
     * @return 用户标签关联
     */
    @Override
    public MemberTagRef selectMemberTagRefById(Long id) {
        return memberTagRefMapper.selectById(id);
    }

    /**
     * 查询用户标签关联列表
     *
     * @param memberTagRef 用户标签关联
     * @return 用户标签关联
     */
    @Override
    public List<MemberTagRef> selectMemberTagRefList(MemberTagRef memberTagRef) {
        return memberTagRefMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增用户标签关联
     *
     * @param memberTagRef 用户标签关联
     * @return 结果
     */
    @Override
    public int insertMemberTagRef(MemberTagRef memberTagRef) {

        return memberTagRefMapper.insert(memberTagRef);
    }

    /**
     * 修改用户标签关联
     *
     * @param memberTagRef 用户标签关联
     * @return 结果
     */
    @Override
    public int updateMemberTagRef(MemberTagRef memberTagRef) {
        return memberTagRefMapper.updateById(memberTagRef);
    }

    /**
     * 批量删除用户标签关联信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return memberTagRefMapper.deleteBatchIds(ids);
    }

    @Override
    public List<MemberTagRef> selectByTagIds(List<Long> idList) {
        return memberTagRefMapper.selectList(new LambdaQueryWrapper<MemberTagRef>().in(MemberTagRef::getTagId, idList));
    }

    @Override
    public void deleteByTagIds(List<Long> tagIdList) {
        memberTagRefMapper.delete(new LambdaQueryWrapper<MemberTagRef>().in(MemberTagRef::getTagId, tagIdList));
    }

    @Override
    public List<MemberTagRefDTO> selectByUserIds(List<Long> userIds) {
        return memberTagRefMapper.selectByUserIds(userIds);
    }

    @Override
    public void deleteByUserId(Long userId) {
        memberTagRefMapper.delete(Wrappers.<MemberTagRef>lambdaQuery().eq(MemberTagRef::getUserId, userId));

    }
}
