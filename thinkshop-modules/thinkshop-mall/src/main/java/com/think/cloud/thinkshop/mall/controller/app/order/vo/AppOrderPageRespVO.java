package com.think.cloud.thinkshop.mall.controller.app.order.vo;


import com.think.cloud.thinkshop.mall.annotation.CrossBorderZoneTimeAmend;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * 订单查询响应VO
 *
 * @author zkthink
 * @date 2024-05-24
 */
@Data
public class AppOrderPageRespVO {

    @ApiModelProperty("订单id")
    private Long id;

    @ApiModelProperty("订单号")
    private String orderCode;

    @ApiModelProperty("订单状态（-1：已取消；0：待付款；1：待发货；2：待收货；3：已收货；4：已完成；）")
    private Integer status;

    @ApiModelProperty("商品总数")
    private Integer totalNum;

    @ApiModelProperty("剩余支付时间")
    private Long time;

    @ApiModelProperty("实际支付金额")
    private BigDecimal payPrice;

    @ApiModelProperty("创建时间")
    @CrossBorderZoneTimeAmend
    private Timestamp createTime;

    @ApiModelProperty("订单完成时间")
    private Date completeTime;

    @ApiModelProperty("订单明细")
    List<AppOrderDetailPageRespVO> details;

    @ApiModelProperty("订单是否可以评价")
    private Boolean canComment;

    @ApiModelProperty("订单是否可以售后")
    private Boolean canAfterSale;

}
