package com.think.cloud.thinkshop.mall.enums.integral;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IntegralBillTypeEnum {
    ADD(1, "订单赠送积分"),
    USE(2, "消费积分"),
    OUT(3, "积分过期"),
    CANCEL(4, "订单取消退还积分"),
    SALE_AFTER(5, "售后退还积分"),



    ;
    private int code;
    private String desc;
}
