package com.think.cloud.thinkshop.mall.controller.app.integral;

import com.think.cloud.thinkshop.mall.service.integral.AppIntegralService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/integral")
@Api(tags = "APP: 积分相关")
public class AppIntegralController extends BaseController {
    @Autowired
    private AppIntegralService integralService;


    @GetMapping("/balance")
    @ApiOperation(value = "我的积分值")
    public AjaxResult balance() {
        return success(integralService.getUserIntegral(SecurityUtils.getUserId()));
    }

    @GetMapping("/bill")
    @ApiOperation(value = "积分流水")
    public TableDataInfo bill() {
        startPage();
        return getDataTable(integralService.getUserIntegralBill(SecurityUtils.getUserId()));
    }

    @GetMapping("/rule")
    @ApiOperation(value = "积分规则")
    public AjaxResult rule() {
        return success(integralService.getRuleInfo());
    }

}
