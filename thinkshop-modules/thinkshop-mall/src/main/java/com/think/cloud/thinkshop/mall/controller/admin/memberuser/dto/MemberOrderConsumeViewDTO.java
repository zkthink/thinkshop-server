package com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
public class MemberOrderConsumeViewDTO {
    @ApiModelProperty(value = "消费次数", required = true, example = "")
    private Float consumeCount=0.0F;
    @ApiModelProperty(value = "消费金额", required = true, example = "")
    private Float consumeAmount=0.0F;
    @ApiModelProperty(value = "最后消费时间", required = true, example = "")
    private Date lastConsumeTime;
}
