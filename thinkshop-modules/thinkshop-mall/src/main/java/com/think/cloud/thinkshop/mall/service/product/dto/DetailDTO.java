
package com.think.cloud.thinkshop.mall.service.product.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;


@Data
public class DetailDTO {
    private List<String> data;


    private List<Map<String, Map<String, String>>> res;
}
