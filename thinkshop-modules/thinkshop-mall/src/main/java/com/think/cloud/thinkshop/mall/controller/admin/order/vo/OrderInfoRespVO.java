package com.think.cloud.thinkshop.mall.controller.admin.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * 订单信息响应VO
 *
 * @author zkthink
 * @date 2024-05-29
 */
@Data
public class OrderInfoRespVO {

    @ApiModelProperty("订单id")
    private Long id;

    @ApiModelProperty("订单号")
    private String orderCode;

//    @ApiModelProperty("用户id")
//    private Long userId;

    @ApiModelProperty("下单用户")
    private String orderUser;

    @ApiModelProperty("优惠券id")
    private Long couponId;

    @ApiModelProperty("first name")
    private String firstName;

    @ApiModelProperty("last name")
    private String lastName;

    @ApiModelProperty("电话")
    private String userPhone;

    @ApiModelProperty("详细地址")
    private String addressDetail;

    @ApiModelProperty("邮编")
    private String postCode;

    @ApiModelProperty("订单商品总数")
    private Integer totalNum;

    @ApiModelProperty("订单总价（商品总计）")
    private BigDecimal totalPrice;

    @ApiModelProperty("邮费")
    private BigDecimal totalPostage;

    @ApiModelProperty("优惠券金额")
    private BigDecimal couponPrice;

    @ApiModelProperty("消费税费")
    private BigDecimal taxation;

    @ApiModelProperty("运费税费")
    private BigDecimal postageTaxation;

    @ApiModelProperty("总税费")
    private BigDecimal totalTaxation;

    @ApiModelProperty("实际支付金额")
    private BigDecimal payPrice;

    @ApiModelProperty("支付状态")
    private Integer paid;

    @ApiModelProperty("支付单号")
//    private String payCode;   payCode 都没值
    private String paymentIntentId;

    @ApiModelProperty("支付时间")
    private Timestamp payTime;

    @ApiModelProperty("支付方式")
    private String payType;

    @ApiModelProperty("订单状态:-1：已取消；0：待付款；1：待发货；2：待收货；3：已收货；4：已完成；")
    private Integer status;

    @ApiModelProperty("快递公司编号")
    private String deliverySn;

    @ApiModelProperty("快递名称")
    private String deliveryName;

    @ApiModelProperty("快递单号")
    private String deliveryId;

    @ApiModelProperty("积分抵扣金额")
    private BigDecimal integralDeduct;

    @ApiModelProperty("备注")
    private String mark;

    @ApiModelProperty("创建人")
    private String createBy;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("修改人")
    private String updateBy;

    @ApiModelProperty("修改时间")
    private Timestamp updateTime;

    @ApiModelProperty("明细")
    private List<OrderDetailRespVO> details;

    @ApiModelProperty("操作记录")
    private List<OrderLogRespVO> logs;

}
