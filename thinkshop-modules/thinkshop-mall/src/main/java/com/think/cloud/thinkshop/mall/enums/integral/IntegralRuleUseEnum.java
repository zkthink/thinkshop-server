package com.think.cloud.thinkshop.mall.enums.integral;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IntegralRuleUseEnum {
    NO_USE(0, "不可用"),
    USE(1, "可用"),
    ;
    private int code;
    private String desc;
}
