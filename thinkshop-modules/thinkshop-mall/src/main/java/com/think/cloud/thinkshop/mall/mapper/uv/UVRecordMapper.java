package com.think.cloud.thinkshop.mall.mapper.uv;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.uv.UVRecord;
import org.apache.ibatis.annotations.Mapper;


/**
 * BannerMapper接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Mapper
public interface UVRecordMapper extends BaseMapper<UVRecord> {

}

