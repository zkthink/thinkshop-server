package com.think.cloud.thinkshop.mall.service.shippingtemplates;

import java.util.List;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplatesProductRef;

/**
 * 物流方案商品关联Service接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
public interface IShippingTemplatesProductRefService
{
    /**
     * 查询物流方案商品关联
     *
     * @param id 物流方案商品关联主键
     * @return 物流方案商品关联
     */
    public ShippingTemplatesProductRef selectShippingTemplatesProductRefById(Long id);

    /**
     * 查询物流方案商品关联列表
     *
     * @param shippingTemplatesProductRef 物流方案商品关联
     * @return 物流方案商品关联集合
     */
    public List<ShippingTemplatesProductRef> selectShippingTemplatesProductRefList(ShippingTemplatesProductRef shippingTemplatesProductRef);

    /**
     * 新增物流方案商品关联
     *
     * @param shippingTemplatesProductRef 物流方案商品关联
     * @return 结果
     */
    public int insertShippingTemplatesProductRef(ShippingTemplatesProductRef shippingTemplatesProductRef);

    /**
     * 修改物流方案商品关联
     *
     * @param shippingTemplatesProductRef 物流方案商品关联
     * @return 结果
     */
    public int updateShippingTemplatesProductRef(ShippingTemplatesProductRef shippingTemplatesProductRef);

    /**
     * 批量删除物流方案商品关联信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    List<Long> selectProductIdsByTemplateId(Long id);

    /**
     * 保存关联
     * @param id 方案id
     * @param productIds 商品id
     * @param isAdd 是否新增
     */
    void saveBatch(Long id, List<Long> productIds, boolean isAdd);

    /**
     * 根据方案id删除关联
     * @param templateId 方案id
     */
    void deleteByTemplateId(Long templateId);

    List<ShippingTemplatesProductRef> selectByProductIds(List<Long> productIds);

    /**
     * 根据商品id删除关联
     * @param productIds 商品id
     */
    void deleteByProductIds(List<Long> productIds);
}
