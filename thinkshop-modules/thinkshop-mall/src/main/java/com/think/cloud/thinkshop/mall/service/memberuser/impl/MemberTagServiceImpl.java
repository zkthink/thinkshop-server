package com.think.cloud.thinkshop.mall.service.memberuser.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagQueryDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTag;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTagRef;
import com.think.cloud.thinkshop.mall.mapper.memberuser.MemberTagMapper;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberTagRefService;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberTagService;
import com.think.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * 会员标签Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Service
public class MemberTagServiceImpl implements IMemberTagService {
    @Autowired
    private MemberTagMapper memberTagMapper;
    @Autowired
    @Lazy
    private IMemberTagRefService memberTagRefService;

    /**
     * 查询会员标签
     *
     * @param id 会员标签主键
     * @return 会员标签
     */
    @Override
    public MemberTag selectMemberTagById(Long id) {
        return memberTagMapper.selectById(id);
    }

    /**
     * 查询会员标签列表
     *
     * @param dto@return 会员标签
     */
    @Override
    public List<MemberTag> selectMemberTagList(MemberTagQueryDTO dto) {
        List<MemberTag> memberTags = memberTagMapper.selectMemberTagList(dto);
        return memberTags;
    }


    /**
     * 新增会员标签
     *
     * @param memberTag 会员标签
     * @return 结果
     */
    @Override
    public int insertMemberTag(MemberTag memberTag) {

        return memberTagMapper.insert(memberTag);
    }

    /**
     * 修改会员标签
     *
     * @param memberTag 会员标签
     * @return 结果
     */
    @Override
    public int updateMemberTag(MemberTag memberTag) {
        return memberTagMapper.updateById(memberTag);
    }

    /**
     * 批量删除会员标签信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int batchDelete(List<Long> ids) {
        //删除 用户标签关联
        memberTagRefService.deleteByTagIds(ids);
        return memberTagMapper.deleteBatchIds(ids);
    }

    private LambdaQueryWrapper<MemberTag> buildQueryWrapper(MemberTagQueryDTO dto) {
        return Wrappers.<MemberTag>lambdaQuery()
                .eq(StringUtils.hasText(dto.getName()), MemberTag::getName, dto.getName())
                .ge(Objects.nonNull(dto.getUserCountMin()), MemberTag::getUserCount, dto.getUserCountMin())
                .le(Objects.nonNull(dto.getUserCountMin()), MemberTag::getUserCount, dto.getUserCountMax());

    }
}
