package com.think.cloud.thinkshop.mall.domain.message;


import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 系统消息模版配置对象 mall_message_template
 *
 * @author moxiangrong
 * @date 2024-07-19
 */
@TableName("mall_message_template")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageTemplate {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 场景名称
     */
    @Excel(name = "场景名称")
    private String sceneName;

    /**
     * 开启站内信,0:不使用 1：使用
     */
    @Excel(name = "开启站内信,0:不使用 1：使用")
    private Integer useInform;

    /**
     * 开启邮件通知,0:不使用 1：使用
     */
    @Excel(name = "开启邮件通知,0:不使用 1：使用")
    private Integer useEmail;

    /**
     * 站内信标题
     */
    @Excel(name = "站内信标题")
    private String informTitle;

    /**
     * 站内信内容
     */
    @Excel(name = "站内信内容模版")
    private String informTemplate;

    /**
     * 站内信标题
     */
    @Excel(name = "邮件标题")
    private String emailTitle;

    /**
     * 站内信内容
     */
    @Excel(name = "邮件内容模版")
    private String emailTemplate;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer deleted;

}
