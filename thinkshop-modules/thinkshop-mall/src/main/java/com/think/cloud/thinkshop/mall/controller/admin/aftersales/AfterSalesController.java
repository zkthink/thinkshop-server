package com.think.cloud.thinkshop.mall.controller.admin.aftersales;


import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesAuditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesPageReqVO;
import com.think.cloud.thinkshop.mall.service.aftersales.IAfterSalesService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 售后记录Controller
 *
 * @author zkthink
 * @date 2024-06-12
 */
@RestController
@RequestMapping("/admin/sales")
@Api(tags = "ADMIN:售后记录")
public class AfterSalesController extends BaseController {
    @Autowired
    private IAfterSalesService afterSalesService;

    /**
     * 查询售后记录列表
     */
    @RequiresPermissions("mall:sales:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询售后记录列表")
    public TableDataInfo list(AfterSalesPageReqVO vo) {
        startPage();
        return afterSalesService.selectAfterSalesList(vo);
    }

    /**
     * 获取售后记录详细信息
     */
    @RequiresPermissions("mall:sales:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取售后记录详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(afterSalesService.selectAfterSalesById(id));
    }

    /**
     * 售后审核
     */
    @RequiresPermissions("mall:sales:audit")
    @PostMapping(value = "/audit")
    @ApiOperation(value = "售后审核")
    public AjaxResult audit(@RequestBody AfterSalesAuditReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        afterSalesService.audit(vo);
        return success();
    }

    /**
     * 删除售后记录
     */
    @DeleteMapping("/delete/{id}")
    @RequiresPermissions("mall:sales:delete")
    @ApiOperation(value = "删除售后订单记录")
    public AjaxResult delete(@PathVariable Long id) {
        return success( afterSalesService.delete(id));
    }

}
