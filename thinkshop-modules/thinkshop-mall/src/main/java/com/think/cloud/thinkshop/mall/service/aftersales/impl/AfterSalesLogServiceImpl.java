package com.think.cloud.thinkshop.mall.service.aftersales.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesLogRespVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesLogRespVO;
import com.think.cloud.thinkshop.mall.convert.aftersales.AfterSalesConvert;
import com.think.cloud.thinkshop.mall.domain.aftersales.AfterSalesLog;
import com.think.cloud.thinkshop.mall.enums.aftersales.AfterSalesOperateTypeEnum;
import com.think.cloud.thinkshop.mall.enums.order.OrderOperateUserTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.think.cloud.thinkshop.mall.mapper.aftersales.AfterSalesLogMapper;
import com.think.cloud.thinkshop.mall.service.aftersales.IAfterSalesLogService;

import java.util.List;

/**
 * 售后日志Service业务层处理
 *
 * @author zkthink
 * @date 2024-06-13
 */
@Service
public class AfterSalesLogServiceImpl implements IAfterSalesLogService {
    @Autowired
    private AfterSalesLogMapper afterSalesLogMapper;

    //查询售后日志
    private List<AfterSalesLog> findAfterSalesLogs(LambdaQueryWrapper<AfterSalesLog> wrapper){
        return afterSalesLogMapper.selectList(wrapper);
    }

    @Override
    public List<AppAfterSalesLogRespVO> getAfterSalesLogList(Long afterSalesId) {
        List<AfterSalesLog> afterSalesLogs = this.findAfterSalesLogs(
                new LambdaQueryWrapper<AfterSalesLog>()
                .eq(AfterSalesLog::getAfterSalesId, afterSalesId)
        );
        return AfterSalesConvert.INSTANCE.convertList(afterSalesLogs);
    }

    @Override
    public void insertSalesLog(AfterSalesLog afterSalesLog) {
        afterSalesLogMapper.insert(afterSalesLog);
    }

    @Override
    public List<AfterSalesLogRespVO> selectAfterSalesLogListByAfterSalesId(Long orderId) {
        List<AfterSalesLogRespVO> logs = AfterSalesConvert.INSTANCE.convertLogList(
                this.findAfterSalesLogs(
                        new LambdaQueryWrapper<AfterSalesLog>()
                        .eq(AfterSalesLog::getAfterSalesId, orderId)
                        .orderByAsc(AfterSalesLog::getId)
                )
        );
        if (CollectionUtils.isNotEmpty(logs)) {
            for (AfterSalesLogRespVO vo : logs) {
                vo.setUserTypeName(OrderOperateUserTypeEnum.toEnum(vo.getUserType()).getDesc());
                vo.setOperateTypeName(AfterSalesOperateTypeEnum.toEnum(vo.getOperateType()).getDesc());
            }
        }
        return logs;
    }
}
