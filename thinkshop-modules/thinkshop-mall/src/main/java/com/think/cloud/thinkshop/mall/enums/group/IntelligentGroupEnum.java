package com.think.cloud.thinkshop.mall.enums.group;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 分组智能筛选枚举
 */
@Getter
@AllArgsConstructor
public enum IntelligentGroupEnum {

    HAND(0, "手动筛选"),
    INTELLIGENT(1, "智能筛选");

    private Integer type;
    private String value;


    public static IntelligentGroupEnum toType(int type) {
        return Stream.of(IntelligentGroupEnum.values())
                .filter(p -> p.type == type)
                .findAny()
                .orElse(null);
    }


}
