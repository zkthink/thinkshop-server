package com.think.cloud.thinkshop.mall.domain.memberuser;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;

/**
 * 会员标签对象 mall_member_tag
 *
 * @author zkthink
 * @date 2024-05-15
 */
@TableName("mall_member_tag")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberTag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 标签名称 */
    @Excel(name = "标签名称")
    private String name;

    /** 客户数量 */
    @Excel(name = "客户数量")
    @TableField(exist = false)
    private Integer userCount;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;
}
