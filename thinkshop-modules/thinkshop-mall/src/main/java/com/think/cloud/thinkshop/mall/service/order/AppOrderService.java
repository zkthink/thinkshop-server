package com.think.cloud.thinkshop.mall.service.order;


import cn.hutool.core.date.DateTime;
import cn.hutool.json.JSONArray;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.*;
import com.think.cloud.thinkshop.mall.domain.order.Order;
import com.think.common.core.model.LoginUser;
import com.think.common.core.web.page.TableDataInfo;

import java.math.BigDecimal;

/**
 * 订单AppService接口
 *
 * @author zkthink
 * @date 2024-05-24
 */
public interface AppOrderService {

    /**
     * 订单信息确认
     *
     * @param vo
     * @return
     */
    public AppConfirmOrderRespVO confirm(AppConfirmOrderReqVO vo);

    /**
     * 创建订单
     *
     * @param vo
     * @return
     */
    public Long create(AppCreateOrderReqVO vo);

    /**
     * 订单列表
     *
     * @param vo
     * @return
     */
    public TableDataInfo list(AppOrderPageReqVO vo);

    /**
     * 获取订单详情
     *
     * @param orderId
     * @return
     */
    public AppOrderInfoRespVO get(Long orderId, Long userId);

    /**
     * 获取订单支付状态
     *
     * @param orderId
     * @return
     */
    public Boolean getPaymentStatus(Long orderId);

    /**
     * 手动取消订单
     *
     * @param orderId
     * @param loginUser
     */
    public Order cancel(Long orderId, LoginUser loginUser);

    /**
     * 自动取消订单
     *
     * @param orderId
     */
    public void cancel(Long orderId);

    /**
     * 用户手动收货
     *
     * @param orderId
     * @param loginUser
     */
    public void receipt(Long orderId, LoginUser loginUser);

    /**
     * 自动收货
     *
     * @param orderId
     */
    public void receipt(Long orderId);

    /**
     * 支付成功
     *
     * @param orderCode
     * @param latestCharge
     * @param paymentIntentId
     * @param payType
     */
    public void paySuccess(String orderCode, String latestCharge, String paymentIntentId, String payType);

    /**
     * 再次购买
     *
     * @param orderId
     */
    public AppOrderPurchaseAgainRespVO purchaseAgain(Long orderId);

    /**
     * 删除订单
     *
     * @param orderId
     */
    public void delete(Long orderId);

    /**
     * 获取物流信息
     *
     * @param deliveryId /
     * @return /
     */
    JSONArray getLogistics(String deliveryId);

    /**
     * 更新支付状态
     *
     * @param orderId /
     * @param paymentStatus /
     */
    void updatePayStatus(Long orderId, Integer paymentStatus);

    void payFailed(String orderCode);

    public Order selectOrderById(Long orderId) ;
    public Order selectOrderByOrderCode(String orderCode) ;

    public void verifyMyselfOrder(Long loginUserId, Long orderUserId);

    /**
     * 获取订单人数
     * @param planId 运营计划id
     * @param start 开始时间
     * @param end 结束时间
     * @param paid 支付状态
     * @return /
     */
    Long getOrderUserCount(Long planId, DateTime start, DateTime end, Integer paid);

    /**
     * 获取订单数
     * @param planId 运营计划id
     * @param start 开始时间
     * @param end 结束时间
     * @param paid 支付状态
     * @return /
     */
    Long getOrderCount(Long planId, DateTime start, DateTime end,Integer paid);

    /**
     * 获取订单金额
     * @param planId 运营计划id
     * @param start 开始时间
     * @param end 结束时间
     * @param paid 支付状态
     * @return /
     */
    BigDecimal getOrderAmount(Long planId, DateTime start, DateTime end, Integer paid);

    /**
     * 提交评论
     * @param param
     * @param loginUser
     * @return
     */
    int submitOrderComment(AppOrderCommentReqVO param, LoginUser loginUser);

    /**
     * 发送待支付的通知
     * @param orderId
     */
    void waitPayNotice(Long orderId);

    /**
     * 自动收货前的通知
     * @param orderId
     */
    void autoNoticeBeforeReceipt(Long orderId);

    /**
     * 超过售后之后赠送积分
     * @param id
     */
    void sendIntegral(Long id);

}
