package com.think.cloud.thinkshop.mall.controller.app.memberuser.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class ForgetPasswordDTO {
    @ApiModelProperty(value = "邮箱", required = true)
    private String email;
    @ApiModelProperty(value = "验证码", required = true)
    private String code;
    @ApiModelProperty(value = "密码", required = true)
    private String password;
}
