package com.think.cloud.thinkshop.mall.mapper.memberuser;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanSearchReqVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationPlan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 客户运营计划Mapper接口
 *
 * @author zkthink
 * @date 2024-06-07
 */
@Mapper
public interface MemberOperationPlanMapper extends BaseMapper<MemberOperationPlan> {

    void updateStatus(@Param("id") Long id, @Param("status")Integer status);

    List<MemberOperationPlan> selectListRef(MemberOperationPlanSearchReqVO vo);
}
