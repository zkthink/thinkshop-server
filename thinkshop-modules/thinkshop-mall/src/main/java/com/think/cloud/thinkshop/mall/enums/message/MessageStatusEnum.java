package com.think.cloud.thinkshop.mall.enums.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageStatusEnum {
    NO(0, "不可用/未读"),
    YES(1, "可用/已读"),
    ;
    private final int status;
    private final String desc;
}
