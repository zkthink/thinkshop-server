package com.think.cloud.thinkshop.mall.controller.admin.message.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageTemplateReplaceDTO {

    private String email;
    private String code;
    private String currencyUnit;
    private String payPrice;
    private Long orderId;
    private String orderCode;
    private String product;
    private String timeLimit;
    private String payLink;
    private String rejectReason;

}
