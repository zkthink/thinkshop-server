package com.think.cloud.thinkshop.mall.service.order.impl;

import java.util.List;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.think.cloud.thinkshop.mall.domain.order.Order;
import com.think.cloud.thinkshop.mall.service.order.ILogisticsLogService;
import com.think.cloud.thinkshop.mall.utils.Logistics17TrackClient;
import com.think.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.think.cloud.thinkshop.mall.mapper.LogisticsLogMapper;
import com.think.cloud.thinkshop.mall.domain.LogisticsLog;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 物流日志Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-31
 */
@Service
public class LogisticsLogServiceImpl implements ILogisticsLogService {
    @Autowired
    private LogisticsLogMapper logisticsLogMapper;
    @Autowired
    private Logistics17TrackClient logistics17TrackClient;

    /**
     * 查询物流日志
     *
     * @param id 物流日志主键
     * @return 物流日志
     */
    @Override
    public LogisticsLog selectLogisticsLogById(Long id) {
        return logisticsLogMapper.selectById(id);
    }

    /**
     * 查询物流日志列表
     *
     * @param logisticsLog 物流日志
     * @return 物流日志
     */
    @Override
    public List<LogisticsLog> selectLogisticsLogList(LogisticsLog logisticsLog) {
        return logisticsLogMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增物流日志
     *
     * @param logisticsLog 物流日志
     * @return 结果
     */
    @Override
    public int insertLogisticsLog(LogisticsLog logisticsLog) {

        return logisticsLogMapper.insert(logisticsLog);
    }

    /**
     * 修改物流日志
     *
     * @param logisticsLog 物流日志
     * @return 结果
     */
    @Override
    public int updateLogisticsLog(LogisticsLog logisticsLog) {
        return logisticsLogMapper.updateById(logisticsLog);
    }

    /**
     * 批量删除物流日志信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return logisticsLogMapper.deleteBatchIds(ids);
    }

    @Override
    public void register(Long orderId,Integer type,String deliveryId,String deliveryName,String deliverySn) {
        logistics17TrackClient.register(deliveryId);

        LogisticsLog logisticsLog = new LogisticsLog();
        logisticsLog.setOrderId(orderId);
        logisticsLog.setDeliveryId(deliveryId);
        logisticsLog.setDeliveryName(deliveryName);
        logisticsLog.setDeliverySn(deliverySn);
        logisticsLog.setType(type);
        logisticsLogMapper.insert(logisticsLog);
    }

    @Override
    public LogisticsLog selectByOrderId(Long orderId) {
        return logisticsLogMapper.selectOne(new LambdaQueryWrapper<LogisticsLog>().eq(LogisticsLog::getOrderId, orderId));
    }

    @Override
    public void webHook17Track(String body) {
        JSONObject entries = new JSONObject(body);
        String event = entries.getStr("event");
        //保存物流更新
        if(event.equals("TRACKING_UPDATED")){
            JSONObject data = entries.getJSONObject("data");
            //物流单号
            String number = data.getStr("number");
            //关键节点
            JSONArray milestone = data.getJSONObject("track_info").getJSONArray("milestone");
            logisticsLogMapper.update(null, new LambdaUpdateWrapper<LogisticsLog>()
                    .eq(LogisticsLog::getDeliveryId, number)
                    .set(LogisticsLog::getLogisticsInfo, milestone.toString())
            );
        }
    }

    @Override
    public LogisticsLog selectByDeliveryId(String deliveryId) {
        //同一快递单号可能有多条日志,这里取最早的一条返回给前端
        return logisticsLogMapper.selectOne(new LambdaQueryWrapper<LogisticsLog>().eq(LogisticsLog::getDeliveryId, deliveryId).orderByAsc(LogisticsLog::getId).last("limit 1"));
    }
}
