package com.think.cloud.thinkshop.mall.controller.admin.message;


import com.think.cloud.thinkshop.mall.controller.admin.message.param.MessageTemplateUpdateParam;
import com.think.cloud.thinkshop.mall.domain.message.MessageTemplate;
import com.think.cloud.thinkshop.mall.service.message.IMessageTemplateService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统消息模版配置Controller
 *
 * @author moxiangrong
 * @date 2024-07-19
 */
@RestController
@RequestMapping("/admin/messageTemplate")
@Api(tags="系统消息模版配置")
public class MessageTemplateController extends BaseController
{
    @Autowired
    private IMessageTemplateService messageTemplateService;

    /**
     * 查询系统消息模版配置列表
     */
    @RequiresPermissions("mall:messageTemplate:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询系统消息模版配置列表")
    public TableDataInfo list()
    {
        startPage();
        List<MessageTemplate> list = messageTemplateService.selectMessageTemplateList();
        return getDataTable(list);
    }

    /**
     * 获取系统消息模版配置详细信息
     */
    @RequiresPermissions("mall:messageTemplate:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取系统消息模版配置详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(messageTemplateService.selectMessageTemplateById(id));
    }

    /**
     * 修改系统消息模版配置
     */
    @RequiresPermissions("mall:messageTemplate:edit")
    @Log(title = "系统消息模版配置", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改系统消息模版配置")
    public AjaxResult edit(@RequestBody MessageTemplateUpdateParam messageTemplate)
    {
        return toAjax(messageTemplateService.updateMessageTemplate(messageTemplate));
    }

//    /**
//     * 新增系统消息模版配置
//     */
//    @RequiresPermissions("mall:messageTemplate:add")
//    @Log(title = "系统消息模版配置", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ApiOperation(value = "新增系统消息模版配置")
//    public AjaxResult add(@RequestBody MessageTemplate messageTemplate)
//    {
//        return toAjax(messageTemplateService.insertMessageTemplate(messageTemplate));
//    }

//    /**
//     * 删除系统消息模版配置
//     */
//    @RequiresPermissions("mall:messageTemplate:remove")
//    @Log(title = "系统消息模版配置", businessType = BusinessType.DELETE)
//    @DeleteMapping("/delete")
//    @ApiOperation(value = "删除系统消息模版配置")
//    public AjaxResult remove(@Valid Long[] ids)
//    {
//        return toAjax(messageTemplateService.batchDelete(Arrays.asList(ids)));
//    }
    //    /**
//     * 导出系统消息模版配置列表
//     */
//    @RequiresPermissions("mall:messageTemplate:export")
//    @Log(title = "系统消息模版配置", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ApiOperation(value = "导出系统消息模版配置列表")
//    public void export(HttpServletResponse response, MessageTemplate messageTemplate)
//    {
//        List<MessageTemplate> list = messageTemplateService.selectMessageTemplateList(messageTemplate);
//        ExcelUtil<MessageTemplate> util = new ExcelUtil<MessageTemplate>(MessageTemplate.class);
//        util.exportExcel(response, list, "系统消息模版配置数据");
//    }

}
