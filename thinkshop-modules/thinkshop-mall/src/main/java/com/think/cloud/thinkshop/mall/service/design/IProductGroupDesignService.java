package com.think.cloud.thinkshop.mall.service.design;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.GroupNameBlurRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexProductGroupDesignRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.ProductGroupDesignStatusReqVO;

import java.util.List;

public interface IProductGroupDesignService {
    /**
     * 获取首页商品组合的列表
     */
    List<IndexProductGroupDesignRespVO> getProductGroupDesignList();

    /**
     * 新增一个首页的产品组合
     * @param groupId
     * @return
     */
    int insert(Long groupId);

    /**
     * 删除首页的产品组
     * @param id
     * @return
     */
    int delete(Long id);

    /**
     * 更新产品组使用状态
     * @param vo
     * @return
     */
    int updateStatusById(ProductGroupDesignStatusReqVO vo);

    /**
     * 模糊查询商品组名
     * @param name
     */
    List<GroupNameBlurRespVO> getGroupNameBlur(String name);
}
