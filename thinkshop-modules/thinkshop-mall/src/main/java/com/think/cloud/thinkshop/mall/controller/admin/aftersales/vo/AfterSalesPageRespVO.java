package com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * 售后单分页响应VO
 *
 * @author zkthink
 * @date 2024-06-14
 */
@Data
public class AfterSalesPageRespVO {

    @ApiModelProperty("售后id")
    private Long id;

    @ApiModelProperty("售后单号")
    private String afterSalesCode;

    @ApiModelProperty("订单编号")
    private String orderCode;

    @ApiModelProperty("退货数量")
    private Integer refundNumber;

    @ApiModelProperty("退款金额")
    private BigDecimal refundAmount;

    @ApiModelProperty("服务类型：0、仅退款，1、退货退款")
    private Integer serviceType;

    @ApiModelProperty("申请用户")
    private String userName;

    @ApiModelProperty("状态：-2、商家拒绝，-1、用户取消，0、已提交等待平台审核，1、平台已审核 等待用户发货，2、用户已发货，3、退款成功")
    private Integer state;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("用户是否删除")
    private Integer userDel;

    @ApiModelProperty("明细")
    private List<AfterSalesOrderDetailRespVO> details;
}
