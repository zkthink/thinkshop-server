package com.think.cloud.thinkshop.mall.mapper.integral;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralRule;
import org.apache.ibatis.annotations.Mapper;


/**
 * 积分规则Mapper接口
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@Mapper
public interface IntegralRuleMapper extends BaseMapper<IntegralRule> {

}
