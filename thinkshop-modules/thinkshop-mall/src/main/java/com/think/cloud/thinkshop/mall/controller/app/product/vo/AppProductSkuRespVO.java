
package com.think.cloud.thinkshop.mall.controller.app.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

/**
 * App商品sku响应VO
 *
 * @author zkthink
 * @date 2024-05-16
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppProductSkuRespVO {

    /** id */
    @ApiModelProperty("id")
    private Long skuId;

    /** 商品ID */
    @ApiModelProperty("商品ID")
    private Long productId;

    /** 商品属性索引值 (attr_value|attr_value[|....]) */
    @ApiModelProperty("商品属性索引值")
    private String sku;

    /** 图片 */
    @ApiModelProperty("图片")
    private String image;

    /** 原价 */
    @ApiModelProperty("原价")
    private BigDecimal originalPrice;

    /** 价格 */
    @ApiModelProperty("价格")
    private BigDecimal price;

    /** 属性对应的库存 */
    @ApiModelProperty("属性对应的库存")
    private Integer stock;

    /** 销量 */
    @ApiModelProperty("销量")
    private Integer sales;

    /** 重量 */
    @ApiModelProperty("重量")
    private BigDecimal weight;

    /** 体积 */
    @ApiModelProperty("体积")
    private BigDecimal volume;

}
