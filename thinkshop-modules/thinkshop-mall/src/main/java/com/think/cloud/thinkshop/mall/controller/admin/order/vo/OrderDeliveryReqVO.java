package com.think.cloud.thinkshop.mall.controller.admin.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 订单发货响应VO
 *
 * @author zkthink
 * @date 2024-05-29
 */
@Data
public class OrderDeliveryReqVO {

    @ApiModelProperty(value = "订单id", required = true)
    private Long id;

    @ApiModelProperty(value = "快递公司编号", required = true)
    private String deliverySn;

    @ApiModelProperty(value = "快递公司名称", required = true)
    private String deliveryName;

    @ApiModelProperty(value = "快递编号", required = true)
    private String deliveryId;

}
