package com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto;

import com.think.common.core.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class MemberTagQueryDTO {
    @ApiModelProperty("标签名称")
    private String name;

    /** 客户数量 */
    @ApiModelProperty("客户数量")
    private Integer userCountMin;

    /** 客户数量 */
    @ApiModelProperty("客户数量")
    private Integer userCountMax;
}
