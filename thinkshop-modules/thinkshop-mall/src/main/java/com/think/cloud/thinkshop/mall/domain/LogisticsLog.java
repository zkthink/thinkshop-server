package com.think.cloud.thinkshop.mall.domain;

import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 物流日志对象 mall_logistics_log
 *
 * @author zkthink
 * @date 2024-05-31
 */
@TableName("mall_logistics_log")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogisticsLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 物流信息 */
    @Excel(name = "物流信息")
    private String logisticsInfo;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String deliveryId;

    /** 快递公司编号 */
    @Excel(name = "快递公司编号")
    private String deliverySn;

    /** 快递名称 */
    @Excel(name = "快递名称")
    private String deliveryName;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Long orderId;

    @Excel(name = "类型 1订单 2售后订单")
    private Integer type;

}
