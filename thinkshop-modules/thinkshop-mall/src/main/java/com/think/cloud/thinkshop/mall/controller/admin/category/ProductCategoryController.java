package com.think.cloud.thinkshop.mall.controller.admin.category;

import com.think.cloud.thinkshop.mall.controller.admin.category.vo.ProductRelationCategoryReqVO;
import com.think.cloud.thinkshop.mall.domain.ProductCategory;
import com.think.cloud.thinkshop.mall.service.category.IProductCategoryService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;

/**
 * 商品类目Controller
 *
 * @author zkthink
 * @date 2024-05-08
 */
@RestController
@RequestMapping("/admin/category")
@Api(tags = "ADMIN:商品类目")
public class ProductCategoryController extends BaseController {
    @Autowired
    private IProductCategoryService productCategoryService;

    /**
     * 查询商品类目列表
     */
    @RequiresPermissions("mall:category:list")
    @GetMapping("/list")
    public TableDataInfo list(ProductCategory productCategory) {
        return getDataTable(productCategoryService.getProductTree());
    }

    /**
     * 获取商品类目详细信息
     */
    @RequiresPermissions("mall:category:query")
    @GetMapping(value = "/get/{categoryId}")
    public AjaxResult getInfo(@PathVariable("categoryId") Long categoryId) {
        return success(productCategoryService.selectProductCategoryByCategoryId(categoryId));
    }

    /**
     * 新增商品类目
     */
    @RequiresPermissions("mall:category:add")
    @Log(title = "商品类目", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody ProductCategory productCategory) {
        productCategory.setCreateBy(SecurityUtils.getUsername());
        return toAjax(productCategoryService.insertProductCategory(productCategory));
    }

    /**
     * 修改商品类目
     */
    @RequiresPermissions("mall:category:edit")
    @Log(title = "商品类目", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody ProductCategory productCategory) {
        productCategory.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(productCategoryService.updateProductCategory(productCategory));
    }

    /**
     * 删除商品类目
     */
    @RequiresPermissions("mall:category:remove")
    @Log(title = "商品类目", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@Valid Long[] categoryIds) {
        return toAjax(productCategoryService.batchDelete(Arrays.asList(categoryIds)));
    }

    /**
     * 取消关联
     */
    @RequiresPermissions("mall:category:cancel")
    @Log(title = "取消关联", businessType = BusinessType.GRANT)
    @GetMapping("/cancel")
    public AjaxResult cancel(@Valid Long[] productIds) {
        productCategoryService.cancel(Arrays.asList(productIds));
        return toAjax(Boolean.TRUE);
    }

    /**
     * 关联
     */
    @RequiresPermissions("mall:category:relation")
    @Log(title = "关联", businessType = BusinessType.GRANT)
    @PostMapping("/relation")
    public AjaxResult relation(@RequestBody ProductRelationCategoryReqVO vo) {
        productCategoryService.relation(vo);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 根据级别查询列表
     */
    @RequiresPermissions("mall:category:level-list")
    @GetMapping("/level-list/{level}")
    public TableDataInfo levelList(@PathVariable("level") Long level) {
        return getDataTable(productCategoryService.selectProductCategoryList(ProductCategory.builder().level(level).build()));
    }
}
