package com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * 售后单详情响应VO
 *
 * @author zkthink
 * @date 2024-06-17
 */
@Data
public class AfterSalesInfoRespVO {

    @ApiModelProperty("售后id")
    private Long id;

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("售后单号")
    private String afterSalesCode;

    @ApiModelProperty("订单编号")
    private String orderCode;

    @ApiModelProperty("退货数量")
    private Integer refundNumber;

    @ApiModelProperty(name = "退邮费")
    private BigDecimal totalPostage;

    @ApiModelProperty("退款金额")
    private BigDecimal refundAmount;

    @ApiModelProperty("服务类型：0、仅退款，1、退货退款")
    private Integer serviceType;

    @ApiModelProperty("申请原因")
    private String reasons;

    @ApiModelProperty("说明")
    private String explains;

    @ApiModelProperty("说明图片")
    private String explainImg;

    @ApiModelProperty("物流公司编码")
    private String shipperCode;

    @ApiModelProperty("物流单号")
    private String deliverySn;

    @ApiModelProperty("物流名称")
    private String deliveryName;

    @ApiModelProperty("状态：-2、商家拒绝，-1、用户取消，0、已提交等待平台审核，1、平台已审核 等待用户发货，2、用户已发货，3、退款成功")
    private Integer state;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("下单用户")
    private String orderUser;

    @ApiModelProperty("商家收货人")
    private String consignee;

    @ApiModelProperty("商家手机号")
    private String phoneNumber;

    @ApiModelProperty("商家地址")
    private String address;

    @ApiModelProperty("退货说明")
    private String returnPolicy;

    @ApiModelProperty("退货凭证")
    private String returnVoucher;

    @ApiModelProperty("收货状态：0、未收到，1、已收到")
    private Integer receivingStatus;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("明细")
    private List<AfterSalesOrderDetailRespVO> details;

    @ApiModelProperty("操作")
    private List<AfterSalesLogRespVO> logs;
}
