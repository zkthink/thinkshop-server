package com.think.cloud.thinkshop.mall.service.payment;

import com.paypal.api.payments.Payment;
import com.think.cloud.thinkshop.mall.controller.app.payment.dto.RefundDTO;
import com.think.cloud.thinkshop.mall.controller.app.payment.dto.ToPayDTO;
import com.think.cloud.thinkshop.mall.controller.app.payment.vo.PayResultVO;

/**
 * @author zkthink
 * @apiNote
 **/
public interface PaymentService {
    /**
     * 支付下单
     *
     * @param dto /
     * @param userId 用户id
     * @param client 客户端标识
     * @return /
     */
    PayResultVO paypalPay(ToPayDTO dto,Long userId,String client);

    Payment executePay(String paymentId, String payerId);

    /**
     * stripe支付
     *
     * @param dto
     * @param userId
     * @return
     */
    PayResultVO stripePay(ToPayDTO dto,Long userId);

    /**
     * 支付回调成功
     *
     * @param orderCode       订单id
     * @param latestCharge    最新的支付信息
     * @param paymentIntentId 支付id
     * @param payType 支付类型
     */
    void paySuccess(String orderCode,String latestCharge,String paymentIntentId, String payType);

    /**
     * 支付回调失败
     * @param orderCode 订单id
     */
    void payFailed(String orderCode);

    /**
     * 支付状态查询
     * @param orderId 订单id
     * @return 是否支付成功
     */
    Boolean paymentStatus(Long orderId);

    /**
     * 发起退款
     * @param dto 退款信息
     * @param type 支付类型
     * @return
     */
    public Boolean createRefund(RefundDTO dto, String type);
}
