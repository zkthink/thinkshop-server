package com.think.cloud.thinkshop.mall.controller.app.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 商品查询请求VO
 *
 * @author zkthink
 * @date 2024-05-14
 */
@Data
public class AppProductPageReqVO {

    /**
     * 商品名称
     */
    @ApiModelProperty("商品名称")
    private String productName;


    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private Long groupId;

    /**
     * 类目id集合
     */
    @ApiModelProperty("类目id集合")
    private List<Object> categoryIds;

    /**
     * 排序方式
     */
    @ApiModelProperty("排序方式")
    private Integer sort;

    /**
     * 商品id集合
     */
    @ApiModelProperty("商品id集合")
    private List<Long> productIds;

    /**
     * 排除商品id集合
     */
    @ApiModelProperty("排除商品id集合")
    private List<Long> excludeProductIds;
}
