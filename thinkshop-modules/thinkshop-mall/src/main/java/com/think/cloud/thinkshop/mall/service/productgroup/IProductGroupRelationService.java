package com.think.cloud.thinkshop.mall.service.productgroup;

import java.util.List;
import com.think.cloud.thinkshop.mall.domain.ProductGroupRelation;

/**
 * 商品分组关联Service接口
 *
 * @author zkthink
 * @date 2024-05-10
 */
public interface IProductGroupRelationService
{
    /**
     * 查询商品分组关联
     *
     * @param groupRelationId 商品分组关联主键
     * @return 商品分组关联
     */
    public ProductGroupRelation selectProductGroupRelationByGroupRelationId(Long groupRelationId);

    /**
     * 查询商品分组关联列表
     *
     * @param productGroupRelation 商品分组关联
     * @return 商品分组关联集合
     */
    public List<ProductGroupRelation> selectProductGroupRelationList(ProductGroupRelation productGroupRelation);

    /**
     * 新增商品分组关联
     *
     * @param productGroupRelation 商品分组关联
     * @return 结果
     */
    public int insertProductGroupRelation(ProductGroupRelation productGroupRelation);

    /**
     * 修改商品分组关联
     *
     * @param productGroupRelation 商品分组关联
     * @return 结果
     */
    public int updateProductGroupRelation(ProductGroupRelation productGroupRelation);

    /**
     * 批量删除商品分组关联信息
     *
     * @param groupRelationIds 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> groupRelationIds);


    /**
     * 查询分组内商品id
     *
     * @param groupId 分组id
     * @return 结果
     */
    public List<Long> selectProductIdByGroupId(Long groupId);
}
