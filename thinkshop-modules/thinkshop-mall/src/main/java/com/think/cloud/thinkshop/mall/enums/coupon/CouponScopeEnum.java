package com.think.cloud.thinkshop.mall.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 优惠券范围类型枚举
 */
@Getter
@AllArgsConstructor
public enum CouponScopeEnum {
    ALL(1, "所有商品"),
    PRODUCT(2, "选中商品"),
    DISABLE_PRODUCT(3, "选中商品不可用");

    private Integer value;
    private String desc;
}
