package com.think.cloud.thinkshop.mall.controller.app.order.vo;

import com.think.cloud.thinkshop.mall.annotation.CrossBorderZoneTimeAmend;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;


/**
 * 订单操作响应VO
 *
 * @author zkthink
 * @date 2024-06-03
 */
@Data
public class AppOrderLogRespVO {

    @ApiModelProperty("操作类型")
    private Integer operateType;

    @ApiModelProperty("创建时间")
    @CrossBorderZoneTimeAmend
    private Timestamp createTime;
}
