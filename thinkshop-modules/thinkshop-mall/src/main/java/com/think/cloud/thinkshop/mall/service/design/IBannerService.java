package com.think.cloud.thinkshop.mall.service.design;


import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerPageRespVO;
import com.think.cloud.thinkshop.mall.domain.design.Banner;

import java.util.List;


/**
 * BannerService接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
public interface IBannerService {
    /**
     * 查询Banner
     *
     * @param id Banner主键
     * @return Banner
     */
    public Banner selectBannerById(Long id);

    /**
     * 查询Banner列表
     *

     * @return Banner集合
     */
    public List<BannerPageRespVO> selectBannerList();

    /**
     * 新增Banner
     *
     * @param vo
     * @return 结果
     */
    public int insertBanner(BannerAddReqVO vo);

    /**
     * 修改Banner
     *
     * @param banner Banner
     * @return 结果
     */
    public int updateBanner(BannerEditReqVO banner);

    /**
     * 批量删除Banner信息
     *
     * @param ids 主键集合
     * @return 结果
     */
     int batchDelete(List<Long> ids);
}
