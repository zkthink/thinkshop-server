package com.think.cloud.thinkshop.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.ProductGroupPageRespVO;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.ProductGroupRelation;

import java.util.List;

/**
 * 商品分组关联Mapper接口
 *
 * @author zkthink
 * @date 2024-05-10
 */
@Mapper
public interface ProductGroupRelationMapper extends BaseMapper<ProductGroupRelation> {

    void batchInsert(List<ProductGroupRelation> list);

    List<ProductGroupPageRespVO> getRelationCount(List<Long> list);
}
