package com.think.cloud.thinkshop.mall.domain.order;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import java.sql.Timestamp;

/**
 * 订单日志对象 mall_order_log
 * 
 * @author zkthink
 * @date 2024-05-23
 */
@TableName("mall_order_log")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderLog
{
    private static final long serialVersionUID = 1L;

    /** 订单日志id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 用户类型：1、用户；2、商户；3、系统 */
    @Excel(name = "用户类型：1、用户；2、商户；3、系统")
    private Integer userType;

    /** 操作类型：1、提交订单；2、取消订单；3、超时自动取消订单；4、完成支付；5、商家发货；6、确认收货；7、超时自动确认收货 */
    @Excel(name = "操作类型：1、提交订单；2、取消订单；3、超时自动取消订单；4、完成支付；5、商家发货；6、确认收货；7、超时自动确认收货")
    private Integer operateType;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Timestamp createTime;

}
