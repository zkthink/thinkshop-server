package com.think.cloud.thinkshop.mall.mapper.websitesetting;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.websitesetting.Agreement;
import org.apache.ibatis.annotations.Mapper;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Mapper
public interface AgreementMapper extends BaseMapper<Agreement> {

}
