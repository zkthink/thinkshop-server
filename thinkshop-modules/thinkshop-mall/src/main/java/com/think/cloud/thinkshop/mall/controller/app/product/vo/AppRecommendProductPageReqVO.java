package com.think.cloud.thinkshop.mall.controller.app.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 推荐好物查询请求VO
 *
 * @author zkthink
 * @date 2024-05-14
 */
@Data
public class AppRecommendProductPageReqVO {

    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private Long productId;
}
