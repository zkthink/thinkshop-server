package com.think.cloud.thinkshop.mall.domain.coupon;

import java.sql.Timestamp;
import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 商品优惠券关联对象 mall_product_coupon_relation
 *
 * @author zkthink
 * @date 2024-05-21
 */
@TableName("mall_product_coupon_relation")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductCouponRelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 优惠券id */
    @Excel(name = "优惠券id")
    private Long couponId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 是否使用 */
    @Excel(name = "是否使用")
    private Integer isUsed;

    /** 领取时间 */
    @Excel(name = "领取时间")
    private Timestamp receiveTime;

    /** 使用时间 */
    @Excel(name = "使用时间")
    private Timestamp usageTime;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 运营计划id */
    @Excel(name = "运营计划id")
    private Long planId;

}
