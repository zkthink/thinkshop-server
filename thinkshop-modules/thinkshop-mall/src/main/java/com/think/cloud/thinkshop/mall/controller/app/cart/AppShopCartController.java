package com.think.cloud.thinkshop.mall.controller.app.cart;


import com.think.cloud.thinkshop.mall.controller.app.cart.vo.AddShopCartReqVO;
import com.think.cloud.thinkshop.mall.controller.app.cart.vo.UpdateShopCartReqVO;
import com.think.cloud.thinkshop.mall.service.cart.AppShopCartService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;

/**
 * 购物车AppController
 *
 * @author zkthink
 * @date 2024-05-17
 */
@RestController
@RequestMapping("/app/cart")
@Api(tags = "APP:购物车")
public class AppShopCartController extends BaseController {
    @Autowired
    private AppShopCartService appShopCartService;

    /**
     * 我的购物车
     */
    @GetMapping("/my-cart")
    public TableDataInfo getMyCart() {
        startPage();
        return appShopCartService.getMyCart(SecurityUtils.getUserId());
    }

    /**
     * 新增购物车
     */
    @PostMapping("/add")
    public AjaxResult add(@RequestBody AddShopCartReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        return success(appShopCartService.addCart(vo));
    }

    /**
     * 修改购物车
     */
    @PutMapping("/update")
    public AjaxResult update(@RequestBody UpdateShopCartReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        appShopCartService.updateCart(vo);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 删除购物车
     */
    @DeleteMapping("/delete")
    public AjaxResult remove(@Valid Long[] cartIds) {
        appShopCartService.deleteCart(Arrays.asList(cartIds));
        return toAjax(Boolean.TRUE);
    }

    @GetMapping("/coupon")
    public TableDataInfo searchCartAvailableCoupon(@RequestParam String cartIds) {
        return getDataTable(appShopCartService.searchCartAvailableCoupon(cartIds, SecurityUtils.getUserId()));
    }
}
