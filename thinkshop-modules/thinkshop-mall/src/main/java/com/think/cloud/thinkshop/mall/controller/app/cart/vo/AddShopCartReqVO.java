package com.think.cloud.thinkshop.mall.controller.app.cart.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * 添加购物车请求VO
 *
 * @author zkthink
 * @date 2024-05-17
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddShopCartReqVO {

    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private Long userId;

    /**
     * 商品ID
     */
    @ApiModelProperty("商品ID")
    private Long productId;

    /**
     * 商品属性id
     */
    @ApiModelProperty("商品属性id")
    private Long skuId;

    /**
     * 商品数量
     */
    @ApiModelProperty("商品数量")
    private Integer num;

    /** 是否为立即购买 */
    @ApiModelProperty("是否为立即购买")
    private Integer isNew;

}
