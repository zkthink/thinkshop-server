package com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.List;

/**
 * 商品分组详情响应VO
 *
 * @author zkthink
 * @date 2024-05-13
 */
@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductGroupDetailRespVO {

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private Long groupId;

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    private String name;

    /**
     * 分组描述
     */
    @ApiModelProperty("分组描述")
    private String remark;

    /** 类型: 0、手动选择 1、智能选择 */
    @ApiModelProperty("类型: 0、手动选择 1、智能选择")
    private Integer type;

    /** 智能选择条件json */
    @ApiModelProperty("智能选择条件json")
    private String conditionJson;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    /**
     * 关联商品id集合
     */
    @ApiModelProperty("关联商品id集合")
   private List<Long> productIds;

}
