package com.think.cloud.thinkshop.mall.controller.admin.coupon;


import java.util.Arrays;

import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponCreateReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponUpdateReqVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.cloud.thinkshop.mall.service.coupon.IProductCouponService;

import javax.validation.Valid;

import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 商品优惠券Controller
 *
 * @author zkthink
 * @date 2024-05-21
 */
@RestController
@RequestMapping("/admin/coupon")
@Api(tags = "ADMIN:商品优惠券")
public class ProductCouponController extends BaseController {
    @Autowired
    private IProductCouponService productCouponService;

    /**
     * 查询商品优惠券列表
     */
    @RequiresPermissions("mall:coupon:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询商品优惠券列表")
    public TableDataInfo list(@Valid ProductCouponPageReqVO vo) {
        startPage();
        return productCouponService.selectProductCouponList(vo);
    }

    /**
     * 获取商品优惠券详细信息
     */
    @RequiresPermissions("mall:coupon:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取商品优惠券详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(productCouponService.selectProductCouponById(id));
    }

    /**
     * 新增商品优惠券
     */
    @RequiresPermissions("mall:coupon:add")
    @Log(title = "商品优惠券", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增商品优惠券")
    public AjaxResult add(@Validated @RequestBody ProductCouponCreateReqVO vo) {
        return success(productCouponService.insertProductCoupon(vo));
    }

    /**
     * 修改商品优惠券
     */
    @RequiresPermissions("mall:coupon:edit")
    @Log(title = "商品优惠券", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改商品优惠券")
    public AjaxResult edit(@Validated @RequestBody ProductCouponUpdateReqVO vo) {
        return success(productCouponService.updateProductCoupon(vo));
    }

    /**
     * 删除商品优惠券
     */
    @RequiresPermissions("mall:coupon:remove")
    @Log(title = "商品优惠券", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除商品优惠券")
    public AjaxResult remove(@Valid Long[] ids) {
        return toAjax(productCouponService.batchDelete(Arrays.asList(ids)));
    }

    /**
     * 结束优惠券活动
     */
    @RequiresPermissions("mall:coupon:end")
    @GetMapping(value = "/end/{id}")
    @ApiOperation(value = "结束优惠券活动")
    public AjaxResult end(@PathVariable("id") Long id) {
        productCouponService.end(id);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 优惠券数据
     */
    @RequiresPermissions("mall:coupon:data")
    @GetMapping(value = "/data/{id}")
    @ApiOperation(value = "优惠券数据")
    public AjaxResult data(@PathVariable("id") Long id) {
        return success(productCouponService.data(id));
    }
}
