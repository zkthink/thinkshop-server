package com.think.cloud.thinkshop.mall.controller.app.order.dto;

import com.think.cloud.thinkshop.mall.controller.app.integral.vo.IntegralRuleVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


/**
 * 订单信息DTO
 *
 * @author zkthink
 * @date 2024-05-24
 */
@Data
public class AppOrderInfoDTO {

    @ApiModelProperty("商品总数")
    private Integer totalNum;

    @ApiModelProperty("商品总价")
    private BigDecimal totalPrice;

    @ApiModelProperty("优惠金额")
    private BigDecimal discountAmount;

    @ApiModelProperty("运费")
    private BigDecimal totalPostage;

    @ApiModelProperty("税费")
    private BigDecimal taxation;

    @ApiModelProperty("运费税费")
    private BigDecimal postageTaxation;

    @ApiModelProperty("应付金额")
    private BigDecimal payPrice;

    @ApiModelProperty("当前积分规则")
    private IntegralRuleVO integralRule;

    @ApiModelProperty("用户积分余额")
    private Integer integralBalance;

    @ApiModelProperty("使用积分的选项")
    private List<BigDecimal> useIntegralOptions;

    @ApiModelProperty("使用多少积分或者比例进行抵扣")
    private BigDecimal payIntegral;

    @ApiModelProperty("积分抵扣金额")
    private BigDecimal integralDeduct = BigDecimal.ZERO;


}
