package com.think.cloud.thinkshop.mall.mapper;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.enums.common.CommonWhetherEnum;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.UserAddress;

import java.util.List;

/**
 * 用户地址Mapper接口
 *
 * @author zkthink
 * @date 2024-05-20
 */
@Mapper
public interface UserAddressMapper extends BaseMapper<UserAddress> {

    default List<UserAddress> selectListByUserId(Long  userId) {
        return selectList(new LambdaQueryWrapper<UserAddress>()
                .eq(UserAddress::getUserId, userId)
                .orderByDesc(UserAddress::getIsDefault));
    }

    default UserAddress selectDefaultAddress(Long addressId, Long userId) {
        return selectOne(new LambdaQueryWrapper<UserAddress>()
                .eq(ObjectUtil.isNotNull(addressId), UserAddress::getAddressId, addressId)
                .eq(UserAddress::getUserId, userId)
                .eq(ObjectUtil.isNull(addressId), UserAddress::getIsDefault, CommonWhetherEnum.YES.getValue()));
    }

}
