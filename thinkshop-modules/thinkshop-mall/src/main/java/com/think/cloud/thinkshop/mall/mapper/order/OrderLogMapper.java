package com.think.cloud.thinkshop.mall.mapper.order;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.order.OrderLog;

import java.util.List;

/**
 * 订单日志Mapper接口
 *
 * @author zkthink
 * @date 2024-05-23
 */
@Mapper
public interface OrderLogMapper extends BaseMapper<OrderLog> {

    default List<OrderLog> getOrderLogListByOrderId(Long orderId){
        return selectList(new LambdaQueryWrapper<OrderLog>()
                .eq(OrderLog::getOrderId, orderId).orderByAsc(OrderLog::getCreateTime));
    }

}
