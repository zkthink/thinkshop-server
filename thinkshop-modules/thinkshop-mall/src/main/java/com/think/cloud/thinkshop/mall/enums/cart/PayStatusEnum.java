package com.think.cloud.thinkshop.mall.enums.cart;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 支付状态枚举
 */
@Getter
@AllArgsConstructor
public enum PayStatusEnum {

    NO(0, "未支付"),
    YES(1, "已支付");

    private Integer value;
    private String desc;

}
