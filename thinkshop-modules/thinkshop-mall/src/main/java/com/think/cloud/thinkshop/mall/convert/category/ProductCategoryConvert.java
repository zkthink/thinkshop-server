package com.think.cloud.thinkshop.mall.convert.category;

import com.think.cloud.thinkshop.mall.controller.admin.category.vo.ProductCategoryLevelVO;
import com.think.cloud.thinkshop.mall.controller.app.category.vo.AppProductCategoryLevelVO;
import com.think.cloud.thinkshop.mall.domain.ProductCategory;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 类目 Convert
 *
 * @author zkthink
 */
@Mapper
public interface ProductCategoryConvert {

    ProductCategoryConvert INSTANCE = Mappers.getMapper(ProductCategoryConvert.class);

    ProductCategoryLevelVO convert(ProductCategory bean);

    List<ProductCategoryLevelVO> convertList(List<ProductCategory> list);

    AppProductCategoryLevelVO convert(ProductCategoryLevelVO bean);

    List<AppProductCategoryLevelVO> convertList1(List<ProductCategory> list);

}
