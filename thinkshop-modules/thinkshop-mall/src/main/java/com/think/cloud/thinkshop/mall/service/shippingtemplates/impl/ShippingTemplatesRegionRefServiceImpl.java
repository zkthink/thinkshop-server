package com.think.cloud.thinkshop.mall.service.shippingtemplates.impl;

import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.think.cloud.thinkshop.mall.service.shippingtemplates.IShippingTemplatesRegionRefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.think.cloud.thinkshop.mall.mapper.shippingtemplates.ShippingTemplatesRegionRefMapper;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplatesRegionRef;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 物流方案地区关联Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Service
public class ShippingTemplatesRegionRefServiceImpl implements IShippingTemplatesRegionRefService {
    @Autowired
    private ShippingTemplatesRegionRefMapper shippingTemplatesRegionRefMapper;

    /**
     * 查询物流方案地区关联
     *
     * @param id 物流方案地区关联主键
     * @return 物流方案地区关联
     */
    @Override
    public ShippingTemplatesRegionRef selectShippingTemplatesRegionRefById(Long id) {
        return shippingTemplatesRegionRefMapper.selectById(id);
    }

    /**
     * 查询物流方案地区关联列表
     *
     * @param shippingTemplatesRegionRef 物流方案地区关联
     * @return 物流方案地区关联
     */
    @Override
    public List<ShippingTemplatesRegionRef> selectShippingTemplatesRegionRefList(ShippingTemplatesRegionRef shippingTemplatesRegionRef) {
        return shippingTemplatesRegionRefMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增物流方案地区关联
     *
     * @param shippingTemplatesRegionRef 物流方案地区关联
     * @return 结果
     */
    @Override
    public int insertShippingTemplatesRegionRef(ShippingTemplatesRegionRef shippingTemplatesRegionRef) {

        return shippingTemplatesRegionRefMapper.insert(shippingTemplatesRegionRef);
    }

    /**
     * 修改物流方案地区关联
     *
     * @param shippingTemplatesRegionRef 物流方案地区关联
     * @return 结果
     */
    @Override
    public int updateShippingTemplatesRegionRef(ShippingTemplatesRegionRef shippingTemplatesRegionRef) {
        return shippingTemplatesRegionRefMapper.updateById(shippingTemplatesRegionRef);
    }

    /**
     * 批量删除物流方案地区关联信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return shippingTemplatesRegionRefMapper.deleteBatchIds(ids);
    }

    @Override
    public List<String> selectRegionIdsByTemplateId(Long id) {
        return shippingTemplatesRegionRefMapper.selectList(Wrappers.<ShippingTemplatesRegionRef>lambdaQuery().eq(ShippingTemplatesRegionRef::getTemplateId, id)).stream().map(ShippingTemplatesRegionRef::getRegionId).collect(Collectors.toList());
    }

    @Override
    public void saveBatch(Long id, List<String> regionIds, boolean isAdd) {
        if (!isAdd) {
            deleteByTemplateId(id);
        }
        if(ObjectUtil.isEmpty(regionIds)){
            return;
        }
        List<ShippingTemplatesRegionRef> collect = regionIds.stream().map(regionId -> {
            ShippingTemplatesRegionRef shippingTemplatesRegionRef = new ShippingTemplatesRegionRef();
            shippingTemplatesRegionRef.setTemplateId(id);
            shippingTemplatesRegionRef.setRegionId(regionId);
            return shippingTemplatesRegionRef;
        }).collect(Collectors.toList());
        Db.saveBatch(collect, collect.size());
    }

    @Override
    public void deleteByTemplateId(Long templateId) {
        shippingTemplatesRegionRefMapper.delete(Wrappers.<ShippingTemplatesRegionRef>lambdaQuery().eq(ShippingTemplatesRegionRef::getTemplateId, templateId));
    }

    @Override
    public ShippingTemplatesRegionRef selectByRegionId(String regionId) {
        return shippingTemplatesRegionRefMapper.selectOne(Wrappers.<ShippingTemplatesRegionRef>lambdaQuery().eq(ShippingTemplatesRegionRef::getRegionId, regionId));
    }

    @Override
    public List<ShippingTemplatesRegionRef> selectByRegionIds(List<String> regionIds) {
        return shippingTemplatesRegionRefMapper.selectList(Wrappers.<ShippingTemplatesRegionRef>lambdaQuery().in(ShippingTemplatesRegionRef::getRegionId, regionIds));
    }
}
