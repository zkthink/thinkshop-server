package com.think.cloud.thinkshop.mall.service.message.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.domain.message.MessageUserRecord;
import com.think.cloud.thinkshop.mall.enums.message.MessageStatusEnum;
import com.think.cloud.thinkshop.mall.mapper.message.MessageUserRecordMapper;
import com.think.cloud.thinkshop.mall.service.message.IMessageUserRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户接收的站内信Service业务层处理
 *
 * @author moxiangrong
 * @date 2024-07-22
 */
@Service
public class MessageUserRecordServiceImpl implements IMessageUserRecordService {
    @Autowired
    private MessageUserRecordMapper messageUserRecordMapper;

    /**
     * 查询用户接收的站内信
     *
     * @param id 用户接收的站内信主键
     * @return 用户接收的站内信
     */
    @Override
    public MessageUserRecord selectMessageUserRecordById(Long id, Long userId) {
        MessageUserRecord message = messageUserRecordMapper.selectOne(Wrappers.<MessageUserRecord>lambdaQuery()
                .eq(MessageUserRecord::getId, id)
                .eq(MessageUserRecord::getUserId, userId)
        );
//        MessageUserRecord message = messageUserRecordMapper.selectById(id);
        if (message!=null && message.getUserRead().equals(MessageStatusEnum.NO.getStatus())) {
            message.setUserRead(MessageStatusEnum.YES.getStatus());
            updateMessageUserRecord(message);
        }
        return message;
    }

    /**
     * 查询用户接收的站内信列表
     *
     * @param messageUserRecord 用户接收的站内信
     * @return 用户接收的站内信
     */
    @Override
    public List<MessageUserRecord> selectMessageUserRecordList(MessageUserRecord messageUserRecord) {
        return messageUserRecordMapper.selectList(new LambdaQueryWrapper<>());
    }

    @Override
    public List<MessageUserRecord> listMessageUserRecord(Long userId) {
        return messageUserRecordMapper.selectList(
                Wrappers.<MessageUserRecord>lambdaQuery()
                        .eq(MessageUserRecord::getUserId, userId)
                        .orderByAsc(MessageUserRecord::getUserRead)
                        .orderByDesc(MessageUserRecord::getCreateTime)
        );
    }

    /**
     * 新增用户接收的站内信
     *
     * @param messageUserRecord 用户接收的站内信
     * @return 结果
     */
    @Override
    public int insertMessageUserRecord(MessageUserRecord messageUserRecord) {

        return messageUserRecordMapper.insert(messageUserRecord);
    }

    /**
     * 修改用户接收的站内信
     *
     * @param messageUserRecord 用户接收的站内信
     * @return 结果
     */
    @Override
    public int updateMessageUserRecord(MessageUserRecord messageUserRecord) {
//        messageUserRecord.setUpdateTime(DateUtils.getNowDate());
        return messageUserRecordMapper.updateById(messageUserRecord);
    }

    /**
     * 批量删除用户接收的站内信信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return messageUserRecordMapper.deleteBatchIds(ids);
    }
}
