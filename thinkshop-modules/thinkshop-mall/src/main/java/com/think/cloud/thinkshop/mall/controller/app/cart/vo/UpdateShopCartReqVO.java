package com.think.cloud.thinkshop.mall.controller.app.cart.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * 编辑购物车请求VO
 *
 * @author zkthink
 * @date 2024-05-17
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateShopCartReqVO {

    /**
     * 购物车id
     */
    @ApiModelProperty("购物车id")
    private Long cartId;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private Long userId;

    /**
     * 商品数量
     */
    @ApiModelProperty("商品数量")
    private Integer num;

}
