package com.think.cloud.thinkshop.mall.service.shippingtemplates.impl;

import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplatesRegionRef;
import com.think.cloud.thinkshop.mall.service.shippingtemplates.IShippingTemplatesProductRefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.think.cloud.thinkshop.mall.mapper.shippingtemplates.ShippingTemplatesProductRefMapper;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplatesProductRef;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 物流方案商品关联Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Service
public class ShippingTemplatesProductRefServiceImpl implements IShippingTemplatesProductRefService {
    @Autowired
    private ShippingTemplatesProductRefMapper shippingTemplatesProductRefMapper;

    /**
     * 查询物流方案商品关联
     *
     * @param id 物流方案商品关联主键
     * @return 物流方案商品关联
     */
    @Override
    public ShippingTemplatesProductRef selectShippingTemplatesProductRefById(Long id) {
        return shippingTemplatesProductRefMapper.selectById(id);
    }

    /**
     * 查询物流方案商品关联列表
     *
     * @param shippingTemplatesProductRef 物流方案商品关联
     * @return 物流方案商品关联
     */
    @Override
    public List<ShippingTemplatesProductRef> selectShippingTemplatesProductRefList(ShippingTemplatesProductRef shippingTemplatesProductRef) {
        return shippingTemplatesProductRefMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增物流方案商品关联
     *
     * @param shippingTemplatesProductRef 物流方案商品关联
     * @return 结果
     */
    @Override
    public int insertShippingTemplatesProductRef(ShippingTemplatesProductRef shippingTemplatesProductRef) {

        return shippingTemplatesProductRefMapper.insert(shippingTemplatesProductRef);
    }

    /**
     * 修改物流方案商品关联
     *
     * @param shippingTemplatesProductRef 物流方案商品关联
     * @return 结果
     */
    @Override
    public int updateShippingTemplatesProductRef(ShippingTemplatesProductRef shippingTemplatesProductRef) {
        return shippingTemplatesProductRefMapper.updateById(shippingTemplatesProductRef);
    }

    /**
     * 批量删除物流方案商品关联信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return shippingTemplatesProductRefMapper.deleteBatchIds(ids);
    }

    @Override
    public List<Long> selectProductIdsByTemplateId(Long id) {
        return shippingTemplatesProductRefMapper.selectList(Wrappers.<ShippingTemplatesProductRef>lambdaQuery().eq(ShippingTemplatesProductRef::getTemplateId, id)).stream().map(ShippingTemplatesProductRef::getProductId).collect(Collectors.toList());
    }

    @Override
    public void saveBatch(Long id, List<Long> productIds, boolean isAdd) {
        if (!isAdd) {
            deleteByTemplateId(id);
        }
        if(ObjectUtil.isEmpty(productIds)){
            return;
        }
        List<ShippingTemplatesProductRef> collect = productIds.stream().map(productId -> {
            ShippingTemplatesProductRef shippingTemplatesRegionRef = new ShippingTemplatesProductRef();
            shippingTemplatesRegionRef.setTemplateId(id);
            shippingTemplatesRegionRef.setProductId(productId);
            return shippingTemplatesRegionRef;
        }).collect(Collectors.toList());
        Db.saveBatch(collect, collect.size());
    }

    @Override
    public void deleteByTemplateId(Long templateId) {
        shippingTemplatesProductRefMapper.delete(Wrappers.<ShippingTemplatesProductRef>lambdaQuery().eq(ShippingTemplatesProductRef::getTemplateId, templateId));
    }

    @Override
    public List<ShippingTemplatesProductRef> selectByProductIds(List<Long> productIds) {
        return shippingTemplatesProductRefMapper.selectList(Wrappers.<ShippingTemplatesProductRef>lambdaQuery().in(ShippingTemplatesProductRef::getProductId, productIds));
    }

    @Override
    public void deleteByProductIds(List<Long> productIds) {
        shippingTemplatesProductRefMapper.delete(Wrappers.<ShippingTemplatesProductRef>lambdaQuery().in(ShippingTemplatesProductRef::getProductId, productIds));
    }

}
