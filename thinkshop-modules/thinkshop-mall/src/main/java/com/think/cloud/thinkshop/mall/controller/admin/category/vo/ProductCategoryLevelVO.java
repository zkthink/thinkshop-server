package com.think.cloud.thinkshop.mall.controller.admin.category.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;


@Data
@ToString(callSuper = true)
public class ProductCategoryLevelVO {

    /**
     * 类目id
     */
    @ApiModelProperty("类目id")
    private Long categoryId;

    /**
     * 类目级别
     */
    @ApiModelProperty("类目级别")
    private Long level;

    /**
     * 父类目id
     */
    @ApiModelProperty("父类目id")
    private Long parentCategoryId;

    /**
     * 类路径
     */
    @ApiModelProperty("类路径")
    private String path;

    /**
     * 类目名称
     */
    @ApiModelProperty("类目名称")
    private String name;

    /**
     * 类目图片
     */
    @ApiModelProperty("类目图片")
    private String pictureUrl;

    /**
     * 权重
     */
    @ApiModelProperty("权重")
    private Long weight;

    /**
     * 关联商品数量
     */
    @ApiModelProperty("关联商品数量")
    private Integer relationCount;

    /**
     * 子类目
     */
    @ApiModelProperty("子类目")
    private List<ProductCategoryLevelVO> child;

}
