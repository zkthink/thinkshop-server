package com.think.cloud.thinkshop.mall.service.aftersales;

import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesLogRespVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesLogRespVO;
import com.think.cloud.thinkshop.mall.domain.aftersales.AfterSalesLog;

import java.util.List;

/**
 * 售后日志Service接口
 *
 * @author zkthink
 * @date 2024-06-13
 */
public interface IAfterSalesLogService
{
    /**
     * 获取售后操作日志
     *
     * @param afterSalesId 售后id
     */
    public List<AppAfterSalesLogRespVO> getAfterSalesLogList(Long afterSalesId);


    public void insertSalesLog(AfterSalesLog afterSalesLog);

    /**
     * 查询售后单日志列表
     *
     * @param orderId
     * @return
     */
    public List<AfterSalesLogRespVO> selectAfterSalesLogListByAfterSalesId(Long orderId);
}
