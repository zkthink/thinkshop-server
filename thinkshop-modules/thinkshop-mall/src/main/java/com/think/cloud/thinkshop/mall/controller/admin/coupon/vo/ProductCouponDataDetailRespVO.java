package com.think.cloud.thinkshop.mall.controller.admin.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;



@Data
@ToString(callSuper = true)
public class ProductCouponDataDetailRespVO {

    @ApiModelProperty("商品名")
    private String productName;

    @ApiModelProperty("件数")
    private Long number;

    @ApiModelProperty("人数")
    private Long person;
}
