package com.think.cloud.thinkshop.mall.controller.admin.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * 订单分页响应VO
 *
 * @author zkthink
 * @date 2024-05-29
 */
@Data
public class OrderPageRespVO {

    @ApiModelProperty("订单id")
    private Long id;

    @ApiModelProperty("订单号")
    private String orderCode;

    @ApiModelProperty("订单商品总数")
    private Integer totalNum;

    @ApiModelProperty("订单总价")
    private BigDecimal totalPrice;

    @ApiModelProperty("实际支付金额")
    private BigDecimal payPrice;

    @ApiModelProperty("下单用户")
    private String userName;

    @ApiModelProperty("订单状态（-1：已取消；0：待付款；1：待发货；2：待收货；3：已收货；4：已完成；）")
    private Integer status;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;

    @ApiModelProperty("明细")
    private List<OrderDetailRespVO> details;
    @ApiModelProperty("用户侧是否删除 0：否  1：已经删除")
    private Integer userDel;


}
