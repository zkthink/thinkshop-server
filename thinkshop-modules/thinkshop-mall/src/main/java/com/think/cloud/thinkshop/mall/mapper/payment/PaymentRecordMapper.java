package com.think.cloud.thinkshop.mall.mapper.payment;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 支付记录Mapper接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Mapper
public interface PaymentRecordMapper extends BaseMapper<PaymentRecord> {

}
