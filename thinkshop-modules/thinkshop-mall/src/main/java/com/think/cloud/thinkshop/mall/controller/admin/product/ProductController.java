package com.think.cloud.thinkshop.mall.controller.admin.product;


import com.think.cloud.thinkshop.mall.controller.admin.product.vo.*;
import com.think.cloud.thinkshop.mall.service.product.IProductService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;

/**
 * 商品Controller
 *
 * @author zkthink
 * @date 2024-05-09
 */
@RestController
@RequestMapping("/admin/product")
@Api(tags = "ADMIN:商品")
public class ProductController extends BaseController {
    @Autowired
    private IProductService productService;

    /**
     * 查询商品列表
     */
    @RequiresPermissions("mall:product:list")
    @GetMapping("/list")
    public TableDataInfo list(ProductPageReqVO vo) {
        startPage();
        return productService.selectProductList(vo);
    }

    /**
     * 获取商品详细信息
     */
    @RequiresPermissions("mall:product:query")
    @GetMapping(value = "/get/{productId}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId) {
        return success(productService.selectProductByProductId(productId));
    }

    /**
     * 新增商品
     */
    @RequiresPermissions("mall:product:add")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@Validated @RequestBody SaveProductReqVO vo) {
        vo.setCreateBy(SecurityUtils.getUsername());
        productService.insertProduct(vo);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 修改商品
     */
    @RequiresPermissions("mall:product:edit")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody UpdateProductReqVO vo) {
        vo.setUpdateBy(SecurityUtils.getUsername());
        productService.updateProduct(vo);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 删除商品
     */
    @RequiresPermissions("mall:product:remove")
    @Log(title = "商品", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@Valid Long[] productIds) {
        productService.batchDelete(Arrays.asList(productIds));
        return toAjax(Boolean.TRUE);
    }

    /**
     * 上架商品
     */
    @RequiresPermissions("mall:product:grounding")
    @Log(title = "商品", businessType = BusinessType.GRANT)
    @GetMapping("/grounding")
    public AjaxResult grounding(@Valid GroundingProductReqVO vo) {
        productService.grounding(vo);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 智能查找商品
     */
    @RequiresPermissions("mall:product:intelligent-search")
    @PostMapping("/intelligent-search")
    public TableDataInfo intelligentSearch(@RequestBody IntelligentSearchProductReqVO vo) {
        return getDataTable(productService.intelligentSearch(vo));
    }

    /**
     * 生成属性
     */
    @RequiresPermissions("mall:product:isFormatAttr")
    @PostMapping(value = "/isFormatAttr/{id}")
    public AjaxResult isFormatAttr(@PathVariable Long id, @RequestBody String jsonStr) {
        return success(productService.getFormatAttr(id, jsonStr));
    }

    @ApiOperation("下载导入的通用模版")
    @SneakyThrows
    @RequiresPermissions("mall:product:getTemplate")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        productService.getImportTemplate(response);
    }


    @ApiOperation("通过excel模版导入商品")
    @SneakyThrows
    @RequiresPermissions("mall:product:importProduct")
    @PostMapping("/importProduct")
    public AjaxResult importProduct(MultipartFile file) {
       return success( productService.importProductByExcel(file));
    }


    @ApiOperation("导出商品")
    @SneakyThrows
    @RequiresPermissions("mall:product:exportProduct")
    @PostMapping("/exportProduct")
    public void exportProduct(HttpServletResponse response) {
        productService.exportProduct(response);
    }


}
