package com.think.cloud.thinkshop.mall.convert.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanSearchRespVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationPlan;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Mapper
public interface MemberOperationConvert {
    MemberOperationConvert INSTANCE = Mappers.getMapper(MemberOperationConvert.class);

    MemberOperationPlan convert(MemberOperationPlanAddReqVO bean);

    MemberOperationPlan convert(MemberOperationPlanEditReqVO bean);

    List<MemberOperationPlanSearchRespVO> convertList(List<MemberOperationPlan> bean);

    MemberOperationPlanSearchRespVO convert(MemberOperationPlan memberOperationPlan);

}
