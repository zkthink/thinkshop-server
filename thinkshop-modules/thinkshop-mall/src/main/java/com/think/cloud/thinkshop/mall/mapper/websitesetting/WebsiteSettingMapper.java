package com.think.cloud.thinkshop.mall.mapper.websitesetting;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.websitesetting.WebsiteSetting;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * 网站设置Mapper接口
 *
 * @author zkthink
 * @date 2024-05-14
 */
public interface WebsiteSettingMapper extends BaseMapper<WebsiteSetting> {
    @Select("select now()")
    Date getDataBaseNow();
}
