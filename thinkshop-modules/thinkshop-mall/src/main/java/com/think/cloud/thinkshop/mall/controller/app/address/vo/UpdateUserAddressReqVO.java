package com.think.cloud.thinkshop.mall.controller.app.address.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Size;

/**
 * 修改用户地址请求VO
 *
 * @author zkthink
 * @date 2024-05-20
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserAddressReqVO {

    /**
     * 用户地址id
     */
    @ApiModelProperty("用户地址id")
    private Long addressId;

    /**
     * first name
     */
    @ApiModelProperty("first name")
    @Size(max = 32, message = "First name is too long")
    private String firstName;

    /**
     * last name
     */
    @ApiModelProperty("last name")
    @Size(max = 32, message = "Last name is too long")
    private String lastName;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    @Size(max = 16, message = "The phone number is too long")
    private String phone;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    @Size(max = 256, message = "Detailed address is too long")
    private String detail;

    /**
     * 国家编码
     */
    @ApiModelProperty("国家编码")
    private String countryId;


    /**
     * 收货人所在国家
     */
    @ApiModelProperty("收货人所在国家")
    private String country;

    /**
     * 省份编码
     */
    @ApiModelProperty("省份编码")
    private String provinceId;

    /**
     * 收货人所在省
     */
    @ApiModelProperty("收货人所在省")
    private String province;

    /**
     * 收货人所在市
     */
    @ApiModelProperty("收货人所在市")
    @Size(max = 64, message = "City name is too long")
    private String city;

    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    @Size(max = 20, message = "PostCode is too long")
    private String postCode;

    /**
     * 是否默认
     */
    @ApiModelProperty("是否默认")
    private Integer isDefault;

    /** 修改人 */
    @ApiModelProperty("修改人")
    private String updateBy;


}
