package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationPlanRef;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(description = "用户运营计划 新增 ReqVO")
@ToString(callSuper = true)
public class MemberOperationPlanAddReqVO {

    @NotNull(message = "计划名称不能为空")
    @Size(max = 20, message = "计划名称需在20字以内")
    @ApiModelProperty("计划名称")
    private String name;

    @ApiModelProperty("计划人群id")
    private List<Long> memberGroupIds;

    @ApiModelProperty("配置的优惠券id")
    @NotEmpty(message = "优惠券不能为空")
    private List<MemberOperationPlanRef> couponRefs;

    @ApiModelProperty("计划类型  ： 1：自动  ，2：手动")
    @NotNull(message = "计划类型不能为空")
    private Integer planType;

    /**
     * 手动执行类型 1 立即执行 2 定时执行
     */
    @ApiModelProperty("手动执行类型 1 立即执行 2 定时执行")
    private Integer manualExecutionType;

    @ApiModelProperty("任务开时间，手动类型下的立刻执行不填")
    private Timestamp planStart;

    @ApiModelProperty("任务结束时间，手动类型下不填")
    private Timestamp planEnd;

    public static void main(String[] args) {

        LocalDateTime start = LocalDateTime.now().plusDays(1);
        LocalDateTime end = LocalDateTime.now().plusDays(10);

        Timestamp startT = Timestamp.valueOf(start);
        Timestamp endT = Timestamp.valueOf(end);
        System.out.println(startT.getTime());
        System.out.println(endT.getTime());
    }
}
