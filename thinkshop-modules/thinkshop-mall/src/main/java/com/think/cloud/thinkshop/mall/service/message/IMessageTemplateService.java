package com.think.cloud.thinkshop.mall.service.message;

import com.think.cloud.thinkshop.mall.controller.admin.message.dto.MessageTemplateDTO;
import com.think.cloud.thinkshop.mall.controller.admin.message.dto.MessageTemplateReplaceDTO;
import com.think.cloud.thinkshop.mall.controller.admin.message.param.MessageTemplateUpdateParam;
import com.think.cloud.thinkshop.mall.domain.message.MessageTemplate;
import com.think.cloud.thinkshop.mall.enums.message.MessageTemplateEnum;

import java.util.List;


/**
 * 系统消息模版配置Service接口
 *
 * @author moxiangrong
 * @date 2024-07-19
 */
public interface IMessageTemplateService {
    /**
     * 查询系统消息模版配置
     *
     * @param id 系统消息模版配置主键
     * @return 系统消息模版配置
     */
    public MessageTemplate selectMessageTemplateById(Long id);

    /**
     * 获取消息模版
     * @param messageTemplateEnum
     * @return
     */
    public MessageTemplate getMessageTemplate(MessageTemplateEnum messageTemplateEnum);

    /**
     * 通过消息模版构建消息内容
     * @param enums
     * @param param
     * @return
     */
    public MessageTemplateDTO getMessageText(MessageTemplateEnum enums, MessageTemplateReplaceDTO param);
    public Boolean sendMessageText(MessageTemplateEnum enums, MessageTemplateReplaceDTO param,String email);

    public void sendMessageText(MessageTemplateEnum enums, MessageTemplateReplaceDTO param,Long userId);
    /**
     * 查询系统消息模版配置列表
     *
     * @return 系统消息模版配置集合
     */
    public List<MessageTemplate> selectMessageTemplateList();

    /**
     * 新增系统消息模版配置
     *
     * @param messageTemplate 系统消息模版配置
     * @return 结果
     */
    public int insertMessageTemplate(MessageTemplate messageTemplate);

    /**
     * 修改系统消息模版配置
     *
     * @param messageTemplate 系统消息模版配置
     * @return 结果
     */
    public int updateMessageTemplate(MessageTemplateUpdateParam messageTemplate);

    /**
     * 批量删除系统消息模版配置信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);
}
