
package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import lombok.*;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 商品sku请求VO
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductSkuReqVO {

    /** 商品属性索引值 (attr_value|attr_value[|....]) */
    private String sku;

    private Map<String,String> detail;

    /** 图片 */
    private String image;

    /** 原价 */
    private BigDecimal originalPrice;

    /** 价格 */
    private BigDecimal price;

    /** 属性对应的库存 */
    private Integer stock;

    /** 重量 */
    private BigDecimal weight;

    /** 体积 */
    private BigDecimal volume;

}
