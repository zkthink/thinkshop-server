package com.think.cloud.thinkshop.mall.service.design;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerPageRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignRespVO;
import com.think.cloud.thinkshop.mall.controller.app.design.vo.AppIndexDesignVO;
import com.think.cloud.thinkshop.mall.controller.app.design.vo.AppIndexProductGroupInfoVO;
import com.think.cloud.thinkshop.mall.domain.design.PageConfig;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AppDesignService {
    /**
     * 客户端获取首页的配置
     */
    AppIndexDesignVO getIndexDesign(HttpServletRequest request);

    /**
     * 查询banner list
     * @param status
     * @return
     */
     List<BannerPageRespVO> selectBannerList(Integer status);

    /**
     * 查询页配置（页眉页脚）
     * @return
     */
    PageConfig selectPageConfig();

    /**
     * 获取图文视频
     * @return
     */
    List<IndexDesignRespVO> getImageTextVideo();

    /**
     * 获取首页的商品组
     */
    List<AppIndexProductGroupInfoVO> getIndexProductGroup();
}
