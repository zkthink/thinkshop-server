package com.think.cloud.thinkshop.mall.controller.app.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * 购物车优惠券VO
 */
@Data
public class CartCouponRespVO {

    @ApiModelProperty("优惠券明细id")
    private Long detailId;

    @ApiModelProperty("优惠券id")
    private Long id;

    @ApiModelProperty("优惠券名称")
    private String couponName;

    @ApiModelProperty("优惠券面值")
    private BigDecimal couponValue;

    @ApiModelProperty("优惠券类型：1、满减券，2、折扣券")
    private Integer couponType;

    @ApiModelProperty("满减门槛")
    private BigDecimal threshold;

    @ApiModelProperty("折扣")
    private BigDecimal discount;

    @ApiModelProperty("优惠券范围：1、所有商品，2、选中类型，3、选中商品")
    private Integer couponScope;


    @ApiModelProperty("范围值")
    private String scopeValues;

    @ApiModelProperty("生效时间")
    private Timestamp takingEffectTime;

    @ApiModelProperty("过期时间")
    private Timestamp expirationTime;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("领取时间")
    private Timestamp receiveTime;

    @ApiModelProperty("优惠金额")
    private BigDecimal discountAmount;

    @ApiModelProperty("购物车优惠券明细")
    private List<CartCouponDetailRespVO> detailVOList;


}
