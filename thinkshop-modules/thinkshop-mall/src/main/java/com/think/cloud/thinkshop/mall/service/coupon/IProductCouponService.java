package com.think.cloud.thinkshop.mall.service.coupon;

import java.util.List;

import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.*;
import com.think.cloud.thinkshop.mall.domain.coupon.ProductCoupon;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 商品优惠券Service接口
 *
 * @author zkthink
 * @date 2024-05-21
 */
public interface IProductCouponService {
    /**
     * 查询商品优惠券
     *
     * @param id 商品优惠券主键
     * @return 商品优惠券
     */
    public ProductCouponDetailRespVO selectProductCouponById(Long id);

    /**
     * 查询商品优惠券列表
     *
     * @param vo
     * @return 商品优惠券集合
     */
    public TableDataInfo selectProductCouponList(ProductCouponPageReqVO vo);

    /**
     * 新增商品优惠券
     *
     * @param vo
     * @return 结果
     */
    public Long insertProductCoupon(ProductCouponCreateReqVO vo);

    /**
     * 修改商品优惠券
     *
     * @param vo
     * @return 结果
     */
    public Long updateProductCoupon(ProductCouponUpdateReqVO vo);

    /**
     * 批量删除商品优惠券信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    /**
     * 结束优惠券活动
     *
     * @param id 主键
     * @return
     */
    public void end(Long id);

    /**
     * 自动开关
     *
     * @param id 主键
     * @return
     */
    public void autoSwitch(Long id);

    /**
     * 优惠券数据
     *
     * @param id
     * @return
     */
    public ProductCouponDataRespVO data(Long id);

    List<ProductCoupon> selectProductCouponByIds(List<Long> couponIds);
}
