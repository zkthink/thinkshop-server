package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 智能查找商品请求VO
 *
 * @author zkthink
 * @date 2024-05-11
 */
@Data
public class IntelligentSearchProductReqVO {

    /**
     * 筛选条件 0-全部满足 1-任意满足
     */
    @ApiModelProperty("筛选条件 0-全部满足 1-任意满足")
    private Integer screen;

    /**
     * 过滤条件集合
     */
    @ApiModelProperty("过滤条件集合")
    private List<IntelligentSearchFilterReqVO> filters;
}
