package com.think.cloud.thinkshop.mall.enums.product;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 商品规格类型枚举
 */
@Getter
@AllArgsConstructor
public enum ProductSkuTypeEnum {

    SINGLE(1, "单规格"),
    MULTI(2, "多规格");

    private Integer value;
    private String desc;

}
