package com.think.cloud.thinkshop.mall.controller.app.memberuser.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class RegisterDTO {
    @ApiModelProperty(value = "邮箱", required = true)
    private String email;
    @ApiModelProperty(value = "密码", required = true)
    private String password;
    @ApiModelProperty(value = "验证码", required = true)
    private String code;
    @ApiModelProperty(value = "名字", required = true)
    private String firstName;
    @ApiModelProperty(value = "姓氏", required = true)
    private String lastName;
}
