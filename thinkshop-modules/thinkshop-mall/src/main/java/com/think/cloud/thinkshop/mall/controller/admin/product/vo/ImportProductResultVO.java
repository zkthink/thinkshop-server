package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ImportProductResultVO {
    private Integer successful;
    private Integer failed;
    private List<String> failedProduct;
}
