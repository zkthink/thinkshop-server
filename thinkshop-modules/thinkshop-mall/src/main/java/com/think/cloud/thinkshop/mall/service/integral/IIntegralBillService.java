package com.think.cloud.thinkshop.mall.service.integral;

import com.think.cloud.thinkshop.mall.controller.admin.integral.param.IntegralBillSearchParam;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralBill;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;


/**
 * 积分流水Service接口
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
public interface IIntegralBillService
{
    /**
     * 查询积分流水
     *
     * @param id 积分流水主键
     * @return 积分流水
     */
    public IntegralBill selectIntegralBillById(Long id);

    /**
     * 查询积分流水列表
     *
     * @param integralBill 积分流水
     * @return 积分流水集合
     */
    public List<IntegralBill> selectIntegralBillList(IntegralBill integralBill);

    /**
     * 用户积分账单
     * @param userId
     * @return
     */
    public List<IntegralBill> userIntegralBillList(Long userId);
    /**
     * 记录分页查询
     * @param param
     * @return
     */
    public TableDataInfo page(IntegralBillSearchParam param);

    /**
     * 新增积分流水
     *
     * @param integralBill 积分流水
     * @return 结果
     */
    public int insertIntegralBill(IntegralBill integralBill);

    /**
     * 修改积分流水
     *
     * @param integralBill 积分流水
     * @return 结果
     */
    public int updateIntegralBill(IntegralBill integralBill);

    /**
     * 批量删除积分流水信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);
}
