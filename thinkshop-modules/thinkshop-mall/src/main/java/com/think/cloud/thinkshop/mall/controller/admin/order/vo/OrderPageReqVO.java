package com.think.cloud.thinkshop.mall.controller.admin.order.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

/**
 * 订单分页请求VO
 *
 * @author zkthink
 * @date 2024-05-29
 */
@Data
public class OrderPageReqVO {

    @ApiModelProperty("状态")
    private String status;

    @ApiModelProperty("订单编号")
    private String orderCode;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("开始时间")
    @DateTimeFormat
    private Timestamp startTime;

    @ApiModelProperty("结束时间")
    @DateTimeFormat
    private Timestamp endTime;
}
