package com.think.cloud.thinkshop.mall.service.coupon.impl;


import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.AppCouponDetailRespVO;
import com.think.cloud.thinkshop.mall.convert.coupon.ProductCouponConvert;
import com.think.cloud.thinkshop.mall.domain.coupon.ProductCoupon;
import com.think.cloud.thinkshop.mall.enums.coupon.CouponReceiveTypeEnum;
import com.think.cloud.thinkshop.mall.enums.coupon.CouponScopeEnum;
import com.think.cloud.thinkshop.mall.mapper.coupon.ProductCouponMapper;
import com.think.cloud.thinkshop.mall.service.coupon.AppProductCouponRelationService;
import com.think.cloud.thinkshop.mall.service.coupon.AppProductCouponService;
import com.think.cloud.thinkshop.mall.service.product.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 商品优惠券AppService业务层处理
 *
 * @author zkthink
 * @date 2024-05-22
 */
@Service
public class AppProductCouponServiceImpl implements AppProductCouponService {

    @Autowired
    private ProductCouponMapper productCouponMapper;
    @Autowired
    private AppProductCouponRelationService appProductCouponRelationService;


    @Override
    public List<AppCouponDetailRespVO> receiveList(Long productId, Long uid) {
        List<ProductCoupon> couponDOS = productCouponMapper.receiveList(productId);
        if (CollectionUtils.isEmpty(couponDOS)) return null;
        List<AppCouponDetailRespVO> couponDetailList = ProductCouponConvert.INSTANCE.convertList2(couponDOS);
        // 用户登录时 过滤已领取优惠券
        Predicate<AppCouponDetailRespVO> predicate = getPredicate(uid);

        couponDetailList = couponDetailList.stream()
                .filter(predicate)
                .sorted(Comparator.comparing(AppCouponDetailRespVO::getCouponType, Comparator.reverseOrder())
                ).collect(Collectors.toList());
        return couponDetailList;
    }

    @NotNull
    private Predicate<AppCouponDetailRespVO> getPredicate(Long uid) {
        Predicate<AppCouponDetailRespVO> predicate = item -> true;
        if (Objects.nonNull(uid)) {
            predicate = item -> {
                //过滤用户已经领取的优惠券
                if (CouponReceiveTypeEnum.LIMITATION.getValue().equals(item.getReceiveType())) {
                    long receivedCount = appProductCouponRelationService.getReceivedCount(item.getId(), uid);
                    return receivedCount < item.getLimitNumber();
                }
                return true;
            };
        }
        return predicate;
    }

    @Override
    public List<Long> getCouponProductList(Long id) {
        List<Long> productIdList = new ArrayList<>();
        ProductCoupon couponDO = productCouponMapper.selectById(id);
        if (CouponScopeEnum.PRODUCT.getValue().equals(couponDO.getCouponScope())) {
            String[] productIdArr = couponDO.getScopeValues().split(",");
            for (String s : productIdArr) productIdList.add(Long.parseLong(s));
        }
        return productIdList;
    }
}
