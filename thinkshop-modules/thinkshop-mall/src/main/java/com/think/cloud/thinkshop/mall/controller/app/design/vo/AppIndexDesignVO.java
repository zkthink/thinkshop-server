package com.think.cloud.thinkshop.mall.controller.app.design.vo;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerPageRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignRespVO;
import com.think.cloud.thinkshop.mall.domain.design.PageConfig;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "app  首页的页面配置")
@ToString(callSuper = true)
public class AppIndexDesignVO {
    @Schema(description = "banner", required = true, example = "")
    private List<BannerPageRespVO> banners;
    @Schema(description = "图文", required = true, example = "")
    private List<IndexDesignRespVO> imageAndTexts;
    @Schema(description = "视频", required = true, example = "")
    private List<IndexDesignRespVO> videos;
    @Schema(description = "页眉页脚", required = true, example = "")
    private PageConfig pageConfig;
    @Schema(description = "商品组", required = true, example = "")
    private List<AppIndexProductGroupInfoVO> productGroupInfo;
}
