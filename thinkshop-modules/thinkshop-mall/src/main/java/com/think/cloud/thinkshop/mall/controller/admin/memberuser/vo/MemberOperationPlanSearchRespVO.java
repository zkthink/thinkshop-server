package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponDetailRespVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.List;

@Data
@ApiModel(description = "用户运营计划 查询 RespVO")
@ToString(callSuper = true)
public class MemberOperationPlanSearchRespVO {
    @ApiModelProperty(value = "id", required = true, example = "")
    private Long id;

    @ApiModelProperty(value = "计划名称", required = true, example = "")
    private String name;

    @ApiModelProperty(value = "计划人群组id", required = true, example = "")
    private List<Long> memberGroupIds;

    @ApiModelProperty(value = "优惠券id配置", required = true, example = "")
    private List<Long> couponIds;

    @ApiModelProperty(value = "计划类型  ： 1：自动  ，2：手动", required = true, example = "")
    private Integer planType;

    @ApiModelProperty(value = "任务开时间，手动类型下的立刻执行不填", required = true, example = "")
    private Timestamp planStart;

    @ApiModelProperty(value = "任务结束时间，手动类型下不填", required = true, example = "")
    private Timestamp planEnd;

    @ApiModelProperty(value = "是否立刻执行", required = true, example = "")
    private Boolean atOnce;

    @ApiModelProperty(value = "计划人群组id", required = true, example = "")
    private List<MemberGroupDetailRespVO> memberGroupList;


    @ApiModelProperty(value = "优惠券id配置", required = true, example = "")
    private List<MemberOperationPlanRefCouponVO> couponList;

    /**
     * 状态  0：未开始 ，1进行中， 2：结束
     */
    @ApiModelProperty(value = "状态  0：未开始 ，1进行中， 2：结束", required = true, example = "")
    private Integer status;

    /**
     * 手动执行类型 1 立即执行 2 定时执行
     */
    private Integer manualExecutionType;
}
