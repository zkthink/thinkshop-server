package com.think.cloud.thinkshop.mall.domain.memberuser;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.util.Date;

/**
 * 客户分群对象 mall_member_group_user_ref
 *
 * @author guangxian
 * @date 2024-06-03
 */
@TableName("mall_member_group_user_ref")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberGroupRef {
    /** ID */
    private Long id;
    private Long memberGroupId;
    private Long userId;
    private Date joinTime;

}
