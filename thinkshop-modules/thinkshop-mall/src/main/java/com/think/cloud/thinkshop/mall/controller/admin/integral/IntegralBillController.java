package com.think.cloud.thinkshop.mall.controller.admin.integral;


import com.think.cloud.thinkshop.mall.controller.admin.integral.param.IntegralBillSearchParam;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralBillService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 积分流水Controller
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@RestController
@RequestMapping("/admin/integralBill")
@Api(tags="积分记录")
public class IntegralBillController extends BaseController
{
    @Autowired
    private IIntegralBillService integralBillService;

    /**
     * 查询积分流水列表
     */
    @RequiresPermissions("mall:integralBill:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询积分流水列表")
    public TableDataInfo list(IntegralBillSearchParam param) {
        startPage();
        return integralBillService.page(param);
    }
}
