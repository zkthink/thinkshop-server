package com.think.cloud.thinkshop.mall.domain;

import java.math.BigDecimal;
import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 商品属性值对象 mall_product_attr_value
 * 
 * @author zkthink
 * @date 2024-05-09
 */
@TableName("mall_product_attr_value")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductAttrValue
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type = IdType.AUTO)
    private Long skuId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

    /** 商品属性索引值 (attr_value|attr_value[|....]) */
    @Excel(name = "商品属性索引值 (attr_value|attr_value[|....])")
    private String sku;

    /** 图片 */
    @Excel(name = "图片")
    private String image;

    /** 原价 */
    @Excel(name = "原价")
    private BigDecimal originalPrice;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 属性对应的库存 */
    @Excel(name = "属性对应的库存")
    private Integer stock;

    /** 销量 */
    @Excel(name = "销量")
    private Integer sales;

    /** 重量 */
    @Excel(name = "重量")
    private BigDecimal weight;

    /** 体积 */
    @Excel(name = "体积")
    private BigDecimal volume;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

}
