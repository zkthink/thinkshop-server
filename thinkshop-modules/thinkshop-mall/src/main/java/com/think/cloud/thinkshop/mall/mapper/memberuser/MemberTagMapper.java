package com.think.cloud.thinkshop.mall.mapper.memberuser;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagQueryDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 会员标签Mapper接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
@Mapper
public interface MemberTagMapper extends BaseMapper<MemberTag> {

    /**
     * 查询会员标签列表
     *
     * @param dto 会员标签
     * @return 会员标签集合
     */
    List<MemberTag> selectMemberTagList(@Param("param") MemberTagQueryDTO dto);
}
