package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

@Data
@ApiModel("用户运营计划 查询列表 ReqVO")
@ToString(callSuper = true)
public class MemberOperationPlanSearchReqVO {
    @ApiModelProperty("计划名称")
    private String name;
    @ApiModelProperty("含有的组id")
    private Long group;
    @ApiModelProperty("计划类型")
    private Integer planType;
    @ApiModelProperty("计划开始时间")
    @DateTimeFormat
    private Timestamp startTime;
    @ApiModelProperty("计划结束时间")
    @DateTimeFormat
    private Timestamp endTime;
}
