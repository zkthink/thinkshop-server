package com.think.cloud.thinkshop.mall.service.productcomment;

import com.think.cloud.thinkshop.mall.controller.admin.productcomment.param.ProductCommentAddParam;
import com.think.cloud.thinkshop.mall.domain.ProductComment;
import com.think.common.core.web.page.TableDataInfo;

public interface AppProductCommentService {
    /**
     * 获取商品评论
     * @param id  商品id
     * @return
     */
    TableDataInfo getProductComments(Long id);

    int submitOrderComment(ProductCommentAddParam param);

    /**
     * 通过订单号查询评论记录
     * @param orderId
     * @return
     */
    ProductComment findCommentByOrderId(Long orderId);
}
