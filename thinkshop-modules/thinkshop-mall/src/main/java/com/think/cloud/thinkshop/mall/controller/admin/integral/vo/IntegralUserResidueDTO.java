package com.think.cloud.thinkshop.mall.controller.admin.integral.vo;

import lombok.Data;

@Data
public class IntegralUserResidueDTO {
    private Long userId;
    private Integer remain;
}
