package com.think.cloud.thinkshop.mall.controller.admin.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

/**
 * 订单分页响应VO
 *
 * @author zkthink
 * @date 2024-05-29
 */
@Data
public class OrderLogRespVO {

    @ApiModelProperty("订单日志id")
    private Long id;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户类型：1、用户；2、商户；3、系统")
    private Integer userType;

    @ApiModelProperty("用户类型名称")
    private String userTypeName;

    @ApiModelProperty("操作类型：1、提交订单；2、取消订单；3、超时自动取消订单；4、完成支付；5、商家发货；6、确认收货；7、超时自动确认收货")
    private Integer operateType;

    @ApiModelProperty("操作名称")
    private String operateTypeName;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;


}
