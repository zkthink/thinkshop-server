package com.think.cloud.thinkshop.mall.service.payment.paypal.config;

public enum PaypalPaymentIntent {
    sale, authorize, order
}
