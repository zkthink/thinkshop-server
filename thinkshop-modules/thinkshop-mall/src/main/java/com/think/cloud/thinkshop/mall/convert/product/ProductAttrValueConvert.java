package com.think.cloud.thinkshop.mall.convert.product;

import com.think.cloud.thinkshop.mall.controller.admin.product.vo.ProductSkuReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.product.vo.ProductSkuRespVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductSkuRespVO;
import com.think.cloud.thinkshop.mall.domain.ProductAttrValue;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 商品sku Convert
 *
 * @author zkthink
 */
@Mapper
public interface ProductAttrValueConvert {

    ProductAttrValueConvert INSTANCE = Mappers.getMapper(ProductAttrValueConvert.class);

    ProductAttrValue convert(ProductSkuReqVO bean);

    List<ProductAttrValue> convertList(List<ProductSkuReqVO> list);

    List<ProductSkuRespVO> convertList1(List<ProductAttrValue> list);

    AppProductSkuRespVO convert(ProductAttrValue bean);

    List<AppProductSkuRespVO> convertList2(List<ProductAttrValue> list);
}
