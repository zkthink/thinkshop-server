package com.think.cloud.thinkshop.mall.mapper.coupon;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.AppUserCouponRespVO;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.coupon.ProductCouponRelation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品优惠券关联Mapper接口
 *
 * @author zkthink
 * @date 2024-05-21
 */
@Mapper
public interface ProductCouponRelationMapper extends BaseMapper<ProductCouponRelation> {

    List<AppUserCouponRespVO> searchUserCoupon(@Param("type") Integer type, @Param("uid") Long uid, @Param("id") Long id);

    Integer getCouponNumber(@Param("userId") Long userId);

    Long getReceivePerson(@Param("couponId") Long couponId);

    Long getCouponUserCount(@Param("planId")Long planId, @Param("start")DateTime start, @Param("end") DateTime end);
}
