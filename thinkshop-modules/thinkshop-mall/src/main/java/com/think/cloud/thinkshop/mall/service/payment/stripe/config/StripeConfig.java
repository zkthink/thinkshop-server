package com.think.cloud.thinkshop.mall.service.payment.stripe.config;

import com.stripe.Stripe;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentConfig;
import com.think.cloud.thinkshop.mall.service.payment.IPaymentConfigService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Objects;

/**
 * @author zkthink
 * @apiNote
 **/
@Configuration
@Data
@Slf4j
public class StripeConfig {
    @Autowired
    private IPaymentConfigService paymentConfigService;
    /**
     * webhook秘钥签名
     */
    private String endpointSecret;

    @PostConstruct
    public void init() {
        PaymentConfig paymentConfig = paymentConfigService.selectPaymentConfigById(2L);
        if (Objects.isNull(paymentConfig)) {
            log.error("paypal 支付配置不存在");
        }
        endpointSecret = paymentConfig.getEndpointSecret();
        Stripe.apiKey = paymentConfig.getSecret();
        log.info("stripe 初始化成功");
    }
}
