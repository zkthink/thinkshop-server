package com.think.cloud.thinkshop.mall.service.design.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignRespVO;
import com.think.cloud.thinkshop.mall.convert.design.IndexDesignConvert;
import com.think.cloud.thinkshop.mall.domain.design.IndexDesign;
import com.think.cloud.thinkshop.mall.mapper.design.IndexDesignMapper;
import com.think.cloud.thinkshop.mall.service.design.IIndexDesignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexDesignServiceImpl implements IIndexDesignService {
    @Autowired
    private IndexDesignMapper indexDesignMapper;

    @Override
    public List<IndexDesignRespVO> getIndexDesign(Integer style) {
        List<IndexDesign> indexDesigns = indexDesignMapper.selectList(Wrappers.<IndexDesign>lambdaQuery()
                .eq(style == 0, IndexDesign::getType, 0)
                .ne(style != 0, IndexDesign::getType, 0)
        );
        return IndexDesignConvert.INSTANCE.convertList(indexDesigns);
    }

    @Override
    public int insetIndexDesign(IndexDesignAddReqVO vo) {
        IndexDesign convert = IndexDesignConvert.INSTANCE.convert(vo);
        return indexDesignMapper.insert(convert);
    }

    @Override
    public int delete(Long id) {
        return indexDesignMapper.deleteById(id);
    }

    @Override
    public int updateIndexDesign(IndexDesignEditReqVO vo) {
        return indexDesignMapper.updateById(IndexDesignConvert.INSTANCE.convert(vo));
    }

    @Override
    public IndexDesignRespVO selectById(Long id) {
        return IndexDesignConvert.INSTANCE.convert(indexDesignMapper.selectById(id));
    }
}
