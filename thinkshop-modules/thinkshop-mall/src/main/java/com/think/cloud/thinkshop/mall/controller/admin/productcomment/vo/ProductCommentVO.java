package com.think.cloud.thinkshop.mall.controller.admin.productcomment.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 商品评价对象 mall_product_comment
 *
 * @author moxiangrong
 * @date 2024-07-12
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("商品评论")
public class ProductCommentVO {
    @ApiModelProperty("id")
    private Long id;
    /**
     * 商品id
     */
    @JsonIgnore
    private Long productId;
    /**
     * 商品规格id
     */
    @JsonIgnore
    private Long skuId;
    @ApiModelProperty("商品名称")
    private String productName;
    @ApiModelProperty("商品图片")
    private String productImage;

    @ApiModelProperty("商品规格")
    private String productSku;

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("评论内容")
    private String comment;

    @ApiModelProperty("评分")
    private BigDecimal score;

    @ApiModelProperty("评论人")
    private String commentUser;

    @JsonIgnore
    private String imageUrl;
    @ApiModelProperty("评论图链接")
    private List<String> imageUrlSet;

    @ApiModelProperty("状态:0-不展示 ，1-展示")
    private Integer status;

    @ApiModelProperty("评论时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date commentTime;

}
