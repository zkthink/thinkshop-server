package com.think.cloud.thinkshop.mall.controller.admin.product.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GroundingProductReqVO {

    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private Long productId;

    /**
     * 类型
     */
    @ApiModelProperty("类型")
    private Integer type;
}
