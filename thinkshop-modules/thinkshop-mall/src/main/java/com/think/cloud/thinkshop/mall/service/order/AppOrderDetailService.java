package com.think.cloud.thinkshop.mall.service.order;


import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderCartRespVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderDetailPageRespVO;
import com.think.cloud.thinkshop.mall.domain.order.OrderDetail;

import java.util.List;

/**
 * 订单明细AppService接口
 *
 * @author zkthink
 * @date 2024-05-27
 */
public interface AppOrderDetailService {

    public void batchInsert(List<AppOrderCartRespVO> carts, Long orderId);

    public List<AppOrderDetailPageRespVO> getOrderDetail(List<Long> orderIds);

    /**
     * 通过订单id查询订单明细
     *
     * @param orderId
     * @return
     */
    public List<OrderDetail> getOrderDetailByOrderId(Long orderId);

}
