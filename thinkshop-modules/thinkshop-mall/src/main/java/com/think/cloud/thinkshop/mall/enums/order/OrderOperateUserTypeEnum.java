package com.think.cloud.thinkshop.mall.enums.order;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 订单操作用户类型枚举
 */
@Getter
@AllArgsConstructor
public enum OrderOperateUserTypeEnum {

    USER(1, "用户"),
    MERCHANT(2, "商户"),
    SYSTEM(3, "系统");

    private Integer value;
    private String desc;


    public static OrderOperateUserTypeEnum toEnum(Integer value) {
        return Stream.of(OrderOperateUserTypeEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
