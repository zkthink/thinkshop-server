package com.think.cloud.thinkshop.mall.convert.payment;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberUserVO;
import com.think.cloud.thinkshop.mall.controller.app.payment.vo.PaymentConfigVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberUser;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentConfig;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Mapper
public interface PaymentConfigConvert {
    PaymentConfigConvert INSTANCE = Mappers.getMapper(PaymentConfigConvert.class);

    PaymentConfigVO convert(PaymentConfig bean);

    List<PaymentConfigVO> convertList(List<PaymentConfig> list);
}
