package com.think.cloud.thinkshop.mall.controller.app.product;


import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductPageReqVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppRecommendProductPageReqVO;
import com.think.cloud.thinkshop.mall.service.product.AppProductService;
import com.think.cloud.thinkshop.mall.service.productcomment.AppProductCommentService;
import com.think.cloud.thinkshop.mall.service.productgroup.IProductGroupService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品AppController
 *
 * @author zkthink
 * @date 2024-05-09
 */
@RestController
@RequestMapping("/app/product")
@Api(tags = "APP:商品")
public class AppProductController extends BaseController {
    @Autowired
    private AppProductService appProductService;

    @Autowired
    private IProductGroupService iProductGroupService;
    @Autowired
    private AppProductCommentService appProductCommentService;

    /**
     * 查询商品列表
     */
    @GetMapping("/list")
    public TableDataInfo list(AppProductPageReqVO vo) {
        startPage();
        return appProductService.selectProductList(vo);
    }

    /**
     * 获取商品详细信息
     */
    @GetMapping(value = "/get/{productId}")
    public AjaxResult getProductDetail(@PathVariable("productId") Long productId) {
        return success(appProductService.getProductDetail(productId));
    }

    /**
     * 推荐好物
     */
    @GetMapping("/recommend-list")
    public TableDataInfo recommendList(AppRecommendProductPageReqVO vo) {
        return getDataTable(appProductService.recommendList(vo));
    }

    /**
     * 查询商品分组信息
     */
    @GetMapping("/group/get/{groupId}")
    public AjaxResult getGroupInfo(@PathVariable("groupId") Long groupId) {
        return success(iProductGroupService.selectProductGroupByGroupId(groupId));
    }

    @GetMapping("/comments/{id}")
    @ApiOperation(value = "商品评价列表")
    public TableDataInfo getProductComments(@ApiParam("商品id") @PathVariable Long id) {
        startPage();
        return appProductCommentService.getProductComments(id);
    }

}
