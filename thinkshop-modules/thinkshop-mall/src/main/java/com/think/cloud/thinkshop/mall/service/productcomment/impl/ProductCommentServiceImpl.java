package com.think.cloud.thinkshop.mall.service.productcomment.impl;


import cn.hutool.core.util.DesensitizedUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.think.cloud.thinkshop.mall.controller.admin.productcomment.param.ProductCommentAddParam;
import com.think.cloud.thinkshop.mall.controller.admin.productcomment.vo.ProductCommentVO;
import com.think.cloud.thinkshop.mall.convert.productcomment.ProductCommentConvert;
import com.think.cloud.thinkshop.mall.domain.Product;
import com.think.cloud.thinkshop.mall.domain.ProductComment;
import com.think.cloud.thinkshop.mall.mapper.ProductCommentMapper;
import com.think.cloud.thinkshop.mall.service.product.IProductAttrValueService;
import com.think.cloud.thinkshop.mall.service.product.IProductService;
import com.think.cloud.thinkshop.mall.service.productcomment.IProductCommentService;
import com.think.common.core.utils.PageUtils;
import com.think.common.core.web.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品评价Service业务层处理
 *
 * @author moxiangrong
 * @date 2024-07-12
 */
@Service
public class ProductCommentServiceImpl implements IProductCommentService {
    @Autowired
    private ProductCommentMapper productCommentMapper;
    @Autowired
    private IProductService productService;
    @Autowired
    private IProductAttrValueService productAttrValueService;

    /**
     * 查询商品评价
     *
     * @param id 商品评价主键
     * @return 商品评价
     */
    @Override
    public ProductComment selectProductCommentById(Long id) {
        return productCommentMapper.selectById(id);
    }

    /**
     * 查询商品评价列表
     *
     * @return 商品评价
     */
    @Override
    public TableDataInfo selectProductCommentList() {
        List<ProductComment> list = productCommentMapper.selectList(
                new LambdaQueryWrapper<ProductComment>()
//                        .orderByDesc(ProductComment::getCommentTime)
                        .orderByDesc(ProductComment::getId)
        );
        List<ProductCommentVO> vos = viewProductCommentList(list, false, true, true);
        return new TableDataInfo(vos, PageUtils.getTotal(list));
    }

    @Override
    public List<ProductCommentVO> viewProductCommentList(List<ProductComment> list, boolean userHidden, boolean showProduct, boolean showOrder) {
        List<ProductCommentVO> result = ProductCommentConvert.INSTANCE.convertList(list);
        //填充产品详情
        result = result.stream().peek(comment -> {
            String imageUrl = comment.getImageUrl();
            if (imageUrl != null && !imageUrl.isEmpty()) {
                comment.setImageUrlSet(Arrays.asList(imageUrl.split(",")));
            }
            if (showProduct) {
                Product product = productService.findHistoryProductById(comment.getProductId());
                String productName = product.getProductName();
                String image = product.getImage();
                comment.setProductName(productName);
                comment.setProductImage(image != null && !image.isEmpty() ? image.split(",")[0] : "");
            }
            String sku = productAttrValueService.findProductAttrValueBySkuId(comment.getSkuId()).getSku();
            comment.setProductSku(sku);
            if (userHidden) {
                comment.setCommentUser(desensitized(comment.getCommentUser()));
            }
            if (!showOrder) {
                comment.setOrderId(null);
            }
        }).collect(Collectors.toList());
        return result;
    }

    /**
     * 邮箱脱敏
     *
     * @param name
     * @return
     */
    public static String desensitized(String name) {
       return DesensitizedUtil.email(name);
//        if (name == null || name.isEmpty()) {
//            return "****";
//        }
//        if (name.length() <= 2) {
//            return name.substring(0, 1) + "***";
//        }
//        return name.substring(0, 1) + "***" + name.substring(name.length() - 1);
    }

    /**
     * 新增商品评价
     *
     * @param param 商品评价
     * @return 结果
     */
    @Override
    public int insertProductComment(ProductCommentAddParam param) {
        ProductComment convert = ProductCommentConvert.INSTANCE.convert(param);
        if (convert.getComment() == null) {
            convert.setComment("");
        }
        List<String> imageUrls = param.getImageUrls();
        if (imageUrls != null && !imageUrls.isEmpty()) {
            String imageSets = String.join(",", imageUrls);
            convert.setImageUrl(imageSets);
        } else {
            convert.setImageUrl("");
        }
        return productCommentMapper.insert(convert);
    }

    /**
     * 修改商品评价
     *
     * @param productComment 商品评价
     * @return 结果
     */
    @Override
    public int updateProductComment(ProductComment productComment) {
        return productCommentMapper.updateById(productComment);
    }

    @Override
    public int updateProductCommentStatus(Long id, Integer status) {
        ProductComment productComment = new ProductComment();
        productComment.setId(id);
        productComment.setStatus(status);
        return this.updateProductComment(productComment);
    }


    /**
     * 批量删除商品评价信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return productCommentMapper.deleteBatchIds(ids);
    }
}
