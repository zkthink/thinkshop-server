package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * 商品修改请求VO
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Data
public class UpdateProductReqVO {

    /**
     * 商品id
     */
    @NotNull(message = "商品id不能为空")
    private Long productId;

    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不能为空")
    private String productName;

    /** 简介 */
    private String introduce;

    /**
     * 商品图片
     */
    @NotBlank(message = "商品图片必传")
    private String image;

    /**
     * 视频
     */
    private String video;

    /**
     * 商品详情
     */
    private String detail;

    /**
     * 分类id
     */
    private Long categoryId;


    /**
     * 分组id集合
     */
    private List<Long> groupIds;


    /**
     * 是否收税 0否 1是
     */
    private Integer tax;

    /**
     * 状态（0：未上架，1：上架）
     */
    @NotNull(message = "上架状态不能为空")
    private Integer isShow;

    private String updateBy;
    /**
     * 商品规格类型  1：单规格 2：多规格
     */
    private Integer skuType;

    /**
     * 属性集合
     */
    private List<ProductAttrReqVO> attrs;

    /**
     * sku集合
     */
    private List<ProductSkuReqVO> skus;

}
