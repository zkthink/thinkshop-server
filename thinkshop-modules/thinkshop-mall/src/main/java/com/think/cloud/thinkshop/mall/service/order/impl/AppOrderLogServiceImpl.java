package com.think.cloud.thinkshop.mall.service.order.impl;


import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderLogRespVO;
import com.think.cloud.thinkshop.mall.convert.order.OrderConvert;
import com.think.cloud.thinkshop.mall.domain.order.OrderLog;
import com.think.cloud.thinkshop.mall.mapper.order.OrderLogMapper;
import com.think.cloud.thinkshop.mall.service.order.AppOrderLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 订单日志AppService业务层处理
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Service
public class AppOrderLogServiceImpl implements AppOrderLogService {
    @Autowired
    private OrderLogMapper orderLogMapper;


    /**
     * 新增订单日志
     *
     * @param orderLog 订单日志
     */
    @Override
    public void insertOrderLog(OrderLog orderLog) {
        orderLogMapper.insert(orderLog);
    }

    @Override
    public List<AppOrderLogRespVO> getOrderLogList(Long orderId) {
        List<OrderLog> orderLogs = orderLogMapper.getOrderLogListByOrderId(orderId);
        return OrderConvert.INSTANCE.convertLogList1(orderLogs);
    }
}
