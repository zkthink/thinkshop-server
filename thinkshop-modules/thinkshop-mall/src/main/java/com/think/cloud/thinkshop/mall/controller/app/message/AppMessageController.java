package com.think.cloud.thinkshop.mall.controller.app.message;

import com.think.cloud.thinkshop.mall.service.message.IMessageUserRecordService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/message")
@Api(tags = "APP: 站内信")
public class AppMessageController extends BaseController {
    @Autowired
    private IMessageUserRecordService messageUserRecordService;

    @GetMapping("/list")
    @ApiOperation(value = "站内信列表")
    public TableDataInfo list() {
        startPage();
        return getDataTable(messageUserRecordService.listMessageUserRecord(SecurityUtils.getUserId()));
    }

    @GetMapping("/get/{id}")
    @ApiOperation(value = "站内信详情")
    public AjaxResult get(@PathVariable Long id) {
        return success(messageUserRecordService.selectMessageUserRecordById(id,SecurityUtils.getUserId()));
    }


}
