package com.think.cloud.thinkshop.mall.service.websitesetting;

import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto.WebsiteSettingDTO;
import com.think.cloud.thinkshop.mall.domain.websitesetting.WebsiteSetting;

import java.util.Date;
import java.util.List;

/**
 * 网站设置Service接口
 *
 * @author zkthink
 * @date 2024-05-14
 */
public interface IWebsiteSettingService
{
    /**
     * 查询网站设置
     *
     * @param id 网站设置主键
     * @return 网站设置
     */
    public WebsiteSetting selectWebsiteSettingById(Long id);

    /**
     * 查询网站设置列表
     *
     * @param websiteSetting 网站设置
     * @return 网站设置集合
     */
    public List<WebsiteSetting> selectWebsiteSettingList(WebsiteSetting websiteSetting);

    /**
     * 新增网站设置
     *
     * @param websiteSetting 网站设置
     * @return 结果
     */
    public int insertWebsiteSetting(WebsiteSetting websiteSetting);

    /**
     * 修改网站设置
     *
     * @param websiteSetting 网站设置
     * @return 结果
     */
    public int updateWebsiteSetting(WebsiteSetting websiteSetting);

    /**
     * 批量删除网站设置
     *
     * @param ids 需要删除的网站设置主键集合
     * @return 结果
     */
    public int deleteWebsiteSettingByIds(Long[] ids);

    /**
     * 删除网站设置信息
     *
     * @param id 网站设置主键
     * @return 结果
     */
    public int deleteWebsiteSettingById(Long id);

    WebsiteSetting getCache();

    List<String> refundOnlyReason();

    List<String> returnGoodsRefund();

    void saveRefundOnlyReason(List<String> reasons);

    void saveReturnGoodsRefund(List<String> reasons);

    WebsiteSettingDTO getInfo();

    int save(WebsiteSettingDTO websiteSetting);

    /**
     * 获取时区、时间相关的信息
     * @return
     */
    String getZoneTimeInfo();

    /**
     * 获取数据库时间
     * @return
     */
    Date getDataBaseNow();

    /**
     * 获取系统时区 和 配置时区 时间差
     * @return
     */
    int zoneTimeGap();
}
