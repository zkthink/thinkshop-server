package com.think.cloud.thinkshop.mall.service.design;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexDesignRespVO;

import java.util.List;

public interface IIndexDesignService {
    /**
     * 查询 图文 or 视频的配置列表
     *
     * @param style 0：视频  1：图文
     * @return
     */
    List<IndexDesignRespVO> getIndexDesign(Integer style);

    /**
     * 新增 图文、视频
     *
     * @param vo
     * @return
     */
    int insetIndexDesign(IndexDesignAddReqVO vo);

    /**
     * 通过id删除记录
     *
     * @param id
     * @return
     */
    int delete(Long id);

    /**
     * 编辑图文、视频
     *
     * @param vo
     * @return
     */
    int updateIndexDesign(IndexDesignEditReqVO vo);

    /**
     * 通过id查询文图，视频配置
     *
     * @param id
     * @return
     */
    IndexDesignRespVO selectById(Long id);
}
