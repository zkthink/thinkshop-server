package com.think.cloud.thinkshop.mall.controller.app.payment.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class RefundDTO {
    private String orderCode;
    private BigDecimal total;
    private String currency;

    private String paymentIntent;
    /**
     * stripe支付信息(用于退款)
     */
    private String latestCharge;
}
