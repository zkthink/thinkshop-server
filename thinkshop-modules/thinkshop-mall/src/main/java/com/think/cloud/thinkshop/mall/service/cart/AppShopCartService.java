package com.think.cloud.thinkshop.mall.service.cart;


import com.think.cloud.thinkshop.mall.controller.app.cart.vo.AddShopCartReqVO;
import com.think.cloud.thinkshop.mall.controller.app.cart.vo.MyCartRespVO;
import com.think.cloud.thinkshop.mall.controller.app.cart.vo.UpdateShopCartReqVO;
import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.CartCouponRespVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderCartRespVO;
import com.think.cloud.thinkshop.mall.domain.ShopCart;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;

/**
 * 购物车Service接口
 *
 * @author zkthink
 * @date 2024-05-17
 */
public interface AppShopCartService {

    /**
     * 添加购物车
     *
     * @param vo
     */
    public Long addCart(AddShopCartReqVO vo);

    /**
     * 编辑购物车
     *
     * @param vo
     */
    public void updateCart(UpdateShopCartReqVO vo);


    /**
     * 获取购物车列表
     *
     * @param userId
     * @return
     */
    public TableDataInfo getMyCart(Long userId);

    /**
     * 删除购物车
     *
     * @param cartIds
     * @return
     */
    public void deleteCart(List<Long> cartIds);

    /**
     * 获取订单购物车信息
     *
     * @return
     */
    public List<AppOrderCartRespVO> getOrderCartInfo(List<Long> cartIds);

    /**
     * 购物车购买
     *
     * @param cartIds
     * @param isPay
     */
    public void cartsPay(List<Long> cartIds, Integer isPay);

    /**
     * 查询购物车列表
     *
     * @return
     */
    public List<ShopCart> selectShopCartByCartIds(List<Long> cartIds);

    /**
     * 批量保存
     *
     * @return
     */
    public void batchInsert(List<ShopCart> list);

    /**
     * 查询购物车可用优惠券
     *
     * @param cartIds
     * @return
     */
    List<CartCouponRespVO> searchCartAvailableCoupon(String cartIds, Long userId);
}
