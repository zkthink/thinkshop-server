package com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;

/**
 * 售后单审核请求VO
 *
 * @author zkthink
 * @date 2024-06-17
 */
@Data
public class AfterSalesAuditReqVO {

    @ApiModelProperty("售后id")
    private Long id;

    @ApiModelProperty("意见")
    private Integer isAgree;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("登录用户id")
    private Long userId;
}
