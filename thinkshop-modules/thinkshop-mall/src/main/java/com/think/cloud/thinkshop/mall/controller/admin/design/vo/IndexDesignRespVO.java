package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;
@Data
@ToString(callSuper = true)
@Schema(description = "管理后台 - 图文、视频查询VO")
public class IndexDesignRespVO {
    private Long id;
    @Schema(description = "资源地址", required = true, example = "http://xxxxxx")
    private String resourceUrl;

    @Schema(description = "标题", required = true, example = "title")
    private String title;

    @Schema(description = "简介", required = true, example = "xxxx")
    private String introduction;

    @Schema(description = "链接", required = true, example = "http://xxxxx")
    private String redirectUrl;

    @Schema(description = "类型，1：左图右文，2：左文右图，视频：0", required = true, example = "1")
    private Integer type;

    @Schema(description = "状态：1 可用，0：不可用", required = true, example = "1")
    private Integer status;
}
