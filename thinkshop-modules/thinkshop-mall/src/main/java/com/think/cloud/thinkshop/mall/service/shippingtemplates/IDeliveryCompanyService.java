package com.think.cloud.thinkshop.mall.service.shippingtemplates;

import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.vo.DeliveryCompanySimpleVO;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.DeliveryCompany;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
/**
 * 快递公司Service接口
 *
 * @author zkthink
 * @date 2024-06-05
 */
public interface IDeliveryCompanyService
{
    /**
     * 查询快递公司
     *
     * @param id 快递公司主键
     * @return 快递公司
     */
    public DeliveryCompany selectDeliveryCompanyById(Long id);

    /**
     * 查询快递公司列表
     *
     * @param deliveryCompany 快递公司
     * @return 快递公司集合
     */
    public List<DeliveryCompany> selectDeliveryCompanyList(DeliveryCompany deliveryCompany);

    /**
     * 新增快递公司
     *
     * @param deliveryCompany 快递公司
     * @return 结果
     */
    public int insertDeliveryCompany(DeliveryCompany deliveryCompany);

    /**
     * 修改快递公司
     *
     * @param deliveryCompany 快递公司
     * @return 结果
     */
    public int updateDeliveryCompany(DeliveryCompany deliveryCompany);

    /**
     * 批量删除快递公司信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    /**
     * 获取快递公司列表
     *
     * @return
     */
    List<DeliveryCompanySimpleVO> simpleList();

    /**
     * 导入快递公司
     *
     * @param file
     */
    void importData(MultipartFile file);
}
