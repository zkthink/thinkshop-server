package com.think.cloud.thinkshop.mall.domain.uv;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@TableName("mall_uv_record")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UVRecord {
    private Long id;
    private String ip;
    private String recordDay;
    private String createTime;
}
