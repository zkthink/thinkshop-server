package com.think.cloud.thinkshop.mall.service.shippingtemplates;

import java.math.BigDecimal;
import java.util.List;

import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.dto.FreightComputeDTO;
import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.dto.ShippingTemplatesDTO;
import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.vo.ShippingTemplatesVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderCommonReqVO;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.ShippingTemplates;

/**
 * 物流方案Service接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
public interface IShippingTemplatesService
{
    /**
     * 查询物流方案
     *
     * @param id 物流方案主键
     * @return 物流方案
     */
    public ShippingTemplatesVO selectShippingTemplatesById(Long id);

    /**
     * 查询物流方案列表
     *
     * @param shippingTemplates 物流方案
     * @return 物流方案集合
     */
    public List<ShippingTemplates> selectShippingTemplatesList(ShippingTemplates shippingTemplates);

    /**
     * 新增物流方案
     *
     * @param shippingTemplates 物流方案
     * @return 结果
     */
    public int insertShippingTemplates(ShippingTemplatesDTO shippingTemplates);

    /**
     * 修改物流方案
     *
     * @param shippingTemplates 物流方案
     * @return 结果
     */
    public int updateShippingTemplates(ShippingTemplatesDTO shippingTemplates);

    /**
     * 批量删除物流方案信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    int deleteById(Long id);

    BigDecimal calculatePrice(FreightComputeDTO dto, AppOrderCommonReqVO vo);
}
