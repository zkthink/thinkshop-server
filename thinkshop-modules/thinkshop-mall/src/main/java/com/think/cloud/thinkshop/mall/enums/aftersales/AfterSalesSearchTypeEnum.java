package com.think.cloud.thinkshop.mall.enums.aftersales;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 售后查询类型枚举
 */
@Getter
@AllArgsConstructor
public enum AfterSalesSearchTypeEnum {

    ALL(1, "所有"),
    IN_PROGRESS(2, "售后中"),
    COMPLETE(3, "完成");

    private Integer value;
    private String desc;


    public static AfterSalesSearchTypeEnum toEnum(Integer value) {
        return Stream.of(AfterSalesSearchTypeEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
