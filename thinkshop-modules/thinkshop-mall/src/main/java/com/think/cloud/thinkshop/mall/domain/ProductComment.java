package com.think.cloud.thinkshop.mall.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 商品评价对象 mall_product_comment
 *
 * @author moxiangrong
 * @date 2024-07-12
 */
@TableName("mall_product_comment")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductComment {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 商品id
     */
    @Excel(name = "商品id")
    private Long productId;

    /**
     * 商品规格id
     */
    @Excel(name = "商品规格id")
    private Long skuId;

    /**
     * 订单id
     */
    @Excel(name = "订单id")
    private Long orderId;

    /**
     * 评论内容
     */
    @Excel(name = "评论内容")
    private String comment;

    /**
     * 评分
     */
    @Excel(name = "评分 ")
    private BigDecimal score;

    /**
     * 评论人
     */
    @Excel(name = "评论人")
    private String commentUser;

    /**
     * 评论图链接
     */
    @Excel(name = "评论图链接")
    private String imageUrl;

    /**
     * 状态:0-不展示 ，1-展示
     */
    @Excel(name = "状态:0-不展示 ，1-展示")
    private Integer status;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer deleted;
    /**
     *  评论时间
     */
    private Date commentTime;

}
