package com.think.cloud.thinkshop.mall.domain.memberuser;

import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import lombok.*;

/**
 * 客户分群对象 mall_member_group
 *
 * @author guangxian
 * @date 2024-06-03
 */
@TableName("mall_member_group")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberGroup {
    /** ID */
    private Long id;

    /** 人群名称 */
    @Excel(name = "人群名称")
    private String name;

    /** 人群定义 */
    @Excel(name = "人群定义")
    private String ruleInfo;

    /** 人群数量 */
    @Excel(name = "人群数量")
    private Long userCount;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

}
