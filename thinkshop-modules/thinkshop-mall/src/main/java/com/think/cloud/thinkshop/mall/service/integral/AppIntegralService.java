package com.think.cloud.thinkshop.mall.service.integral;

import com.think.cloud.thinkshop.mall.domain.integral.IntegralBill;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralRule;
import com.think.cloud.thinkshop.mall.enums.integral.IntegralBillTypeEnum;

import java.util.List;

public interface AppIntegralService {
    /**
     * 获取用户积分
     * @param userId
     * @return
     */
    Integer getUserIntegral(Long userId);

    /**
     * 积分账单
     * @param userId
     * @return
     */
    List<IntegralBill> getUserIntegralBill(Long userId);

    /**
     * 获取积分规则
     * @return
     */
    String getRuleInfo();

    /**
     * 获取积分规则缓存
     * @return
     */
    IntegralRule getRuleCache();

    /**
     * 扣减积分
     * @param userId
     * @param num   扣减值，需要大于0
     * @return
     */
    Boolean reduceUserIntegral(Long userId,int num);

    /**
     * 增加积分详情
     * @return
     */
    public int insertIntegralDetail(Long userId, Integer integral, IntegralBillTypeEnum billTypeEnum);
}
