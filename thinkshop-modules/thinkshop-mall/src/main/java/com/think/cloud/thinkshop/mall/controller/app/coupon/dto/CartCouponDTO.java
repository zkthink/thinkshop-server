package com.think.cloud.thinkshop.mall.controller.app.coupon.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zkthink
 * @Date 2024/05/22
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartCouponDTO implements Serializable {

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 规格id
     */
    private Long skuId;

    /**
     * 商品价格
     */
    private BigDecimal price;

}
