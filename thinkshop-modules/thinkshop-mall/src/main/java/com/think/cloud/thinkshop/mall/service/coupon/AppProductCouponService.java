package com.think.cloud.thinkshop.mall.service.coupon;


import com.think.cloud.thinkshop.mall.controller.app.coupon.vo.AppCouponDetailRespVO;

import java.util.List;

/**
 * 商品优惠券AppService接口
 *
 * @author zkthink
 * @date 2024-05-22
 */
public interface AppProductCouponService {

    /**
     * 获取商品可领优惠券列表
     *
     * @param productId 商品id
     * @param uid       用户id
     * @return
     */
    List<AppCouponDetailRespVO> receiveList(Long productId, Long uid);

    /**
     * 获取优惠券可用商品列表
     *
     * @param id 优惠券id
     * @return
     */
    List<Long> getCouponProductList(Long id);

}
