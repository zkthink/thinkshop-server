package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagRefDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class MemberUserVO {
    /** ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /** 账号 */
    @ApiModelProperty(value = "账号")
    private String username;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /** 密码 */
    @ApiModelProperty(value = "密码")
    private String password;

    /** first value */
    @ApiModelProperty(value = "first value")
    private String firstName;

    /** last value */
    @ApiModelProperty(value = "last value")
    private String lastName;

    /** 头像 */
    @ApiModelProperty(value = "头像")
    private String avatar;

    /** 注册来源 */
    @ApiModelProperty(value = "注册来源")
    private String source;

    /** 三方用户id */
    @ApiModelProperty(value = "三方用户id")
    private String thirdUid;

    /** 帐号状态（0正常 1停用） */
    @ApiModelProperty(value = "帐号状态")
    private Integer status;

    /** 手机号码 */
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;

    /** 是否删除 */
    @ApiModelProperty(value = "是否删除")
    private Integer deleted;

    /** 消费次数 */
    @ApiModelProperty(value = "消费次数")
    private Integer orderCount;

    /** 累计消费金额 */
    @ApiModelProperty(value = "累计消费金额")
    private BigDecimal totalOrderAmount;

    /** 上次消费时间 */
    @ApiModelProperty(value = "上次消费时间")
    private Timestamp lastConsumeTime;

    /** 累计退款订单数 */
    @ApiModelProperty(value = "累计退款订单数")
    private Integer refundCount;

    /** 累计退款金额 */
    @ApiModelProperty(value = "累计退款金额")
    private BigDecimal totalRefundAmount;

    @ApiModelProperty(value = "标签列表")
    private List<MemberTagRefDTO> tagList;

    @ApiModelProperty(value = "创建时间")
    private Timestamp createTime;

    @ApiModelProperty(value = "更新时间")
    private Timestamp updateTime;

    @ApiModelProperty(value = "用户积分")
    private Integer integral;
}
