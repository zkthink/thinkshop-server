package com.think.cloud.thinkshop.mall.convert.product;

import com.think.cloud.thinkshop.mall.controller.admin.product.vo.ProductAttrReqVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductAttrRespVO;
import com.think.cloud.thinkshop.mall.domain.ProductAttr;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 商品 Convert
 *
 * @author zkthink
 */
@Mapper
public interface ProductAttrConvert {

    ProductAttrConvert INSTANCE = Mappers.getMapper(ProductAttrConvert.class);

    ProductAttr convert(ProductAttrReqVO bean);

    List<AppProductAttrRespVO> convertList(List<ProductAttr> list);


}
