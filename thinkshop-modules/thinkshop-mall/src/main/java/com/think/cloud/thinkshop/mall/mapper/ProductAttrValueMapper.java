package com.think.cloud.thinkshop.mall.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.ProductAttrValue;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 商品属性值Mapper接口
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Mapper
public interface ProductAttrValueMapper extends BaseMapper<ProductAttrValue> {

    void batchInsertProductAttrValue(List<ProductAttrValue> list);

    default List<ProductAttrValue> selectList(ProductAttrValue productAttrValue) {
        return selectList(new LambdaQueryWrapper<ProductAttrValue>()
                .eq(ProductAttrValue::getProductId, productAttrValue.getProductId())
                .orderByAsc(ProductAttrValue::getSkuId));
    }


    List<ProductAttrValue> selectListWithDeleted(@Param("skuIds") List<Long> skuIds);

    @Update("update mall_product_attr_value set stock = stock + #{num},sales  = sales - #{num} where sku_id = #{skuId}")
    void addStack(@Param("skuId") Long skuId, @Param("num") Integer num);

    @Update("update mall_product_attr_value set stock = stock - #{num},sales  = sales + #{num} where sku_id = #{skuId}")
    void subStack(@Param("skuId") Long skuId, @Param("num") Integer num);

    @Select("select * from mall_product_attr_value where sku_id = #{skuId}")
    ProductAttrValue findProductAttrValueBySkuId(@Param("skuId") Long skuId);
}
