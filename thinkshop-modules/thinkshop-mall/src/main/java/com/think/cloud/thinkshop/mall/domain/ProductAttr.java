package com.think.cloud.thinkshop.mall.domain;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 商品属性对象 mall_product_attr
 * 
 * @author zkthink
 * @date 2024-05-09
 */
@TableName("mall_product_attr")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductAttr
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(type = IdType.AUTO)
    private Long attrId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

    /** 属性名 */
    @Excel(name = "属性名")
    private String attrName;

    /** 属性值 */
    @Excel(name = "属性值")
    private String attrValues;

}
