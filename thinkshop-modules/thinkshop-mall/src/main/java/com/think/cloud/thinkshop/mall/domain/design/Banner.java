package com.think.cloud.thinkshop.mall.domain.design;

import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import lombok.*;

/**
 * Banner对象 mall_banner
 *
 * @author zkthink
 * @date 2024-05-27
 */
@TableName("mall_banner")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Banner {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 图片地址
     */
    @Excel(name = "图片地址")
    private String imageUrl;

    /**
     * 重定向地址
     */
    @Excel(name = "重定向地址")
    private String redirectUrl;

    /**
     * 状态
     */
    @Excel(name = "状态")
    private Integer status;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer deleted;

}
