package com.think.cloud.thinkshop.mall.controller.admin.integral.param;

import lombok.Data;

@Data
public class IntegralBillSearchParam {
    /**
     * 查询时间范围
     */
    private String startTime;
    private String endTime;
    /**
     * 类型 1：增加 2：消费 3：过期
     */
    private Integer type;
    /**
     * 邮箱
     */
    private String email;
}
