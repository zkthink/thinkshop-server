package com.think.cloud.thinkshop.mall.controller.admin.order;


import cn.hutool.json.JSONArray;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderDeliveryReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderPageRespVO;
import com.think.cloud.thinkshop.mall.service.order.IOrderService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.security.annotation.RequiresPermissions;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单Controller
 *
 * @author zkthink
 * @date 2024-05-23
 */
@RestController
@RequestMapping("/admin/order")
@Api(tags = "ADMIN:订单")
public class OrderController extends BaseController {

    @Autowired
    private IOrderService orderService;

    /**
     * 查询订单列表
     */
    @RequiresPermissions("mall:order:list")
    @GetMapping("/list")
    @ApiOperation(value = "查询订单列表")
    public TableDataInfo list(OrderPageReqVO vo) {
        startPage();
        List<OrderPageRespVO> list = orderService.selectOrderList(vo);
        return getDataTable(list);
    }

    /**
     * 获取订单详细信息
     */
    @RequiresPermissions("mall:order:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation(value = "获取订单详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(orderService.selectOrderById(id));
    }


    /**
     * 发货
     */
    @RequiresPermissions("mall:order:delivery")
    @PostMapping("/delivery")
    @ApiOperation(value = "发货")
    public AjaxResult delivery(@RequestBody OrderDeliveryReqVO vo) {
        orderService.delivery(vo, SecurityUtils.getLoginUser());
        return success();
    }

    @GetMapping("/logistics/{deliveryId}")
    @ApiOperation(value = "物流信息(订单/售后订单公用)")
    public AjaxResult logistics(@PathVariable String deliveryId) {
        JSONArray logistics = orderService.getLogistics(deliveryId);
        return success(logistics);
    }

    @DeleteMapping("/delete/{id}")
    @RequiresPermissions("mall:order:delete")
    @ApiOperation(value = "删除订单")
    public AjaxResult delete(@PathVariable Long id) {
        return success(orderService.deleteOrder(id));
    }

}
