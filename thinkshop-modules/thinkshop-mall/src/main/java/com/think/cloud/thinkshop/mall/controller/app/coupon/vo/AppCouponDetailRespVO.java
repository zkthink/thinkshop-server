package com.think.cloud.thinkshop.mall.controller.app.coupon.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Schema(description = "用户 APP - 优惠券明细")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppCouponDetailRespVO extends AppCouponBaseVO {

}
