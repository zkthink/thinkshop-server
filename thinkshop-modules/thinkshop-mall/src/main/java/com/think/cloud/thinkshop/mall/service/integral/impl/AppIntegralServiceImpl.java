package com.think.cloud.thinkshop.mall.service.integral.impl;

import com.think.cloud.thinkshop.mall.domain.integral.IntegralBill;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralRule;
import com.think.cloud.thinkshop.mall.enums.integral.IntegralBillTypeEnum;
import com.think.cloud.thinkshop.mall.service.integral.AppIntegralService;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralBillService;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralDetailService;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppIntegralServiceImpl implements AppIntegralService {
    @Autowired
    private IIntegralDetailService integralDetailService;
    @Autowired
    private IIntegralRuleService integralRuleService;
    @Autowired
    private IIntegralBillService integralBillService;

    @Override
    public Integer getUserIntegral(Long userId) {
        return integralDetailService.getUserCurrentIntegral(userId);
    }

    @Override
    public List<IntegralBill> getUserIntegralBill(Long userId) {
        return integralBillService.userIntegralBillList(userId);
    }

    @Override
    public String getRuleInfo() {
        IntegralRule integralRule = integralRuleService.getIntegralRule();
        return integralRule.getDescribeInfo();
    }

    @Override
    public IntegralRule getRuleCache() {
        return integralRuleService.getRuleCache();
    }

    @Override
    public Boolean reduceUserIntegral(Long userId, int num) {
        return integralDetailService.reduceUserIntegral(userId, num);
    }

    @Override
    public int insertIntegralDetail(Long userId, Integer integral, IntegralBillTypeEnum billTypeEnum) {
        return integralDetailService.insertIntegralDetail(userId,integral,billTypeEnum);
    }
}
