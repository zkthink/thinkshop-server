package com.think.cloud.thinkshop.mall.controller.app.memberuser.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class UpdatePasswordDTO {
    @ApiModelProperty(value = "旧密码", required = true)
    private String oldPassword;
    @ApiModelProperty(value = "新密码", required = true)
    private String newPassword;
    @ApiModelProperty(value = "验证码", required = true)
    private String code;
}
