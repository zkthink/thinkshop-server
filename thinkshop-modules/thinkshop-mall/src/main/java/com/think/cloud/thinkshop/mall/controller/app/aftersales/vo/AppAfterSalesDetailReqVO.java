package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * 用户退款申请明细VO
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppAfterSalesDetailReqVO {

    @ApiModelProperty("规格id")
    private Long skuId;

    @ApiModelProperty("退货数量")
    private Integer number;

}
