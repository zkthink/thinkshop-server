package com.think.cloud.thinkshop.mall.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import lombok.*;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 商品对象 mall_product
 *
 * @author zkthink
 * @date 2024-05-09
 */
@TableName("mall_product")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
    @TableId(type = IdType.AUTO)
    private Long productId;

    /**
     * 商品编号
     */
    @Excel(name = "商品编号")
    private String productCode;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String productName;

    /**
     * 简介
     */
    @Excel(name = "简介")
    private String introduce;

    /**
     * 商品图片
     */
    @Excel(name = "商品图片")
    private String image;

    /**
     * 视频
     */
    @Excel(name = "视频")
    private String video;

    /**
     * 商品详情
     */
    @Excel(name = "商品详情")
    private String detail;

    /**
     * 分类id
     */
    @Excel(name = "分类id")
    private Long categoryId;

    /**
     * 是否收税 0否 1是
     */
    @Excel(name = "收否收税 0否 1是")
    private Integer tax;

    /**
     * 规格最低价
     */
    @Excel(name = "规格最低价")
    private BigDecimal minPrice;

    /**
     * 规格最高价
     */
    @Excel(name = "规格最高价")
    private BigDecimal maxPrice;

    /**
     * 销量
     */
    @Excel(name = "销量")
    private Integer sales;

    /**
     * 库存
     */
    @Excel(name = "库存")
    private Integer stock;

    /**
     * 状态（0：未上架，1：上架）
     */
    @Excel(name = "状态", readConverterExp = "0=：未上架，1：上架")
    private Integer isShow;

    /**
     * 浏览量
     */
    @Excel(name = "浏览量")
    private Integer pageViews;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer deleted;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    private Timestamp createTime;

    /**
     * 修改人
     */
    @Excel(name = "修改人")
    private String updateBy;

    /**
     * 修改时间
     */
    @Excel(name = "修改时间")
    private Timestamp updateTime;
    /**
     * 商品规格类型  1：单规格 2：多规格
     */
    private Integer skuType;

}
