package com.think.cloud.thinkshop.mall.mapper.integral;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 积分规则Mapper接口
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@Mapper
public interface IntegralDetailMapper extends BaseMapper<IntegralDetail> {
    /**
     * 查询用户上次时间段的剩余积分
     * type :    1 =年  2=季度  3=月
     * @return
     */
    @Select("<script>" +
            "select * \n" +
            "from  mall_integral_detail \n" +
            "where status=1 and remain_integral >0 and deleted=0\n" +
            "   <if test=\"userId != null and userId != ''\">\n" +
            "      and user_id=#{userId}\n" +
            "   </if>"+
            "   <if test=\"type != null and type != '' and type=1\">\n" +
            "      and YEAR(create_time) = YEAR(CURDATE()) - 1\n" +
            "   </if>"+
            "   <if test=\"type != null and type != '' and type=2\">\n" +
            "      and QUARTER(create_time)=QUARTER(DATE_SUB(now(),interval 1 QUARTER))\n" +
            "   </if>"+
            "   <if test=\"type != null and type != '' and type=3\">\n" +
            "      and PERIOD_DIFF( date_format( now( ) , '%Y%m' ) , date_format( create_time, '%Y%m' ) ) =1\n" +
            "   </if>"+
            " order by user_id"+
            "</script>")
    List<IntegralDetail> selectUserLastTimeIntegral(@Param("type") Integer type,@Param("userId") Long userId);

}
