package com.think.cloud.thinkshop.mall.enums.coupon;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 优惠券类型枚举
 */
@Getter
@AllArgsConstructor
public enum CouponTypeEnum {
    FULL_REDUCTION(1, "满减"),
    DISCOUNT(2, "折扣");

    private Integer value;
    private String desc;
}
