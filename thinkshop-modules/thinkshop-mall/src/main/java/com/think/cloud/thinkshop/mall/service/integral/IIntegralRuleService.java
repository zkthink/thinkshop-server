package com.think.cloud.thinkshop.mall.service.integral;

import com.think.cloud.thinkshop.mall.domain.integral.IntegralRule;

import java.util.List;


/**
 * 积分规则Service接口
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
public interface IIntegralRuleService
{
    /**
     * 查询积分规则
     *
     * @param id 积分规则主键
     * @return 积分规则
     */
    public IntegralRule selectIntegralRuleById(Long id);

    /**
     * 查询积分规则列表
     *
     * @param integralRule 积分规则
     * @return 积分规则集合
     */
    public List<IntegralRule> selectIntegralRuleList(IntegralRule integralRule);

    /**
     * 新增积分规则
     *
     * @param integralRule 积分规则
     * @return 结果
     */
    public int insertIntegralRule(IntegralRule integralRule);

    /**
     * 修改积分规则
     *
     * @param integralRule 积分规则
     * @return 结果
     */
    public int updateIntegralRule(IntegralRule integralRule);

    /**
     * 批量删除积分规则信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);

    /**
     * 查积分规则
     * @return
     */
    public IntegralRule getIntegralRule();
    /*
    获取积分规则缓存
     */
    public IntegralRule getRuleCache();

    /**
     *  清空积分的任务
     */
   void cleanIntegralTask();
}
