package com.think.cloud.thinkshop.mall.controller.app.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


/**
 * 订单通用请求VO
 *
 * @author zkthink
 * @date 2024-05-24
 */
@Data
public class AppOrderCommonReqVO {

    /**
     * 地址id
     */
    @ApiModelProperty("地址id")
    private Long addressId;

    /**
     * 购物车id集合
     */
    @ApiModelProperty("购物车id集合")
    private List<Long> cartIds;

    /**
     * 优惠券id
     */
    @ApiModelProperty("优惠券id")
    private Long couponId;

    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private Long userId;


    /**
     * 默认用券
     */
    @ApiModelProperty("默认用券")
    private Integer isDefaultCoupon;

    /**
     * 地址是否可用
     */
    @ApiModelProperty("地址是否可用")
    private Boolean addressValid;

    @ApiModelProperty("使用多少积分或者比例进行抵扣")
    private BigDecimal payIntegral;
}
