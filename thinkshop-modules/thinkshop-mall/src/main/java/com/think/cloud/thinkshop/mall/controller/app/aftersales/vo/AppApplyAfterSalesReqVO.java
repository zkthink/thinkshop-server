package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

/**
 * 用户退款申请请求VO
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppApplyAfterSalesReqVO {

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("订单id")
    private Long orderId;

    @ApiModelProperty("服务类型：0、仅退款，1、退货退款")
    private Integer serviceType;

    @ApiModelProperty("申请原因")
    private String reasons;

    @ApiModelProperty("说明")
    private String explains;

    @ApiModelProperty("说明图片")
    private String explainImg;

    @ApiModelProperty("退货明细")
    private List<AppAfterSalesDetailReqVO> details;

}
