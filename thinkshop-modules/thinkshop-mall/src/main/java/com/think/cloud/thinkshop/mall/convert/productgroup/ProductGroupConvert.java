package com.think.cloud.thinkshop.mall.convert.productgroup;

import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.ProductGroupDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.ProductGroupPageRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.SaveProductGroupReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.UpdateProductGroupReqVO;
import com.think.cloud.thinkshop.mall.domain.ProductGroup;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 商品分组 Convert
 *
 * @author zkthink
 */
@Mapper
public interface ProductGroupConvert {

    ProductGroupConvert INSTANCE = Mappers.getMapper(ProductGroupConvert.class);

    ProductGroupPageRespVO convert(ProductGroup bean);

    ProductGroup convert(SaveProductGroupReqVO bean);

    ProductGroup convert(UpdateProductGroupReqVO bean);

    List<ProductGroupPageRespVO> convertList(List<ProductGroup> list);

    ProductGroupDetailRespVO convert1(ProductGroup bean);
}
