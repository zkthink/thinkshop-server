package com.think.cloud.thinkshop.mall.service.productgroup.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.think.cloud.thinkshop.mall.service.productgroup.IProductGroupRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.think.cloud.thinkshop.mall.mapper.ProductGroupRelationMapper;
import com.think.cloud.thinkshop.mall.domain.ProductGroupRelation;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 商品分组关联Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-10
 */
@Service
public class ProductGroupRelationServiceImpl implements IProductGroupRelationService {
    @Autowired
    private ProductGroupRelationMapper productGroupRelationMapper;

    /**
     * 查询商品分组关联
     *
     * @param groupRelationId 商品分组关联主键
     * @return 商品分组关联
     */
    @Override
    public ProductGroupRelation selectProductGroupRelationByGroupRelationId(Long groupRelationId) {
        return productGroupRelationMapper.selectById(groupRelationId);
    }

    /**
     * 查询商品分组关联列表
     *
     * @param productGroupRelation 商品分组关联
     * @return 商品分组关联
     */
    @Override
    public List<ProductGroupRelation> selectProductGroupRelationList(ProductGroupRelation productGroupRelation) {
        return productGroupRelationMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增商品分组关联
     *
     * @param productGroupRelation 商品分组关联
     * @return 结果
     */
    @Override
    public int insertProductGroupRelation(ProductGroupRelation productGroupRelation) {
        return productGroupRelationMapper.insert(productGroupRelation);
    }

    /**
     * 修改商品分组关联
     *
     * @param productGroupRelation 商品分组关联
     * @return 结果
     */
    @Override
    public int updateProductGroupRelation(ProductGroupRelation productGroupRelation) {
        return productGroupRelationMapper.updateById(productGroupRelation);
    }

    /**
     * 批量删除商品分组关联信息
     *
     * @param groupRelationIds 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> groupRelationIds) {
        return productGroupRelationMapper.deleteBatchIds(groupRelationIds);
    }

    @Override
    public List<Long> selectProductIdByGroupId(Long groupId) {
        List<ProductGroupRelation> groupRelations =
                productGroupRelationMapper.selectList(new LambdaQueryWrapper<ProductGroupRelation>()
                        .eq(ProductGroupRelation::getGroupId, groupId));
        List<Long> productIds = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(groupRelations)) {
            productIds = groupRelations.stream().map(ProductGroupRelation::getProductId).collect(Collectors.toList());
        }
        return productIds;
    }
}
