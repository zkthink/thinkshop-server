package com.think.cloud.thinkshop.mall.enums.aftersales;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 售后状态枚举
 */
@Getter
@AllArgsConstructor
public enum AfterSalesStatusEnum {

    BUSINESS_REFUSE(-2, "商家拒绝"),//售后失败
    USER_CANCELED(-1, "用户取消"), //售后取消
    AWAITING_AUDIT(0, "等待平台审核"),//待审核
    AWAITING_SHIPMENT(1, "等待用户发货"), // 待寄回货物
    AWAITING_RECEIPT(2, "用户已发货"),//待商家收货
    AGREE_REFUND(3, "商家同意退款"),//退款中
    COMPLETED(4, "退款成功"); //退款成功

    private Integer value;
    private String desc;


    public static AfterSalesStatusEnum toEnum(Integer value) {
        return Stream.of(AfterSalesStatusEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
