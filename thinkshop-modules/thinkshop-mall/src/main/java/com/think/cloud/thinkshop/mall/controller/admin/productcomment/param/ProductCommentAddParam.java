package com.think.cloud.thinkshop.mall.controller.admin.productcomment.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 商品评价对象 mall_product_comment
 *
 * @author moxiangrong
 * @date 2024-07-12
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("手动条件评论")
public class ProductCommentAddParam {
    @ApiModelProperty("商品id")
    @NotNull(message = "商品参数不能为空")
    private Long productId;

    @ApiModelProperty("商品规格id")
    @NotNull(message = "商品规格参数不能为空")
    private Long skuId;

    @ApiModelProperty("订单id，后台添加时不传")
    private Long orderId;

    @ApiModelProperty("评论内容")
//    @NotNull(message = "评论内容不能为空")
    private String comment;

    @ApiModelProperty("评分")
    @NotNull(message = "评分不能为空")
    @DecimalMin(value = "0",message = "评分最小为0分")
    @DecimalMax(value = "5",message = "评分最大为5分")
    private BigDecimal score;

    @ApiModelProperty("评论人")
    @NotNull(message = "评论人不能为空")
    private String commentUser;

    @ApiModelProperty("评论图链接")
    private List<String> imageUrls;

    @ApiModelProperty("评价时间戳(毫秒)")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date commentTime;
}
