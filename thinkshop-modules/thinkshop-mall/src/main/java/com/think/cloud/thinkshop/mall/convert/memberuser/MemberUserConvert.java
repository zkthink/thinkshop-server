package com.think.cloud.thinkshop.mall.convert.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberGroupUserTraitDetailDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberUserVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author zkthink
 * @apiNote
 **/
@Mapper
public interface MemberUserConvert {
    MemberUserConvert INSTANCE = Mappers.getMapper(MemberUserConvert.class);

    MemberUserVO convert(MemberUser bean);

    List<MemberUserVO> convertList(List<MemberUser> list);
    List<MemberGroupUserTraitDetailDTO> convertList1(List<MemberUser> list);
}
