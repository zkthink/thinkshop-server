package com.think.cloud.thinkshop.mall.service.memberuser;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagQueryDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTag;

import java.util.List;

/**
 * 会员标签Service接口
 *
 * @author zkthink
 * @date 2024-05-15
 */
public interface IMemberTagService
{
    /**
     * 查询会员标签
     *
     * @param id 会员标签主键
     * @return 会员标签
     */
    public MemberTag selectMemberTagById(Long id);

    /**
     * 查询会员标签列表
     *
     * @param dto@return 会员标签集合
     */
    public List<MemberTag> selectMemberTagList(MemberTagQueryDTO dto);

    /**
     * 新增会员标签
     *
     * @param memberTag 会员标签
     * @return 结果
     */
    public int insertMemberTag(MemberTag memberTag);

    /**
     * 修改会员标签
     *
     * @param memberTag 会员标签
     * @return 结果
     */
    public int updateMemberTag(MemberTag memberTag);

    /**
     * 批量删除会员标签信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);
}
