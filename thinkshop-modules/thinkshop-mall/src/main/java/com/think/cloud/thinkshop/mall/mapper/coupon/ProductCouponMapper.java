package com.think.cloud.thinkshop.mall.mapper.coupon;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponPageReqVO;
import org.apache.ibatis.annotations.Mapper;
import com.think.cloud.thinkshop.mall.domain.coupon.ProductCoupon;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品优惠券Mapper接口
 *
 * @author zkthink
 * @date 2024-05-21
 */
@Mapper
public interface ProductCouponMapper extends BaseMapper<ProductCoupon> {

    default List<ProductCoupon> selectList(ProductCouponPageReqVO vo) {
        LambdaQueryWrapper<ProductCoupon> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(ObjectUtil.isNotNull(vo.getCouponName()), ProductCoupon::getCouponName, vo.getCouponName())
                .eq(ObjectUtil.isNotNull(vo.getCouponType()), ProductCoupon::getCouponType, vo.getCouponType())
                .eq(ObjectUtil.isNotNull(vo.getStatus()), ProductCoupon::getStatus, vo.getStatus())
                .orderByDesc(ProductCoupon::getId);
        return selectList(wrapper);
    }

    List<ProductCoupon> receiveList(@Param("productId") Long productId);
}
