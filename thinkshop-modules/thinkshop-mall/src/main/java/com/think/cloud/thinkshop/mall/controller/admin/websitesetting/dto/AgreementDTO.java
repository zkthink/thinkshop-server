package com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class AgreementDTO {
    @ApiModelProperty("用户协议")
    private String userAgreement;

    @ApiModelProperty("隐私政策")
    private String privacyPolicy;

}

