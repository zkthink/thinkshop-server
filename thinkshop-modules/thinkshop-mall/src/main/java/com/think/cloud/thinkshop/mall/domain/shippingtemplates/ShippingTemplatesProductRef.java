package com.think.cloud.thinkshop.mall.domain.shippingtemplates;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 物流方案商品关联对象 mall_shipping_templates_product_ref
 *
 * @author zkthink
 * @date 2024-05-15
 */
@TableName("mall_shipping_templates_product_ref")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShippingTemplatesProductRef
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 方案ID */
    @Excel(name = "方案ID")
    private Long templateId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long productId;

}
