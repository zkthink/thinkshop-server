package com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

/**
 * 售后操作响应VO
 *
 * @author zkthink
 * @date 2024-06-17
 */
@Data
public class AfterSalesLogRespVO {

    @ApiModelProperty("售后id")
    private Long afterSalesId;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户类型：1、用户；2、商户；3、系统")
    private Integer userType;

    @ApiModelProperty("用户类型名称")
    private String userTypeName;

    @ApiModelProperty("操作类型")
    private Integer operateType;

    @ApiModelProperty("操作名称")
    private String operateTypeName;

    @ApiModelProperty("创建时间")
    private Timestamp createTime;


}
