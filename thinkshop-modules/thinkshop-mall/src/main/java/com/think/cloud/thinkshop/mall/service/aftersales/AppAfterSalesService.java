package com.think.cloud.thinkshop.mall.service.aftersales;

import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesInfoRespVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesPageReqVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppAfterSalesSendReqVO;
import com.think.cloud.thinkshop.mall.controller.app.aftersales.vo.AppApplyAfterSalesReqVO;
import com.think.common.core.web.page.TableDataInfo;

import java.util.Date;

/**
 * App售后记录Service接口
 *
 * @author zkthink
 * @date 2024-06-12
 */
public interface AppAfterSalesService {

    /**
     * 退货申请
     *
     * @param vo
     */
    public Long apply(AppApplyAfterSalesReqVO vo);

    /**
     * 订单是否可以售后
     * @param status
     * @param completeTime
     * @return   true :可以售后  false：不可以
     */
    boolean checkAfterState(Integer status, Date completeTime);

    /**
     * 售后详情
     *
     * @param afterSalesId
     * @return
     */
    public AppAfterSalesInfoRespVO get(Long afterSalesId);

    /**
     * 售后分页
     *
     * @param vo
     * @return
     */
    public TableDataInfo list(AppAfterSalesPageReqVO vo);

    /**
     * 发货
     *
     * @param vo
     */
    public void send(AppAfterSalesSendReqVO vo);

    /**
     * 取消售后
     *
     * @param afterSalesId
     * @param userId
     * @return
     */
    public void cancel(Long afterSalesId, Long userId);

    /**
     * 删除售后
     *
     * @param afterSalesId
     * @param userId
     * @return
     */
    public void remove(Long afterSalesId, Long userId);
}
