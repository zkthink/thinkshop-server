package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
@Schema(description = "产品组状态编辑Resp VO")
@Data
public class ProductGroupDesignStatusReqVO {
    @Schema(description = "序号", required = true, example = "1")
    private Long id;
    @Schema(description = "状态 1:可用，0：不可用", required = true, example = "1")
    private Integer status;
}
