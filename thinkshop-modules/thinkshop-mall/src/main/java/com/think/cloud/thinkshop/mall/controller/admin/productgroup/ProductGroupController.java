package com.think.cloud.thinkshop.mall.controller.admin.productgroup;


import java.util.Arrays;

import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.ProductGroupPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.RemoveProductReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.UpdateProductGroupReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.productgroup.vo.SaveProductGroupReqVO;
import com.think.cloud.thinkshop.mall.service.productgroup.IProductGroupService;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;

import javax.validation.Valid;

import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 商品分组Controller
 *
 * @author zkthink
 * @date 2024-05-10
 */
@RestController
@RequestMapping("/admin/group")
@Api(tags = "ADMIN:商品分组")
public class ProductGroupController extends BaseController {
    @Autowired
    private IProductGroupService productGroupService;

    /**
     * 查询商品分组列表
     */
    @RequiresPermissions("mall:group:list")
    @GetMapping("/list")
    public TableDataInfo list(ProductGroupPageReqVO vo) {
        startPage();
        return productGroupService.selectProductGroupList(vo);
    }

    /**
     * 获取商品分组详细信息
     */
    @RequiresPermissions("mall:group:query")
    @GetMapping(value = "/get/{groupId}")
    public AjaxResult getInfo(@PathVariable("groupId") Long groupId) {
        return success(productGroupService.selectProductGroupByGroupId(groupId));
    }

    /**
     * 新增商品分组
     */
    @RequiresPermissions("mall:group:add")
    @Log(title = "商品分组", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody SaveProductGroupReqVO vo) {
        productGroupService.insertProductGroup(vo);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 修改商品分组
     */
    @RequiresPermissions("mall:group:edit")
    @Log(title = "商品分组", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult edit(@RequestBody UpdateProductGroupReqVO vo) {
        productGroupService.updateProductGroup(vo);
        return toAjax(Boolean.TRUE);
    }

    /**
     * 删除商品分组
     */
    @RequiresPermissions("mall:group:remove")
    @Log(title = "商品分组", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete")
    public AjaxResult remove(@Valid Long[] groupIds) {
        return toAjax(productGroupService.batchDelete(Arrays.asList(groupIds)));
    }

    /**
     * 商品移除分组
     */
    @RequiresPermissions("mall:group:remove-product")
    @Log(title = "商品分组", businessType = BusinessType.GRANT)
    @PostMapping("/remove-product")
    public AjaxResult removeProduct(@RequestBody RemoveProductReqVO vo) {
        productGroupService.removeProduct(vo);
        return toAjax(Boolean.TRUE);
    }
}
