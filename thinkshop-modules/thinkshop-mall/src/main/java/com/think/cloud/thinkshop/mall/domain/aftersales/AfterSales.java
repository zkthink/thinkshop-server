package com.think.cloud.thinkshop.mall.domain.aftersales;

import com.baomidou.mybatisplus.annotation.TableName;
import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;

import java.math.BigDecimal;

/**
 * 售后记录对象 mall_after_sales
 *
 * @author zkthink
 * @date 2024-06-12
 */
@TableName("mall_after_sales")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AfterSales extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 售后单号 */
    @Excel(name = "售后单号")
    private String afterSalesCode;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 退货数量 */
    @Excel(name = "退货数量")
    private Integer number;

    /** 商品总价 */
    @Excel(name = "商品总价")
    private BigDecimal totalPrice;

    /** 邮费 */
    @Excel(name = "邮费")
    private BigDecimal totalPostage;

    /** 优惠券金额 */
    @Excel(name = "优惠券金额")
    private BigDecimal couponPrice;

    /** 税费 */
    @Excel(name = "税费")
    private BigDecimal taxation;

    /** 退款金额 */
    @Excel(name = "退款金额")
    private BigDecimal refundAmount;

    /** 服务类型：0、仅退款，1、退货退款 */
    @Excel(name = "服务类型：0、仅退款，1、退货退款")
    private Integer serviceType;

    /** 申请原因 */
    @Excel(name = "申请原因")
    private String reasons;

    /** 说明 */
    @Excel(name = "说明")
    private String explains;

    /** 说明图片 */
    @Excel(name = "说明图片")
    private String explainImg;

    /** 物流公司编码 */
    @Excel(name = "物流公司编码")
    private String shipperCode;

    /** 物流单号 */
    @Excel(name = "物流单号")
    private String deliverySn;

    /** 物流名称 */
    @Excel(name = "物流名称")
    private String deliveryName;

    /** 状态：-2、商家拒绝，-1、用户取消，0、已提交等待平台审核，1、平台已审核 等待用户发货，2、用户已发货，3、退款成功 */
    @Excel(name = "状态：-2、商家拒绝，-1、用户取消，0、已提交等待平台审核，1、平台已审核 等待用户发货，2、用户已发货，3、退款成功")
    private Integer state;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 商家收货人 */
    @Excel(name = "商家收货人")
    private String consignee;

    /** 商家手机号 */
    @Excel(name = "商家手机号")
    private String phoneNumber;

    /** 商家地址 */
    @Excel(name = "商家地址")
    private String address;

    /** 商家国家 */
    @Excel(name = "商家国家")
    private String country;

    /** 商家邮编 */
    @Excel(name = "商家邮编")
    private String postCode;

    /** 退货说明 */
    @Excel(name = "退货说明")
    private String returnPolicy;

    /** 退货凭证 */
    @Excel(name = "退货凭证")
    private String returnVoucher;

    /** 收货状态：0、未收到，1、已收到 */
    @Excel(name = "收货状态：0、未收到，1、已收到")
    private Integer receivingStatus;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    @Excel(name = "用户是否删除")
    private Integer userDel;
    private String remark;



}
