package com.think.cloud.thinkshop.mall.domain.message;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.think.common.core.annotation.Excel;
import lombok.*;

import java.util.Date;

/**
 * 用户接收的站内信对象 mall_message_user_record
 *
 * @author moxiangrong
 * @date 2024-07-22
 */
@TableName("mall_message_user_record")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageUserRecord
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 是否已读,0:未读 1：已读 */
    @Excel(name = "是否已读,0:未读 1：已读")
    private Integer userRead;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间")
    private Date createTime;

    private Long orderId;

}
