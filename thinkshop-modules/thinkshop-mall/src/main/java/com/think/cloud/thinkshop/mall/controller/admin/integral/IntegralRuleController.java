package com.think.cloud.thinkshop.mall.controller.admin.integral;


import com.think.cloud.thinkshop.mall.domain.integral.IntegralRule;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralRuleService;
import com.think.common.core.domain.R;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 积分规则Controller
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@RestController
@RequestMapping("/admin/integralRule")
@Api(tags = "积分规则")
@Validated
public class IntegralRuleController extends BaseController {
    @Autowired
    private IIntegralRuleService integralRuleService;

    /**
     * 获取积分规则详细信息
     */
    @RequiresPermissions("mall:integralRule:query")
    @GetMapping(value = "/get")
    @ApiOperation(value = "获取积分规则详细信息")
    public AjaxResult getInfo() {
        return success(integralRuleService.getIntegralRule());
    }

    /**
     * 修改积分规则
     */
    @RequiresPermissions("mall:integralRule:edit")
    @Log(title = "积分规则", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation(value = "修改积分规则")
    public AjaxResult edit(@RequestBody IntegralRule integralRule) {
        return toAjax(integralRuleService.updateIntegralRule(integralRule));
    }

    /**
     * 远程调用 ，触发积分清空
     */
    @GetMapping("/cleanTask")
    R<Boolean> cleanTask(){
        integralRuleService.cleanIntegralTask();
       return R.ok();
    }


}
