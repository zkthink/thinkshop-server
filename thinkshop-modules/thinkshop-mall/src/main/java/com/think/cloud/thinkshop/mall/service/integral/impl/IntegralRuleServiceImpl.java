package com.think.cloud.thinkshop.mall.service.integral.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralRule;
import com.think.cloud.thinkshop.mall.enums.integral.IntegralCleanTypeEnum;
import com.think.cloud.thinkshop.mall.enums.integral.IntegralRuleDeductionTypeEnum;
import com.think.cloud.thinkshop.mall.enums.integral.IntegralRuleUseEnum;
import com.think.cloud.thinkshop.mall.mapper.integral.IntegralRuleMapper;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralDetailService;
import com.think.cloud.thinkshop.mall.service.integral.IIntegralRuleService;
import com.think.common.core.exception.ServiceException;
import com.think.common.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 积分规则Service业务层处理
 *
 * @author moxiangrong
 * @date 2024-07-17
 */
@Service
public class IntegralRuleServiceImpl implements IIntegralRuleService {
    private static final Long ID = 1L;
    private static final String INTEGRAL_RULE_REDIS_KEY = "integral:rule";
    @Autowired
    private IntegralRuleMapper integralRuleMapper;
    @Autowired
    private RedisService redisService;
    @Autowired
    private IIntegralDetailService integralDetailService;

    /**
     * 查询积分规则
     *
     * @param id 积分规则主键
     * @return 积分规则
     */
    @Override
    public IntegralRule selectIntegralRuleById(Long id) {
        return integralRuleMapper.selectById(id);
    }

    /**
     * 查询积分规则列表
     *
     * @param integralRule 积分规则
     * @return 积分规则
     */
    @Override
    public List<IntegralRule> selectIntegralRuleList(IntegralRule integralRule) {
        return integralRuleMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增积分规则
     *
     * @param integralRule 积分规则
     * @return 结果
     */
    @Override
    public int insertIntegralRule(IntegralRule integralRule) {
        return integralRuleMapper.insert(integralRule);
    }

    /**
     * 修改积分规则
     *
     * @param integralRule 积分规则
     * @return 结果
     */
    @Override
    public int updateIntegralRule(IntegralRule integralRule) {
        this.integralRuleParamCheck(integralRule);
        integralRule.setId(ID);
        int i = integralRuleMapper.updateById(integralRule);
        if (i > 0) {
            redisService.deleteObject(INTEGRAL_RULE_REDIS_KEY);
        }
        return i;
    }

    private void integralRuleParamCheck(IntegralRule integralRule){
        BigDecimal deductionRatio = integralRule.getDeductionRatio();
        if (deductionRatio.compareTo(new BigDecimal("0.01")) <0 ||
                deductionRatio.compareTo(new BigDecimal("1000")) >0
        ){
            throw new ServiceException("参数错误，请输入抵扣比值在0.01-1000之间");
        }
        Integer deductionType = integralRule.getDeductionType();
        BigDecimal deductionValue = integralRule.getDeductionValue();
        if ( deductionType.equals(IntegralRuleDeductionTypeEnum.MAX_SCALE.getCode()) &&
                (deductionValue.intValue() < 1 || deductionValue.intValue() > 100)
        ){
            throw new ServiceException("参数错误，请输入最大抵扣比例在1%-100%之间");
        }
        if (deductionType.equals(IntegralRuleDeductionTypeEnum.MAX_INTEGRAL.getCode()) &&
                (deductionValue.intValue() < 1 || deductionValue.intValue() > 90000)
        ) {
            throw new ServiceException("参数错误，请输入最多抵扣积分值在1-90000之间");
        }
    }

    /**
     * 批量删除积分规则信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return integralRuleMapper.deleteBatchIds(ids);
    }

    @Override
    public IntegralRule getIntegralRule() {
        IntegralRule rule = this.selectIntegralRuleById(ID);
        if (rule == null) {
            rule = IntegralRule.builder()
                    .id(ID)
                    .isUse(IntegralRuleUseEnum.NO_USE.getCode())
                    .deductionRatio(new BigDecimal("0.01"))
                    .deductionType(1)
                    .deductionValue(new BigDecimal("100"))
                    .orderAward(1)
                    .cleanType(IntegralCleanTypeEnum.NO.getCode())
                    .describeInfo("积分规则")
                    .build();
            this.insertIntegralRule(rule);
            return rule;
        }
        return rule;
    }

    @Override
    public IntegralRule getRuleCache() {
        Object cacheObject = redisService.getCacheObject(INTEGRAL_RULE_REDIS_KEY);
        if (cacheObject == null) {
            IntegralRule rule = getIntegralRule();
            rule.setDescribeInfo("");
            redisService.setCacheObject(INTEGRAL_RULE_REDIS_KEY, rule);
            return rule;
        }
        return (IntegralRule) cacheObject;
    }

    @Override
    public void cleanIntegralTask() {
        IntegralRule integralRule = this.getIntegralRule();
        Integer cleanType = integralRule.getCleanType();
        //不处理
        if (cleanType.equals(IntegralCleanTypeEnum.NO.getCode())) {
            return;
        }
        integralDetailService.cleanIntegral(cleanType);
    }
}
