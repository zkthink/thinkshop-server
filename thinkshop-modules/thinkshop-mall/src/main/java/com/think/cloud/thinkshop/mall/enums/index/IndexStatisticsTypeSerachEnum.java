package com.think.cloud.thinkshop.mall.enums.index;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IndexStatisticsTypeSerachEnum {
    TODAY("今天",1),
    LAST_WEEK("近7天",2),
    LAST_MONTH("近一个月",3),
    CUR_YEAR("本年",4)
    ;
    private String description;
    private Integer type;

    public static IndexStatisticsTypeSerachEnum get(Integer value) {
        for (IndexStatisticsTypeSerachEnum indexStatisticsTypeSerachEnum : IndexStatisticsTypeSerachEnum.values()) {
            if (indexStatisticsTypeSerachEnum.type.equals(value)) {
                return indexStatisticsTypeSerachEnum ;
            }
        }
        return null;
    }

}
