package com.think.cloud.thinkshop.mall.service.category;

import com.think.cloud.thinkshop.mall.controller.admin.category.vo.ProductCategoryLevelVO;
import com.think.cloud.thinkshop.mall.controller.admin.category.vo.ProductRelationCategoryReqVO;
import com.think.cloud.thinkshop.mall.domain.ProductCategory;

import java.util.List;

/**
 * 商品类目Service接口
 * 
 * @author zkthink
 * @date 2024-05-08
 */
public interface IProductCategoryService 
{
    /**
     * 查询商品类目
     * 
     * @param categoryId 商品类目主键
     * @return 商品类目
     */
    public ProductCategory selectProductCategoryByCategoryId(Long categoryId);

    /**
     * 查询商品类目树
     *
     * @return 商品类目集合
     */
    public List<ProductCategoryLevelVO> getProductTree();

    /**
     * 查询商品类目列表
     *
     * @param productCategory 商品类目
     * @return 商品类目集合
     */
    public List<ProductCategory> selectProductCategoryList(ProductCategory productCategory);


    /**
     * 新增商品类目
     * 
     * @param productCategory 商品类目
     * @return 结果
     */
    public int insertProductCategory(ProductCategory productCategory);

    /**
     * 修改商品类目
     * 
     * @param productCategory 商品类目
     * @return 结果
     */
    public int updateProductCategory(ProductCategory productCategory);

    /**
     * 批量删除商品类目信息
     * 
     * @param categoryIds 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> categoryIds);

    /**
     * 批量取消商品类目关联
     *
     * @param productIds 商品id集合
     * @return 结果
     */
    public void cancel(List<Long> productIds);

    /**
     * 批量关联商品类目
     *
     * @param vo
     * @return 结果
     */
    public void relation(ProductRelationCategoryReqVO vo);

    /**
     * 通过类目名找到记录id
     * @param name
     * @return
     */
    public Long findIdByName(String name);
}
