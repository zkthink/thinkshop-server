package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;


@Data
@Schema(description = "管理后台 - 页眉 resp VO")
@ToString(callSuper = true)
public class PageConfigPageTopRespVO  {
    @Schema(description = "序号", required = true, example = "1")
    private Long id;
    @Schema(description = "页眉图片地址", required = true, example = "http://xxxxx")
    private String pageTopUrl;
    @Schema(description = "客服链接", required = true, example = "xxxx")
    private String customerServiceUrl;



}
