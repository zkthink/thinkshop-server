package com.think.cloud.thinkshop.mall.controller.app.order;


import cn.hutool.json.JSONArray;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppConfirmOrderReqVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppCreateOrderReqVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderCommentReqVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderPageReqVO;
import com.think.cloud.thinkshop.mall.service.order.AppOrderService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.web.page.TableDataInfo;
import com.think.common.idempotent.annotation.RepeatSubmit;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 订单AppController
 *
 * @author zkthink
 * @date 2024-05-23
 */
@RestController
@RequestMapping("/app/order")
@Api(tags = "APP:订单")
public class AppOrderController extends BaseController {

    @Autowired
    private AppOrderService appOrderService;

    /**
     * 确认订单
     */
    @PostMapping("/confirm")
    @ApiOperation(value = "确认订单")
    public AjaxResult confirm(@Validated @RequestBody AppConfirmOrderReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        return success(appOrderService.confirm(vo));
    }

    /**
     * 创建订单
     */
    @PostMapping("/create")
    @ApiOperation(value = "创建订单")
    @RepeatSubmit
    public AjaxResult create(@Validated @RequestBody AppCreateOrderReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        vo.setUserName(SecurityUtils.getUsername());
        return success(appOrderService.create(vo));
    }

    /**
     * 查询订单列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "查询订单列表")
    public TableDataInfo list(AppOrderPageReqVO vo) {
        vo.setUserId(SecurityUtils.getUserId());
        startPage();
        return appOrderService.list(vo);
    }

    /**
     * 订单详情
     */
    @GetMapping("/get/{orderId}")
    @ApiOperation(value = "订单详情")
    public AjaxResult get(@PathVariable Long orderId) {
        return success(appOrderService.get(orderId, SecurityUtils.getUserId()));
    }

    /**
     * 取消订单
     */
    @GetMapping("/cancel/{orderId}")
    @ApiOperation(value = "取消订单")
    public AjaxResult cancel(@PathVariable Long orderId) {
        appOrderService.cancel(orderId, SecurityUtils.getLoginUser());
        return toAjax(Boolean.TRUE);
    }

    /**
     * 收货
     */
    @GetMapping("/receipt/{orderId}")
    @ApiOperation(value = "收货")
    public AjaxResult receipt(@PathVariable Long orderId) {
        appOrderService.receipt(orderId, SecurityUtils.getLoginUser());
        return toAjax(Boolean.TRUE);
    }

    /**
     * 再次购买
     */
    @GetMapping("/purchase-again/{orderId}")
    @ApiOperation(value = "再次购买")
    public AjaxResult purchaseAgain(@PathVariable Long orderId) {
        return success(appOrderService.purchaseAgain(orderId));
    }

    /**
     * 删除订单
     */
    @DeleteMapping("/delete/{orderId}")
    @ApiOperation(value = "删除订单")
    public AjaxResult delete(@PathVariable Long orderId) {
        appOrderService.delete(orderId);
        return toAjax(Boolean.TRUE);
    }

    @GetMapping("/logistics/{deliveryId}")
    @ApiOperation(value = "物流信息(订单/售后订单公用)")
    public AjaxResult logistics(@PathVariable String deliveryId) {
        JSONArray logistics = appOrderService.getLogistics(deliveryId);
        return success(logistics);
    }

    @PostMapping("/comment")
    @ApiOperation(value = "评论订单商品")
    public AjaxResult comment(@RequestBody AppOrderCommentReqVO param) {
        return success(appOrderService.submitOrderComment(param, SecurityUtils.getLoginUser()));
    }
}
