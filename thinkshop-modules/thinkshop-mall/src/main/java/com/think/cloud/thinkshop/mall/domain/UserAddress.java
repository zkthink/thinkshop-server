package com.think.cloud.thinkshop.mall.domain;

import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

import java.sql.Timestamp;

/**
 * 用户地址对象 mall_user_address
 *
 * @author zkthink
 * @date 2024-05-20
 */
@TableName("mall_user_address")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAddress
{
    private static final long serialVersionUID = 1L;

    /** 用户地址id */
    @TableId(type = IdType.AUTO)
    private Long addressId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** first name */
    @Excel(name = "first name")
    private String firstName;

    /** last name */
    @Excel(name = "last name")
    private String lastName;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String detail;

    /** 国家编码 */
    @Excel(name = "国家编码")
    private String countryId;

    /** 收货人所在国家 */
    @Excel(name = "收货人所在国家")
    private String country;

    /** 省份编码 */
    @Excel(name = "省份编码")
    private String provinceId;

    /** 收货人所在省 */
    @Excel(name = "收货人所在省")
    private String province;

    /** 收货人所在市 */
    @Excel(name = "收货人所在市")
    private String city;

    /** 邮编 */
    @Excel(name = "邮编")
    private String postCode;

    /** 是否默认 */
    @Excel(name = "是否默认")
    private Integer isDefault;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createBy;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private Timestamp createTime;

    /** 修改人 */
    @Excel(name = "修改人")
    private String updateBy;

    /** 修改时间 */
    @Excel(name = "修改时间")
    private Timestamp updateTime;

}
