package com.think.cloud.thinkshop.mall.enums.aftersales;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 售后类型枚举
 */
@Getter
@AllArgsConstructor
public enum AfterSalesDelEnum {

    NO(0, "未删除"),
    YES(1, "已删除");

    private Integer value;
    private String desc;


    public static AfterSalesDelEnum toEnum(Integer value) {
        return Stream.of(AfterSalesDelEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
