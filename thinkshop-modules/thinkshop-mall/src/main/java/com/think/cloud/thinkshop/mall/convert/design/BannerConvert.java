package com.think.cloud.thinkshop.mall.convert.design;


import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerAddReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.BannerPageRespVO;
import com.think.cloud.thinkshop.mall.domain.design.Banner;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * banner Convert
 *
 * @author zkthink
 */
@Mapper
public interface BannerConvert {

    BannerConvert INSTANCE = Mappers.getMapper(BannerConvert.class);

    Banner convert(BannerAddReqVO dto);
    Banner convert(BannerEditReqVO dto);
    List<BannerPageRespVO> convertList(List<Banner> list);

}
