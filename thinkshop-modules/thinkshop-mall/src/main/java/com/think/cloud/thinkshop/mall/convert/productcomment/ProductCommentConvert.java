package com.think.cloud.thinkshop.mall.convert.productcomment;

import com.think.cloud.thinkshop.mall.controller.admin.productcomment.param.ProductCommentAddParam;
import com.think.cloud.thinkshop.mall.controller.admin.productcomment.vo.ProductCommentVO;
import com.think.cloud.thinkshop.mall.domain.ProductComment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 商品 Convert
 *
 * @author zkthink
 */
@Mapper
public interface ProductCommentConvert {

    ProductCommentConvert INSTANCE = Mappers.getMapper(ProductCommentConvert.class);

    List<ProductCommentVO> convertList(List<ProductComment> list);

    ProductComment convert(ProductCommentAddParam param);

}
