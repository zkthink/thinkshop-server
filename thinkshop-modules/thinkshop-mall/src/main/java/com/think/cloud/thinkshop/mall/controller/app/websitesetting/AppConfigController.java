package com.think.cloud.thinkshop.mall.controller.app.websitesetting;

import cn.hutool.core.util.ObjectUtil;
import com.think.cloud.thinkshop.mall.controller.app.websitesetting.vo.WebsiteSettingVO;
import com.think.cloud.thinkshop.mall.convert.websitesetting.WebsiteSettingConvert;
import com.think.cloud.thinkshop.mall.domain.websitesetting.WebsiteSetting;
import com.think.cloud.thinkshop.mall.service.shippingtemplates.IDeliveryCompanyService;
import com.think.cloud.thinkshop.mall.service.websitesetting.IAgreementService;
import com.think.cloud.thinkshop.mall.service.websitesetting.IWebsiteSettingService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.idempotent.annotation.RepeatSubmit;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @author zkthink
 * @date 2024-05-14
 */
@RestController
@RequestMapping("/app")
@Api(tags = "APP:网站设置")
public class AppConfigController extends BaseController {
    @Autowired
    private IWebsiteSettingService websiteSettingService;
    @Autowired
    private IAgreementService agreementService;
    @Autowired
    private IDeliveryCompanyService deliveryCompanyService;

    @Value("${common.backend-url}")
    private String backendUrl;

    @Value("${common.h5-url}")
    private String h5Url;

    @Value("${common.pc-url}")
    private String pcUrl;

    /**
     * 获取网站设置详细信息
     */
    @GetMapping("/websitesetting")
    @ApiOperation(value = "获取网站设置详细信息")
    public AjaxResult websiteSetting() {
        WebsiteSetting cache = websiteSettingService.getCache();
        WebsiteSettingVO vo = WebsiteSettingConvert.INSTANCE.convert01(cache);
        vo.setBackendUrl(backendUrl);
        vo.setH5Url(h5Url);
        vo.setPcUrl(pcUrl);
        if (ObjectUtil.isNotNull(vo)) {
            vo.setRefundOnlyReason(websiteSettingService.refundOnlyReason());
            vo.setReturnGoodsRefund(websiteSettingService.returnGoodsRefund());
        }
        return success(vo);
    }


    @ApiOperation(value = "获取协议 1用户协议 2隐私政策")
    @GetMapping("/getAgreement")
    public AjaxResult getAgreement(@RequestParam(value = "type") Integer type) {
        String content = agreementService.getAgreementByType(type);
        return AjaxResult.success("", content);
    }

    @GetMapping("/delivery-company/simple-list")
    @ApiOperation(value = "获取快递公司列表")
    public AjaxResult simpleList()
    {
        return success(deliveryCompanyService.simpleList());
    }


}
