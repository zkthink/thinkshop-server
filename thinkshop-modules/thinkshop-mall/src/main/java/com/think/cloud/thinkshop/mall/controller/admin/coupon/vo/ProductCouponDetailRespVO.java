package com.think.cloud.thinkshop.mall.controller.admin.coupon.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductCouponDetailRespVO extends ProductCouponBaseVO {

    @ApiModelProperty("优惠券状态：1、进行中，2、已结束,0、未开始")
    private Integer status;

    @ApiModelProperty("优惠券状态名")
    private String statusName;
}
