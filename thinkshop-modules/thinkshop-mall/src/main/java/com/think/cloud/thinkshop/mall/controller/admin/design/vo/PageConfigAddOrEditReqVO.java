package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@Schema(description = "管理后台 - 页脚，页脚 add VO")
@ToString(callSuper = true)
public class PageConfigAddOrEditReqVO {
//    @Schema(description = "序号", required = true, example = "1")
//    private Long id;
    @Schema(description = "页眉图片地址", required = true, example = "http://xxxxx")
    private String pageTopUrl;

    @Schema(description = "客服链接", required = true, example = "xxxx")
    private String customerServiceUrl;

    @Schema(description = "页脚图片地址", required = true, example = "http://xxxxx")
    private String pageBottomUrl;

    @Schema(description = "页脚二维码地址", required = true, example = "http://xxxxx")
    private List<String> pageBottomQrCodeUrls;

    @Schema(description = "版权备案号", required = true, example = "xxxxxx")
    private String copyright;

    @Schema(description = "电话", required = true, example = "1234567890")
    private String phone;

    @Schema(description = "邮箱", required = true, example = "122@qq.com")
    private String email;

    @Schema(description = "联系地址", required = true, example = "1")
    private String address;

    @Schema(description = "其他", required = true, example = "1")
    private Integer other;

    @Schema(description = "other内容", required = true, example = "1")
    private String otherComment;
}
