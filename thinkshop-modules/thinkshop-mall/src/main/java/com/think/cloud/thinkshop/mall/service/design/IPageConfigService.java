package com.think.cloud.thinkshop.mall.service.design;

import com.think.cloud.thinkshop.mall.controller.admin.design.vo.*;

/**
 * PageConfigService接口
 *
 * @author dengguangxian
 * @date 2024-05-29
 */
public interface IPageConfigService {
    /**
     * 查询PageConfigTop
     */
    PageConfigPageTopRespVO selectPageConfigTop();

    /**
     * 查询PageConfigBottom
     */
    PageConfigPageBottomRespVO selectPageConfigBottom();

    /**
     * 新增PageConfig
     *
     * @param vo
     * @return 结果
     */
    int insertPageConfig(PageConfigAddOrEditReqVO vo);

    /**
     * 修改PageConfig
     *
     * @return 结果
     */
    int updatePageConfig(PageConfigAddOrEditReqVO vo);
    /**
     * 新增修改PageConfig
     *
     * @return 结果
     */
    int insertOrUpdatePageConfig(PageConfigAddOrEditReqVO vo);

}
