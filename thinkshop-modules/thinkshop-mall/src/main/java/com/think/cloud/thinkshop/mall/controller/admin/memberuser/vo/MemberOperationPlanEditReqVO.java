package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MemberOperationPlanEditReqVO extends MemberOperationPlanAddReqVO {
    @ApiModelProperty("运营计划id")
    private Long id;
}
