package com.think.cloud.thinkshop.mall.enums.common;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zkthink
 * 是否枚举
 */
@Getter
@AllArgsConstructor
public enum CommonWhetherEnum {

    NO(0, "否"),
    YES(1, "是");

    private Integer value;
    private String desc;

}
