package com.think.cloud.thinkshop.mall.mapper.memberuser;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberGroupDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberOperationPlanRefCouponVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationPlanRef;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 客户运营计划Mapper接口
 *
 * @author zkthink
 * @date 2024-06-07
 */
@Mapper
public interface MemberOperationPlanRefMapper extends BaseMapper<MemberOperationPlanRef> {

    List<MemberOperationPlanRefCouponVO> selectCouponList(@Param("planId") Long planId);

    List<Long> selectPlanIdByGroupId(@Param("groupId") Long groupId);

    /**
     * 批量查询运营计划对应分群
     * @param planIdList 运营计划
     * @return /
     */
    List<MemberGroupDetailRespVO> selectGroupList(@Param("planIdList") List<Long> planIdList);
}
