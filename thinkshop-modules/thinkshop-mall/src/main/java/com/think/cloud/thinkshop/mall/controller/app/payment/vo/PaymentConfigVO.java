package com.think.cloud.thinkshop.mall.controller.app.payment.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class PaymentConfigVO {
    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 名称 */
    @ApiModelProperty( "名称")
    private String name;

    /** logo */
    @ApiModelProperty( "logo")
    private String logo;

    /** 状态 */
    @ApiModelProperty( "状态")
    private Boolean status;

    /** 支付client_id */
    @ApiModelProperty( "支付client_id")
    private String clientId;

    /** 支付编码 */
    @ApiModelProperty( "支付编码")
    private String code;
}
