package com.think.cloud.thinkshop.mall.domain.memberuser;

import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 商城用户对象 mall_member_user
 *
 * @author zkthink
 * @date 2024-05-09
 */
@TableName("mall_member_user")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class MemberUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 账号 */
    @Excel(name = "账号")
    private String username;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** first name */
    @Excel(name = "first name")
    private String firstName;

    /** last name */
    @Excel(name = "last name")
    private String lastName;

    /** 头像 */
    @Excel(name = "头像")
    private String avatar;

    /** 注册来源 */
    @Excel(name = "注册来源")
    private String source;

    /** 三方用户id */
    @Excel(name = "三方用户id")
    private String thirdUid;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private Integer status;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phonenumber;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 消费次数 */
    @Excel(name = "消费次数")
    private Integer orderCount;

    /** 累计消费金额 */
    @Excel(name = "累计消费金额")
    private BigDecimal totalOrderAmount;

    /** 上次消费时间 */
    @Excel(name = "上次消费时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Timestamp lastConsumeTime;

    /** 累计退款订单数 */
    @Excel(name = "累计退款订单数")
    private Integer refundCount;

    /** 累计退款金额 */
    @Excel(name = "累计退款金额")
    private BigDecimal totalRefundAmount;

}
