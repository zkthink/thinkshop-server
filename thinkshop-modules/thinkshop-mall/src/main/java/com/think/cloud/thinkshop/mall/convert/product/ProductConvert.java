package com.think.cloud.thinkshop.mall.convert.product;

import com.think.cloud.thinkshop.mall.controller.admin.product.vo.*;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.app.product.vo.AppProductPageRespVO;
import com.think.cloud.thinkshop.mall.domain.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 商品 Convert
 *
 * @author zkthink
 */
@Mapper
public interface ProductConvert {

    ProductConvert INSTANCE = Mappers.getMapper(ProductConvert.class);

    Product convert(SaveProductReqVO bean);

    Product convert(UpdateProductReqVO bean);

    ProductDetailRespVO convert(Product bean);

    ProductPageRespVO convert1(Product bean);

    List<ProductPageRespVO> convertList(List<Product> list);

    AppProductPageRespVO convert2(Product bean);

    List<AppProductPageRespVO> convertList1(List<Product> list);

    AppProductDetailRespVO convert3(Product bean);

    SaveProductReqVO convert4(SaveProductExcelTemplateVO bean);
    List<SaveProductExcelTemplateVO> convertList2( List<Product> list);
}
