package com.think.cloud.thinkshop.mall.mapper.order;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.think.cloud.thinkshop.mall.controller.admin.coupon.vo.ProductCouponOrderDetailRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.index.dto.StatisticalTrendViewDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberOrderConsumeViewDTO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.order.vo.OrderPageRespVO;
import com.think.cloud.thinkshop.mall.controller.app.order.vo.AppOrderPageReqVO;
import com.think.cloud.thinkshop.mall.domain.order.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 订单Mapper接口
 *
 * @author zkthink
 * @date 2024-05-23
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    List<Order> selectList01(@Param("param") AppOrderPageReqVO vo);

    List<OrderPageRespVO> selectOrderList(OrderPageReqVO vo);

    @Select("select sum(total_price) from mall_order \n" +
            "where create_time >='#{start}' and create_time <='#{end}' ")
    Long getTurnoverTotal(@Param("start") String start, @Param("end") String end);
    @Select("select DATE_FORMAT(create_time , '%Y-%m-%d') as dateStr,sum(total_price) as value \n" +
            "from mall_order \n" +
            "${ew.customSqlSegment} group by dateStr")
    List<StatisticalTrendViewDTO> getTurnoverTotalTrend(@Param(Constants.WRAPPER) LambdaQueryWrapper<Order> wrapper);

    @Select("")
    List<String> orderAmountRecord(String startTime, String endTime);

    @Select(" select count(*) as consume_count,ifnull(sum(pay_price),0) as consume_amount,max(pay_time) as last_consume_time\n" +
            " from mall_order mo where user_id=#{userId}\n" +
            "and paid =1 ")
    MemberOrderConsumeViewDTO userOrderConsumeView(@Param("userId") Long userId);

    @Select("select DATE_FORMAT(create_time , '%Y-%m-%d') as dateStr,count(*) as value " +
            "from mall_order " +
            "${ew.customSqlSegment} group by dateStr")
    List<StatisticalTrendViewDTO> orderCountTrend(@Param(Constants.WRAPPER) LambdaQueryWrapper<Order> wrapper);

   List<ProductCouponOrderDetailRespVO> getCouponDataDetails(@Param("couponId") Long couponId);

    @Select("select count(distinct user_id)" +
            "from mall_order " +
            "${ew.customSqlSegment}")
    double payUserCount(@Param(Constants.WRAPPER) LambdaQueryWrapper<Order> wrapper);


    @Select("select * from mall_order where id = #{id}")
    Order getOrderById(@Param("id") Long id);
}
