package com.think.cloud.thinkshop.mall.controller.app.memberuser;

import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.ForgetPasswordDTO;
import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.RegisterDTO;
import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.UpdatePasswordDTO;
import com.think.cloud.thinkshop.mall.controller.app.memberuser.dto.UpdateUserInfoDTO;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberUserService;
import com.think.common.core.exception.util.ServiceExceptionUtil;
import com.think.common.core.utils.StringUtils;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.think.common.core.exception.enums.ErrorCode.PARAMETER_ERROR;

/**
 * @author zkthink
 * @apiNote
 **/
@RestController
@RequestMapping("/app/user")
@Api(tags = "APP:商城用户")
public class AppMemberUserController {
    @Autowired
    private IMemberUserService memberUserService;

    @PostMapping("register")
    @ApiOperation(value = "注册")
    public AjaxResult register(@RequestBody RegisterDTO registerDTO) {
        if (StringUtils.isBlank(registerDTO.getEmail())
                || StringUtils.isBlank(registerDTO.getFirstName())
                || StringUtils.isBlank(registerDTO.getLastName())
                || StringUtils.isBlank(registerDTO.getPassword())
                || StringUtils.isBlank(registerDTO.getCode())
        ) {
            throw ServiceExceptionUtil.exception(PARAMETER_ERROR);
        }
        memberUserService.register(registerDTO);
        return AjaxResult.success();
    }

    @GetMapping("info")
    @ApiOperation(value = "获取用户信息")
    public AjaxResult info() {
        return AjaxResult.success(memberUserService.selectMemberUserById(SecurityUtils.getUserId()));
    }

    @PostMapping("update")
    @ApiOperation("修改用户信息")
    public AjaxResult updateUserInfo(@RequestBody UpdateUserInfoDTO infoDTO) {
        memberUserService.updateUserInfo(SecurityUtils.getUserId(),infoDTO);
        return AjaxResult.success();
    }

    @PostMapping("forgetPassword")
    @ApiOperation("忘记密码")
    public AjaxResult forgetPassword(@RequestBody ForgetPasswordDTO forgetPassword) {
        memberUserService.forgetPassword(forgetPassword);
        return AjaxResult.success();
    }

    @PostMapping("updatePassword")
    @ApiOperation("修改密码")
    public AjaxResult updatePassword(@RequestBody UpdatePasswordDTO updatePassword){
        memberUserService.updatePassword(SecurityUtils.getUserId(),updatePassword);
        return AjaxResult.success();
    }
}
