package com.think.cloud.thinkshop.mall.service.websitesetting.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto.WebsiteSettingDTO;
import com.think.cloud.thinkshop.mall.convert.websitesetting.WebsiteSettingConvert;
import com.think.cloud.thinkshop.mall.domain.websitesetting.WebsiteSetting;
import com.think.cloud.thinkshop.mall.mapper.websitesetting.WebsiteSettingMapper;
import com.think.cloud.thinkshop.mall.service.websitesetting.IWebsiteSettingService;
import com.think.common.core.utils.DateUtils;
import com.think.common.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.think.cloud.thinkshop.common.constant.CacheConstant.*;

/**
 * 网站设置Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-14
 */
@Service("websiteSettingService")
public class WebsiteSettingServiceImpl implements IWebsiteSettingService {
    @Autowired
    private WebsiteSettingMapper websiteSettingMapper;
    @Autowired
    private RedisService redisService;

    /**
     * 查询网站设置
     *
     * @param id 网站设置主键
     * @return 网站设置
     */
    @Override
    public WebsiteSetting selectWebsiteSettingById(Long id) {
        return websiteSettingMapper.selectById(id);
    }

    /**
     * 查询网站设置列表
     *
     * @param websiteSetting 网站设置
     * @return 网站设置
     */
    @Override
    public List<WebsiteSetting> selectWebsiteSettingList(WebsiteSetting websiteSetting) {
        return websiteSettingMapper.selectList(Wrappers.emptyWrapper());
    }

    /**
     * 新增网站设置
     *
     * @param websiteSetting 网站设置
     * @return 结果
     */
    @Override
    public int insertWebsiteSetting(WebsiteSetting websiteSetting) {
        return websiteSettingMapper.insert(websiteSetting);
    }

    /**
     * 修改网站设置
     *
     * @param websiteSetting 网站设置
     * @return 结果
     */
    @Override
    public int updateWebsiteSetting(WebsiteSetting websiteSetting) {
        int res = websiteSettingMapper.updateById(websiteSetting);
        if(res > 0){
            //删除缓存
            redisService.deleteObject(CACHE_WEBSITE_SETTING);
        }
        return res;
    }

    /**
     * 批量删除网站设置
     *
     * @param ids 需要删除的网站设置主键
     * @return 结果
     */
    @Override
    public int deleteWebsiteSettingByIds(Long[] ids) {
        return websiteSettingMapper.deleteBatchIds(Arrays.asList(ids));
    }

    /**
     * 删除网站设置信息
     *
     * @param id 网站设置主键
     * @return 结果
     */
    @Override
    public int deleteWebsiteSettingById(Long id) {
        return websiteSettingMapper.deleteById(id);
    }

    @Override
    public WebsiteSetting getCache() {
       String res = redisService.getCacheObject(CACHE_WEBSITE_SETTING);
       if (ObjectUtil.isEmpty(res)) {
           WebsiteSetting websiteSetting = websiteSettingMapper.selectById(1L);
           redisService.setCacheObject(CACHE_WEBSITE_SETTING, JSONUtil.toJsonStr(websiteSetting));
           return websiteSetting;
       }
        return JSONUtil.toBean(res, WebsiteSetting.class);
    }

    @Override
    public List<String> refundOnlyReason() {
        List<String> list = redisService.getCacheObject(REFUND_ONLY_REASON);
        if (ObjectUtil.isEmpty(list)) {
            list = new ArrayList<>();
        }
        return list;
    }

    @Override
    public List<String> returnGoodsRefund() {
        List<String> list = redisService.getCacheObject(RETURN_GOODS_REFUND);
        if (ObjectUtil.isEmpty(list)) {
            list = new ArrayList<>();
        }
        return list;
    }

    @Override
    public void saveRefundOnlyReason(List<String> reasons) {
        redisService.setCacheObject(REFUND_ONLY_REASON, reasons);
    }

    @Override
    public void saveReturnGoodsRefund(List<String> reasons) {
        redisService.setCacheObject(RETURN_GOODS_REFUND, reasons);
    }

    @Override
    public WebsiteSettingDTO getInfo() {
        WebsiteSetting websiteSetting = websiteSettingMapper.selectById(1L);
        WebsiteSettingDTO dto = WebsiteSettingConvert.INSTANCE.convert(websiteSetting);
        if (ObjectUtil.isNotNull(dto)) {
            dto.setRefundOnlyReason(refundOnlyReason());
            dto.setReturnGoodsRefund(returnGoodsRefund());
            return dto;
        }
        return dto;
    }

    @Override
    public int save(WebsiteSettingDTO websiteSetting) {
        WebsiteSetting setting = WebsiteSettingConvert.INSTANCE.convert(websiteSetting);
        setting.setId(1L);
        this.updateWebsiteSetting(setting);
        this.saveRefundOnlyReason(websiteSetting.getRefundOnlyReason());
        this.saveReturnGoodsRefund(websiteSetting.getReturnGoodsRefund());
        return 1;
    }

    @Override
    public String getZoneTimeInfo() {
        //系统时区
        int sysZone = Integer.parseInt(this.getSysZoneStr());
        //配置的时区
        int configZone = Integer.parseInt(this.getCache().getTimeZone());
        int i = sysZone - configZone;
        //数据库时间
        Date dataBaseNow = this.getDataBaseNow();
        String format="当前系统时区：{0}，系统时间：{1}，参数配置时区：{2}，数据库时间：{3} ,系统时区与配置时区差：{4}";
        return MessageFormat.format(format,sysZone, LocalDateTime.now(),configZone,dataBaseNow,i);
    }

    @Override
    public Date getDataBaseNow() {
        return  websiteSettingMapper.getDataBaseNow();
    }

    /**
     * 获取系统时区字符
     */
    private String getSysZoneStr() {
        // 获取系统默认时区
        ZoneId sysZoneId = ZoneId.systemDefault();
        // 获取当前时间以及时区信息
        ZonedDateTime zonedDateTime = ZonedDateTime.now(sysZoneId);
        // 获取时区偏移量
        int offsetHours = zonedDateTime.getOffset().getTotalSeconds() / 3600;
        return (offsetHours >= 0 ? "+" : "-") + String.format("%02d", Math.abs(offsetHours));
    }

    @Override
    public int zoneTimeGap() {
        //系统时区
        int sysZone = Integer.parseInt(this.getSysZoneStr());
        //配置的时区
        int configZone = Integer.parseInt(this.getCache().getTimeZone());
        return sysZone - configZone;
    }
}
