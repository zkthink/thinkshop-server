package com.think.cloud.thinkshop.mall.convert.design;


import com.think.cloud.thinkshop.mall.controller.admin.design.vo.IndexProductGroupDesignRespVO;
import com.think.cloud.thinkshop.mall.controller.app.design.vo.AppIndexProductGroupInfoVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * pageConfig Convert
 *
 * @author zkthink
 */
@Mapper
public interface IndexProductGroupConvert {

    IndexProductGroupConvert INSTANCE = Mappers.getMapper(IndexProductGroupConvert.class);

    List<AppIndexProductGroupInfoVO> convertList(List<IndexProductGroupDesignRespVO> vo);

}
