package com.think.cloud.thinkshop.mall.controller.admin.memberuser;


import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberTagQueryDTO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberTag;
import com.think.cloud.thinkshop.mall.service.memberuser.IMemberTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.think.common.log.annotation.Log;
import com.think.common.log.enums.BusinessType;
import com.think.common.security.annotation.RequiresPermissions;
import javax.validation.Valid;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.core.utils.poi.ExcelUtil;
import com.think.common.core.web.page.TableDataInfo;

/**
 * 会员标签Controller
 *
 * @author zkthink
 * @date 2024-05-15
 */
@RestController
@RequestMapping("/admin/member-tag")
@Api(tags = "ADMIN:会员标签")
public class MemberTagController extends BaseController
{
    @Autowired
    private IMemberTagService memberTagService;

    /**
     * 查询会员标签列表
     */
    @RequiresPermissions("mall:member-tag:list")
    @GetMapping("/list")
    @ApiOperation("会员标签列表")
    public TableDataInfo list(MemberTagQueryDTO dto)
    {
        startPage();
        List<MemberTag> list = memberTagService.selectMemberTagList(dto);
        return getDataTable(list);
    }

    /**
     * 导出会员标签列表
     */
    @RequiresPermissions("mall:member-tag:export")
    @Log(title = "会员标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation("会员标签导出")
    public void export(HttpServletResponse response, MemberTagQueryDTO dto)
    {
        List<MemberTag> list = memberTagService.selectMemberTagList(dto);
        ExcelUtil<MemberTag> util = new ExcelUtil<MemberTag>(MemberTag.class);
        util.exportExcel(response, list, "会员标签数据");
    }

    /**
     * 获取会员标签详细信息
     */
    @RequiresPermissions("mall:member-tag:query")
    @GetMapping(value = "/get/{id}")
    @ApiOperation("会员标签详细信息")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(memberTagService.selectMemberTagById(id));
    }

    /**
     * 新增会员标签
     */
    @RequiresPermissions("mall:member-tag:add")
    @Log(title = "会员标签", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("会员标签新增")
    public AjaxResult add(@RequestBody MemberTag memberTag)
    {
        return toAjax(memberTagService.insertMemberTag(memberTag));
    }

    /**
     * 修改会员标签
     */
    @RequiresPermissions("mall:member-tag:edit")
    @Log(title = "会员标签", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    @ApiOperation("会员标签修改")
    public AjaxResult edit(@RequestBody MemberTag memberTag)
    {
        return toAjax(memberTagService.updateMemberTag(memberTag));
    }

    /**
     * 删除会员标签
     */
    @RequiresPermissions("mall:member-tag:remove")
    @Log(title = "会员标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/delete")
    @ApiOperation("会员标签删除")
    public AjaxResult remove(@Valid Long[] ids)
    {
        return toAjax(memberTagService.batchDelete(Arrays.asList(ids)));
    }
}
