package com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

@Data
@ApiModel(description = "用户分组 新增、编辑的特征列表 RespVO")
@ToString(callSuper = true)
public class MemberGroupEditTraitRespVO {
    @ApiModelProperty(value = "名称", required = true, example = "")
    private String dictLabel;
    @ApiModelProperty(value = "序号", required = true, example = "")
    private String dictValue;

}
