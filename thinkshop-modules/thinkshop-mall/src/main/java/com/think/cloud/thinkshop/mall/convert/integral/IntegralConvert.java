package com.think.cloud.thinkshop.mall.convert.integral;


import com.think.cloud.thinkshop.mall.controller.app.integral.vo.IntegralRuleVO;
import com.think.cloud.thinkshop.mall.domain.integral.IntegralRule;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * banner Convert
 *
 * @author zkthink
 */
@Mapper
public interface IntegralConvert {

    IntegralConvert INSTANCE = Mappers.getMapper(IntegralConvert.class);

    IntegralRuleVO convert(IntegralRule dto);


}
