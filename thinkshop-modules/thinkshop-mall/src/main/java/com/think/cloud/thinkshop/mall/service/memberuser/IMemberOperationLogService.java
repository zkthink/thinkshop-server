package com.think.cloud.thinkshop.mall.service.memberuser;

import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationLog;

import java.util.List;

/**
 * 客户运营数据Service接口
 *
 * @author zkthink
 * @date 2024-06-14
 */
public interface IMemberOperationLogService
{
    /**
     * 查询客户运营数据
     *
     * @param id 客户运营数据主键
     * @return 客户运营数据
     */
    public MemberOperationLog selectMemberOperationLogById(Long id);

    /**
     * 查询客户运营数据列表
     *
     * @param memberOperationLog 客户运营数据
     * @return 客户运营数据集合
     */
    public List<MemberOperationLog> selectMemberOperationLogList(MemberOperationLog memberOperationLog);

    /**
     * 新增客户运营数据
     *
     * @param memberOperationLog 客户运营数据
     * @return 结果
     */
    public int insertMemberOperationLog(MemberOperationLog memberOperationLog);

    /**
     * 修改客户运营数据
     *
     * @param memberOperationLog 客户运营数据
     * @return 结果
     */
    public int updateMemberOperationLog(MemberOperationLog memberOperationLog);

    /**
     * 批量删除客户运营数据信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);


    /**
     * 客户运营数据运行
     */
    void run();
}
