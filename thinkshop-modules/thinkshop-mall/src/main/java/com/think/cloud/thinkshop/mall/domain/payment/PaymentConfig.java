package com.think.cloud.thinkshop.mall.domain.payment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.think.common.core.annotation.Excel;
import com.think.common.core.web.domain.BaseEntity;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 支付配置对象 mall_payment_config
 *
 * @author zkthink
 * @date 2024-05-14
 */
@TableName("mall_payment_config")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** logo */
    @Excel(name = "logo")
    private String logo;

    /** 状态 */
    @Excel(name = "状态")
    private Boolean status;

    /** 支付client_id */
    @Excel(name = "支付client_id")
    private String clientId;

    /** 支付密钥 */
    @Excel(name = "支付密钥")
    private String secret;

    /** webhook秘钥 */
    @Excel(name = "webhook秘钥")
    private String endpointSecret;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 支付编码 */
    @Excel(name = "支付编码")
    private String code;

}
