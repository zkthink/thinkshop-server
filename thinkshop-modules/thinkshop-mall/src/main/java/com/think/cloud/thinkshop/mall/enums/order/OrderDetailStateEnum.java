package com.think.cloud.thinkshop.mall.enums.order;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 订单明细状态枚举
 */
@Getter
@AllArgsConstructor
public enum OrderDetailStateEnum {

    NORMAL(1, "正常"),
    IN_AFTER_SALES(2, "售后中"),
    COMPLETED_AFTER_SALES(3, "售后完成");

    private Integer value;
    private String desc;


    public static OrderDetailStateEnum toEnum(Integer value) {
        return Stream.of(OrderDetailStateEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

}
