package com.think.cloud.thinkshop.mall.controller.admin.message.dto;


import lombok.*;

@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageTemplateDTO {
    private Long id;

    /**
     * 场景名称
     */
    private String sceneName;

    /**
     * 开启站内信,0:不使用 1：使用
     */
    private Integer useInform;

    /**
     * 开启邮件通知,0:不使用 1：使用
     */
    private Integer useEmail;

    /**
     * 站内信标题
     */
    private String informTitle;

    /**
     * 站内信内容
     */
    private String informTemplate;

    /**
     * 站内信标题
     */
    private String emailTitle;

    /**
     * 站内信内容
     */
    private String emailTemplate;

    private String spendEmail;

    private String spendInform;

}
