package com.think.cloud.thinkshop.mall.service.payment;

import java.util.List;
import com.think.cloud.thinkshop.mall.domain.payment.PaymentConfig;

/**
 * 支付配置Service接口
 *
 * @author zkthink
 * @date 2024-05-14
 */
public interface IPaymentConfigService
{
    /**
     * 查询支付配置
     *
     * @param id 支付配置主键
     * @return 支付配置
     */
    public PaymentConfig selectPaymentConfigById(Long id);

    /**
     * 查询支付配置列表
     *
     * @param paymentConfig 支付配置
     * @return 支付配置集合
     */
    public List<PaymentConfig> selectPaymentConfigList(PaymentConfig paymentConfig);

    /**
     * 新增支付配置
     *
     * @param paymentConfig 支付配置
     * @return 结果
     */
    public int insertPaymentConfig(PaymentConfig paymentConfig);

    /**
     * 修改支付配置
     *
     * @param paymentConfig 支付配置
     * @return 结果
     */
    public int updatePaymentConfig(PaymentConfig paymentConfig);

    /**
     * 批量删除支付配置信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    public int batchDelete(List<Long> ids);
}
