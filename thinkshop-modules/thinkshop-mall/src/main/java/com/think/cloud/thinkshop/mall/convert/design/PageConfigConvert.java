package com.think.cloud.thinkshop.mall.convert.design;


import com.think.cloud.thinkshop.mall.controller.admin.design.vo.PageConfigAddOrEditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.PageConfigPageBottomRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.design.vo.PageConfigPageTopRespVO;
import com.think.cloud.thinkshop.mall.domain.design.PageConfig;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * pageConfig Convert
 *
 * @author zkthink
 */
@Mapper
public interface PageConfigConvert {

    PageConfigConvert INSTANCE = Mappers.getMapper(PageConfigConvert.class);

    PageConfigPageTopRespVO convert(PageConfig dto);
    PageConfigPageBottomRespVO convert1(PageConfig dto);
    PageConfig convert2(PageConfigAddOrEditReqVO dto);
}
