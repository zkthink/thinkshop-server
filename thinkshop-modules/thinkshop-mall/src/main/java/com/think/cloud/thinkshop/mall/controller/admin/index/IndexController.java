package com.think.cloud.thinkshop.mall.controller.admin.index;

import com.think.cloud.thinkshop.mall.controller.admin.index.vo.IndexStatisticsReqVO;
import com.think.cloud.thinkshop.mall.service.statistics.IStatisticsService;
import com.think.common.core.web.controller.BaseController;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/index")
@Api(tags = "ADMIN:首页统计")
public class IndexController extends BaseController {
    @Autowired
    private IStatisticsService statisticsService;

    @RequiresPermissions("mall:index:query")
    @GetMapping(value = "/statistics")
    @ApiOperation(value = "获取首页统计面板")
    public AjaxResult statistics(IndexStatisticsReqVO vo) {
        return success(statisticsService.indexStatistics(vo));
    }

}
