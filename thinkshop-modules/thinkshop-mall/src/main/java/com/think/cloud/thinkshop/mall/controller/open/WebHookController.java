package com.think.cloud.thinkshop.mall.controller.open;

import com.think.cloud.thinkshop.mall.service.order.ILogisticsLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zkthink
 * @apiNote
 **/
@RestController
@RequestMapping("/webhook")
@Slf4j
public class WebHookController {
    @Autowired
    private ILogisticsLogService logisticsLogService;

    @PostMapping("17track")
    public ResponseEntity<Void> webHook17Track(@RequestBody String body) {
        log.info("17track webhook:{}", body);
        logisticsLogService.webHook17Track(body);
        return ResponseEntity.ok().build();
    }

}
