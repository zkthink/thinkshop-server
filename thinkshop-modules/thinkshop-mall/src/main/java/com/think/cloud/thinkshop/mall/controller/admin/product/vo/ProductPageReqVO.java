package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.List;


/**
 * 商品查询请求VO
 *
 * @author zkthink
 * @date 2024-05-09
 */
@Data
public class ProductPageReqVO {

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品编码
     */
    private String productCode;


    /**
     * 类目id
     */
    private Long categoryId;

    /**
     * 状态（0：未上架，1：上架）
     */
    private Integer isShow;

    /**
     * 开始时间
     */
    @DateTimeFormat
    private Timestamp startTime;

    /**
     * 结束时间
     */
    @DateTimeFormat
    private Timestamp endTime;

    /**
     * 商品id集合
     */
    private List<Long> productIds;

    /**
     * 商品名称或商品编码搜索
     */
    private String key;

    /**
     * 是否已关联
     */
    private Integer isRelation;
}
