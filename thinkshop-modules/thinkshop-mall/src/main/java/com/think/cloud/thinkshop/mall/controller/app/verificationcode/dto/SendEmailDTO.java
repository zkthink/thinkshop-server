package com.think.cloud.thinkshop.mall.controller.app.verificationcode.dto;

import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class SendEmailDTO {

    private String email;
}
