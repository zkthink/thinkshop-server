package com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class MemberGroupUserTraitDetailDTO {
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 最后支付时间
     */
    private LocalDateTime lastBuyTime;
    /**
     * 最后下单时间
     */
    private LocalDateTime lastOrderTime;
    /**
     * 最后加购时间
     */
    private LocalDateTime lastCarTime;
    /**
     * 最后访问时间
     */
    private LocalDateTime lastVisitTime;
    /**
     * 购买次数
     */
    private Integer buyCount;
    /**
     * 购买金额
     */
    private Float buyAmount;
    /**
     * 标签
     */
    private List<Long> tags=new ArrayList<>();


}
