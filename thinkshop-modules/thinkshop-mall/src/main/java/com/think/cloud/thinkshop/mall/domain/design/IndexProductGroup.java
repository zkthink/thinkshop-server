package com.think.cloud.thinkshop.mall.domain.design;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

@TableName("mall_index_product_group")
@Data
@ToString(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IndexProductGroup {
    private Long id;
    /**
     * 产品分组id
     */
    private Long groupId;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 是否删除
     */
    private Integer deleted;
}
