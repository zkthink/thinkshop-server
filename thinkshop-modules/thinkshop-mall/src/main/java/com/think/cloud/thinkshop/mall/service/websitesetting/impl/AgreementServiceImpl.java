package com.think.cloud.thinkshop.mall.service.websitesetting.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.think.cloud.thinkshop.common.enums.websitesetting.AgreementEnum;
import com.think.cloud.thinkshop.mall.controller.admin.websitesetting.dto.AgreementDTO;
import com.think.cloud.thinkshop.mall.domain.websitesetting.Agreement;
import com.think.cloud.thinkshop.mall.mapper.websitesetting.AgreementMapper;
import com.think.cloud.thinkshop.mall.service.websitesetting.IAgreementService;
import com.think.common.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 协议Service业务层处理
 *
 * @author zkthink
 * @date 2024-05-27
 */
@Service
public class AgreementServiceImpl implements IAgreementService {
    @Autowired
    private AgreementMapper agreementMapper;

    /**
     * 查询协议
     *
     * @param id 协议主键
     * @return 协议
     */
    @Override
    public Agreement selectAgreementById(Long id) {
        return agreementMapper.selectById(id);
    }

    /**
     * 查询协议列表
     *
     * @param agreement 协议
     * @return 协议
     */
    @Override
    public List<Agreement> selectAgreementList(Agreement agreement) {
        return agreementMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增协议
     *
     * @param agreement 协议
     * @return 结果
     */
    @Override
    public int insertAgreement(Agreement agreement) {

        return agreementMapper.insert(agreement);
    }

    /**
     * 修改协议
     *
     * @param agreement 协议
     * @return 结果
     */
    @Override
    public int updateAgreement(Agreement agreement) {
        return agreementMapper.updateById(agreement);
    }

    /**
     * 批量删除协议信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return agreementMapper.deleteBatchIds(ids);
    }

    @Override
    public AgreementDTO getAgreement() {
        List<Agreement> agreements = agreementMapper.selectList(new LambdaQueryWrapper<>());
        AgreementDTO agreementDTO = new AgreementDTO();

        if (agreements != null && !agreements.isEmpty()) {
            Map<Integer, String> map = agreements.stream().collect(Collectors.toMap(Agreement::getType, Agreement::getContent));
            agreementDTO.setUserAgreement(map.get(AgreementEnum.AGREEMENT_TYPE_1.getCode()));
            agreementDTO.setPrivacyPolicy(map.get(AgreementEnum.AGREEMENT_TYPE_2.getCode()));
        }
        return agreementDTO;
    }

    @Override
    public void saveAgreement(AgreementDTO dto) {
        if(StringUtils.isNotBlank(dto.getUserAgreement())){
            agreementMapper.update(null,new LambdaUpdateWrapper<Agreement>().eq(Agreement::getType,AgreementEnum.AGREEMENT_TYPE_1.getCode()).set(Agreement::getContent,dto.getUserAgreement()));
        }
        if(StringUtils.isNotBlank(dto.getPrivacyPolicy())){
            agreementMapper.update(null,new LambdaUpdateWrapper<Agreement>().eq(Agreement::getType,AgreementEnum.AGREEMENT_TYPE_2.getCode()).set(Agreement::getContent,dto.getPrivacyPolicy()));
        }
    }

    @Override
    public String getAgreementByType(Integer type) {
        Agreement agreement = agreementMapper.selectOne(new LambdaQueryWrapper<Agreement>().eq(Agreement::getType, type));
        return agreement.getContent();
    }
}
