package com.think.cloud.thinkshop.mall.controller.app.payment.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class ToPayDTO {
    /**
     * 订单id
     */
    @ApiModelProperty("订单id")
    private Long orderId;

    /**
     * 支付方式
     */
    @ApiModelProperty("支付方式 paypal stripe")
    private String payMethod;
}
