package com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 运费计算
 *
 * @author zkthink
 * @apiNote
 **/
@Data
@Builder
public class FreightComputeDTO {

    @ApiModelProperty("商品id")
    private List<Long> productIds;

    @ApiModelProperty("地区ID")
    private String regionId;

    @ApiModelProperty("订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty("重量")
    private BigDecimal wight;

}
