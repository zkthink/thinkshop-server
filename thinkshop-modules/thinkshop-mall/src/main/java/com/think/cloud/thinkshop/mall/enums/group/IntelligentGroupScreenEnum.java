package com.think.cloud.thinkshop.mall.enums.group;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @author zkthink
 * 分组智能筛选枚举
 */
@Getter
@AllArgsConstructor
public enum IntelligentGroupScreenEnum {

    SCREEN_AND(0, "and"),
    SCREEN_OR(1, "or");

    private Integer type;
    private String value;


    public static IntelligentGroupScreenEnum toType(int type) {
        return Stream.of(IntelligentGroupScreenEnum.values())
                .filter(p -> p.type == type)
                .findAny()
                .orElse(null);
    }


}
