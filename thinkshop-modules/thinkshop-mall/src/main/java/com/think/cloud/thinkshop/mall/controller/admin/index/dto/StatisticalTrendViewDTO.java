package com.think.cloud.thinkshop.mall.controller.admin.index.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StatisticalTrendViewDTO {
    private String dateStr;
    private BigDecimal value;
}
