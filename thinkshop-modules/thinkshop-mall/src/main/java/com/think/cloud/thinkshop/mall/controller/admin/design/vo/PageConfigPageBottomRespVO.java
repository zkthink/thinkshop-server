package com.think.cloud.thinkshop.mall.controller.admin.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import java.util.List;


@Data
@Schema(description = "管理后台 - 页脚 Resp VO")
@ToString(callSuper = true)
public class PageConfigPageBottomRespVO {
    @Schema(description = "序号", required = true, example = "1")
    private Long id;

    @Schema(description = "页脚图片地址", required = true, example = "http://xxxxx")
    private String pageBottomUrl;

    @Schema(description = "页脚二维码地址", required = true, example = "http://xxxxx")
    private List<String> pageBottomQrCodeUrls;

    @Schema(description = "版权备案号", required = true, example = "xxxxxx")
    private String copyright;

    @Schema(description = "电话", required = true, example = "1234567890")
    private String phone;

    @Schema(description = "邮箱", required = true, example = "122@qq.com")
    private String email;

    @Schema(description = "联系地址", required = true, example = "1")
    private String address;

    @Schema(description = "其他展示：\n" +
            "1：法律条款，\n" +
            "2：隐私政策，\n" +
            "3：法律声明，\n" +
            "4：付款，\n" +
            "5：配送，\n" +
            "6：换货及退货，\n" +
            "7：关于订单，\n" +
            "8：产品维护，\n" +
            "9：产品维修，", required = true, example = "1")
    private Integer other;
    @Schema(description = "other 的内容", required = true, example = "1")
    private String otherComment;
}
