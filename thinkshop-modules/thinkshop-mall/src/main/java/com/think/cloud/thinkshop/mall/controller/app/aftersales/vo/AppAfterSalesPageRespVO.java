package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import com.think.cloud.thinkshop.mall.annotation.CrossBorderZoneTimeAmend;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.sql.Timestamp;
import java.util.List;

/**
 * 售后分页响应VO
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Data
public class AppAfterSalesPageRespVO {

    @ApiModelProperty("售后单id")
    private Long id;

    @ApiModelProperty("订单编号")
    private String orderCode;

    @ApiModelProperty("下单时间")
    @CrossBorderZoneTimeAmend
    private Timestamp orderCreateTime;

    @ApiModelProperty("申请时间")
    @CrossBorderZoneTimeAmend
    private Timestamp createTime;

    @ApiModelProperty("状态：-2、商家拒绝，-1、用户取消，0、已提交等待平台审核，1、平台已审核 等待用户发货，2、用户已发货，3、退款成功")
    private Integer state;

    @ApiModelProperty("售后明细")
    private List<AppAfterSalesDetailRespVO> details;

}
