package com.think.cloud.thinkshop.mall.service.aftersales;

import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesAuditReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesInfoRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.aftersales.vo.AfterSalesPageReqVO;
import com.think.cloud.thinkshop.mall.controller.admin.index.dto.StatisticalTrendViewDTO;
import com.think.common.core.web.page.TableDataInfo;

import java.util.List;

/**
 * 售后记录Service接口
 * 
 * @author zkthink
 * @date 2024-06-12
 */
public interface IAfterSalesService 
{
    /**
     * 查询售后记录
     * 
     * @param id 售后记录主键
     * @return 售后记录
     */
    public AfterSalesInfoRespVO selectAfterSalesById(Long id);

    /**
     * 查询售后记录列表
     * 
     * @param vo
     * @return 售后记录集合
     */
    public TableDataInfo selectAfterSalesList(AfterSalesPageReqVO vo);

    /**
     * 审核
     *
     * @param vo
     * @return
     */
    public void audit(AfterSalesAuditReqVO vo);

    /**
     * 删除
     * @param id
     */
    public int delete(Long id);

    /**
     * 售后订单数 趋势
     * @return
     */
    List<StatisticalTrendViewDTO> saledOrderCountTrend(String startTime, String endTime);

    /**
     * 售后订单金额 趋势
     * @return
     */
    List<StatisticalTrendViewDTO> saledOrderAmountTrend(String startTime,String endTime);

}
