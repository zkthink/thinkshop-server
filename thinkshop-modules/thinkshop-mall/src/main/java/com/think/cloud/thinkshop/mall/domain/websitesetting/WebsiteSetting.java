package com.think.cloud.thinkshop.mall.domain.websitesetting;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 网站设置对象 mall_website_setting
 *
 * @author zkthink
 * @date 2024-05-14
 */
@Data
@TableName("mall_website_setting")
@ApiModel("网站设置")
public class WebsiteSetting implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 网站名称
     */
    @ApiModelProperty("网站名称")
    private String websiteName;

    /**
     * 网站logo
     */
    @ApiModelProperty("网站logo")
    private String websiteLogo;

    /**
     * 货币编码
     */
    @ApiModelProperty("货币编码")
    private String currencyCode;

    /**
     * 时区
     */
    @ApiModelProperty("时区")
    private String timeZone;

    /**
     * 网站语言
     */
    @ApiModelProperty("网站语言")
    private String languageCode;

    /**
     * 地址联系人
     */
    @ApiModelProperty("地址联系人")
    private String addressUsername;

    /**
     * 国家编码
     */
    @ApiModelProperty("国家编码")
    private String addressNation;

    /**
     * 省份
     */
    @ApiModelProperty("省份")
    private String addressProvince;

    /**
     * 城市
     */
    @ApiModelProperty("城市")
    private String addressCity;

    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String addressDetails;

    /**
     * 联系人手机号
     */
    @ApiModelProperty("联系人手机号")
    private String addressPhoneNumber;

    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    private String addressPostcode;

    /**
     * 客户默认头像
     */
    @ApiModelProperty("客户默认头像")
    private String defaultAvatar;

    /**
     * 销售税率
     */
    @ApiModelProperty("销售税率")
    private BigDecimal salesTaxRate;

    /**
     * 运费税率
     */
    @ApiModelProperty("运费税率")
    private BigDecimal freightRate;
    /**
     * 订单自动取消小时
     */
    @ApiModelProperty("订单自动取消小时")
    private Integer orderCancelHour;
    /**
     * 自动收货天数
     */
    @ApiModelProperty("自动收货天数")
    private Integer automaticReceiptDays;
    /**
     * 客户确认收货x天后，不可进行售后
     */
    @ApiModelProperty("客户确认收货x天后，不可进行售后")
    private Integer closeAfterSale;
    /**
     * 同意退货退款后,x天客户未寄回商品，则售后自动取消
     */
    @ApiModelProperty("同意退货退款后,x天客户未寄回商品，则售后自动取消")
    private Integer automaticCancellationAfterSale;

    @ApiModelProperty("订单完成后X天不能评价")
    private Integer passDayRejectComment;
    /**
     * 货币符号
     */
    @ApiModelProperty("货币符号")
    private String currencySymbol;
}
