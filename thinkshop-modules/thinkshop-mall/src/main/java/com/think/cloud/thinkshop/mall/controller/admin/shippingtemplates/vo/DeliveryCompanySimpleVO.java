package com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.vo;

import lombok.Data;

/**
 * @author zkthink
 * @apiNote
 **/
@Data
public class DeliveryCompanySimpleVO {
    private String label;

    private String value;
}
