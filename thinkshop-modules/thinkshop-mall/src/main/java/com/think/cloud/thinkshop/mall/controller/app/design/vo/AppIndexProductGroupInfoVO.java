package com.think.cloud.thinkshop.mall.controller.app.design.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
public class AppIndexProductGroupInfoVO {
    private Long id;
    @Schema(description = "组id", required = true, example = "1")
    private Long groupId;
    @Schema(description = "组名称", required = true, example = "xxx")
    private String name;
    @Schema(description = "组描述", required = true, example = "xxx")
    private String remark;

    @Schema(description = "商品详情", required = true, example = "xxx")
    private List<AppIndexProductGroupDetailVO> productDetail;
}
