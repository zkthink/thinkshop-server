package com.think.cloud.thinkshop.mall.mapper.memberuser;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberOperationLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 客户运营数据Mapper接口
 *
 * @author zkthink
 * @date 2024-06-14
 */
@Mapper
public interface MemberOperationLogMapper extends BaseMapper<MemberOperationLog> {

}
