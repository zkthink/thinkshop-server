package com.think.cloud.thinkshop.mall.controller.admin.coupon.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class ProductCouponPageReqVO {

    @ApiModelProperty("优惠券编号")
    private String couponCode;

    @ApiModelProperty("优惠券名称")
    private String couponName;

    @ApiModelProperty("优惠券类型：1、满减券，2、折扣券")
    private Integer couponType;

    @ApiModelProperty("优惠券状态：0、未开始，1、进行中，2、已结束")
    private Integer status;

}
