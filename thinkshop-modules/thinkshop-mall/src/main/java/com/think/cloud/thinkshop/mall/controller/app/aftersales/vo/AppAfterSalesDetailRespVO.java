package com.think.cloud.thinkshop.mall.controller.app.aftersales.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 售后详情响应VO
 *
 * @author zkthink
 * @date 2024-06-12
 */
@Data
public class AppAfterSalesDetailRespVO {

    @ApiModelProperty("售后id")
    private Long AfterSalesId;

    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("规格")
    private String sku;

    @ApiModelProperty("规格图片")
    private String image;

    @ApiModelProperty("产品图片")
    private String productImage;

    @ApiModelProperty("商品数量")
    private Integer num;

    @ApiModelProperty("原价")
    private BigDecimal originalPrice;

    @ApiModelProperty("实际价格")
    private BigDecimal price;

    @ApiModelProperty("状态：1、正常，2、售后中，3、售后完成")
    private Integer state;

}
