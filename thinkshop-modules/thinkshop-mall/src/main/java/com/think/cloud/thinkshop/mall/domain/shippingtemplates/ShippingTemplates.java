package com.think.cloud.thinkshop.mall.domain.shippingtemplates;

import java.math.BigDecimal;
import com.think.common.core.annotation.Excel;
import lombok.*;
import com.baomidou.mybatisplus.annotation.*;

/**
 * 物流方案对象 mall_shipping_templates
 *
 * @author zkthink
 * @date 2024-05-15
 */
@TableName("mall_shipping_templates")
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShippingTemplates
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 方案名称 */
    @Excel(name = "方案名称")
    private String name;

    /** 方案介绍 */
    @Excel(name = "方案介绍")
    @TableField(value = "`desc`")
    private String desc;

    /** 计费方式 */
    @Excel(name = "计费方式")
    private Integer chargingType;

    /** 首件/首重 */
    @Excel(name = "首件/首重")
    private BigDecimal firstPiece;

    /** 运费 */
    @Excel(name = "运费")
    private BigDecimal firstPrice;

    /** 续件 */
    @Excel(name = "续件")
    private BigDecimal otherPiece;

    /** 续费 */
    @Excel(name = "续费")
    private BigDecimal otherPrice;

    /** 固定运费 */
    @Excel(name = "固定运费")
    private BigDecimal fixedPrice;

    /** 开启免运费 */
    @Excel(name = "开启免运费")
    private Boolean enableFree;

    /** 订单金额大于xx */
    @Excel(name = "订单金额大于xx")
    private BigDecimal orderAmount;

    /** 订单数量大于xx */
    @Excel(name = "订单数量大于xx")
    private Long orderItemCount;

    /** 是否默认方案 */
    @Excel(name = "是否默认方案")
    private Boolean isDefault;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

}
