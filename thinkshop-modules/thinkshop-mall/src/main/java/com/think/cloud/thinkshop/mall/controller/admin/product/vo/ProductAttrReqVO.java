package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;


/**
 * 商品属性请求VO
 *
 * @author zkthink
 * @date 2024-05-09
 */

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductAttrReqVO {


    /**
     * 属性名
     */
    @ApiModelProperty("属性名")
    private String value;

    /**
     * 属性值
     */
    @ApiModelProperty("属性值")
    private List<String> detail;

}
