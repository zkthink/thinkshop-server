package com.think.cloud.thinkshop.mall.mapper.design;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.domain.design.IndexDesign;
import org.apache.ibatis.annotations.Mapper;


/**
 * IndexDesignMapper
 *
 * @author dengguanxian
 * @date 2024-05-30
 */
@Mapper
public interface IndexDesignMapper extends BaseMapper<IndexDesign> {

}

