package com.think.cloud.thinkshop.mall.controller.app.cart.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.math.BigDecimal;

/**
 * 购物车响应VO
 *
 * @author zkthink
 * @date 2024-06-17
 */
@Data
public class ShopCartRespVO {
    /**
     * 购物车ID
     */
    @ApiModelProperty("购物车ID")
    private Long cartId;

    /**
     * 商品ID
     */
    @ApiModelProperty("商品ID")
    private Long productId;

    /**
     * 商品名称
     */
    @ApiModelProperty("商品名称")
    private String productName;

    /**
     * 商品属性id
     */
    @ApiModelProperty("商品属性id")
    private Long skuId;

    /**
     * 规格
     */
    @ApiModelProperty("规格")
    private String sku;

    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private BigDecimal price;

    /**
     * 规格图片
     */
    @ApiModelProperty("规格图片")
    private String image;

    /**
     * 商品数量
     */
    @ApiModelProperty("商品数量")
    private Integer num;

}
