package com.think.cloud.thinkshop.mall.service.shippingtemplates.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.db.DbUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.think.cloud.thinkshop.mall.controller.admin.shippingtemplates.vo.DeliveryCompanySimpleVO;
import com.think.cloud.thinkshop.mall.domain.shippingtemplates.DeliveryCompany;
import com.think.cloud.thinkshop.mall.mapper.shippingtemplates.DeliveryCompanyMapper;
import com.think.cloud.thinkshop.mall.service.shippingtemplates.IDeliveryCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.web.multipart.MultipartFile;

/**
 * 快递公司Service业务层处理
 *
 * @author zkthink
 * @date 2024-06-05
 */
@Service
public class DeliveryCompanyServiceImpl implements IDeliveryCompanyService {
    @Autowired
    private DeliveryCompanyMapper deliveryCompanyMapper;

    /**
     * 查询快递公司
     *
     * @param id 快递公司主键
     * @return 快递公司
     */
    @Override
    public DeliveryCompany selectDeliveryCompanyById(Long id) {
        return deliveryCompanyMapper.selectById(id);
    }

    /**
     * 查询快递公司列表
     *
     * @param deliveryCompany 快递公司
     * @return 快递公司
     */
    @Override
    public List<DeliveryCompany> selectDeliveryCompanyList(DeliveryCompany deliveryCompany) {
        return deliveryCompanyMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 新增快递公司
     *
     * @param deliveryCompany 快递公司
     * @return 结果
     */
    @Override
    public int insertDeliveryCompany(DeliveryCompany deliveryCompany) {

        return deliveryCompanyMapper.insert(deliveryCompany);
    }

    /**
     * 修改快递公司
     *
     * @param deliveryCompany 快递公司
     * @return 结果
     */
    @Override
    public int updateDeliveryCompany(DeliveryCompany deliveryCompany) {
        return deliveryCompanyMapper.updateById(deliveryCompany);
    }

    /**
     * 批量删除快递公司信息
     *
     * @param ids 主键集合
     * @return 结果
     */
    @Override
    public int batchDelete(List<Long> ids) {
        return deliveryCompanyMapper.deleteBatchIds(ids);
    }

    @Override
    public List<DeliveryCompanySimpleVO> simpleList() {
        List<DeliveryCompany> deliveryCompanies = deliveryCompanyMapper.selectList(new LambdaQueryWrapper<>());
        return deliveryCompanies.stream().map(item -> {
                    DeliveryCompanySimpleVO vo = new DeliveryCompanySimpleVO();
                    vo.setLabel(item.getNameZhCn());
                    vo.setValue(item.getKey());
                    return vo;
                })
                .collect(Collectors.toList());
    }

    @Override
    public void importData(MultipartFile file) {
        //文件内容
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String string = new String(bytes, StandardCharsets.UTF_8);
        JSONArray jsonArray = new JSONArray(string);
        List<DeliveryCompany> list = jsonArray.stream()
                .map(DeliveryCompanyServiceImpl::convert)
                .collect(Collectors.toList());
        //删除 保存新数据
        deliveryCompanyMapper.deleteAll();
        Db.saveBatch(list,list.size());
    }

    private static DeliveryCompany convert(Object item) {
        JSONObject jsonObject = new JSONObject(item);
        DeliveryCompany deliveryCompany = new DeliveryCompany();
        deliveryCompany.setKey(jsonObject.getStr("key"));
        deliveryCompany.setCountry(jsonObject.getStr("_country"));
        deliveryCompany.setCountryIso(jsonObject.getStr("_country_iso"));
        deliveryCompany.setEmail(jsonObject.getStr("_email"));
        deliveryCompany.setTel(jsonObject.getStr("_tel"));
        deliveryCompany.setUrl(jsonObject.getStr("_url"));
        deliveryCompany.setName(jsonObject.getStr("_name"));
        deliveryCompany.setNameZhCn(jsonObject.getStr("_name_zh-cn"));
        deliveryCompany.setNameZhHk(jsonObject.getStr("_name_zh-hk"));
        deliveryCompany.setGroup(jsonObject.getStr("_group"));
        return deliveryCompany;
    }
}
