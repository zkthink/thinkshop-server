package com.think.cloud.thinkshop.mall.controller.admin.product.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * 商品属性响应VO
 *
 * @author zkthink
 * @date 2024-05-09
 */

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductAttrRespVO {


    /** id */
    @ApiModelProperty("id")
    private Long attrId;

    /** 商品ID */
    @ApiModelProperty("商品ID")
    private Long productId;

    /** 属性名 */
    @ApiModelProperty("属性名")
    private String value;

    /** 属性值 */
    @ApiModelProperty("属性值")
    private List<String> detail;

}
