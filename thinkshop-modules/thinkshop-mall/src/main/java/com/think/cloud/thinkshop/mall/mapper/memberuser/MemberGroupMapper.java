package com.think.cloud.thinkshop.mall.mapper.memberuser;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.dto.MemberGroupUserTraitDetailDTO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberGroupEditTraitRespVO;
import com.think.cloud.thinkshop.mall.controller.admin.memberuser.vo.MemberGroupUserRespVO;
import com.think.cloud.thinkshop.mall.domain.memberuser.MemberGroup;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 客户分群Mapper接口
 *
 * @author guangxian
 * @date 2024-06-03
 */
@Mapper
public interface MemberGroupMapper extends BaseMapper<MemberGroup> {

    @Select("select dict_label,dict_value from sys_dict_data where " +
            "dict_type='group_trait_conditions' and status='0' " +
            "order by dict_sort asc")
    List<MemberGroupEditTraitRespVO> getTraitList();

    /**
     * 查询人群组中的成员
     *
     * @param groupId
     * @return
     */
    @Select("select t2.id,t2.first_name,t2.last_name,t2.email,t2.create_time ,t2.order_count,t2.total_order_amount,t2.last_consume_time \n" +
            "from\n" +
            "(select member_group_id,user_id from mall_member_group_user_ref mmgur \n" +
            "where  member_group_id=#{groupId}) t1\n" +
            "left join mall_member_user t2 on t1.user_id=t2.id")
    List<MemberGroupUserRespVO> memberGroupUserList(@Param("groupId") Long groupId);

    /**
     * 删除 groupId 对应的人群组-用户关系
     *
     * @param groupId
     * @return
     */
    @Delete("DELETE FROM mall_member_group_user_ref\n" +
            "WHERE member_group_id=#{groupId}")
    int deleteMemberGroupRefByGroupId(@Param("groupId") Long groupId);

    /**
     * 批量插入 人群-用户关系
     *
     * @param groupId
     * @param vo
     * @return
     */
    @Insert("<script>insert into mall_member_group_user_ref (member_group_id, user_id, join_time)\n" +
            "values" +
            "<foreach collection=\"list\" item=\"item\" separator=\",\" >" +
            "(#{groupId},#{item.userId},sysdate())" +
            "</foreach>" +
            "</script>")
    int insertMemberGroupRef(@Param("groupId") Long groupId, @Param("list") List<MemberGroupUserTraitDetailDTO> vo);

    /**
     * 查询用户所在的人群组的id
     *
     * @param userId
     * @return
     */
    @Select("select member_group_id from mall_member_group_user_ref where user_id=#{userId}")
    List<Long> selectGroupIdByUserId(@Param("userId") Long userId);

    /**
     * 删除人群组-用户关系（通过组id,用户id）
     *
     * @param groupId
     * @param userId
     * @return
     */
    @Delete("DELETE FROM mall_member_group_user_ref\n" +
            "WHERE member_group_id=#{groupId} and user_id=#{userId}")
    int deleteGroupRef(@Param("groupId") Long groupId, @Param("userId") Long userId);

    /**
     * 插入人群组-用户关系
     *
     * @param groupId
     * @param userId
     * @return
     */
    @Insert("insert into mall_member_group_user_ref (member_group_id,user_id,join_time)\n " +
            "values(#{groupId},#{userId},sysdate())")
    int addGroupRef(@Param("groupId") Long groupId, @Param("userId") Long userId);

    /**
     * 更新人群组中人数
     *
     * @param groupId
     * @param value
     * @return
     */
    @Update("UPDATE mall_member_group\n" +
            "SET user_count=user_count+#{value}\n" +
            "WHERE id=#{groupId}")
    int updateGroupMemberCount(@Param("groupId") Long groupId, @Param("value") Integer value);



    /**
     * 查询客户分群中所有客户
     * @param groupIdList /
     * @return /
     */
    List<Long> selectUseridListByGroupIds(@Param("groupIdList") List<Long> groupIdList);
}
