package com.think.cloud.thinkshop.gen.log;

import com.think.cloud.thinkshop.system.api.RemoteLogService;
import com.think.cloud.thinkshop.system.api.domain.SysOperLog;
import com.think.common.core.constant.SecurityConstants;
import com.think.common.core.domain.BaseOperLog;
import com.think.common.log.service.AsyncLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MyAsyncLogService implements AsyncLogService {

    @Autowired
    private RemoteLogService remoteLogService;

    /**
     * 保存系统日志记录
     */
    @Override
    @Async
    public void saveSysLog(BaseOperLog baseOperLog) throws Exception {
        SysOperLog sysOperLog = new SysOperLog();
        BeanUtils.copyProperties(baseOperLog, sysOperLog);
        remoteLogService.saveLog(sysOperLog, SecurityConstants.INNER);
    }
}
