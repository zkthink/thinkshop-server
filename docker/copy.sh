#!/bin/sh

# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh copy.sh"
	exit 1
}


# copy sql
echo "begin copy sql "
cp ../sql/ry_20231130.sql ./mysql/db
cp ../sql/ry_config_20231204.sql ./mysql/db

# copy html
echo "begin copy html "
cp -r ../thinkshop-ui/dist/** ./nginx/html/dist


# copy jar
echo "begin copy thinkshop-gateway "
cp ../thinkshop-gateway/target/thinkshop-gateway.jar ./thinkshop/gateway/jar

echo "begin copy thinkshop-auth "
cp ../thinkshop-auth/target/thinkshop-auth.jar ./thinkshop/auth/jar

echo "begin copy thinkshop-visual "
cp ../thinkshop-visual/thinkshop-monitor/target/thinkshop-monitor.jar  ./thinkshop/visual/monitor/jar

echo "begin copy thinkshop-modules-system "
cp ../thinkshop-modules/thinkshop-system/target/thinkshop-system.jar ./thinkshop/modules/system/jar

echo "begin copy thinkshop-modules-mall "
cp ../thinkshop-modules/thinkshop-mall/target/thinkshop-mall.jar ./thinkshop/modules/mall/jar


echo "begin copy thinkshop-modules-file "
cp ../thinkshop-modules/thinkshop-file/target/thinkshop-file.jar ./thinkshop/modules/file/jar

echo "begin copy thinkshop-modules-job "
cp ../thinkshop-modules/thinkshop-job/target/thinkshop-job.jar ./thinkshop/modules/job/jar

echo "begin copy thinkshop-modules-gen "
cp ../thinkshop-modules/thinkshop-gen/target/thinkshop-gen.jar ./thinkshop/modules/gen/jar

