package com.think.cloud.thinkshop.auth.config;

import me.zhyd.oauth.config.AuthConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@ConfigurationProperties(
    prefix = "justauth"
)
@Configuration
public class JustAuthProperties {
    private boolean enabled;
    private Map<String, AuthConfig> type = new HashMap();

    public JustAuthProperties() {
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public Map<String, AuthConfig> getType() {
        return this.type;
    }



    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setType(Map<String, AuthConfig> type) {
        this.type = type;
    }

    public static class JustAuthHttpConfig {
        private int timeout;
        private Map<String, JustAuthProxyConfig> proxy;

        public JustAuthHttpConfig() {
        }

        public int getTimeout() {
            return this.timeout;
        }

        public Map<String, JustAuthProxyConfig> getProxy() {
            return this.proxy;
        }

        public void setTimeout(int timeout) {
            this.timeout = timeout;
        }

        public void setProxy(Map<String, JustAuthProxyConfig> proxy) {
            this.proxy = proxy;
        }
    }

    public static class JustAuthProxyConfig {
        private String type;
        private String hostname;
        private int port;

        public JustAuthProxyConfig() {
            this.type = "HTTP";
        }

        public String getType() {
            return this.type;
        }

        public String getHostname() {
            return this.hostname;
        }

        public int getPort() {
            return this.port;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setHostname(String hostname) {
            this.hostname = hostname;
        }

        public void setPort(int port) {
            this.port = port;
        }
    }
}
