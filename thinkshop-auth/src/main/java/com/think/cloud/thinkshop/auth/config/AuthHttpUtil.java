package com.think.cloud.thinkshop.auth.config;


import com.xkcoding.http.HttpUtil;
import com.xkcoding.http.support.hutool.HutoolImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;

/**
 * @author zkthink
 * @apiNote
 **/
@Component
public class AuthHttpUtil {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Bean
    public RedisStateCache redisStateCache() {
        CacheProperties cacheProperties = new CacheProperties();
        cacheProperties.setTimeout(Duration.ofMinutes(60));
        cacheProperties.setType(CacheProperties.CacheType.REDIS);
        return new RedisStateCache(redisTemplate, cacheProperties);
    }


    @PostConstruct
    public void initHttpUtil() {
        HttpUtil.setHttp(new HutoolImpl());
    }
}
