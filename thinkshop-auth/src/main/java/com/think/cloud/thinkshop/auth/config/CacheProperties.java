package com.think.cloud.thinkshop.auth.config;

import java.time.Duration;

public class CacheProperties {
    private CacheType type;
    private String prefix;
    private Duration timeout;

    public CacheProperties() {
        this.type = CacheProperties.CacheType.DEFAULT;
        this.prefix = "JUSTAUTH::STATE::";
        this.timeout = Duration.ofMinutes(3L);
    }

    public CacheType getType() {
        return this.type;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public Duration getTimeout() {
        return this.timeout;
    }

    public void setType(CacheType type) {
        this.type = type;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setTimeout(Duration timeout) {
        this.timeout = timeout;
    }

    public static enum CacheType {
        DEFAULT,
        REDIS,
        CUSTOM;

        private CacheType() {
        }

        public String toString() {
            return "CacheProperties.CacheType." + this.name();
        }
    }
}
