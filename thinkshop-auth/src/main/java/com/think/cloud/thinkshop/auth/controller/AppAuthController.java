package com.think.cloud.thinkshop.auth.controller;

import com.alibaba.fastjson.JSONObject;
import com.think.cloud.thinkshop.auth.config.JustAuthProperties;
import com.think.cloud.thinkshop.auth.config.RedisStateCache;
import com.think.cloud.thinkshop.auth.form.LoginDTO;
import com.think.cloud.thinkshop.auth.service.AppAuthService;
import com.think.cloud.thinkshop.auth.service.SysLoginService;
import com.think.common.core.domain.R;
import com.think.common.core.exception.ServiceException;
import com.think.common.core.exception.enums.ErrorCode;
import com.think.common.core.exception.util.ServiceExceptionUtil;
import com.think.common.core.model.LoginUser;
import com.think.common.core.utils.JwtUtils;
import com.think.common.core.utils.StringUtils;
import com.think.common.core.web.domain.AjaxResult;
import com.think.common.security.auth.AuthUtil;
import com.think.common.security.service.LoginVO;
import com.think.common.security.service.TokenService;
import com.think.common.security.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.AuthFacebookRequest;
import me.zhyd.oauth.request.AuthGoogleRequest;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * @author zkthink
 * @apiNote
 **/
@Controller
@Api(tags = "app认证")
@RequestMapping("/app/auth")
public class AppAuthController {
    private final Logger log = LoggerFactory.getLogger(AppAuthController.class);
    @Autowired
    private AppAuthService appAuthService;
    @Autowired
    private JustAuthProperties justAuthProperties;
    @Autowired
    private RedisStateCache redisStateCache;
    @Autowired
    private SysLoginService sysLoginService;
    @Autowired
    private TokenService tokenService;


    @Value("${common.h5-url}")
    private String h5Url;

    @Value("${common.pc-url}")
    private String pcUrl;

    @PostMapping("login")
    @ApiOperation(value = "登录")
    @ResponseBody
    public AjaxResult login(@RequestBody LoginDTO loginDTO) {
        if (StringUtils.isBlank(loginDTO.getEmail()) || StringUtils.isBlank(loginDTO.getPassword())) {
            throw ServiceExceptionUtil.exception(ErrorCode.PARAMETER_ERROR);
        }
        LoginVO vo = appAuthService.login(loginDTO);
        return AjaxResult.success(vo);
    }

    @DeleteMapping("logout")
    @ResponseBody
    public AjaxResult logout(HttpServletRequest request) {
        String token = SecurityUtils.getToken(request);
        if (StringUtils.isNotEmpty(token)) {
            String username = JwtUtils.getUserName(token);
            // 删除用户缓存记录
            AuthUtil.logoutByToken(token);
            // 记录用户退出日志
            sysLoginService.logout(username);
        }
        return AjaxResult.success();
    }

    @PostMapping("refresh")
    @ResponseBody
    public AjaxResult refresh(HttpServletRequest request) {
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser)) {
            // 刷新令牌有效期
            tokenService.refreshToken(loginUser);
            return AjaxResult.success();
        }
        return AjaxResult.success();
    }

    @RequestMapping("/render/{source}")
    @ResponseBody
    public AjaxResult renderAuth(@PathVariable("source") String source) throws IOException {
        log.info("进入render：" + source);
        AuthRequest authRequest = getAuthRequest(source);
        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
        log.info(authorizeUrl);
        return AjaxResult.success("", authorizeUrl);
    }

    /**
     * oauth平台中配置的授权回调地址，以本项目为例，在创建github授权应用时的回调地址应为：http://127.0.0.1:8443/oauth/callback/github
     */
    @RequestMapping("/callback/{client}/{source}")
    public ModelAndView callback(@PathVariable("client") String client, @PathVariable("source") String source, AuthCallback callback, HttpServletRequest request) {
        log.info("进入callback：" + client + "-" + source + " callback params：" + JSONObject.toJSONString(callback));
        AuthRequest authRequest = getAuthRequest(source);
        AuthResponse<AuthUser> response = authRequest.login(callback);
        log.info(JSONObject.toJSONString(response));

        RedirectView redirectView = new RedirectView();
        String redirectUrl = "h5".equals(client) ? h5Url : pcUrl;
        if (response.ok()) {
            LoginVO vo = appAuthService.callback(response.getData());
            redirectUrl = redirectUrl + "?success=true&access_token=" + vo.getAccess_token();
            redirectView.setUrl(redirectUrl);
            return new ModelAndView(redirectView, null);
        } else {
            log.error("登录失败");
            redirectUrl = redirectUrl + "?success=false";
            redirectView.setUrl(redirectUrl);
            return new ModelAndView(redirectView, null);
        }

    }


    /**
     * // 创建授权request
     *
     * @param source 三方平台
     * @return AuthRequest
     */
    private AuthRequest getAuthRequest(String source) {
        AuthRequest authRequest = null;
        if (source.contains("facebook")) {
            authRequest = new AuthFacebookRequest(justAuthProperties.getType().get(source), redisStateCache);
        } else {
            authRequest = new AuthGoogleRequest(justAuthProperties.getType().get(source), redisStateCache);
        }
        return authRequest;
    }


}
